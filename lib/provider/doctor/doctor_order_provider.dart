import 'dart:convert';
import 'package:doctor_portal_cms/app/ip_config.dart';
import 'package:doctor_portal_cms/commons/exceptions/http_exceptions.dart';
import 'package:doctor_portal_cms/datastore/database/cms_doctorsportal_database.dart';
import 'package:doctor_portal_cms/model/doctor/doctor_order_modal.dart';
import 'package:doctor_portal_cms/model/drug/drug_order_api_modal.dart';
import 'package:doctor_portal_cms/provider/drug/drug_order_api_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_plugin_pdf_viewer/flutter_plugin_pdf_viewer.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

// class PersonalDataProvider with ChangeNotifier {
//   Future<void> loadPersonalData(email) async {
//     DoctorsPortalDatabase().profileDao.insertProfile(Profile(
//         profileid: 1,
//         firstname: 'JAIPAL',
//         lastname: 'JOHNSON',
//         gender: 'MALE',
//         email: 'jaipal@gmail.com'));
//     notifyListeners();
//   }
// }

class DrugOrderModalDoctor with ChangeNotifier {
  List<AllergyType> _allergyType = [
    AllergyType(id: '757', value: 'Drug'),
    AllergyType(id: '288', value: 'Enivironmental'),
    AllergyType(id: '286', value: 'Food'),
    AllergyType(id: '287', value: 'General'),
  ];
  List<AllergyOnset> _allergyOnset = [
    AllergyOnset(id: '585', value: 'Insidious'),
    AllergyOnset(id: '584', value: 'Sudden'),
  ];
  List<AllergyOnsetType> _allergyOnsetType = [
    AllergyOnsetType(id: '918', value: 'Approximate'),
    AllergyOnsetType(id: '586', value: 'Exact'),
  ];
  List<AllergyStatus> _allergyStatus = [
    AllergyStatus(id: '471', value: 'Active'),
    AllergyStatus(id: '93503853', value: 'Inactive'),
    AllergyStatus(id: '472', value: 'Resolved'),
    AllergyStatus(id: '897', value: 'Uncharted'),
  ];
  List<AllergyType> get allergyType {
    return [..._allergyType];
  }

  List<AllergyOnset> get allergyOnset {
    return [..._allergyOnset];
  }

  List<AllergyOnsetType> get allergyOnsetType {
    return [..._allergyOnsetType];
  }

  List<AllergyStatus> get allergyStatus {
    return [..._allergyStatus];
  }

  List<DoctorsOpPatients> _loadOpPatientsDoctor = [];
  List<DoctorsOpPatients> get loadOpPatientsDoctor {
    return [..._loadOpPatientsDoctor];
  }

  Future<void> loadOpPatientsDoctorWise(searchDate) async {
    _loadOpPatientsDoctor.clear();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    String userid = prefs.getString('userid');
    String deptId = prefs.getString('departmentid');
    final url =
        "$IP_CONFIG/life/mobileoplists/?userid=$userid&deptid=$deptId&visitdate=$searchDate&siteid=2";
    print(url);
    try {
      final response = await http.post(url,
          headers: {"Accept": "application/json", "Authorization": token});
      final responseData = json.decode(response.body);

      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }
      print(response.body);
      var rest = responseData["OP_patient_details"] as List;

      List<DoctorsOpPatients> list = rest
          .map<DoctorsOpPatients>((json) => DoctorsOpPatients.fromJson(json))
          .toList();

      _loadOpPatientsDoctor = list;
      notifyListeners();
    } catch (error) {
      throw error;
    }
  }

  List<DoctorsIpPatients> _loadIpPatientsDoctor = [];
  List<DoctorsIpPatients> get loadIpPatientsDoctor {
    return [..._loadIpPatientsDoctor];
  }

  Future<void> loadIpPatientsDoctorWise(like) async {
    _loadIpPatientsDoctor.clear();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    String userid = prefs.getString('userid');
    final url =
        "$IP_CONFIG/life/mobileippatientlist/?userid=$userid&siteid=2&like=$like";
    print(url);
    try {
      final response = await http.post(url,
          headers: {"Accept": "application/json", "Authorization": token});
      final responseData = json.decode(response.body);

      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }
      print(response.body);
      var rest = responseData["IP_patient_details"] as List;

      List<DoctorsIpPatients> list = rest
          .map<DoctorsIpPatients>((json) => DoctorsIpPatients.fromJson(json))
          .toList();

      _loadIpPatientsDoctor = list;
      notifyListeners();
    } catch (error) {
      throw error;
    }
  }

  List<DoctorsSoapNoteDetials> _loadSoapNoteDetials = [];
  List<DoctorsSoapNoteDetials> get loadSoapNoteDetials {
    return [..._loadSoapNoteDetials];
  }

  List<DoctorProvisionalDiagnosis> _provisionalDiagnosisText = [];
  List<DoctorProvisionalDiagnosis> get provisionalDiagnosisText {
    return [..._provisionalDiagnosisText];
  }

  // String provisionalDiagnosisText;
  // Map<String, DoctorsSoapTextDetials> _loadSoapTextDetials = {};
  // Map<String, DoctorsSoapTextDetials> get loadSoapTextDetials {
  //   return {..._loadSoapTextDetials};
  // }

  String tansationId;
  Future<void> loadSoapNoteDetialslist(visitid) async {
    _provisionalDiagnosisText.clear();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    String departmentid = prefs.getString('departmentid');
    final url =
        "$IP_CONFIG/life/mobilesoaplists/?deptid=$departmentid&visitid=$visitid";

    print(url);
    try {
      final response = await http.post(url,
          headers: {"Accept": "application/json", "Authorization": token});
      final responseData = json.decode(response.body);

      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }
      print(response.body);
      var rest = responseData["soapnote_details"] as List;
      // var text = responseData["soaptext_details"] as List;
      var provisional = responseData["provisional_diagnosis"] as List;
      print(provisional);
      var tansation = responseData["transactionId"] as List;
      List<TransationId> trans = tansation
          .map<TransationId>((json) => TransationId.fromJson(json))
          .toList();
      List<DoctorsSoapNoteDetials> list = rest
          .map<DoctorsSoapNoteDetials>(
              (json) => DoctorsSoapNoteDetials.fromJson(json))
          .toList();
      List<DoctorProvisionalDiagnosis> prov = provisional
          .map<DoctorProvisionalDiagnosis>(
              (json) => DoctorProvisionalDiagnosis.fromJson(json))
          .toList();
      tansationId = trans[0].transation;
      _loadSoapNoteDetials = list;
      _provisionalDiagnosisText = prov;
      print(prov[0].provisional);
      // List<DoctorsSoapTextDetials> soapText = text
      //     .map<DoctorsSoapTextDetials>(
      //         (json) => DoctorsSoapTextDetials.fromJson(json))
      //     .toList();
      // _loadSoapNoteValues = soapText;
      // await mapDataFromList(soapText);
      notifyListeners();
    } catch (error) {
      print(error);
      throw error;
    }
  }

  List<DoctorLabResults> _loadLabResults = [];
  List<DoctorLabResults> get loadLabResults {
    return [..._loadLabResults];
  }

  // Map<String, DoctorLabResultsParameters> _loadLabResultparameter = {};
  // Map<String, DoctorLabResultsParameters> get loadLabResultparameter {
  //   return {..._loadLabResultparameter};
  // }

  Future<void> loadlabResults(visitid, patientId) async {
    _loadLabResults.clear();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    final url =
        "$IP_CONFIG/life/labresults/?patientid=$patientId&visitid=$visitid";

    print(url);
    try {
      final response = await http.post(url,
          headers: {"Accept": "application/json", "Authorization": token});
      final responseData = json.decode(response.body);

      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }
      print(response.body);
      var rest = responseData["lab_results"] as List;
      // var para = responseData["parameter"] as List;
      List<DoctorLabResults> list = rest
          .map<DoctorLabResults>((json) => DoctorLabResults.fromJson(json))
          .toList();
      // List<DoctorLabResultsParameters> parameter = para
      //     .map<DoctorLabResultsParameters>(
      //         (json) => DoctorLabResultsParameters.fromJson(json))
      //     .toList();
      // for (int i = 0; i < parameter.length; i++) {
      //   _loadLabResultparameter.putIfAbsent(
      //       parameter[i].labresultId, () => parameter[i]);
      // }

      _loadLabResults = list;
      notifyListeners();
    } catch (error) {
      print(error);
      throw error;
    }
  }

  Future<void> loadAlllabResults() async {
    _loadLabResults.clear();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    String patientId = prefs.getString('selectedPatient');
    final url = "$IP_CONFIG/life/mobileEHRLabresults/?patientid=$patientId";

    print(url);
    try {
      final response = await http.post(url,
          headers: {"Accept": "application/json", "Authorization": token});
      final responseData = json.decode(response.body);

      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }
      print(response.body);
      var rest = responseData["lab_results"] as List;
      // var para = responseData["parameter"] as List;
      List<DoctorLabResults> list = rest
          .map<DoctorLabResults>((json) => DoctorLabResults.fromJson(json))
          .toList();
      _loadLabResults = list;
      notifyListeners();
    } catch (error) {
      print(error);
      throw error;
    }
  }

  List<DoctorLabResultsParameters> _loadLabResultsParameter = [];
  List<DoctorLabResultsParameters> get loadLabResultsParameter {
    return [..._loadLabResultsParameter];
  }

  Future<void> loadlabResultsParameters(labresultId) async {
    _loadLabResultsParameter.clear();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    final url =
        "$IP_CONFIG/life/labresultsparameters/?labResultId=$labresultId";

    print(url);
    try {
      final response = await http.post(url,
          headers: {"Accept": "application/json", "Authorization": token});
      final responseData = json.decode(response.body);

      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }
      print(response.body);
      var rest = responseData["parameter"] as List;
      List<DoctorLabResultsParameters> list = rest
          .map<DoctorLabResultsParameters>(
              (json) => DoctorLabResultsParameters.fromJson(json))
          .toList();
      _loadLabResultsParameter = list;
      notifyListeners();
    } catch (error) {
      print(error);
      throw error;
    }
  }

  // mapDataFromList(List<DoctorsSoapTextDetials> soapText) {
  //   _loadSoapTextDetials = {
  //     for (DoctorsSoapTextDetials v in soapText)
  //       v.componentId:
  //           DoctorsSoapTextDetials(textId: v.textId, textValue: v.textValue)
  //   };
  // }

  // List<SoapNotesOrders> _addProvisionalDiagnosis = [];
  // List<SoapNotesOrders> get addProvisionalDiagnosis {
  //   return [..._addProvisionalDiagnosis];
  // }

  // Future<void> addNewSoapNote(id, DoctorsSoapTextDetials text) async {
  //   if (text != null && id != null)
  //     _addNewSoapNoteItems.add(SoapNotesOrders(
  //         componentId: id, text: text.textValue, textId: text.textId));
  // }

  // Future<void> provisionalDiagnosis(text) async {
  //   print(text);
  //   if (text != null) {
  //     _addProvisionalDiagnosis.clear();
  //     _addProvisionalDiagnosis.add(SoapNotesOrders(text: text));
  //   }
  // }

  List<SaveSoapPatientDetials> _selectedpatientdetials = [];
  List<SaveSoapPatientDetials> get selectedpatientdetials {
    return [..._selectedpatientdetials];
  }

  void addSelectedpatientsDetials(
      DoctorsOpPatients iPpatientDetial, String tansationId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userid = prefs.getString('userid');
    selectedpatientdetials.clear();
    clearAllPerviousPatientData();
    List<SaveSoapPatientDetials> _selectedpatientdetial = [];
    _selectedpatientdetial.add(SaveSoapPatientDetials(
        patientId: iPpatientDetial.patientId,
        visitId: iPpatientDetial.visitId,
        encounterId: iPpatientDetial.encounterId,
        mrn: iPpatientDetial.mrn,
        userId: userid,
        transactionId: tansationId));
    _selectedpatientdetials = _selectedpatientdetial;
    print(_selectedpatientdetials[0].userId);
    print(_selectedpatientdetials[0].patientId);
    notifyListeners();
  }

  void addSelectedIPpatientsDetials(
      DoctorsIpPatients iPpatientDetial, String tansationId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userid = prefs.getString('userid');
    _selectedpatientdetials.clear();
    clearAllPerviousPatientData();
    List<SaveSoapPatientDetials> _selectedpatientdetial = [];
    print(iPpatientDetial.encounterId);
    _selectedpatientdetial.add(SaveSoapPatientDetials(
        patientId: iPpatientDetial.patientId,
        visitId: iPpatientDetial.visitId,
        encounterId: iPpatientDetial.encounterId,
        mrn: iPpatientDetial.mrn,
        userId: userid,
        transactionId: tansationId));
    _selectedpatientdetials = _selectedpatientdetial;
    print(_selectedpatientdetials[0].userId);
    print(_selectedpatientdetials[0].patientId);
    print(_selectedpatientdetials[0].encounterId);
    notifyListeners();
  }

  List<DoctorLabResultTemplate> _loadLabResultsTemplates = [];
  List<DoctorLabResultTemplate> get loadLabResultsTemplates {
    return [..._loadLabResultsTemplates];
  }

  Future<void> loadLabResultsTemplate(labresultId) async {
    _loadLabResultsTemplates.clear();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    final url = "$IP_CONFIG/life/mobileTemplate/?labresultId=$labresultId";

    print(url);
    try {
      final response = await http.post(url,
          headers: {"Accept": "application/json", "Authorization": token});
      final responseData = json.decode(response.body);

      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }
      print(response.body);
      var rest = responseData["lab_template"] as List;
      List<DoctorLabResultTemplate> list = rest
          .map<DoctorLabResultTemplate>(
              (json) => DoctorLabResultTemplate.fromJson(json))
          .toList();
      _loadLabResultsTemplates = list;
      notifyListeners();
    } catch (error) {
      print(error);
      throw error;
    }
  }

  // mapDataFromList(List<DoctorsSoapTextDetials> soapText) {
  //   _loadSoapTextDetials = {
  //     for (DoctorsSoapTextDetials v in soapText)
  //       v.componentId:
  //           DoctorsSoapTextDetials(textId: v.textId, textValue: v.textValue)
  //   };
  // }

  // List<SoapNotesOrders> _addProvisionalDiagnosis = [];
  // List<SoapNotesOrders> get addProvisionalDiagnosis {
  //   return [..._addProvisionalDiagnosis];
  // }

  // Future<void> addNewSoapNote(id, DoctorsSoapTextDetials text) async {
  //   if (text != null && id != null)
  //     _addNewSoapNoteItems.add(SoapNotesOrders(
  //         componentId: id, text: text.textValue, textId: text.textId));
  // }

  // Future<void> provisionalDiagnosis(text) async {
  //   print(text);
  //   if (text != null) {
  //     _addProvisionalDiagnosis.clear();
  //     _addProvisionalDiagnosis.add(SoapNotesOrders(text: text));
  //   }
  // }

  List<DoctorsSoapNoteDetials> _soapNotesResponse = [];
  List<DoctorsSoapNoteDetials> get soapNotesResponse {
    return [..._soapNotesResponse];
  }

  // List<DoctorsSoapNoteDetials> _addNewSoapNoteItems = [];
  // List<DoctorsSoapNoteDetials> get addNewSoapNoteItems {
  //   return [..._addNewSoapNoteItems];
  // }
  List<DoctorsSoapNoteDetials> _addSoapNoteItems = [];
  List<DoctorsSoapNoteDetials> get addSoapNoteItems {
    return [..._addSoapNoteItems];
  }

  Future<void> sendSoapNotes(DoctorsSoapNoteDetials soap) async {
    _soapNotesResponse.clear();
    _addSoapNoteItems.clear();
    _addSoapNoteItems.add(DoctorsSoapNoteDetials(
        componentId: soap.componentId,
        textid: soap.textid,
        textvalue: soap.textvalue,
        transactionId:
            soap.transactionId.isNotEmpty ? soap.transactionId : tansationId));
    List<DoctorsSoapNoteDetials> _saveSoapNoteData = addSoapNoteItems.toList();
    print(_saveSoapNoteData[0].textvalue);
    List<SaveSoapPatientDetials> _selectedpatient = selectedpatientdetials;
    SaveSoapNoteApi saveSoapNoteData =
        SaveSoapNoteApi(_saveSoapNoteData, _selectedpatient);
    print(jsonEncode(saveSoapNoteData));
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    final http.Response response = await http.post(
      '$IP_CONFIG/life/mobilesavesoap/?',
      headers: <String, String>{
        'Content-Type': 'application/json',
        "Authorization": token,
      },
      body: jsonEncode(saveSoapNoteData),
    );
    final responseData = json.decode(response.body);
    var rest = responseData["soapnotes"] as String;
    _soapNotesResponse.add(DoctorsSoapNoteDetials(response: rest));
    notifyListeners();
    _addSoapNoteItems.clear();
    print(response.body);
    return DoctorsSoapNoteDetials.fromJson(jsonDecode(response.body));
  }

  List<SaveProvisionalDiagnosis> _saveProvisionalDiagnosis = [];
  List<SaveProvisionalDiagnosis> get saveProvisionalDiagnosis {
    return [..._saveProvisionalDiagnosis];
  }

  Future<void> sendProvisionalDiagnosis(text, visit) async {
    _saveProvisionalDiagnosis.clear();
    _saveProvisionalDiagnosis
        .add(SaveProvisionalDiagnosis(text: text, visitId: visit));
    SaveProvisionalDiagnosisApi saveProvisionalDiagnosisData =
        SaveProvisionalDiagnosisApi(saveProvisionalDiagnosis.toList());

    print(jsonEncode(saveProvisionalDiagnosisData));
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    final http.Response response = await http.post(
      '$IP_CONFIG/life/saveprovisionaldiagnosis/?',
      headers: <String, String>{
        'Content-Type': 'application/json',
        "Authorization": token,
      },
      body: jsonEncode(saveProvisionalDiagnosisData),
    );
    final responseData = json.decode(response.body);
    var rest = responseData["provisional"] as String;
    print(rest);
    notifyListeners();
    _saveProvisionalDiagnosis.clear();
    return SaveProvisionalDiagnosis.fromJson(jsonDecode(response.body));
  }

  GeneralInfoDetials currentGeneralinformation;
  List<GeneralInfoDetials> _loadGeneralInformation = [];
  List<GeneralInfoDetials> get loadGeneralInformation {
    return [..._loadGeneralInformation];
  }

  Future<void> loadGeneralInformationDetials(patientid) async {
    final url = "$IP_CONFIG/life/mobilegeninfo/?patientid=$patientid";
    print(url);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    try {
      final response = await http.post(url,
          headers: {"Accept": "application/json", "Authorization": token});
      final responseData = json.decode(response.body.replaceAll("&quot;", ""));

      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }
      print(response.body);
      var rest = responseData["GeneralInfo_details"] as List;

      List<GeneralInfoDetials> list = rest
          .map<GeneralInfoDetials>((json) => GeneralInfoDetials.fromJson(json))
          .toList();

      await currentGeneralinformationLoad(list);
      _loadGeneralInformation = list;
      notifyListeners();
    } catch (error) {
      throw error;
    }
  }

  currentGeneralinformationLoad(List<GeneralInfoDetials> list) {
    currentGeneralinformation = list[0];
  }

  List<GeneralInfoDetials> _saveGeneralInformation = [];
  List<GeneralInfoDetials> get saveGeneralInformation {
    return [..._saveGeneralInformation];
  }

  Future<void> addGeneralinformation(
      value, GeneralInfoDetials info, patientid) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userid = prefs.getString('userid');
    if (value != null) {
      _saveGeneralInformation.clear();
      _saveGeneralInformation.add(GeneralInfoDetials(
          generalInfo: value,
          infoId: info.infoId == "null" ? '0' : info.infoId,
          patientid: patientid,
          userid: userid));
    }
  }

  List<GeneralInfoDetials> _generalInformationResponse = [];
  List<GeneralInfoDetials> get generalInformationResponse {
    return [..._generalInformationResponse];
  }

  Future<void> saveGeneralInformationList() async {
    _generalInformationResponse.clear();
    List<GeneralInfoDetials> _saveGeneral = saveGeneralInformation.toList();
    SaveGeneralInformation saveGeneral = SaveGeneralInformation(_saveGeneral);
    print(jsonEncode(saveGeneral));
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    final http.Response response = await http.post(
      '$IP_CONFIG/life/mobilesavegeninfo/?',
      headers: <String, String>{
        'Content-Type': 'application/json',
        "Authorization": token,
      },
      body: jsonEncode(saveGeneral),
    );
    final responseData = json.decode(response.body.replaceAll(r"'", "'"));
    var rest = responseData["gen_info"] as String;
    print(rest);
    _generalInformationResponse.add(GeneralInfoDetials(response: rest));
    notifyListeners();
    return GeneralInfoDetials.fromJson(jsonDecode(response.body));
  }

  List<ExistingAllergies> _loadExistingAllergiesList = [];
  List<ExistingAllergies> get loadExistingAllergiesList {
    return [..._loadExistingAllergiesList];
  }

  Future<void> loadExistingAllergies() async {
    _loadExistingAllergiesList.clear();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    String patientId = prefs.getString('selectedPatient');
    final url = "$IP_CONFIG/life/mobileallergyinfo/?patientid=$patientId";
    print(url);
    try {
      final response = await http.post(url,
          headers: {"Accept": "application/json", "Authorization": token});
      final responseData = json.decode(response.body);

      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }
      print(response.body);
      var rest = responseData["Allergy_details"] as List;

      List<ExistingAllergies> list = rest
          .map<ExistingAllergies>((json) => ExistingAllergies.fromJson(json))
          .toList();

      _loadExistingAllergiesList = list;
      notifyListeners();
    } catch (error) {
      throw error;
    }
  }

  static Future loadAllergyToListItems(
      String allergytype, String pattern) async {
    final url =
        "$IP_CONFIG/life/mobileallergicto/?allergytype=$allergytype&searchvalue=$pattern";
    print(url);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    try {
      final response = await http.post(url,
          headers: {"Accept": "application/json", "Authorization": token});
      final responseData = json.decode(response.body);

      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }
      print(response.body);
      var rest = responseData["Allergic_dropdown"] as List;
      return rest;
    } catch (error) {
      throw error;
    }
  }

  static Future loadBedwithBedtype(String like, String bedtypeId) async {
    final url = "$IP_CONFIG/life/mobileloadbed/?searchvalue=$bedtypeId";
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    try {
      final response = await http.post(url,
          headers: {"Accept": "application/json", "Authorization": token});
      final responseData = json.decode(response.body);

      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }
      print(response.body);
      var rest = responseData["loadbed"] as List;
      return rest;
    } catch (error) {
      throw error;
    }
  }

  List<AllergyReactions> _loadAllergyReaction = [];
  List<AllergyReactions> get loadAllergyReaction {
    return [..._loadAllergyReaction];
  }

  Future<void> loadAllergyReactions() async {
    final url = "$IP_CONFIG/life/mobileallergyreacton/?";
    print(url);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    try {
      final response = await http.post(url,
          headers: {"Accept": "application/json", "Authorization": token});
      final responseData = json.decode(response.body);

      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }
      print(response.body);
      var rest = responseData["Allergic_Reaction"] as List;

      List<AllergyReactions> list = rest
          .map<AllergyReactions>((json) => AllergyReactions.fromJson(json))
          .toList();

      _loadAllergyReaction = list;
      notifyListeners();
    } catch (error) {
      throw error;
    }
  }

  List<String> addedReactionValues = [];
  Map<String, AllergyReactions> checkedAllergyItem =
      new Map<String, AllergyReactions>();
  Map<String, AllergyReactions> _allergyReactionlist = {};
  Map<String, AllergyReactions> get allergyReactionlist {
    return {..._allergyReactionlist};
  }

  void allergyReactionAdd(AllergyReactions item) {
    if (_allergyReactionlist.containsKey(item.lookupid)) {
      //change to drug detial id
      _allergyReactionlist.update(
          item.lookupid,
          (e) => AllergyReactions(
              isSelected: true,
              lookupcode: e.lookupcode,
              lookupid: e.lookupid,
              lookupvalue: e.lookupvalue));

      addedReactionValues.add(item.lookupvalue);
      notifyListeners();
    } else {
      _allergyReactionlist.putIfAbsent(
          item.lookupid,
          () => AllergyReactions(
                isSelected: true,
                lookupcode: item.lookupcode,
                lookupid: item.lookupid,
                lookupvalue: item.lookupvalue,
              ));

      addedReactionValues.add(item.lookupvalue);
      notifyListeners();
    }
  }

  void allergyReactionDelete(AllergyReactions item) {
    _allergyReactionlist.removeWhere((key, value) => key == item.lookupid);
    addedReactionValues.remove(item.lookupvalue);
    notifyListeners();
  }

  void clearAllAllergyData() {
    checkedAllergyItem.clear();
    _allergyReactionlist.clear();
    addedReactionValues.clear();
    saveAllergyProvider.clear();
    notifyListeners();
  }

  void clearAllPerviousPatientData() {
    clearAllAllergyData();
  }

  Map<String, SaveAllergyDetialsData> saveAllergyProvider =
      new Map<String, SaveAllergyDetialsData>();
  Future<void> addToSaveAllergyList({
    String patientId,
    String selectAllergyType,
    String selectAllergyTypeId,
    String allergicToValue,
    String allergicToId,
    String selectOnSet,
    String selectOnSetId,
    String selectOnSetType,
    String selectOnSetTypeId,
    String selectAllergyStatus,
    String selectAllergyStatusId,
    bool checkBoxValue,
    String date,
    String description,
  }) {
    if (saveAllergyProvider.containsKey(patientId)) {
      //change to drug detial id
      saveAllergyProvider.update(
          patientId,
          (e) => SaveAllergyDetialsData(
              allergyType: selectAllergyType,
              allergyTypeId: selectAllergyTypeId,
              allergyStatus: selectAllergyStatus,
              allergytoId: allergicToId,
              allergytoValue: allergicToValue,
              knownAllergies: checkBoxValue,
              onSetDate: date,
              onSetId: selectOnSetId,
              onSetValue: selectOnSet,
              remarks: description,
              onSetTypeValue: selectOnSetType,
              allergyStatusId: selectAllergyStatusId,
              onSetTypeId: selectOnSetTypeId));
      notifyListeners();
    } else {
      saveAllergyProvider.putIfAbsent(
          patientId,
          () => SaveAllergyDetialsData(
              allergyType: selectAllergyType,
              allergyTypeId: selectAllergyTypeId,
              allergyStatus: selectAllergyStatus,
              allergytoId: allergicToId,
              allergytoValue: allergicToValue,
              knownAllergies: checkBoxValue,
              onSetDate: date,
              onSetId: selectOnSetId,
              onSetValue: selectOnSet,
              remarks: description,
              onSetTypeValue: selectOnSetType,
              allergyStatusId: selectAllergyStatusId,
              onSetTypeId: selectOnSetTypeId));
      notifyListeners();
    }
    return null;
  }

  Future<Response> saveAllergyDetials(context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    _responsedata.clear();
    List<SaveAllergyDetialsData> allergy = saveAllergyProvider.values.toList();
    List<SaveSoapPatientDetials> patientIddetials =
        selectedpatientdetials.toList();
    List<SelectedDepartment> departmentDetials =
        Provider.of<DrugPatientProvider>(context, listen: false)
            .selectedDepartmentdetials;
    List<AllergyReactions> reaction = allergyReactionlist.values.toList();
    print(patientIddetials);
    AllergyDetialsSaveApi allergySaveDetials = AllergyDetialsSaveApi(
        allergy, patientIddetials, departmentDetials, reaction);
    print(jsonEncode(allergySaveDetials));
    final http.Response response = await http.post(
      '$IP_CONFIG/life/mobilesaveallergy/?',
      headers: <String, String>{
        'Content-Type': 'application/json',
        "Authorization": token,
      },
      body: jsonEncode(allergySaveDetials),
    );
    print(response.statusCode);
    print(response.body);
    final responseData = json.decode(response.body);
    var rest = responseData["Allergy"] as String;

    _responsedata.add(Response(resp: rest));
    notifyListeners();
    return Response.fromJson(jsonDecode(response.body));
  }

  Future<AllergyReactionsTO> saveAllergyTO(
      {String allergyDesc,
      String allergicTypeId,
      String remarks,
      bool isActive}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    _responsedata.clear();
    List<AllergyReactionsTO> reactionTO = [];
    reactionTO.add(AllergyReactionsTO(
        allergyTypeId: allergicTypeId,
        allergyDesc: allergyDesc,
        isActive: isActive,
        remarks: remarks));
    AllergyReactionToSaveApi reactionToSave =
        AllergyReactionToSaveApi(reactionTO);
    print(jsonEncode(reactionToSave));
    final http.Response response = await http.post(
      '$IP_CONFIG/life/saveAllergicTo/?',
      headers: <String, String>{
        'Content-Type': 'application/json',
        "Authorization": token,
      },
      body: jsonEncode(reactionToSave),
    );
    print(response.statusCode);
    print(response.body);
    final responseData = json.decode(response.body);
    var rest = responseData["allergic_to"] as String;

    _responsedata.add(Response(resp: rest));
    notifyListeners();
    return AllergyReactionsTO.fromJson(jsonDecode(response.body));
  }

  Future<PreviousDiagnosis> saveDiagnosis(
      {String provisional,
      String finalDiagnosis,
      String visitId,
      bool isActive}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    _responsedata.clear();
    List<PreviousDiagnosis> diagnosisList = [];
    diagnosisList.add(PreviousDiagnosis(
        finalDiagnosis: finalDiagnosis,
        provisionalDiagnosis: provisional,
        visitId: visitId));
    DiagnosisSaveApi diagnosisSave = DiagnosisSaveApi(diagnosisList);
    print(jsonEncode(diagnosisSave));
    final http.Response response = await http.post(
      '$IP_CONFIG/life/saveDiagnosis/?',
      headers: <String, String>{
        'Content-Type': 'application/json',
        "Authorization": token,
      },
      body: jsonEncode(diagnosisSave),
    );
    print(response.statusCode);
    print(response.body);
    final responseData = json.decode(response.body);
    var rest = responseData["allergic_to"] as String;

    _responsedata.add(Response(resp: rest));
    notifyListeners();
    return PreviousDiagnosis.fromJson(jsonDecode(response.body));
  }

  List<Response> _responsedata = [];
  List<Response> get responsedata {
    return [..._responsedata];
  }

  List<PreviousDiagnosis> _loadDiagnosisList = [];
  // List<PreviousDiagnosis> get loadDiagnosisList {
  //   return [..._loadDiagnosisList];
  // }

  Future<List<PreviousDiagnosis>> loadDiagnosisInfo(visitId) async {
    final url = "$IP_CONFIG/life/mobilediagnosisinfo/?visitid=$visitId";
    print(url);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    try {
      final response = await http.post(url,
          headers: {"Accept": "application/json", "Authorization": token});
      final responseData = json.decode(response.body);

      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }
      print(response.body);
      var diagnosis = responseData["Diagnosis_Info"] as List;
      List<PreviousDiagnosis> list = diagnosis
          .map<PreviousDiagnosis>((json) => PreviousDiagnosis.fromJson(json))
          .toList();

      _loadDiagnosisList = list;
    } catch (error) {
      throw error;
    }
    return _loadDiagnosisList;
  }

  List<DoctorAdmissionDepartmentlist> _admissionRequestDeptList = [];
  List<DoctorAdmissionDepartmentlist> get admissionRequestDeptList {
    return [..._admissionRequestDeptList];
  }

  List<AdmissionRequestDoctorsList> _admissionRequestDoctorsList = [];
  List<AdmissionRequestDoctorsList> get admissionRequestDoctorsList {
    return [..._admissionRequestDoctorsList];
  }

  List<DoctorAdmissionBedType> _admissionRequestBedType = [];
  List<DoctorAdmissionBedType> get admissionRequestBedType {
    return [..._admissionRequestBedType];
  }

  List<DoctorAdmissionPhonenumber> _admissionRequestphoneNo = [];
  List<DoctorAdmissionPhonenumber> get admissionRequestphoneNo {
    return [..._admissionRequestphoneNo];
  }

  Future<void> loadAdmissionRequestInfo(patientId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    String departmentid = prefs.getString('departmentid');
    final url =
        "$IP_CONFIG/life/mobilegetadmissioninfo/?siteid=2&deptId=$departmentid&patientId=$patientId";
    print(url);

    try {
      final response = await http.post(url,
          headers: {"Accept": "application/json", "Authorization": token});
      final responseData = json.decode(response.body);

      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }
      print(response.body);
      var deptList = responseData["Department_List"] as List;
      var docList = responseData["Doctors_List"] as List;
      var bedlist = responseData["BedType_list"] as List;
      var phNo = responseData["patient_phonenumber"] as List;
      List<DoctorAdmissionDepartmentlist> listDept = deptList
          .map<DoctorAdmissionDepartmentlist>(
              (json) => DoctorAdmissionDepartmentlist.fromJson(json))
          .toList();
      List<AdmissionRequestDoctorsList> listDoc = docList
          .map<AdmissionRequestDoctorsList>(
              (json) => AdmissionRequestDoctorsList.fromJson(json))
          .toList();
      List<DoctorAdmissionBedType> listBed = bedlist
          .map<DoctorAdmissionBedType>(
              (json) => DoctorAdmissionBedType.fromJson(json))
          .toList();
      List<DoctorAdmissionPhonenumber> pno = phNo
          .map<DoctorAdmissionPhonenumber>(
              (json) => DoctorAdmissionPhonenumber.fromJson(json))
          .toList();
      _admissionRequestDeptList = listDept;
      _admissionRequestDoctorsList = listDoc;
      _admissionRequestBedType = listBed;
      _admissionRequestphoneNo = pno;
      notifyListeners();
    } catch (error) {
      throw error;
    }
  }

  List<IPOPpatientsCount> _loadIPOPpatientsCount = [];
  List<IPOPpatientsCount> get loadIPOPpatientsCount {
    return [..._loadIPOPpatientsCount];
  }

  String error;
  Future loadIPOPpatientsCounts(searchDate, context) async {
    error = null;
    _loadIPOPpatientsCount.clear();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    String userid = prefs.getString('userid');
    String deptId = prefs.getString('departmentid');
    await Provider.of<DrugPatientProvider>(context, listen: false)
        .departmentSelected(
            consultantId: prefs.getString('employeeid'),
            deptId: prefs.getString('departmentid'),
            deptName: "",
            locId: prefs.getString('locid'),
            locName: "",
            siteId: "2");
    final url =
        "$IP_CONFIG/life/mobilepatientcount/?userid=$userid&deptid=$deptId&visitdate=$searchDate&siteid=2";
    print(url);
    try {
      final response = await http.post(url,
          headers: {"Accept": "application/json", "Authorization": token});
      final responseData = json.decode(response.body);

      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }
      print(response.body);
      var rest = responseData["patient_detials"] as List;
      print(rest);
      List<IPOPpatientsCount> list = rest
          .map<IPOPpatientsCount>((json) => IPOPpatientsCount.fromJson(json))
          .toList();
      error = list[0].error;
      _loadIPOPpatientsCount = list;
      notifyListeners();
    } catch (error) {
      throw error;
    }
  }

  void clearAllAdmissionRequestInfo() {
    _saveAdmissionrequest.clear();
  }

  Map<String, SaveAdmissionRequest> _saveAdmissionrequest = {};
  Map<String, SaveAdmissionRequest> get saveAdmissionrequest {
    return {..._saveAdmissionrequest};
  }

  // Map<String, SaveAdmissionRequest> saveAdmissionrequest =
  //     new Map<String, SaveAdmissionRequest>();
  Future<void> addToAdmissionRequest({
    String patientId,
    String attachBed,
    String attachBedId,
    String consultantId,
    String date,
    String deptId,
    String email,
    String lengthOfStay,
    String mobileNo,
    String selectedbedTypeId,
  }) {
    if (_saveAdmissionrequest.containsKey(patientId)) {
      _saveAdmissionrequest.update(
          patientId,
          (e) => SaveAdmissionRequest(
              attachBed: attachBed,
              consultantId: consultantId,
              date: date,
              deptId: deptId,
              email: email,
              lengthOfStay: lengthOfStay,
              mobileNo: mobileNo,
              selectedbedTypeId: selectedbedTypeId,
              attachBedId: attachBedId));
      notifyListeners();
    } else {
      _saveAdmissionrequest.putIfAbsent(
          patientId,
          () => SaveAdmissionRequest(
              attachBed: attachBed,
              consultantId: consultantId,
              date: date,
              deptId: deptId,
              email: email,
              lengthOfStay: lengthOfStay,
              mobileNo: mobileNo,
              selectedbedTypeId: selectedbedTypeId,
              attachBedId: attachBedId));
      notifyListeners();
    }
    return null;
  }

  Future<Response> saveAdmissionRequestInfo(context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    _responsedata.clear();
    List<SaveAdmissionRequest> admission = saveAdmissionrequest.values.toList();
    List<SaveSoapPatientDetials> patientIddetials =
        selectedpatientdetials.toList();
    List<SelectedDepartment> departmentDetials =
        Provider.of<DrugPatientProvider>(context, listen: false)
            .selectedDepartmentdetials;
    print(patientIddetials);
    AdmissonRequestSave saveAdmission = AdmissonRequestSave(
      admission,
      patientIddetials,
      departmentDetials,
    );
    print(jsonEncode(saveAdmission));
    final http.Response response = await http.post(
      '$IP_CONFIG/life/mobilesaveadmissoneq/?',
      headers: <String, String>{
        'Content-Type': 'application/json',
        "Authorization": token,
      },
      body: jsonEncode(saveAdmission),
    );
    print(response.statusCode);
    print(response.body);
    final responseData = json.decode(response.body);
    var rest = responseData["Admission_Request"] as String;

    _responsedata.add(Response(resp: rest));
    notifyListeners();
    return Response.fromJson(jsonDecode(response.body));
  }

  Map<String, VitalSignsDetials> saveVitalSign =
      new Map<String, VitalSignsDetials>();

  Future<void> addvitalSigns(parameterId, value, remarks, controlType, refFrom,
      refTo, vitalSignId, uom) async {
    if (saveVitalSign.containsKey(parameterId)) {
      print('contains');
      print(value);
      saveVitalSign.update(
          parameterId,
          (e) => VitalSignsDetials(
              controlType: e.controlType,
              referenceFrom: e.referenceFrom,
              referenceto: e.referenceto,
              parameterId: parameterId,
              vitalSignId: "",
              uom: e.uom,
              remarks: remarks == null ? e.remarks : remarks,
              value: value == null ? e.value : value));
      notifyListeners();
    } else {
      print('not');
      saveVitalSign.putIfAbsent(
          parameterId,
          () => VitalSignsDetials(
              controlType: controlType == null ? "" : controlType,
              referenceFrom: refFrom == null ? "" : refFrom,
              referenceto: refTo == null ? "" : refTo,
              uom: uom == null ? "" : uom,
              vitalSignId: "",
              parameterId: parameterId,
              remarks: remarks,
              value: value));
      notifyListeners();
    }
    return null;
  }

  // List<VitalSignsDetials> _saveVitalSign = [];
  // List<VitalSignsDetials> get saveVitalSign {
  //   return [..._saveVitalSign];
  // }

  // Future<void> addvitalSigns(
  //   pulserate,
  //   systaholicpressure,
  //   diastrolicPressure,
  //   grbs,
  //   weight,
  //   height,
  //   temp,
  //   spo2,
  //   pulserateRemarks,
  //   systaholicpressureRemarks,
  //   diastrolicPressureRemarks,
  //   grbsRemarks,
  //   weightRemarks,
  //   heightRemarks,
  //   tempRemarks,
  //   spo2Remarks,
  // ) async {
  //   _saveVitalSign.clear();
  //   _saveVitalSign.add(VitalSignsDetials(
  //       diastrolicPressure:
  //           diastrolicPressure == null ? "" : diastrolicPressure,
  //       diastrolicPressureRemarks:
  //           diastrolicPressureRemarks == null ? "" : diastrolicPressureRemarks,
  //       grbs: grbs == null ? "" : grbs,
  //       grbsRemarks: grbsRemarks == null ? "" : grbsRemarks,
  //       height: height == null ? "" : height,
  //       heightRemarks: heightRemarks == null ? "" : heightRemarks,
  //       pulserate: pulserate == null ? "" : pulserate,
  //       pulserateRemarks: pulserateRemarks == null ? "" : pulserateRemarks,
  //       spo2: spo2 == null ? "" : spo2,
  //       spo2Remarks: spo2Remarks == null ? "" : spo2Remarks,
  //       systaholicpressure:
  //           systaholicpressure == null ? "" : systaholicpressure,
  //       systaholicpressureRemarks:
  //           systaholicpressureRemarks == null ? "" : systaholicpressureRemarks,
  //       temp: temp == null ? "" : temp,
  //       tempRemarks: tempRemarks == null ? "" : tempRemarks,
  //       weight: weight == null ? "" : weight,
  //       weightRemarks: weightRemarks == null ? "" : weightRemarks));
  // }

  List<VitalSignsDetials> _saveVitalSignResponse = [];
  List<VitalSignsDetials> get saveVitalSignResponse {
    return [..._saveVitalSignResponse];
  }

  // Future<void> sendVitalSign(context) async {
  //   _saveVitalSignResponse.clear();
  //   List<VitalSignsDetials> _saveVital = saveVitalSign.values.toList();
  //   List<SaveSoapPatientDetials> patientIddetials =
  //       selectedpatientdetials.toList();
  //   List<SelectedDepartment> departmentDetials =
  //       Provider.of<DrugPatientProvider>(context, listen: false)
  //           .selectedDepartmentdetials;
  //   SaveVitalSign saveVital =
  //       SaveVitalSign(_saveVital, departmentDetials, patientIddetials);
  //   print(jsonEncode(saveVital));
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   String token = prefs.getString('token');
  //   final http.Response response = await http.post(
  //     '$IP_CONFIG/life/mobilesavevital/?',
  //     headers: <String, String>{
  //       'Content-Type': 'application/json',
  //       "Authorization": token,
  //     },
  //     body: jsonEncode(saveVital),
  //   );
  //   final responseData = json.decode(response.body.replaceAll(r"'", "'"));
  //   var rest = responseData["vital_signs"] as String;
  //   print(rest);
  //   _saveVitalSignResponse.add(VitalSignsDetials(response: rest));
  //   notifyListeners();
  //   return VitalSignsDetials.fromJson(jsonDecode(response.body));
  // }

  Future<Response> sendVitalSign(context) async {
    _responsedata.clear();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    List<VitalSignsDetials> _saveVital = saveVitalSign.values.toList();
    List<SaveSoapPatientDetials> patientIddetials =
        selectedpatientdetials.toList();
    List<SelectedDepartment> departmentDetials =
        Provider.of<DrugPatientProvider>(context, listen: false)
            .selectedDepartmentdetials;
    SaveVitalSign saveVital =
        SaveVitalSign(_saveVital, departmentDetials, patientIddetials);
    print(jsonEncode(saveVital));
    final http.Response response = await http.post(
      '$IP_CONFIG/life/mobilesavevital/?',
      headers: <String, String>{
        'Content-Type': 'application/json',
        "Authorization": token,
      },
      body: jsonEncode(saveVital),
    );
    print(response.statusCode);
    print(response.body);
    final responseData = json.decode(response.body);
    var rest = responseData["vital_signs"] as String;
    saveVitalSign.clear();
    _responsedata.add(Response(resp: rest));
    notifyListeners();
    return Response.fromJson(jsonDecode(response.body));
  }

  List<LoadVitalSigns> _loadVitalParameter = [];
  List<LoadVitalSigns> get loadVitalParameter {
    return [..._loadVitalParameter];
  }

  Future<void> loadVitalParameters() async {
    _loadVitalParameter.clear();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    final url = "$IP_CONFIG/life/loadVitalParameter/?";

    print(url);
    try {
      final response = await http.post(url,
          headers: {"Accept": "application/json", "Authorization": token});
      final responseData = json.decode(response.body);

      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }
      print(response.body);
      var rest = responseData["vital_parameter"] as List;
      List<LoadVitalSigns> list = rest
          .map<LoadVitalSigns>((json) => LoadVitalSigns.fromJson(json))
          .toList();
      _loadVitalParameter = list;
      notifyListeners();
    } catch (error) {
      print(error);
      throw error;
    }
  }

  PDFDocument document;
  Future<void> loadCultureLab({
    context,
    labresultid,
    visitid,
    sampleid,
    orderno,
  }) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String patientid = prefs.getString('selectedPatient');
    final url =
        '$IP_CONFIG/life/birtreports?_reportFormat=pdf&ReportName=cultureresult.rptdesign&__id=parameterPage&__masterpage=true&labresultid=$labresultid&patientId=$patientid&visitid=$visitid&sampleId=*$sampleid';
    print(url);
    document = await PDFDocument.fromURL(url);
  }
}
