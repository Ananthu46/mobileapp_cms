import 'dart:collection';
import 'dart:convert';
import 'package:doctor_portal_cms/app/ip_config.dart';
import 'package:doctor_portal_cms/commons/exceptions/http_exceptions.dart';
import 'package:doctor_portal_cms/model/doctor/doctor_order_modal.dart';
import 'package:doctor_portal_cms/model/drug/drug_order_api_modal.dart';
import 'package:doctor_portal_cms/provider/doctor/doctor_order_provider.dart';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DrugPatientProvider with ChangeNotifier {
  HashMap<String, int> hashMap = new HashMap<String, int>();
  HashMap<String, bool> incrementAdd = new HashMap<String, bool>();
  HashMap<String, int> pharmacynameMap = new HashMap<String, int>();
  HashMap<String, bool> incrementAddPharmacy = new HashMap<String, bool>();
  HashMap<String, String> pharmacyGenericName = new HashMap<String, String>();
  HashMap<String, String> pharmacyFrequencyMap = new HashMap<String, String>();
  HashMap<String, int> pharmacyFrequencyID = new HashMap<String, int>();
  HashMap<String, int> noOfDaysPharmacy = new HashMap<String, int>();
  HashMap<String, double> totalquantitypharmacy = new HashMap<String, double>();
  HashMap<String, bool> previousorderHashMap = new HashMap<String, bool>();
  List<DrugServiceOrderModal> _drugServiceOrder = [];
  List<DrugServiceOrderModal> get drugServiceOrder {
    return [..._drugServiceOrder];
  }

  Future<void> loadDrugservices(search) async {
    final url = "$IP_CONFIG/life/servicelistapi/?serviceName=$search";
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    try {
      final response = await http.post(url,
          headers: {"Accept": "application/json", "Authorization": token});
      final responseData = json.decode(response.body);

      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }

      var rest = responseData["Service_details"] as List;

      List<DrugServiceOrderModal> servicelist = rest
          .map<DrugServiceOrderModal>(
              (json) => DrugServiceOrderModal.fromJson(json))
          .toList();
      _drugServiceOrder = servicelist;
      notifyListeners();
    } catch (error) {
      throw error;
    }
  }

  void clearPreviousOrders() {
    previousOrderList.clear();
    _previousOrderList.clear();
    print(previousOrderList);
  }

  Map<String, PreviousPharmacyOrderModal> _previousOrderList = {};
  Map<String, PreviousPharmacyOrderModal> get previousOrderList {
    return {..._previousOrderList};
  }

  void previousOrderAdd(PreviousPharmacyOrderModal item, String store) {
    print(item.itemname);
    print(_previousOrderList.containsKey(item.itemname));
    if (_previousOrderList.containsKey(item.itemname)) {
      //change to drug detial id
      _previousOrderList.update(
          item.itemname,
          (e) => PreviousPharmacyOrderModal(
                storeid: store,
                drugdetailId: item.drugdetailId,
                frequencyId: item.frequencyId,
                frequencyName: item.frequencyName,
                itemId: item.itemId,
                itemname: item.itemname,
                noOfDays: item.noOfDays,
                quantity: item.quantity,
                renewOrder: true,
                strength: item.strength,
                totalQty: item.totalQty,
                visitId: item.visitId,
                categoryId: item.categoryId,
                genericId: item.genericId,
              ));
      //   }
      notifyListeners();
    } else {
      print(item.frequencyId);
      _previousOrderList.putIfAbsent(
          item.itemname,
          () => PreviousPharmacyOrderModal(
              storeid: store,
              drugdetailId: item.drugdetailId,
              frequencyId: item.frequencyId,
              frequencyName: item.frequencyName,
              itemId: item.itemId,
              itemname: item.itemname,
              noOfDays: item.noOfDays,
              quantity: item.quantity,
              renewOrder: true,
              strength: item.strength,
              totalQty: item.totalQty,
              visitId: item.visitId,
              categoryId: item.categoryId,
              genericId: item.genericId));
      //   }
      notifyListeners();
    }
  }

  void previousOrderDelete(PreviousPharmacyOrderModal item) {
    _previousOrderList.removeWhere((key, value) => key == item.itemname);
    notifyListeners();
  }

  void addRepeatOrders() {
    print(previousOrderList);
    _drugpharmacylist.addAll(previousOrderList);
    notifyListeners();
  }

  Map<String, DrugServicesDetialsList> _drugServicesList = {};
  Map<String, DrugServicesDetialsList> get drugServicesList {
    return {..._drugServicesList};
  }

  clearOnNewPatient() {
    clearallPharmacy();
    clearallServices();
  }

  void addServicesList(
    String servicename,
    String serviceId,
    String serviceCode,
    int quantity,
  ) {
    if (_drugServicesList.containsKey(servicename)) {
      _drugServicesList.update(
          servicename,
          (existingitem) => DrugServicesDetialsList(
                serviceId: existingitem.serviceId,
                serviceCode: existingitem.serviceCode,
                serviceName: existingitem.serviceName,
                quantity: existingitem.quantity + 1,
              ));
    } else {
      _drugServicesList.putIfAbsent(
          servicename,
          () => DrugServicesDetialsList(
                serviceId: serviceId,
                serviceCode: serviceCode,
                serviceName: servicename,
                quantity: quantity + 1,
              ));
    }
    notifyListeners();
  }

  void onTapMinus(
    String servicename,
    String serviceId,
    String serviceCode,
    int quantity,
  ) {
    if (_drugServicesList.containsKey(servicename)) {
      _drugServicesList.update(
          servicename,
          (existingitem) => DrugServicesDetialsList(
                serviceId: existingitem.serviceId,
                serviceCode: existingitem.serviceCode,
                serviceName: existingitem.serviceName,
                quantity: existingitem.quantity - 1,
              ));
    } else {
      _drugServicesList.putIfAbsent(
          servicename,
          () => DrugServicesDetialsList(
                serviceId: serviceId,
                serviceCode: serviceCode,
                serviceName: servicename,
                quantity: quantity,
              ));
    }
    notifyListeners();
  }

  void onTapdelete(
    String servicename,
  ) {
    _drugServicesList.removeWhere((key, value) => key == servicename);
    hashMap.removeWhere((key, value) => key == servicename);
    incrementAdd.removeWhere((key, value) => key == servicename);
    notifyListeners();
  }

  void clearallServices() {
    _drugServicesList.clear();
    hashMap.clear();
    incrementAdd.clear();
    notifyListeners();
  }

  Map<String, PreviousPharmacyOrderModal> _drugpharmacylist = {};
  Map<String, PreviousPharmacyOrderModal> get drugpharmacylist {
    return {..._drugpharmacylist};
  }

  void addPharmacyList(
    String selectedStoreId,
    PharmacyMedicinesModal pharmacy,
    String drugDetialId,
    String frequency,
    int frequencyId,
    int noOfdays,
    double totalQty,
  ) {
    if (_drugpharmacylist.containsKey(pharmacy.itemname)) {
      _drugpharmacylist.update(
          pharmacy.itemname,
          (existingitem) => PreviousPharmacyOrderModal(
                pharmacy: existingitem.pharmacy,
                drugdetailId: '',
                storeid: selectedStoreId,
                itemId: existingitem.itemId,
                frequencyId: frequencyId,
                frequencyName: frequency,
                itemname: existingitem.itemname,
                noOfDays:
                    noOfdays == null ? 0 : double.parse(noOfdays.toString()),
                quantity: totalQty,
                renewOrder: existingitem.renewOrder,
                strength: '',
                totalQty: totalQty,
                visitId: '',
                genericId: existingitem.genericId,
                categoryId: existingitem.categoryId,
              ));
    } else {
      _drugpharmacylist.putIfAbsent(
          pharmacy.itemname,
          () => PreviousPharmacyOrderModal(
                pharmacy: pharmacy,
                drugdetailId: '',
                storeid: selectedStoreId,
                itemId: pharmacy.itemId,
                frequencyId: frequency == null ? 0 : frequencyId,
                frequencyName: frequency == null ? '' : frequency,
                itemname: pharmacy.itemname,
                noOfDays:
                    noOfdays == null ? 0 : double.parse(noOfdays.toString()),
                quantity: 0,
                renewOrder: false,
                strength: '',
                totalQty: 0,
                visitId: '',
                genericId: pharmacy.genericId,
                categoryId: pharmacy.categoryId,
              ));
    }
    notifyListeners();
  }

  void onTapdeletePharmacy(
    String pharmacyName,
  ) {
    noOfDaysPharmacy.removeWhere((key, value) => key == pharmacyName);
    pharmacyFrequencyMap.removeWhere((key, value) => key == pharmacyName);
    pharmacynameMap.removeWhere((key, value) => key == pharmacyName);
    _drugpharmacylist.removeWhere((key, value) => key == pharmacyName);
    incrementAddPharmacy.removeWhere((key, value) => key == pharmacyName);
    pharmacyGenericName.removeWhere((key, value) => key == pharmacyName);
    totalquantitypharmacy.removeWhere((key, value) => key == pharmacyName);
    notifyListeners();
  }

  void clearallPharmacy() {
    totalquantitypharmacy.clear();
    noOfDaysPharmacy.clear();
    pharmacyFrequencyMap.clear();
    _drugpharmacylist.clear();
    pharmacynameMap.clear();
    incrementAddPharmacy.clear();
    pharmacyGenericName.clear();
    notifyListeners();
  }

  void addNoOfDaysPharmacy(PharmacyMedicinesModal pharmacy, noofdays, storeId) {
    if (_drugpharmacylist.containsKey(pharmacy.itemname)) {
      _drugpharmacylist.update(
          pharmacy.itemname,
          (existingitem) => PreviousPharmacyOrderModal(
                pharmacy: existingitem.pharmacy,
                drugdetailId: '',
                storeid: storeId,
                itemId: existingitem.itemId,
                frequencyId: existingitem.frequencyId,
                frequencyName: existingitem.frequencyName,
                itemname: existingitem.itemname,
                noOfDays: double.parse(noofdays.toString()),
                quantity: existingitem.totalQty,
                renewOrder: existingitem.renewOrder,
                strength: '',
                totalQty: existingitem.totalQty,
                visitId: '',
                genericId: existingitem.genericId,
                categoryId: existingitem.categoryId,
              ));
    } else {
      _drugpharmacylist.putIfAbsent(
          pharmacy.itemname,
          () => PreviousPharmacyOrderModal(
                pharmacy: pharmacy,
                drugdetailId: '',
                storeid: storeId,
                itemId: pharmacy.itemId,
                frequencyId: 0,
                frequencyName: '',
                itemname: pharmacy.itemname,
                noOfDays: double.parse(noofdays.toString()),
                quantity: 0,
                renewOrder: false,
                strength: '',
                totalQty: 0,
                visitId: '',
                genericId: pharmacy.genericId,
                categoryId: pharmacy.categoryId,
              ));
    }
    notifyListeners();
  }

  void addtotalQtyPharmacy(PharmacyMedicinesModal pharmacy, total, storeId) {
    if (_drugpharmacylist.containsKey(pharmacy.itemname)) {
      _drugpharmacylist.update(
          pharmacy.itemname,
          (existingitem) => PreviousPharmacyOrderModal(
                pharmacy: existingitem.pharmacy,
                drugdetailId: '',
                storeid: storeId,
                itemId: existingitem.itemId,
                frequencyId: existingitem.frequencyId,
                frequencyName: existingitem.frequencyName,
                itemname: existingitem.itemname,
                noOfDays: existingitem.noOfDays,
                quantity: total,
                renewOrder: existingitem.renewOrder,
                strength: '',
                totalQty: total,
                visitId: '',
                genericId: existingitem.genericId,
                categoryId: existingitem.categoryId,
              ));
    } else {
      _drugpharmacylist.putIfAbsent(
          pharmacy.itemname,
          () => PreviousPharmacyOrderModal(
                pharmacy: pharmacy,
                drugdetailId: '',
                storeid: storeId,
                itemId: pharmacy.itemId,
                frequencyId: 0,
                frequencyName: '',
                itemname: pharmacy.itemname,
                noOfDays: 0,
                quantity: total,
                renewOrder: false,
                strength: '',
                totalQty: total,
                visitId: '',
                genericId: pharmacy.genericId,
                categoryId: pharmacy.categoryId,
              ));
    }
    notifyListeners();
  }

  List<IpPatientDetials> _loadIPPatients = [];
  List<IpPatientDetials> get loadIPPatients {
    return [..._loadIPPatients];
  }

  Future<void> loadIPpatientsApi(locId) async {
    print(locId);
    final url = "$IP_CONFIG/life/patientlistapi/?locationId=$locId";
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    try {
      final response = await http.post(url,
          headers: {"Accept": "application/json", "Authorization": token});
      final responseData = json.decode(response.body);
      print(url);
      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }

      var rest = responseData["IP_patient_details"] as List;

      List<IpPatientDetials> list = rest
          .map<IpPatientDetials>((json) => IpPatientDetials.fromJson(json))
          .toList();
      _loadIPPatients = list;
    } catch (error) {
      throw error;
    }
  }

  List<PharmacyStores> _pharmacyStore = [];
  List<PharmacyStores> get pharmacyStore {
    return [..._pharmacyStore];
  }

  Future<void> loadstores() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    String locId = prefs.getString('locId');
    final url = "$IP_CONFIG/life/storelistapi/?locationId=$locId";
    print(url);

    // try {
    final response = await http.post(url,
        headers: {"Accept": "application/json", "Authorization": token});
    final responseData = json.decode(response.body);

    if (responseData['error'] != null) {
      throw HttpException(responseData['error']['message']);
    }

    var rest = responseData["Store_details"] as List;

    List<PharmacyStores> pharmacyStores = rest
        .map<PharmacyStores>((json) => PharmacyStores.fromJson(json))
        .toList();
    _pharmacyStore = pharmacyStores;

    notifyListeners();
    // } catch (error) {
    //   throw error;
    // }
  }

  Future<void> loadLocIdstores() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    String locId = prefs.getString('locid');
    final url = "$IP_CONFIG/life/storelistapi/?locationId=$locId";
    print(url);

    try {
      final response = await http.post(url,
          headers: {"Accept": "application/json", "Authorization": token});
      final responseData = json.decode(response.body);

      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }

      var rest = responseData["Store_details"] as List;

      List<PharmacyStores> pharmacyStores = rest
          .map<PharmacyStores>((json) => PharmacyStores.fromJson(json))
          .toList();
      _pharmacyStore = pharmacyStores;

      notifyListeners();
    } catch (error) {
      throw error;
    }
  }

  List<PharmacyMedicineFrequency> _pharmacyfrequency = [];
  List<PharmacyMedicineFrequency> get pharmacyfrequency {
    return [..._pharmacyfrequency];
  }

  Future<void> loadFrequency() async {
    final url = "$IP_CONFIG/life/frequencylistapi/?";
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    try {
      final response = await http.post(url,
          headers: {"Accept": "application/json", "Authorization": token});
      final responseData = json.decode(response.body);

      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }

      var rest = responseData["Frequency_details"] as List;
      List<PharmacyMedicineFrequency> pharmacyFrequencyList = rest
          .map<PharmacyMedicineFrequency>(
              (json) => PharmacyMedicineFrequency.fromJson(json))
          .toList();
      _pharmacyfrequency = pharmacyFrequencyList;
      notifyListeners();
    } catch (error) {
      print(error);
      throw error;
    }
  }

  void clearPharmacyitems() {
    pharmacyMedicines.clear();
  }

  List<PharmacyMedicinesModal> _pharmacyMedicines = [];
  List<PharmacyMedicinesModal> get pharmacyMedicines {
    return [..._pharmacyMedicines];
  }

  Future<void> loadPharmacyMedicines(storeId, search) async {
    final url =
        "$IP_CONFIG/life/druglistapi/?storeid=$storeId&drugname=$search";
    print(url);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    try {
      final response = await http.post(url,
          headers: {"Accept": "application/json", "Authorization": token});
      final responseData = json.decode(response.body);

      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }

      var rest = responseData["Drug_details"] as List;

      List<PharmacyMedicinesModal> pharmacyMedicine = rest
          .map<PharmacyMedicinesModal>(
              (json) => PharmacyMedicinesModal.fromJson(json))
          .toList();
      _pharmacyMedicines = pharmacyMedicine;
      notifyListeners();
    } catch (error) {
      print(error);
      throw error;
    }
  }

  List<PreviousPharmacyOrderModal> _previousPharmacyOrder = [];
  List<PreviousPharmacyOrderModal> get previousPharmacyOrder {
    return [..._previousPharmacyOrder];
  }

  Future<void> previousOrderPharmacy() async {
    previousPharmacyOrder.clear();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    String patientId = prefs.getString('selectedPatient');
    final url = "$IP_CONFIG/life/previousdruglistapi/?patientId=$patientId";
    print(url);
    try {
      final response = await http.post(url,
          headers: {"Accept": "application/json", "Authorization": token});
      final responseData = json.decode(response.body);
      print(responseData);
      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }

      var rest = responseData["Previous_order_details"] as List;
      print(rest);
      List<PreviousPharmacyOrderModal> servicelist = rest
          .map<PreviousPharmacyOrderModal>(
              (json) => PreviousPharmacyOrderModal.fromJson(json))
          .toList();
      _previousPharmacyOrder = servicelist;
      notifyListeners();
    } catch (error) {
      print(error);
      throw error;
    }
  }
  // Future<PreviousPharmacyOrderModal> previousOrderPharmacy() async {
  //    previousPharmacyOrder.clear();
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   String token = prefs.getString('token');
  //   String patientId = prefs.getString('selectedPatient');
  //   final http.Response response = await http.post(
  //     "$IP_CONFIG/life/previousdruglistapi/?patientId=$patientId",
  //     headers: <String, String>{
  //       'Content-Type': 'application/json',
  //       "Authorization": token,
  //     },
  //   );
  //   print(response.statusCode);
  //   print(response.body);
  //   final responseData = json.decode(response.body);
  //   var rest = responseData["Previous_order_details"] as List;
  //   List<PreviousPharmacyOrderModal> servicelist = rest
  //         .map<PreviousPharmacyOrderModal>(
  //             (json) => PreviousPharmacyOrderModal.fromJson(json))
  //         .toList();
  //         _previousPharmacyOrder = servicelist;
  //   notifyListeners();
  //   return PreviousPharmacyOrderModal.fromJson(jsonDecode(response.body));
  // }

  Future<Response> sendPharmacyOrders(context) async {
    _responsedataAPI.clear();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    print(drugpharmacylist);
    List<PreviousPharmacyOrderModal> pharmacy =
        drugpharmacylist.values.toList();
    List<DrugServicesDetialsList> services = [];
    List<SelectedDepartment> selected = selectedDepartmentdetials;
    // List<SelectedPatientdetials> patientIddetials = selectedpatientdetials;
    List<SaveSoapPatientDetials> patientIddetials =
        Provider.of<DrugOrderModalDoctor>(context, listen: false)
            .selectedpatientdetials;
    PharmacydetialsApi pharmacyDetials =
        PharmacydetialsApi(pharmacy, selected, patientIddetials, services);
    print(jsonEncode(pharmacyDetials));
    final http.Response response = await http.post(
      '$IP_CONFIG/life/drugorderapi/?',
      headers: <String, String>{
        'Content-Type': 'application/json',
        "Authorization": token,
      },
      body: jsonEncode(pharmacyDetials),
    );
    print(response.statusCode);
    print(response.body);
    final responseData = json.decode(response.body);
    var rest = responseData["Drug_Order"] as String;

    _responsedataAPI.add(Response(resp: rest));
    notifyListeners();
    return Response.fromJson(jsonDecode(response.body));
  }

  List<Response> _responsedataAPI = [];
  List<Response> get responsedataAPI {
    return [..._responsedataAPI];
  }

  Future<Response> sendServiceorder(context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    _responsedataAPI.clear();
    print(drugpharmacylist);
    List<PreviousPharmacyOrderModal> pharmacy = [];
    List<DrugServicesDetialsList> services = drugServicesList.values.toList();
    List<SelectedDepartment> selected = selectedDepartmentdetials;
    List<SaveSoapPatientDetials> patientIddetials =
        Provider.of<DrugOrderModalDoctor>(context, listen: false)
            .selectedpatientdetials;
    ServicesDetialsApi servicelist =
        ServicesDetialsApi(services, selected, patientIddetials, pharmacy);
    print(jsonEncode(servicelist));
    final http.Response response = await http.post(
      '$IP_CONFIG/life/drugorderapi/?',
      headers: <String, String>{
        'Content-Type': 'application/json',
        "Authorization": token,
      },
      body: jsonEncode(servicelist),
    );
    print(response.statusCode);
    print(response.body);
    final responseData = json.decode(response.body);
    var rest = responseData["Drug_Order"] as String;

    _responsedataAPI.add(Response(resp: rest));
    notifyListeners();
    return Response.fromJson(jsonDecode(response.body));
  }

  List<SelectedDepartment> _selectedDepartmentdetials = [];
  List<SelectedDepartment> get selectedDepartmentdetials {
    return [..._selectedDepartmentdetials];
  }

  Future<void> departmentSelected(
      {String siteId,
      String deptName,
      String deptId,
      String locName,
      String locId,
      String consultantId}) {
    _selectedDepartmentdetials.clear();
    List<SelectedDepartment> _selectedDepartmentdetial = [];
    _selectedDepartmentdetial.add(SelectedDepartment(
        consultantId: consultantId,
        selectedBranchId: siteId,
        selectedDepartment: deptName,
        selectedDepartmentId: deptId,
        selectedlocation: locName,
        selectedlocationId: locId));
    _selectedDepartmentdetials = _selectedDepartmentdetial;
    notifyListeners();
    return null;
  }

  Future<void> clearDepartments() {
    _selectedDepartmentdetials.clear();
    return null;
  }

  void addSelecteddepartmentData(String branch, String deptName, String deptId,
      String locName, String locId) {
    List<SelectedDepartment> _selectedDepartmentdetial = [];
    _selectedDepartmentdetial.add(SelectedDepartment(
        selectedBranch: branch,
        selectedBranchId: "",
        selectedDepartment: deptName,
        selectedDepartmentId: deptId,
        selectedlocation: locName,
        selectedlocationId: locId));
    _selectedDepartmentdetials = _selectedDepartmentdetial;
    notifyListeners();
  }

  List<SelectedPatientdetials> _selectedpatientdetials = [];
  List<SelectedPatientdetials> get selectedpatientdetials {
    return [..._selectedpatientdetials];
  }

  void addSelectedpatientsDetials(String patientid, String visitid) {
    List<SelectedPatientdetials> _selectedpatientdetial = [];
    _selectedpatientdetial
        .add(SelectedPatientdetials(patientId: patientid, visitId: visitid));
    _selectedpatientdetials = _selectedpatientdetial;
    clearallServices();
    clearallPharmacy();
    notifyListeners();
  }
}
