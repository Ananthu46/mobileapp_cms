import 'dart:convert';
import 'package:doctor_portal_cms/app/ip_config.dart';
import 'package:doctor_portal_cms/commons/exceptions/http_exceptions.dart';
import 'package:doctor_portal_cms/model/nursing/nursing_patient_modal_Api.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class NursingPatientProvider with ChangeNotifier {
  List<NursingOrderModal> _addNursingorder = [];
  List<NursingOrderModal> get addNursingorder {
    return [..._addNursingorder];
  }

  Future<void> addNursingOrder(Map arguments, description, date) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String patientId = prefs.getString('selectedPatient');
    String empId = prefs.getString('employeeid');
    String visitId = prefs.getString('selectedVisit');
    _addNursingorder.clear();
    _addNursingorder.add(NursingOrderModal(
        date: date,
        note: description,
        patientid: patientId,
        visitid: visitId,
        userid: empId));
  }

  // Future<http.Response> sendNursingOrders() {
  //   List<NursingOrderModal> _nursingOrders = loadIPPatients.toList();
  //   NursingOrderApi nursingOrders = NursingOrderApi(_nursingOrders);
  //   print(jsonEncode(nursingOrders));
  //   return http.post(
  //     '$IP_CONFIG/life/mobilenursingrecord/?',
  //     headers: <String, String>{
  //       'Content-Type': 'application/json',
  //     },
  //     body: jsonEncode(nursingOrders),

  //   );

  // }
  List<NursingOrderModal> _responseDataApi = [];
  List<NursingOrderModal> get responseDataApi {
    return [..._responseDataApi];
  }

  Future<NursingOrderModal> sendNursingOrders() async {
    responseDataApi.clear();
    List<NursingOrderModal> _nursingOrders = addNursingorder.toList();
    NursingOrderApi nursingOrders = NursingOrderApi(_nursingOrders);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    final http.Response response = await http.post(
      '$IP_CONFIG/life/mobilenursingrecord/?',
      headers: <String, String>{
        'Content-Type': 'application/json',
        "Authorization": token,
      },
      body: jsonEncode(nursingOrders),
    );
    print(jsonEncode(nursingOrders));
    print(response.statusCode);
    print(response.body);
    final responseData = json.decode(response.body);
    var rest = responseData["nursing_records"] as String;

    print(rest);
    _responseDataApi.add(NursingOrderModal(response: rest));
    notifyListeners();
    return NursingOrderModal.fromJson(jsonDecode(response.body));
  }

  List<PreviousNursingOrders> _loadpreviousNursingNotes = [];
  List<PreviousNursingOrders> get loadpreviousNursingNotes {
    return [..._loadpreviousNursingNotes];
  }

  Future<void> loadPreviousNursingNotes(
      String patientId, String visitId) async {
    final url =
        "$IP_CONFIG/life/mobilenursingrecordlists/?patientid=$patientId&visitid=$visitId";
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    try {
      print(url);
      final response = await http.post(url,
          headers: {"Accept": "application/json", "Authorization": token});
      final responseData = json.decode(response.body);

      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }

      var rest = responseData["Nursing_record_list"] as List;

      List<PreviousNursingOrders> list = rest
          .map<PreviousNursingOrders>(
              (json) => PreviousNursingOrders.fromJson(json))
          .toList();
      _loadpreviousNursingNotes = list;
    } catch (error) {
      throw error;
    }
  }
}
