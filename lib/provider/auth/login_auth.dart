import 'dart:async';
import 'dart:convert';
import 'package:connectivity/connectivity.dart';
import 'package:doctor_portal_cms/app/ip_config.dart';
import 'package:doctor_portal_cms/commons/exceptions/http_exceptions.dart';
import 'package:doctor_portal_cms/datastore/database/cms_doctorsportal_database.dart';
import 'package:doctor_portal_cms/model/auth/auth_user_resp.dart';
import 'package:doctor_portal_cms/provider/drug/drug_order_api_provider.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

import 'package:shared_preferences/shared_preferences.dart';

class LoginAuth with ChangeNotifier {
  static Future<bool> isNetworkAvailable() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return true;
    }
    return false;
  }

  String _token;
  bool _isAdmin;
  String _userId;
  DateTime _expiryDate;
  Timer _authTimer;
  List<DepartmentData> deptList;
  List<DeptLocation> location;
  bool get isAuth {
    return token != null;
  }

  bool get isAdmin {
    return admin;
  }

  String get token {
    if (_expiryDate != null) {
      return _token;
    }
    return null;
  }

  bool get admin {
    if (_isAdmin != null) {
      return _isAdmin;
    } else {
      return false;
    }
  }

  Future<void> login(String email, String password, context) async {
    final url = '$IP_CONFIG/life/mobileloginapi/?';
    print(url);
    try {
      final response = await http.post(
        url,
        body: json.encode(
          {
            'username': email,
            'password': password,
          },
        ),
      );

      print(json.decode(response.body));
      final responseData = json.decode(response.body);

      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }

      var dept = responseData["department_details"] as List;
      deptList = dept
          .map<DepartmentData>((json) => DepartmentData.fromJson(json))
          .toList();
      var _location = responseData["Location_details"] as List;
      location = _location
          .map<DeptLocation>((json) => DeptLocation.fromJson(json))
          .toList();
      await CMSDoctorsPortalDatabase().departmentDao.deleteAllDepartments();
      for (int i = 0; i < deptList.length; i++) {
        await CMSDoctorsPortalDatabase()
            .departmentDao
            .insertDepartment(Department(
              departmentname: deptList[i].departmentName,
              depatmentID: deptList[i].deptId,
            ));
      }
      await CMSDoctorsPortalDatabase().locationDao.deleteAllLocations();
      for (int i = 0; i < location.length; i++) {
        await CMSDoctorsPortalDatabase().locationDao.insertLocation(Location(
            locationID: location[i].locId, locationname: location[i].locName));
      }
      await setToken(responseData, context);
    } catch (error) {
      print(error);
      throw error;
    }
  }

  setToken(responseData, context) async {
    await Future.delayed(Duration(milliseconds: 1));
    var rest = responseData["Login_details"] as List;
    var emp = responseData["employeeDetials"] as List;
    print(emp);
    List<LoginData> list =
        rest.map<LoginData>((json) => LoginData.fromJson(json)).toList();
    List<LoginEmployeeData> emplist = emp
        .map<LoginEmployeeData>((json) => LoginEmployeeData.fromJson(json))
        .toList();
    await CMSDoctorsPortalDatabase().profileDao.deleteAllProfile();
    print(emplist[0].qualification);
    await CMSDoctorsPortalDatabase().profileDao.insertProfile(Profile(
        profileid: 0,
        firstname: emplist[0].employeename,
        lastname: emplist[0].employeename,
        email: emplist[0].email.isEmpty ? " " : emplist[0].email,
        genderid: emplist[0].gender,
        department: emplist[0].department,
        qualification:
            emplist[0].qualification.isEmpty ? " " : emplist[0].qualification,
        gender: emplist[0].gender == "1" ? "Male" : "Female"));
    if (list[0].connection == 'Sucess') {
      _token = emplist[0].token;
      _isAdmin = false;
      print(_isAdmin);
      _expiryDate = DateTime.now().add(Duration(hours: 5));
      _userId = emplist[0].userid;
      final prefs = await SharedPreferences.getInstance();
      final userData = json.encode({
        'token': emplist[0].token,
        'userId': _userId,
        'expiryDate': _expiryDate.toIso8601String()
      });
      prefs.setString('userData', userData);
      prefs.setString('userid', _userId);
      prefs.setString('locid', emplist[0].serviceid);
      prefs.setString('employeeid', emplist[0].employeeid);
      prefs.setString('token', _token);
      prefs.setString('departmentid', emplist[0].departmentid);
      prefs.setString('locId', emplist[0].serviceid);
      prefs.setString('siteId', "2");
      await Provider.of<DrugPatientProvider>(context, listen: false)
          .clearDepartments();
      await Provider.of<DrugPatientProvider>(context, listen: false)
          .departmentSelected(
              consultantId: emplist[0].employeeid,
              deptId: emplist[0].departmentid,
              deptName: emplist[0].servicecenter,
              locId: emplist[0].serviceid,
              locName: emplist[0].servicecenter,
              siteId: "2");

      _autologout();
      notifyListeners();
    }
  }

  Future<bool> tryAutoLogin() async {
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey('userData')) {
      return false;
    }
    final extracteddata =
        json.decode(prefs.getString('userData')) as Map<String, Object>;
    final expiryDate = DateTime.parse(extracteddata['expiryDate']);
    if (expiryDate.isBefore(DateTime.now())) {
      return false;
    }
    _token = extracteddata['token'];
    _userId = extracteddata['userId'];
    _expiryDate = expiryDate;

    notifyListeners();
    _autologout();
    return true;
  }

  Future<void> logout() async {
    _token = null;
    _userId = null;
    _expiryDate = null;
    if (_authTimer != null) {
      _authTimer.cancel();
    }
    notifyListeners();
    final prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }

  void _autologout() {
    if (_authTimer != null) {
      _authTimer.cancel();
    }
    final timetoExpiry = _expiryDate.difference(DateTime.now()).inSeconds;
    _authTimer = Timer(Duration(seconds: timetoExpiry), logout);
  }
}
