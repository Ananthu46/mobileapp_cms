import 'dart:convert';
import 'dart:typed_data';

import 'package:doctor_portal_cms/app/ip_config.dart';
import 'package:doctor_portal_cms/commons/exceptions/http_exceptions.dart';
import 'package:doctor_portal_cms/model/doctor/doctor_order_modal.dart';
import 'package:doctor_portal_cms/model/ehr_modal/ehr_main_modal.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class EHRMainProvider with ChangeNotifier {
  List<EHRConsultationVisits> _loadEHRVisits = [];
  List<EHRConsultationVisits> get loadEHRVisits {
    return [..._loadEHRVisits];
  }

  List<EHRConsultationNotes> _loadEHRConsultations = [];
  List<EHRConsultationNotes> get loadEHRConsultations {
    return [..._loadEHRConsultations];
  }

  // Map<String, List<EHRConsultationNotes>> _loadEHRConsultationsNote = {};
  // Map<String, List<EHRConsultationNotes>> get loadEHRConsultationsNote {
  //   return {..._loadEHRConsultationsNote};
  // }

  Future<void> loadEHRConsultation() async {
    _loadEHRVisits.clear();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    String patientId = prefs.getString('selectedPatient');
    final url = "$IP_CONFIG/life/ehrConsultationDetials/?patientId=$patientId";

    print(url);
    try {
      final response = await http.post(url,
          headers: {"Accept": "application/json", "Authorization": token});
      final responseData = json.decode(response.body);

      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }
      print(response.body);
      var rest = responseData["visits"] as List;
      var note = responseData["ehr_consultation"] as List;
      List<EHRConsultationVisits> list = rest
          .map<EHRConsultationVisits>(
              (json) => EHRConsultationVisits.fromJson(json))
          .toList();
      List<EHRConsultationNotes> notes = note
          .map<EHRConsultationNotes>(
              (json) => EHRConsultationNotes.fromJson(json))
          .toList();
      _loadEHRVisits = list;
      _loadEHRConsultations = notes;
      notifyListeners();
    } catch (error) {
      print(error);
      throw error;
    }
  }

  List<EHRVitalVisits> _loadEHRvitalvisits = [];
  List<EHRVitalVisits> get loadEHRvitalvisits {
    return [..._loadEHRvitalvisits];
  }

  List<EHRVitalVisitsValue> _loadEhrVitalValues = [];
  List<EHRVitalVisitsValue> get loadEhrVitalValues {
    return [..._loadEhrVitalValues];
  }

  Future<void> loadEHRVitalSigns() async {
    _loadEHRvitalvisits.clear();
    _loadEhrVitalValues.clear();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    String patientId = prefs.getString('selectedPatient');
    final url = "$IP_CONFIG/life/mobileEHRVitalSIgns/?patientid=$patientId";

    print(url);
    try {
      final response = await http.post(url,
          headers: {"Accept": "application/json", "Authorization": token});
      final responseData = json.decode(response.body);

      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }
      print(response.body);
      var rest = responseData["vital_visits"] as List;
      var note = responseData["vital_signs"] as List;
      List<EHRVitalVisits> list = rest
          .map<EHRVitalVisits>((json) => EHRVitalVisits.fromJson(json))
          .toList();
      List<EHRVitalVisitsValue> notes = note
          .map<EHRVitalVisitsValue>(
              (json) => EHRVitalVisitsValue.fromJson(json))
          .toList();
      _loadEHRvitalvisits = list;
      _loadEhrVitalValues = notes;
      print(_loadEHRvitalvisits.length);
      notifyListeners();
    } catch (error) {
      print(error);
      throw error;
    }
  }

  List<EHRReferrals> _loadreferrals = [];
  List<EHRReferrals> get loadreferrals {
    return [..._loadreferrals];
  }

  Future<void> loadreferral() async {
    _loadreferrals.clear();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    String patientId = prefs.getString('selectedPatient');
    final url = "$IP_CONFIG/life/loadPatientReferrals/?patientId=$patientId";
    print(url);
    try {
      final response = await http.post(url,
          headers: {"Accept": "application/json", "Authorization": token});
      final responseData = json.decode(response.body);

      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }
      print(response.body);
      var rest = responseData["Referral_list"] as List;
      List<EHRReferrals> list = rest
          .map<EHRReferrals>((json) => EHRReferrals.fromJson(json))
          .toList();
      _loadreferrals = list;
      print(_loadreferrals.length);
      notifyListeners();
    } catch (error) {
      print(error);
      throw error;
    }
  }

  List<EHRLoadUploadDocuments> _loaduploadDocuments = [];
  List<EHRLoadUploadDocuments> get loaduploadDocuments {
    return [..._loaduploadDocuments];
  }

  Future<void> loaduploadDocument() async {
    _loadreferrals.clear();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    String patientId = prefs.getString('selectedPatient');
    final url = "$IP_CONFIG/life/loadUploadDocuments/?patientId=$patientId";
    print(url);
    try {
      final response = await http.post(url,
          headers: {"Accept": "application/json", "Authorization": token});
      final responseData = json.decode(response.body);

      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }
      print(response.body);
      var rest = responseData["uploadDocument"] as List;
      List<EHRLoadUploadDocuments> list = rest
          .map<EHRLoadUploadDocuments>(
              (json) => EHRLoadUploadDocuments.fromJson(json))
          .toList();
      _loaduploadDocuments = list;
      print(_loaduploadDocuments.length);
      notifyListeners();
    } catch (error) {
      print(error);
      throw error;
    }
  }

  List<EHRUploadDocuments> _uploadmedDocument = [];
  List<EHRUploadDocuments> get uploadmedDocument {
    return [..._uploadmedDocument];
  }

  Future<void> uploadmedDocuments(
      {String docname, Uint8List bytes, String fileName}) async {
    _uploadmedDocument.clear();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    String patientId = prefs.getString('selectedPatient');
    String siteId = prefs.getString('siteId');
    String empId = prefs.getString('employeeid');
    String visitId = prefs.getString('selectedVisit');
    _uploadmedDocument.add(EHRUploadDocuments(
        patientId: patientId,
        documentName: docname,
        bytes: base64Encode(bytes),
        fileName: fileName,
        siteId: siteId,
        consultantId: empId,
        visitId: visitId));
    EHRUploadDocumentAPI upload =
        EHRUploadDocumentAPI(_uploadmedDocument.toList());

    print(jsonEncode(upload));
    final http.Response response = await http.post(
      '$IP_CONFIG/life/uploadmedicalDocuments/?',
      headers: <String, String>{
        'Content-Type': 'application/json',
        "Authorization": token,
      },
      body: jsonEncode(upload),
    );
    final responseData = json.decode(response.body);
    var rest = responseData["UploadDocument"] as String;
    _uploadmedDocument[0].response = rest;
    print(rest);
    notifyListeners();
    return EHRUploadDocuments.fromJson(jsonDecode(response.body));
  }
}
