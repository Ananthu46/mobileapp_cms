import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';

import '../../commons/constants/app_constants.dart';

class SharedPreferenceHelper {
  // shared pref instance
  Future<SharedPreferences> _sharedPreference;

  _getSharedPreferenceInstance(){
    if(_sharedPreference==null){
    _sharedPreference = SharedPreferences.getInstance();
    }
    return _sharedPreference;
  }

  // AUTHENTICATION
  Future<dynamic> get authToken async {
    return _getSharedPreferenceInstance().then((preference) {
      return preference.getString(AppConstants.AUTH_TOKEN);
    });
  }

  Future<void> saveAuthToken(String authToken) async {
    return _getSharedPreferenceInstance().then((preference) {
      preference.setString(AppConstants.AUTH_TOKEN, authToken);
    });
  }

  Future<void> removeAuthToken() async {
    return _getSharedPreferenceInstance().then((preference) {
      preference.remove(AppConstants.AUTH_TOKEN);
    });
  }

  Future<bool> get isLoggedIn async {
    return _getSharedPreferenceInstance().then((preference) {
      return preference.getString(AppConstants.AUTH_TOKEN) ?? false;
    });
  }

}