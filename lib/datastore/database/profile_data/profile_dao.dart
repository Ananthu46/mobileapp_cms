import 'package:doctor_portal_cms/datastore/database/cms_doctorsportal_database.dart';
import 'package:moor_flutter/moor_flutter.dart';
import 'profile_table.dart';
part 'profile_dao.g.dart';

@UseDao(
    tables: [Profiles], queries: {'deleteAllProfile': 'DELETE FROM profiles'})
class ProfileDao extends DatabaseAccessor<CMSDoctorsPortalDatabase>
    with _$ProfileDaoMixin {
  ProfileDao(CMSDoctorsPortalDatabase db) : super(db);

  Future<List<Profile>> getAllProfiles() => select(profiles).get();
  Stream<List<Profile>> watchAllProfiles() => select(profiles).watch();
  Future insertProfile(Insertable<Profile> profile) =>
      into(profiles).insert(profile);
  Future updateProfile(Insertable<Profile> profile) =>
      update(profiles).replace(profile);
  Future deleteProfile(Insertable<Profile> profile) =>
      delete(profiles).delete(profile);
}
