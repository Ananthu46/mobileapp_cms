// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'profile_dao.dart';

// **************************************************************************
// DaoGenerator
// **************************************************************************

mixin _$ProfileDaoMixin on DatabaseAccessor<CMSDoctorsPortalDatabase> {
  $ProfilesTable get profiles => db.profiles;
  Future<int> deleteAllProfile(
      {@Deprecated('No longer needed with Moor 1.6 - see the changelog for details')
          QueryEngine operateOn}) {
    return (operateOn ?? this).customUpdate(
      'DELETE FROM profiles',
      variables: [],
      updates: {profiles},
    );
  }
}
