import 'package:moor/moor.dart';

class Profiles extends Table {
  IntColumn get profileid => integer().autoIncrement()();
  TextColumn get firstname => text().withLength(min: 1, max: 500)();
  TextColumn get lastname => text().withLength(min: 1, max: 500)();
  TextColumn get email => text().withLength(min: 1, max: 500)();
  TextColumn get gender => text().withLength(min: 1, max: 500)();
  TextColumn get genderid => text().withLength(min: 1, max: 500)();
  TextColumn get qualification => text().withLength(min: 1, max: 500)();
  TextColumn get department => text().withLength(min: 1, max: 500)();
}
