import 'package:doctor_portal_cms/datastore/database/cms_doctorsportal_database.dart';
import 'package:doctor_portal_cms/datastore/database/patient_detials/patient_detials_table.dart';
import 'package:moor_flutter/moor_flutter.dart';
part 'patient_detials_dao.g.dart';

@UseDao(tables: [PatientDetials])
class PatientDetialDao extends DatabaseAccessor<CMSDoctorsPortalDatabase>
    with _$PatientDetialDaoMixin {
  PatientDetialDao(CMSDoctorsPortalDatabase db) : super(db);

  Future<List<PatientDetial>> getAllPatientDetials() =>
      select(patientDetials).get();
  Stream<List<PatientDetial>> watchAllPatientDetials() =>
      select(patientDetials).watch();
  Future insertPatientDetial(Insertable<PatientDetial> patientDetial) =>
      into(patientDetials).insert(patientDetial);
  Future updatePatientDetial(Insertable<PatientDetial> patientDetial) =>
      update(patientDetials).replace(patientDetial);
  Future deletePatientDetial(Insertable<PatientDetial> patientDetial) =>
      delete(patientDetials).delete(patientDetial);
}
