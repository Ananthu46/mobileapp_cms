import 'package:moor/moor.dart';

class PatientDetials extends Table {
  IntColumn get patientDetialsId => integer().autoIncrement()();
  RealColumn get billingtype => real()();
  RealColumn get roomNo => real()();
  RealColumn get ward => real()();
  TextColumn get patientName => text().withLength(min: 1, max: 500)();
  RealColumn get age => real()();
  TextColumn get gender => text().withLength(min: 1, max: 500)();
  RealColumn get admissionNo => real()();
  RealColumn get mrn => real()();
  TextColumn get admittingDoctor => text().withLength(min: 1, max: 500)();
  RealColumn get patientId => real()();
}
