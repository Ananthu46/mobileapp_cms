// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cms_doctorsportal_database.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps
class User extends DataClass implements Insertable<User> {
  final int id;
  final String username;
  final String access;
  final String refresh;
  User(
      {@required this.id,
      @required this.username,
      @required this.access,
      @required this.refresh});
  factory User.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    return User(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      username: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}username']),
      access:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}access']),
      refresh:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}refresh']),
    );
  }
  factory User.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return User(
      id: serializer.fromJson<int>(json['id']),
      username: serializer.fromJson<String>(json['username']),
      access: serializer.fromJson<String>(json['access']),
      refresh: serializer.fromJson<String>(json['refresh']),
    );
  }
  @override
  Map<String, dynamic> toJson(
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return {
      'id': serializer.toJson<int>(id),
      'username': serializer.toJson<String>(username),
      'access': serializer.toJson<String>(access),
      'refresh': serializer.toJson<String>(refresh),
    };
  }

  @override
  T createCompanion<T extends UpdateCompanion<User>>(bool nullToAbsent) {
    return UsersCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      username: username == null && nullToAbsent
          ? const Value.absent()
          : Value(username),
      access:
          access == null && nullToAbsent ? const Value.absent() : Value(access),
      refresh: refresh == null && nullToAbsent
          ? const Value.absent()
          : Value(refresh),
    ) as T;
  }

  User copyWith({int id, String username, String access, String refresh}) =>
      User(
        id: id ?? this.id,
        username: username ?? this.username,
        access: access ?? this.access,
        refresh: refresh ?? this.refresh,
      );
  @override
  String toString() {
    return (StringBuffer('User(')
          ..write('id: $id, ')
          ..write('username: $username, ')
          ..write('access: $access, ')
          ..write('refresh: $refresh')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(id.hashCode,
      $mrjc(username.hashCode, $mrjc(access.hashCode, refresh.hashCode))));
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is User &&
          other.id == id &&
          other.username == username &&
          other.access == access &&
          other.refresh == refresh);
}

class UsersCompanion extends UpdateCompanion<User> {
  final Value<int> id;
  final Value<String> username;
  final Value<String> access;
  final Value<String> refresh;
  const UsersCompanion({
    this.id = const Value.absent(),
    this.username = const Value.absent(),
    this.access = const Value.absent(),
    this.refresh = const Value.absent(),
  });
  UsersCompanion copyWith(
      {Value<int> id,
      Value<String> username,
      Value<String> access,
      Value<String> refresh}) {
    return UsersCompanion(
      id: id ?? this.id,
      username: username ?? this.username,
      access: access ?? this.access,
      refresh: refresh ?? this.refresh,
    );
  }
}

class $UsersTable extends Users with TableInfo<$UsersTable, User> {
  final GeneratedDatabase _db;
  final String _alias;
  $UsersTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _usernameMeta = const VerificationMeta('username');
  GeneratedTextColumn _username;
  @override
  GeneratedTextColumn get username => _username ??= _constructUsername();
  GeneratedTextColumn _constructUsername() {
    return GeneratedTextColumn('username', $tableName, false,
        minTextLength: 1, maxTextLength: 500);
  }

  final VerificationMeta _accessMeta = const VerificationMeta('access');
  GeneratedTextColumn _access;
  @override
  GeneratedTextColumn get access => _access ??= _constructAccess();
  GeneratedTextColumn _constructAccess() {
    return GeneratedTextColumn('access', $tableName, false,
        minTextLength: 1, maxTextLength: 500);
  }

  final VerificationMeta _refreshMeta = const VerificationMeta('refresh');
  GeneratedTextColumn _refresh;
  @override
  GeneratedTextColumn get refresh => _refresh ??= _constructRefresh();
  GeneratedTextColumn _constructRefresh() {
    return GeneratedTextColumn('refresh', $tableName, false,
        minTextLength: 1, maxTextLength: 500);
  }

  @override
  List<GeneratedColumn> get $columns => [id, username, access, refresh];
  @override
  $UsersTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'users';
  @override
  final String actualTableName = 'users';
  @override
  VerificationContext validateIntegrity(UsersCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    } else if (id.isRequired && isInserting) {
      context.missing(_idMeta);
    }
    if (d.username.present) {
      context.handle(_usernameMeta,
          username.isAcceptableValue(d.username.value, _usernameMeta));
    } else if (username.isRequired && isInserting) {
      context.missing(_usernameMeta);
    }
    if (d.access.present) {
      context.handle(
          _accessMeta, access.isAcceptableValue(d.access.value, _accessMeta));
    } else if (access.isRequired && isInserting) {
      context.missing(_accessMeta);
    }
    if (d.refresh.present) {
      context.handle(_refreshMeta,
          refresh.isAcceptableValue(d.refresh.value, _refreshMeta));
    } else if (refresh.isRequired && isInserting) {
      context.missing(_refreshMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  User map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return User.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(UsersCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.username.present) {
      map['username'] = Variable<String, StringType>(d.username.value);
    }
    if (d.access.present) {
      map['access'] = Variable<String, StringType>(d.access.value);
    }
    if (d.refresh.present) {
      map['refresh'] = Variable<String, StringType>(d.refresh.value);
    }
    return map;
  }

  @override
  $UsersTable createAlias(String alias) {
    return $UsersTable(_db, alias);
  }
}

class Profile extends DataClass implements Insertable<Profile> {
  final int profileid;
  final String firstname;
  final String lastname;
  final String email;
  final String gender;
  final String genderid;
  final String qualification;
  final String department;
  Profile(
      {@required this.profileid,
      @required this.firstname,
      @required this.lastname,
      @required this.email,
      @required this.gender,
      @required this.genderid,
      @required this.qualification,
      @required this.department});
  factory Profile.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    return Profile(
      profileid:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}profileid']),
      firstname: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}firstname']),
      lastname: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}lastname']),
      email:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}email']),
      gender:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}gender']),
      genderid: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}genderid']),
      qualification: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}qualification']),
      department: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}department']),
    );
  }
  factory Profile.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return Profile(
      profileid: serializer.fromJson<int>(json['profileid']),
      firstname: serializer.fromJson<String>(json['firstname']),
      lastname: serializer.fromJson<String>(json['lastname']),
      email: serializer.fromJson<String>(json['email']),
      gender: serializer.fromJson<String>(json['gender']),
      genderid: serializer.fromJson<String>(json['genderid']),
      qualification: serializer.fromJson<String>(json['qualification']),
      department: serializer.fromJson<String>(json['department']),
    );
  }
  @override
  Map<String, dynamic> toJson(
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return {
      'profileid': serializer.toJson<int>(profileid),
      'firstname': serializer.toJson<String>(firstname),
      'lastname': serializer.toJson<String>(lastname),
      'email': serializer.toJson<String>(email),
      'gender': serializer.toJson<String>(gender),
      'genderid': serializer.toJson<String>(genderid),
      'qualification': serializer.toJson<String>(qualification),
      'department': serializer.toJson<String>(department),
    };
  }

  @override
  T createCompanion<T extends UpdateCompanion<Profile>>(bool nullToAbsent) {
    return ProfilesCompanion(
      profileid: profileid == null && nullToAbsent
          ? const Value.absent()
          : Value(profileid),
      firstname: firstname == null && nullToAbsent
          ? const Value.absent()
          : Value(firstname),
      lastname: lastname == null && nullToAbsent
          ? const Value.absent()
          : Value(lastname),
      email:
          email == null && nullToAbsent ? const Value.absent() : Value(email),
      gender:
          gender == null && nullToAbsent ? const Value.absent() : Value(gender),
      genderid: genderid == null && nullToAbsent
          ? const Value.absent()
          : Value(genderid),
      qualification: qualification == null && nullToAbsent
          ? const Value.absent()
          : Value(qualification),
      department: department == null && nullToAbsent
          ? const Value.absent()
          : Value(department),
    ) as T;
  }

  Profile copyWith(
          {int profileid,
          String firstname,
          String lastname,
          String email,
          String gender,
          String genderid,
          String qualification,
          String department}) =>
      Profile(
        profileid: profileid ?? this.profileid,
        firstname: firstname ?? this.firstname,
        lastname: lastname ?? this.lastname,
        email: email ?? this.email,
        gender: gender ?? this.gender,
        genderid: genderid ?? this.genderid,
        qualification: qualification ?? this.qualification,
        department: department ?? this.department,
      );
  @override
  String toString() {
    return (StringBuffer('Profile(')
          ..write('profileid: $profileid, ')
          ..write('firstname: $firstname, ')
          ..write('lastname: $lastname, ')
          ..write('email: $email, ')
          ..write('gender: $gender, ')
          ..write('genderid: $genderid, ')
          ..write('qualification: $qualification, ')
          ..write('department: $department')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      profileid.hashCode,
      $mrjc(
          firstname.hashCode,
          $mrjc(
              lastname.hashCode,
              $mrjc(
                  email.hashCode,
                  $mrjc(
                      gender.hashCode,
                      $mrjc(
                          genderid.hashCode,
                          $mrjc(qualification.hashCode,
                              department.hashCode))))))));
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is Profile &&
          other.profileid == profileid &&
          other.firstname == firstname &&
          other.lastname == lastname &&
          other.email == email &&
          other.gender == gender &&
          other.genderid == genderid &&
          other.qualification == qualification &&
          other.department == department);
}

class ProfilesCompanion extends UpdateCompanion<Profile> {
  final Value<int> profileid;
  final Value<String> firstname;
  final Value<String> lastname;
  final Value<String> email;
  final Value<String> gender;
  final Value<String> genderid;
  final Value<String> qualification;
  final Value<String> department;
  const ProfilesCompanion({
    this.profileid = const Value.absent(),
    this.firstname = const Value.absent(),
    this.lastname = const Value.absent(),
    this.email = const Value.absent(),
    this.gender = const Value.absent(),
    this.genderid = const Value.absent(),
    this.qualification = const Value.absent(),
    this.department = const Value.absent(),
  });
  ProfilesCompanion copyWith(
      {Value<int> profileid,
      Value<String> firstname,
      Value<String> lastname,
      Value<String> email,
      Value<String> gender,
      Value<String> genderid,
      Value<String> qualification,
      Value<String> department}) {
    return ProfilesCompanion(
      profileid: profileid ?? this.profileid,
      firstname: firstname ?? this.firstname,
      lastname: lastname ?? this.lastname,
      email: email ?? this.email,
      gender: gender ?? this.gender,
      genderid: genderid ?? this.genderid,
      qualification: qualification ?? this.qualification,
      department: department ?? this.department,
    );
  }
}

class $ProfilesTable extends Profiles with TableInfo<$ProfilesTable, Profile> {
  final GeneratedDatabase _db;
  final String _alias;
  $ProfilesTable(this._db, [this._alias]);
  final VerificationMeta _profileidMeta = const VerificationMeta('profileid');
  GeneratedIntColumn _profileid;
  @override
  GeneratedIntColumn get profileid => _profileid ??= _constructProfileid();
  GeneratedIntColumn _constructProfileid() {
    return GeneratedIntColumn('profileid', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _firstnameMeta = const VerificationMeta('firstname');
  GeneratedTextColumn _firstname;
  @override
  GeneratedTextColumn get firstname => _firstname ??= _constructFirstname();
  GeneratedTextColumn _constructFirstname() {
    return GeneratedTextColumn('firstname', $tableName, false,
        minTextLength: 1, maxTextLength: 500);
  }

  final VerificationMeta _lastnameMeta = const VerificationMeta('lastname');
  GeneratedTextColumn _lastname;
  @override
  GeneratedTextColumn get lastname => _lastname ??= _constructLastname();
  GeneratedTextColumn _constructLastname() {
    return GeneratedTextColumn('lastname', $tableName, false,
        minTextLength: 1, maxTextLength: 500);
  }

  final VerificationMeta _emailMeta = const VerificationMeta('email');
  GeneratedTextColumn _email;
  @override
  GeneratedTextColumn get email => _email ??= _constructEmail();
  GeneratedTextColumn _constructEmail() {
    return GeneratedTextColumn('email', $tableName, false,
        minTextLength: 1, maxTextLength: 500);
  }

  final VerificationMeta _genderMeta = const VerificationMeta('gender');
  GeneratedTextColumn _gender;
  @override
  GeneratedTextColumn get gender => _gender ??= _constructGender();
  GeneratedTextColumn _constructGender() {
    return GeneratedTextColumn('gender', $tableName, false,
        minTextLength: 1, maxTextLength: 500);
  }

  final VerificationMeta _genderidMeta = const VerificationMeta('genderid');
  GeneratedTextColumn _genderid;
  @override
  GeneratedTextColumn get genderid => _genderid ??= _constructGenderid();
  GeneratedTextColumn _constructGenderid() {
    return GeneratedTextColumn('genderid', $tableName, false,
        minTextLength: 1, maxTextLength: 500);
  }

  final VerificationMeta _qualificationMeta =
      const VerificationMeta('qualification');
  GeneratedTextColumn _qualification;
  @override
  GeneratedTextColumn get qualification =>
      _qualification ??= _constructQualification();
  GeneratedTextColumn _constructQualification() {
    return GeneratedTextColumn('qualification', $tableName, false,
        minTextLength: 1, maxTextLength: 500);
  }

  final VerificationMeta _departmentMeta = const VerificationMeta('department');
  GeneratedTextColumn _department;
  @override
  GeneratedTextColumn get department => _department ??= _constructDepartment();
  GeneratedTextColumn _constructDepartment() {
    return GeneratedTextColumn('department', $tableName, false,
        minTextLength: 1, maxTextLength: 500);
  }

  @override
  List<GeneratedColumn> get $columns => [
        profileid,
        firstname,
        lastname,
        email,
        gender,
        genderid,
        qualification,
        department
      ];
  @override
  $ProfilesTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'profiles';
  @override
  final String actualTableName = 'profiles';
  @override
  VerificationContext validateIntegrity(ProfilesCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.profileid.present) {
      context.handle(_profileidMeta,
          profileid.isAcceptableValue(d.profileid.value, _profileidMeta));
    } else if (profileid.isRequired && isInserting) {
      context.missing(_profileidMeta);
    }
    if (d.firstname.present) {
      context.handle(_firstnameMeta,
          firstname.isAcceptableValue(d.firstname.value, _firstnameMeta));
    } else if (firstname.isRequired && isInserting) {
      context.missing(_firstnameMeta);
    }
    if (d.lastname.present) {
      context.handle(_lastnameMeta,
          lastname.isAcceptableValue(d.lastname.value, _lastnameMeta));
    } else if (lastname.isRequired && isInserting) {
      context.missing(_lastnameMeta);
    }
    if (d.email.present) {
      context.handle(
          _emailMeta, email.isAcceptableValue(d.email.value, _emailMeta));
    } else if (email.isRequired && isInserting) {
      context.missing(_emailMeta);
    }
    if (d.gender.present) {
      context.handle(
          _genderMeta, gender.isAcceptableValue(d.gender.value, _genderMeta));
    } else if (gender.isRequired && isInserting) {
      context.missing(_genderMeta);
    }
    if (d.genderid.present) {
      context.handle(_genderidMeta,
          genderid.isAcceptableValue(d.genderid.value, _genderidMeta));
    } else if (genderid.isRequired && isInserting) {
      context.missing(_genderidMeta);
    }
    if (d.qualification.present) {
      context.handle(
          _qualificationMeta,
          qualification.isAcceptableValue(
              d.qualification.value, _qualificationMeta));
    } else if (qualification.isRequired && isInserting) {
      context.missing(_qualificationMeta);
    }
    if (d.department.present) {
      context.handle(_departmentMeta,
          department.isAcceptableValue(d.department.value, _departmentMeta));
    } else if (department.isRequired && isInserting) {
      context.missing(_departmentMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {profileid};
  @override
  Profile map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Profile.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(ProfilesCompanion d) {
    final map = <String, Variable>{};
    if (d.profileid.present) {
      map['profileid'] = Variable<int, IntType>(d.profileid.value);
    }
    if (d.firstname.present) {
      map['firstname'] = Variable<String, StringType>(d.firstname.value);
    }
    if (d.lastname.present) {
      map['lastname'] = Variable<String, StringType>(d.lastname.value);
    }
    if (d.email.present) {
      map['email'] = Variable<String, StringType>(d.email.value);
    }
    if (d.gender.present) {
      map['gender'] = Variable<String, StringType>(d.gender.value);
    }
    if (d.genderid.present) {
      map['genderid'] = Variable<String, StringType>(d.genderid.value);
    }
    if (d.qualification.present) {
      map['qualification'] =
          Variable<String, StringType>(d.qualification.value);
    }
    if (d.department.present) {
      map['department'] = Variable<String, StringType>(d.department.value);
    }
    return map;
  }

  @override
  $ProfilesTable createAlias(String alias) {
    return $ProfilesTable(_db, alias);
  }
}

class PatientDetial extends DataClass implements Insertable<PatientDetial> {
  final int patientDetialsId;
  final double billingtype;
  final double roomNo;
  final double ward;
  final String patientName;
  final double age;
  final String gender;
  final double admissionNo;
  final double mrn;
  final String admittingDoctor;
  final double patientId;
  PatientDetial(
      {@required this.patientDetialsId,
      @required this.billingtype,
      @required this.roomNo,
      @required this.ward,
      @required this.patientName,
      @required this.age,
      @required this.gender,
      @required this.admissionNo,
      @required this.mrn,
      @required this.admittingDoctor,
      @required this.patientId});
  factory PatientDetial.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final doubleType = db.typeSystem.forDartType<double>();
    final stringType = db.typeSystem.forDartType<String>();
    return PatientDetial(
      patientDetialsId: intType.mapFromDatabaseResponse(
          data['${effectivePrefix}patient_detials_id']),
      billingtype: doubleType
          .mapFromDatabaseResponse(data['${effectivePrefix}billingtype']),
      roomNo:
          doubleType.mapFromDatabaseResponse(data['${effectivePrefix}room_no']),
      ward: doubleType.mapFromDatabaseResponse(data['${effectivePrefix}ward']),
      patientName: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}patient_name']),
      age: doubleType.mapFromDatabaseResponse(data['${effectivePrefix}age']),
      gender:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}gender']),
      admissionNo: doubleType
          .mapFromDatabaseResponse(data['${effectivePrefix}admission_no']),
      mrn: doubleType.mapFromDatabaseResponse(data['${effectivePrefix}mrn']),
      admittingDoctor: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}admitting_doctor']),
      patientId: doubleType
          .mapFromDatabaseResponse(data['${effectivePrefix}patient_id']),
    );
  }
  factory PatientDetial.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return PatientDetial(
      patientDetialsId: serializer.fromJson<int>(json['patientDetialsId']),
      billingtype: serializer.fromJson<double>(json['billingtype']),
      roomNo: serializer.fromJson<double>(json['roomNo']),
      ward: serializer.fromJson<double>(json['ward']),
      patientName: serializer.fromJson<String>(json['patientName']),
      age: serializer.fromJson<double>(json['age']),
      gender: serializer.fromJson<String>(json['gender']),
      admissionNo: serializer.fromJson<double>(json['admissionNo']),
      mrn: serializer.fromJson<double>(json['mrn']),
      admittingDoctor: serializer.fromJson<String>(json['admittingDoctor']),
      patientId: serializer.fromJson<double>(json['patientId']),
    );
  }
  @override
  Map<String, dynamic> toJson(
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return {
      'patientDetialsId': serializer.toJson<int>(patientDetialsId),
      'billingtype': serializer.toJson<double>(billingtype),
      'roomNo': serializer.toJson<double>(roomNo),
      'ward': serializer.toJson<double>(ward),
      'patientName': serializer.toJson<String>(patientName),
      'age': serializer.toJson<double>(age),
      'gender': serializer.toJson<String>(gender),
      'admissionNo': serializer.toJson<double>(admissionNo),
      'mrn': serializer.toJson<double>(mrn),
      'admittingDoctor': serializer.toJson<String>(admittingDoctor),
      'patientId': serializer.toJson<double>(patientId),
    };
  }

  @override
  T createCompanion<T extends UpdateCompanion<PatientDetial>>(
      bool nullToAbsent) {
    return PatientDetialsCompanion(
      patientDetialsId: patientDetialsId == null && nullToAbsent
          ? const Value.absent()
          : Value(patientDetialsId),
      billingtype: billingtype == null && nullToAbsent
          ? const Value.absent()
          : Value(billingtype),
      roomNo:
          roomNo == null && nullToAbsent ? const Value.absent() : Value(roomNo),
      ward: ward == null && nullToAbsent ? const Value.absent() : Value(ward),
      patientName: patientName == null && nullToAbsent
          ? const Value.absent()
          : Value(patientName),
      age: age == null && nullToAbsent ? const Value.absent() : Value(age),
      gender:
          gender == null && nullToAbsent ? const Value.absent() : Value(gender),
      admissionNo: admissionNo == null && nullToAbsent
          ? const Value.absent()
          : Value(admissionNo),
      mrn: mrn == null && nullToAbsent ? const Value.absent() : Value(mrn),
      admittingDoctor: admittingDoctor == null && nullToAbsent
          ? const Value.absent()
          : Value(admittingDoctor),
      patientId: patientId == null && nullToAbsent
          ? const Value.absent()
          : Value(patientId),
    ) as T;
  }

  PatientDetial copyWith(
          {int patientDetialsId,
          double billingtype,
          double roomNo,
          double ward,
          String patientName,
          double age,
          String gender,
          double admissionNo,
          double mrn,
          String admittingDoctor,
          double patientId}) =>
      PatientDetial(
        patientDetialsId: patientDetialsId ?? this.patientDetialsId,
        billingtype: billingtype ?? this.billingtype,
        roomNo: roomNo ?? this.roomNo,
        ward: ward ?? this.ward,
        patientName: patientName ?? this.patientName,
        age: age ?? this.age,
        gender: gender ?? this.gender,
        admissionNo: admissionNo ?? this.admissionNo,
        mrn: mrn ?? this.mrn,
        admittingDoctor: admittingDoctor ?? this.admittingDoctor,
        patientId: patientId ?? this.patientId,
      );
  @override
  String toString() {
    return (StringBuffer('PatientDetial(')
          ..write('patientDetialsId: $patientDetialsId, ')
          ..write('billingtype: $billingtype, ')
          ..write('roomNo: $roomNo, ')
          ..write('ward: $ward, ')
          ..write('patientName: $patientName, ')
          ..write('age: $age, ')
          ..write('gender: $gender, ')
          ..write('admissionNo: $admissionNo, ')
          ..write('mrn: $mrn, ')
          ..write('admittingDoctor: $admittingDoctor, ')
          ..write('patientId: $patientId')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      patientDetialsId.hashCode,
      $mrjc(
          billingtype.hashCode,
          $mrjc(
              roomNo.hashCode,
              $mrjc(
                  ward.hashCode,
                  $mrjc(
                      patientName.hashCode,
                      $mrjc(
                          age.hashCode,
                          $mrjc(
                              gender.hashCode,
                              $mrjc(
                                  admissionNo.hashCode,
                                  $mrjc(
                                      mrn.hashCode,
                                      $mrjc(admittingDoctor.hashCode,
                                          patientId.hashCode)))))))))));
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is PatientDetial &&
          other.patientDetialsId == patientDetialsId &&
          other.billingtype == billingtype &&
          other.roomNo == roomNo &&
          other.ward == ward &&
          other.patientName == patientName &&
          other.age == age &&
          other.gender == gender &&
          other.admissionNo == admissionNo &&
          other.mrn == mrn &&
          other.admittingDoctor == admittingDoctor &&
          other.patientId == patientId);
}

class PatientDetialsCompanion extends UpdateCompanion<PatientDetial> {
  final Value<int> patientDetialsId;
  final Value<double> billingtype;
  final Value<double> roomNo;
  final Value<double> ward;
  final Value<String> patientName;
  final Value<double> age;
  final Value<String> gender;
  final Value<double> admissionNo;
  final Value<double> mrn;
  final Value<String> admittingDoctor;
  final Value<double> patientId;
  const PatientDetialsCompanion({
    this.patientDetialsId = const Value.absent(),
    this.billingtype = const Value.absent(),
    this.roomNo = const Value.absent(),
    this.ward = const Value.absent(),
    this.patientName = const Value.absent(),
    this.age = const Value.absent(),
    this.gender = const Value.absent(),
    this.admissionNo = const Value.absent(),
    this.mrn = const Value.absent(),
    this.admittingDoctor = const Value.absent(),
    this.patientId = const Value.absent(),
  });
  PatientDetialsCompanion copyWith(
      {Value<int> patientDetialsId,
      Value<double> billingtype,
      Value<double> roomNo,
      Value<double> ward,
      Value<String> patientName,
      Value<double> age,
      Value<String> gender,
      Value<double> admissionNo,
      Value<double> mrn,
      Value<String> admittingDoctor,
      Value<double> patientId}) {
    return PatientDetialsCompanion(
      patientDetialsId: patientDetialsId ?? this.patientDetialsId,
      billingtype: billingtype ?? this.billingtype,
      roomNo: roomNo ?? this.roomNo,
      ward: ward ?? this.ward,
      patientName: patientName ?? this.patientName,
      age: age ?? this.age,
      gender: gender ?? this.gender,
      admissionNo: admissionNo ?? this.admissionNo,
      mrn: mrn ?? this.mrn,
      admittingDoctor: admittingDoctor ?? this.admittingDoctor,
      patientId: patientId ?? this.patientId,
    );
  }
}

class $PatientDetialsTable extends PatientDetials
    with TableInfo<$PatientDetialsTable, PatientDetial> {
  final GeneratedDatabase _db;
  final String _alias;
  $PatientDetialsTable(this._db, [this._alias]);
  final VerificationMeta _patientDetialsIdMeta =
      const VerificationMeta('patientDetialsId');
  GeneratedIntColumn _patientDetialsId;
  @override
  GeneratedIntColumn get patientDetialsId =>
      _patientDetialsId ??= _constructPatientDetialsId();
  GeneratedIntColumn _constructPatientDetialsId() {
    return GeneratedIntColumn('patient_detials_id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _billingtypeMeta =
      const VerificationMeta('billingtype');
  GeneratedRealColumn _billingtype;
  @override
  GeneratedRealColumn get billingtype =>
      _billingtype ??= _constructBillingtype();
  GeneratedRealColumn _constructBillingtype() {
    return GeneratedRealColumn(
      'billingtype',
      $tableName,
      false,
    );
  }

  final VerificationMeta _roomNoMeta = const VerificationMeta('roomNo');
  GeneratedRealColumn _roomNo;
  @override
  GeneratedRealColumn get roomNo => _roomNo ??= _constructRoomNo();
  GeneratedRealColumn _constructRoomNo() {
    return GeneratedRealColumn(
      'room_no',
      $tableName,
      false,
    );
  }

  final VerificationMeta _wardMeta = const VerificationMeta('ward');
  GeneratedRealColumn _ward;
  @override
  GeneratedRealColumn get ward => _ward ??= _constructWard();
  GeneratedRealColumn _constructWard() {
    return GeneratedRealColumn(
      'ward',
      $tableName,
      false,
    );
  }

  final VerificationMeta _patientNameMeta =
      const VerificationMeta('patientName');
  GeneratedTextColumn _patientName;
  @override
  GeneratedTextColumn get patientName =>
      _patientName ??= _constructPatientName();
  GeneratedTextColumn _constructPatientName() {
    return GeneratedTextColumn('patient_name', $tableName, false,
        minTextLength: 1, maxTextLength: 500);
  }

  final VerificationMeta _ageMeta = const VerificationMeta('age');
  GeneratedRealColumn _age;
  @override
  GeneratedRealColumn get age => _age ??= _constructAge();
  GeneratedRealColumn _constructAge() {
    return GeneratedRealColumn(
      'age',
      $tableName,
      false,
    );
  }

  final VerificationMeta _genderMeta = const VerificationMeta('gender');
  GeneratedTextColumn _gender;
  @override
  GeneratedTextColumn get gender => _gender ??= _constructGender();
  GeneratedTextColumn _constructGender() {
    return GeneratedTextColumn('gender', $tableName, false,
        minTextLength: 1, maxTextLength: 500);
  }

  final VerificationMeta _admissionNoMeta =
      const VerificationMeta('admissionNo');
  GeneratedRealColumn _admissionNo;
  @override
  GeneratedRealColumn get admissionNo =>
      _admissionNo ??= _constructAdmissionNo();
  GeneratedRealColumn _constructAdmissionNo() {
    return GeneratedRealColumn(
      'admission_no',
      $tableName,
      false,
    );
  }

  final VerificationMeta _mrnMeta = const VerificationMeta('mrn');
  GeneratedRealColumn _mrn;
  @override
  GeneratedRealColumn get mrn => _mrn ??= _constructMrn();
  GeneratedRealColumn _constructMrn() {
    return GeneratedRealColumn(
      'mrn',
      $tableName,
      false,
    );
  }

  final VerificationMeta _admittingDoctorMeta =
      const VerificationMeta('admittingDoctor');
  GeneratedTextColumn _admittingDoctor;
  @override
  GeneratedTextColumn get admittingDoctor =>
      _admittingDoctor ??= _constructAdmittingDoctor();
  GeneratedTextColumn _constructAdmittingDoctor() {
    return GeneratedTextColumn('admitting_doctor', $tableName, false,
        minTextLength: 1, maxTextLength: 500);
  }

  final VerificationMeta _patientIdMeta = const VerificationMeta('patientId');
  GeneratedRealColumn _patientId;
  @override
  GeneratedRealColumn get patientId => _patientId ??= _constructPatientId();
  GeneratedRealColumn _constructPatientId() {
    return GeneratedRealColumn(
      'patient_id',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [
        patientDetialsId,
        billingtype,
        roomNo,
        ward,
        patientName,
        age,
        gender,
        admissionNo,
        mrn,
        admittingDoctor,
        patientId
      ];
  @override
  $PatientDetialsTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'patient_detials';
  @override
  final String actualTableName = 'patient_detials';
  @override
  VerificationContext validateIntegrity(PatientDetialsCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.patientDetialsId.present) {
      context.handle(
          _patientDetialsIdMeta,
          patientDetialsId.isAcceptableValue(
              d.patientDetialsId.value, _patientDetialsIdMeta));
    } else if (patientDetialsId.isRequired && isInserting) {
      context.missing(_patientDetialsIdMeta);
    }
    if (d.billingtype.present) {
      context.handle(_billingtypeMeta,
          billingtype.isAcceptableValue(d.billingtype.value, _billingtypeMeta));
    } else if (billingtype.isRequired && isInserting) {
      context.missing(_billingtypeMeta);
    }
    if (d.roomNo.present) {
      context.handle(
          _roomNoMeta, roomNo.isAcceptableValue(d.roomNo.value, _roomNoMeta));
    } else if (roomNo.isRequired && isInserting) {
      context.missing(_roomNoMeta);
    }
    if (d.ward.present) {
      context.handle(
          _wardMeta, ward.isAcceptableValue(d.ward.value, _wardMeta));
    } else if (ward.isRequired && isInserting) {
      context.missing(_wardMeta);
    }
    if (d.patientName.present) {
      context.handle(_patientNameMeta,
          patientName.isAcceptableValue(d.patientName.value, _patientNameMeta));
    } else if (patientName.isRequired && isInserting) {
      context.missing(_patientNameMeta);
    }
    if (d.age.present) {
      context.handle(_ageMeta, age.isAcceptableValue(d.age.value, _ageMeta));
    } else if (age.isRequired && isInserting) {
      context.missing(_ageMeta);
    }
    if (d.gender.present) {
      context.handle(
          _genderMeta, gender.isAcceptableValue(d.gender.value, _genderMeta));
    } else if (gender.isRequired && isInserting) {
      context.missing(_genderMeta);
    }
    if (d.admissionNo.present) {
      context.handle(_admissionNoMeta,
          admissionNo.isAcceptableValue(d.admissionNo.value, _admissionNoMeta));
    } else if (admissionNo.isRequired && isInserting) {
      context.missing(_admissionNoMeta);
    }
    if (d.mrn.present) {
      context.handle(_mrnMeta, mrn.isAcceptableValue(d.mrn.value, _mrnMeta));
    } else if (mrn.isRequired && isInserting) {
      context.missing(_mrnMeta);
    }
    if (d.admittingDoctor.present) {
      context.handle(
          _admittingDoctorMeta,
          admittingDoctor.isAcceptableValue(
              d.admittingDoctor.value, _admittingDoctorMeta));
    } else if (admittingDoctor.isRequired && isInserting) {
      context.missing(_admittingDoctorMeta);
    }
    if (d.patientId.present) {
      context.handle(_patientIdMeta,
          patientId.isAcceptableValue(d.patientId.value, _patientIdMeta));
    } else if (patientId.isRequired && isInserting) {
      context.missing(_patientIdMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {patientDetialsId};
  @override
  PatientDetial map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return PatientDetial.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(PatientDetialsCompanion d) {
    final map = <String, Variable>{};
    if (d.patientDetialsId.present) {
      map['patient_detials_id'] =
          Variable<int, IntType>(d.patientDetialsId.value);
    }
    if (d.billingtype.present) {
      map['billingtype'] = Variable<double, RealType>(d.billingtype.value);
    }
    if (d.roomNo.present) {
      map['room_no'] = Variable<double, RealType>(d.roomNo.value);
    }
    if (d.ward.present) {
      map['ward'] = Variable<double, RealType>(d.ward.value);
    }
    if (d.patientName.present) {
      map['patient_name'] = Variable<String, StringType>(d.patientName.value);
    }
    if (d.age.present) {
      map['age'] = Variable<double, RealType>(d.age.value);
    }
    if (d.gender.present) {
      map['gender'] = Variable<String, StringType>(d.gender.value);
    }
    if (d.admissionNo.present) {
      map['admission_no'] = Variable<double, RealType>(d.admissionNo.value);
    }
    if (d.mrn.present) {
      map['mrn'] = Variable<double, RealType>(d.mrn.value);
    }
    if (d.admittingDoctor.present) {
      map['admitting_doctor'] =
          Variable<String, StringType>(d.admittingDoctor.value);
    }
    if (d.patientId.present) {
      map['patient_id'] = Variable<double, RealType>(d.patientId.value);
    }
    return map;
  }

  @override
  $PatientDetialsTable createAlias(String alias) {
    return $PatientDetialsTable(_db, alias);
  }
}

class Department extends DataClass implements Insertable<Department> {
  final String depatmentID;
  final String departmentname;
  Department({@required this.depatmentID, @required this.departmentname});
  factory Department.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final stringType = db.typeSystem.forDartType<String>();
    return Department(
      depatmentID: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}depatment_i_d']),
      departmentname: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}departmentname']),
    );
  }
  factory Department.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return Department(
      depatmentID: serializer.fromJson<String>(json['depatmentID']),
      departmentname: serializer.fromJson<String>(json['departmentname']),
    );
  }
  @override
  Map<String, dynamic> toJson(
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return {
      'depatmentID': serializer.toJson<String>(depatmentID),
      'departmentname': serializer.toJson<String>(departmentname),
    };
  }

  @override
  T createCompanion<T extends UpdateCompanion<Department>>(bool nullToAbsent) {
    return DepartmentsCompanion(
      depatmentID: depatmentID == null && nullToAbsent
          ? const Value.absent()
          : Value(depatmentID),
      departmentname: departmentname == null && nullToAbsent
          ? const Value.absent()
          : Value(departmentname),
    ) as T;
  }

  Department copyWith({String depatmentID, String departmentname}) =>
      Department(
        depatmentID: depatmentID ?? this.depatmentID,
        departmentname: departmentname ?? this.departmentname,
      );
  @override
  String toString() {
    return (StringBuffer('Department(')
          ..write('depatmentID: $depatmentID, ')
          ..write('departmentname: $departmentname')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      $mrjf($mrjc(depatmentID.hashCode, departmentname.hashCode));
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is Department &&
          other.depatmentID == depatmentID &&
          other.departmentname == departmentname);
}

class DepartmentsCompanion extends UpdateCompanion<Department> {
  final Value<String> depatmentID;
  final Value<String> departmentname;
  const DepartmentsCompanion({
    this.depatmentID = const Value.absent(),
    this.departmentname = const Value.absent(),
  });
  DepartmentsCompanion copyWith(
      {Value<String> depatmentID, Value<String> departmentname}) {
    return DepartmentsCompanion(
      depatmentID: depatmentID ?? this.depatmentID,
      departmentname: departmentname ?? this.departmentname,
    );
  }
}

class $DepartmentsTable extends Departments
    with TableInfo<$DepartmentsTable, Department> {
  final GeneratedDatabase _db;
  final String _alias;
  $DepartmentsTable(this._db, [this._alias]);
  final VerificationMeta _depatmentIDMeta =
      const VerificationMeta('depatmentID');
  GeneratedTextColumn _depatmentID;
  @override
  GeneratedTextColumn get depatmentID =>
      _depatmentID ??= _constructDepatmentID();
  GeneratedTextColumn _constructDepatmentID() {
    return GeneratedTextColumn('depatment_i_d', $tableName, false,
        minTextLength: 1, maxTextLength: 500);
  }

  final VerificationMeta _departmentnameMeta =
      const VerificationMeta('departmentname');
  GeneratedTextColumn _departmentname;
  @override
  GeneratedTextColumn get departmentname =>
      _departmentname ??= _constructDepartmentname();
  GeneratedTextColumn _constructDepartmentname() {
    return GeneratedTextColumn('departmentname', $tableName, false,
        minTextLength: 1, maxTextLength: 500);
  }

  @override
  List<GeneratedColumn> get $columns => [depatmentID, departmentname];
  @override
  $DepartmentsTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'departments';
  @override
  final String actualTableName = 'departments';
  @override
  VerificationContext validateIntegrity(DepartmentsCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.depatmentID.present) {
      context.handle(_depatmentIDMeta,
          depatmentID.isAcceptableValue(d.depatmentID.value, _depatmentIDMeta));
    } else if (depatmentID.isRequired && isInserting) {
      context.missing(_depatmentIDMeta);
    }
    if (d.departmentname.present) {
      context.handle(
          _departmentnameMeta,
          departmentname.isAcceptableValue(
              d.departmentname.value, _departmentnameMeta));
    } else if (departmentname.isRequired && isInserting) {
      context.missing(_departmentnameMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => <GeneratedColumn>{};
  @override
  Department map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Department.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(DepartmentsCompanion d) {
    final map = <String, Variable>{};
    if (d.depatmentID.present) {
      map['depatment_i_d'] = Variable<String, StringType>(d.depatmentID.value);
    }
    if (d.departmentname.present) {
      map['departmentname'] =
          Variable<String, StringType>(d.departmentname.value);
    }
    return map;
  }

  @override
  $DepartmentsTable createAlias(String alias) {
    return $DepartmentsTable(_db, alias);
  }
}

class Location extends DataClass implements Insertable<Location> {
  final String locationID;
  final String locationname;
  Location({@required this.locationID, @required this.locationname});
  factory Location.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final stringType = db.typeSystem.forDartType<String>();
    return Location(
      locationID: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}location_i_d']),
      locationname: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}locationname']),
    );
  }
  factory Location.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return Location(
      locationID: serializer.fromJson<String>(json['locationID']),
      locationname: serializer.fromJson<String>(json['locationname']),
    );
  }
  @override
  Map<String, dynamic> toJson(
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return {
      'locationID': serializer.toJson<String>(locationID),
      'locationname': serializer.toJson<String>(locationname),
    };
  }

  @override
  T createCompanion<T extends UpdateCompanion<Location>>(bool nullToAbsent) {
    return LocationsCompanion(
      locationID: locationID == null && nullToAbsent
          ? const Value.absent()
          : Value(locationID),
      locationname: locationname == null && nullToAbsent
          ? const Value.absent()
          : Value(locationname),
    ) as T;
  }

  Location copyWith({String locationID, String locationname}) => Location(
        locationID: locationID ?? this.locationID,
        locationname: locationname ?? this.locationname,
      );
  @override
  String toString() {
    return (StringBuffer('Location(')
          ..write('locationID: $locationID, ')
          ..write('locationname: $locationname')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(locationID.hashCode, locationname.hashCode));
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is Location &&
          other.locationID == locationID &&
          other.locationname == locationname);
}

class LocationsCompanion extends UpdateCompanion<Location> {
  final Value<String> locationID;
  final Value<String> locationname;
  const LocationsCompanion({
    this.locationID = const Value.absent(),
    this.locationname = const Value.absent(),
  });
  LocationsCompanion copyWith(
      {Value<String> locationID, Value<String> locationname}) {
    return LocationsCompanion(
      locationID: locationID ?? this.locationID,
      locationname: locationname ?? this.locationname,
    );
  }
}

class $LocationsTable extends Locations
    with TableInfo<$LocationsTable, Location> {
  final GeneratedDatabase _db;
  final String _alias;
  $LocationsTable(this._db, [this._alias]);
  final VerificationMeta _locationIDMeta = const VerificationMeta('locationID');
  GeneratedTextColumn _locationID;
  @override
  GeneratedTextColumn get locationID => _locationID ??= _constructLocationID();
  GeneratedTextColumn _constructLocationID() {
    return GeneratedTextColumn('location_i_d', $tableName, false,
        minTextLength: 1, maxTextLength: 500);
  }

  final VerificationMeta _locationnameMeta =
      const VerificationMeta('locationname');
  GeneratedTextColumn _locationname;
  @override
  GeneratedTextColumn get locationname =>
      _locationname ??= _constructLocationname();
  GeneratedTextColumn _constructLocationname() {
    return GeneratedTextColumn('locationname', $tableName, false,
        minTextLength: 1, maxTextLength: 500);
  }

  @override
  List<GeneratedColumn> get $columns => [locationID, locationname];
  @override
  $LocationsTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'locations';
  @override
  final String actualTableName = 'locations';
  @override
  VerificationContext validateIntegrity(LocationsCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.locationID.present) {
      context.handle(_locationIDMeta,
          locationID.isAcceptableValue(d.locationID.value, _locationIDMeta));
    } else if (locationID.isRequired && isInserting) {
      context.missing(_locationIDMeta);
    }
    if (d.locationname.present) {
      context.handle(
          _locationnameMeta,
          locationname.isAcceptableValue(
              d.locationname.value, _locationnameMeta));
    } else if (locationname.isRequired && isInserting) {
      context.missing(_locationnameMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => <GeneratedColumn>{};
  @override
  Location map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Location.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(LocationsCompanion d) {
    final map = <String, Variable>{};
    if (d.locationID.present) {
      map['location_i_d'] = Variable<String, StringType>(d.locationID.value);
    }
    if (d.locationname.present) {
      map['locationname'] = Variable<String, StringType>(d.locationname.value);
    }
    return map;
  }

  @override
  $LocationsTable createAlias(String alias) {
    return $LocationsTable(_db, alias);
  }
}

class Service extends DataClass implements Insertable<Service> {
  final double serviceid;
  final String servicename;
  Service({@required this.serviceid, @required this.servicename});
  factory Service.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final doubleType = db.typeSystem.forDartType<double>();
    final stringType = db.typeSystem.forDartType<String>();
    return Service(
      serviceid: doubleType
          .mapFromDatabaseResponse(data['${effectivePrefix}serviceid']),
      servicename: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}servicename']),
    );
  }
  factory Service.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return Service(
      serviceid: serializer.fromJson<double>(json['serviceid']),
      servicename: serializer.fromJson<String>(json['servicename']),
    );
  }
  @override
  Map<String, dynamic> toJson(
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return {
      'serviceid': serializer.toJson<double>(serviceid),
      'servicename': serializer.toJson<String>(servicename),
    };
  }

  @override
  T createCompanion<T extends UpdateCompanion<Service>>(bool nullToAbsent) {
    return ServicesCompanion(
      serviceid: serviceid == null && nullToAbsent
          ? const Value.absent()
          : Value(serviceid),
      servicename: servicename == null && nullToAbsent
          ? const Value.absent()
          : Value(servicename),
    ) as T;
  }

  Service copyWith({double serviceid, String servicename}) => Service(
        serviceid: serviceid ?? this.serviceid,
        servicename: servicename ?? this.servicename,
      );
  @override
  String toString() {
    return (StringBuffer('Service(')
          ..write('serviceid: $serviceid, ')
          ..write('servicename: $servicename')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(serviceid.hashCode, servicename.hashCode));
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is Service &&
          other.serviceid == serviceid &&
          other.servicename == servicename);
}

class ServicesCompanion extends UpdateCompanion<Service> {
  final Value<double> serviceid;
  final Value<String> servicename;
  const ServicesCompanion({
    this.serviceid = const Value.absent(),
    this.servicename = const Value.absent(),
  });
  ServicesCompanion copyWith(
      {Value<double> serviceid, Value<String> servicename}) {
    return ServicesCompanion(
      serviceid: serviceid ?? this.serviceid,
      servicename: servicename ?? this.servicename,
    );
  }
}

class $ServicesTable extends Services with TableInfo<$ServicesTable, Service> {
  final GeneratedDatabase _db;
  final String _alias;
  $ServicesTable(this._db, [this._alias]);
  final VerificationMeta _serviceidMeta = const VerificationMeta('serviceid');
  GeneratedRealColumn _serviceid;
  @override
  GeneratedRealColumn get serviceid => _serviceid ??= _constructServiceid();
  GeneratedRealColumn _constructServiceid() {
    return GeneratedRealColumn(
      'serviceid',
      $tableName,
      false,
    );
  }

  final VerificationMeta _servicenameMeta =
      const VerificationMeta('servicename');
  GeneratedTextColumn _servicename;
  @override
  GeneratedTextColumn get servicename =>
      _servicename ??= _constructServicename();
  GeneratedTextColumn _constructServicename() {
    return GeneratedTextColumn('servicename', $tableName, false,
        minTextLength: 1, maxTextLength: 500);
  }

  @override
  List<GeneratedColumn> get $columns => [serviceid, servicename];
  @override
  $ServicesTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'services';
  @override
  final String actualTableName = 'services';
  @override
  VerificationContext validateIntegrity(ServicesCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.serviceid.present) {
      context.handle(_serviceidMeta,
          serviceid.isAcceptableValue(d.serviceid.value, _serviceidMeta));
    } else if (serviceid.isRequired && isInserting) {
      context.missing(_serviceidMeta);
    }
    if (d.servicename.present) {
      context.handle(_servicenameMeta,
          servicename.isAcceptableValue(d.servicename.value, _servicenameMeta));
    } else if (servicename.isRequired && isInserting) {
      context.missing(_servicenameMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => <GeneratedColumn>{};
  @override
  Service map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Service.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(ServicesCompanion d) {
    final map = <String, Variable>{};
    if (d.serviceid.present) {
      map['serviceid'] = Variable<double, RealType>(d.serviceid.value);
    }
    if (d.servicename.present) {
      map['servicename'] = Variable<String, StringType>(d.servicename.value);
    }
    return map;
  }

  @override
  $ServicesTable createAlias(String alias) {
    return $ServicesTable(_db, alias);
  }
}

abstract class _$CMSDoctorsPortalDatabase extends GeneratedDatabase {
  _$CMSDoctorsPortalDatabase(QueryExecutor e)
      : super(const SqlTypeSystem.withDefaults(), e);
  $UsersTable _users;
  $UsersTable get users => _users ??= $UsersTable(this);
  $ProfilesTable _profiles;
  $ProfilesTable get profiles => _profiles ??= $ProfilesTable(this);
  $PatientDetialsTable _patientDetials;
  $PatientDetialsTable get patientDetials =>
      _patientDetials ??= $PatientDetialsTable(this);
  $DepartmentsTable _departments;
  $DepartmentsTable get departments => _departments ??= $DepartmentsTable(this);
  $LocationsTable _locations;
  $LocationsTable get locations => _locations ??= $LocationsTable(this);
  $ServicesTable _services;
  $ServicesTable get services => _services ??= $ServicesTable(this);
  UserDao _userDao;
  UserDao get userDao => _userDao ??= UserDao(this as CMSDoctorsPortalDatabase);
  ProfileDao _profileDao;
  ProfileDao get profileDao =>
      _profileDao ??= ProfileDao(this as CMSDoctorsPortalDatabase);
  PatientDetialDao _patientDetialDao;
  PatientDetialDao get patientDetialDao =>
      _patientDetialDao ??= PatientDetialDao(this as CMSDoctorsPortalDatabase);
  DepartmentDao _departmentDao;
  DepartmentDao get departmentDao =>
      _departmentDao ??= DepartmentDao(this as CMSDoctorsPortalDatabase);
  LocationDao _locationDao;
  LocationDao get locationDao =>
      _locationDao ??= LocationDao(this as CMSDoctorsPortalDatabase);
  ServiceDao _serviceDao;
  ServiceDao get serviceDao =>
      _serviceDao ??= ServiceDao(this as CMSDoctorsPortalDatabase);
  @override
  List<TableInfo> get allTables =>
      [users, profiles, patientDetials, departments, locations, services];
}
