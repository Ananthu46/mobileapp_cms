import 'package:moor/moor.dart';

class Users extends Table{
  IntColumn get id=>integer().autoIncrement()();
  TextColumn get username =>text().withLength(min: 1, max: 500)();
  TextColumn get access=>text().withLength(min: 1, max: 500)();
  TextColumn get refresh=>text().withLength(min: 1, max: 500)();
}



