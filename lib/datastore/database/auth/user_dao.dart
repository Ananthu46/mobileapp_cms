import 'package:doctor_portal_cms/datastore/database/cms_doctorsportal_database.dart';
import 'package:moor_flutter/moor_flutter.dart';
import 'user_table.dart';
part 'user_dao.g.dart';

@UseDao(tables: [Users])
class UserDao extends DatabaseAccessor<CMSDoctorsPortalDatabase>
    with _$UserDaoMixin {
  UserDao(CMSDoctorsPortalDatabase db) : super(db);

  Future<List<User>> getAllUsers() => select(users).get();
  Stream<List<User>> watchAllUsers() => select(users).watch();
  Future insertUser(Insertable<User> user) => into(users).insert(user);
  Future updateUser(Insertable<User> user) => update(users).replace(user);
  Future deleteUser(Insertable<User> user) => delete(users).delete(user);
}
