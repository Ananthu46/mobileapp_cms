import 'package:doctor_portal_cms/datastore/database/department/department_dao.dart';
import 'package:doctor_portal_cms/datastore/database/department/department_table.dart';
import 'package:doctor_portal_cms/datastore/database/department/location_table/location_dao.dart';
import 'package:doctor_portal_cms/datastore/database/department/location_table/location_table..dart';
import 'package:doctor_portal_cms/datastore/database/patient_detials/patient_detials_dao.dart';
import 'package:doctor_portal_cms/datastore/database/patient_detials/patient_detials_table.dart';
import 'package:doctor_portal_cms/datastore/database/profile_data/profile_dao.dart';
import 'package:doctor_portal_cms/datastore/database/profile_data/profile_table.dart';
import 'package:doctor_portal_cms/datastore/database/services/service_dao.dart';
import 'package:doctor_portal_cms/datastore/database/services/service_table.dart';
import 'package:moor_flutter/moor_flutter.dart';
import 'auth/user_dao.dart';
import 'auth/user_table.dart';
part 'cms_doctorsportal_database.g.dart';

@UseMoor(tables: [
  Users,
  Profiles,
  PatientDetials,
  Departments,
  Locations,
  Services
], daos: [
  UserDao,
  ProfileDao,
  PatientDetialDao,
  DepartmentDao,
  LocationDao,
  ServiceDao
])
class CMSDoctorsPortalDatabase extends _$CMSDoctorsPortalDatabase {
  CMSDoctorsPortalDatabase()
      : super(FlutterQueryExecutor.inDatabaseFolder(
            path: "db.sqlite", logStatements: true)) {}
  @override
  int get schemaVersion => 2;
}
