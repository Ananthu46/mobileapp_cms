import 'package:doctor_portal_cms/datastore/database/department/department_table.dart';
import 'package:doctor_portal_cms/datastore/database/cms_doctorsportal_database.dart';
import 'package:moor_flutter/moor_flutter.dart';

part 'department_dao.g.dart';

@UseDao(tables: [
  Departments,
], queries: {
  'deleteAllDepartments': 'DELETE FROM departments'
})
class DepartmentDao extends DatabaseAccessor<CMSDoctorsPortalDatabase>
    with _$DepartmentDaoMixin {
  DepartmentDao(CMSDoctorsPortalDatabase db) : super(db);

  Future<List<Department>> getAllDepartments() => select(departments).get();
  Stream<List<Department>> watchAllDepartments() => select(departments).watch();
  Future insertDepartment(Insertable<Department> department) =>
      into(departments).insert(department);
  Future updateDepartment(Insertable<Department> department) =>
      update(departments).replace(department);
  Future deleteDepartment(Insertable<Department> department) =>
      delete(departments).delete(department);
}
