import 'package:moor/moor.dart';

class Departments extends Table {
  TextColumn get depatmentID => text().withLength(min: 1, max: 500)();
  TextColumn get departmentname => text().withLength(min: 1, max: 500)();
}
