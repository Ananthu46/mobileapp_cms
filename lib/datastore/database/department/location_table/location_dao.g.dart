// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'location_dao.dart';

// **************************************************************************
// DaoGenerator
// **************************************************************************

mixin _$LocationDaoMixin on DatabaseAccessor<CMSDoctorsPortalDatabase> {
  $LocationsTable get locations => db.locations;
  Future<int> deleteAllLocations(
      {@Deprecated('No longer needed with Moor 1.6 - see the changelog for details')
          QueryEngine operateOn}) {
    return (operateOn ?? this).customUpdate(
      'DELETE FROM locations',
      variables: [],
      updates: {locations},
    );
  }
}
