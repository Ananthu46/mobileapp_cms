import 'package:doctor_portal_cms/datastore/database/department/location_table/location_table..dart';
import 'package:doctor_portal_cms/datastore/database/cms_doctorsportal_database.dart';
import 'package:moor_flutter/moor_flutter.dart';

part 'location_dao.g.dart';

@UseDao(tables: [
  Locations,
], queries: {
  'deleteAllLocations': 'DELETE FROM locations'
})
class LocationDao extends DatabaseAccessor<CMSDoctorsPortalDatabase>
    with _$LocationDaoMixin {
  LocationDao(CMSDoctorsPortalDatabase db) : super(db);

  Future<List<Location>> getAllLocations() => select(locations).get();
  Stream<List<Location>> watchAllLocations() => select(locations).watch();
  Future insertLocation(Insertable<Location> location) =>
      into(locations).insert(location);
  Future updateLocation(Insertable<Location> location) =>
      update(locations).replace(location);
  Future deleteLocation(Insertable<Location> location) =>
      delete(locations).delete(location);
}
