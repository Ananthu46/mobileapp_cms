import 'package:moor/moor.dart';

class Locations extends Table {
  TextColumn get locationID => text().withLength(min: 1, max: 500)();
  TextColumn get locationname => text().withLength(min: 1, max: 500)();
}
