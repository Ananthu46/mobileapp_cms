// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'department_dao.dart';

// **************************************************************************
// DaoGenerator
// **************************************************************************

mixin _$DepartmentDaoMixin on DatabaseAccessor<CMSDoctorsPortalDatabase> {
  $DepartmentsTable get departments => db.departments;
  Future<int> deleteAllDepartments(
      {@Deprecated('No longer needed with Moor 1.6 - see the changelog for details')
          QueryEngine operateOn}) {
    return (operateOn ?? this).customUpdate(
      'DELETE FROM departments',
      variables: [],
      updates: {departments},
    );
  }
}
