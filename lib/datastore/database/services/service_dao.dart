import 'package:doctor_portal_cms/datastore/database/cms_doctorsportal_database.dart';
import 'package:moor_flutter/moor_flutter.dart';
import 'service_table.dart';
part 'service_dao.g.dart';

@UseDao(
    tables: [Services], queries: {'deleteAllServices': 'DELETE FROM services'})
class ServiceDao extends DatabaseAccessor<CMSDoctorsPortalDatabase>
    with _$ServiceDaoMixin {
  ServiceDao(CMSDoctorsPortalDatabase db) : super(db);

  Future<List<Service>> getAllServices() => select(services).get();
  Stream<List<Service>> watchAllServices() => select(services).watch();
  Future insertService(Insertable<Service> service) =>
      into(services).insert(service);
  Future updateService(Insertable<Service> service) =>
      update(services).replace(service);
  Future deleteService(Insertable<Service> service) =>
      delete(services).delete(service);
}
