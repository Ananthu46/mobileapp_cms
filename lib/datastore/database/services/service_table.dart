import 'package:moor/moor.dart';

class Services extends Table {
  RealColumn get serviceid => real()();
  TextColumn get servicename => text().withLength(min: 1, max: 500)();
}
