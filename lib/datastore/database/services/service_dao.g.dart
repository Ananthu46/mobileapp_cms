// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'service_dao.dart';

// **************************************************************************
// DaoGenerator
// **************************************************************************

mixin _$ServiceDaoMixin on DatabaseAccessor<CMSDoctorsPortalDatabase> {
  $ServicesTable get services => db.services;
  Future<int> deleteAllServices(
      {@Deprecated('No longer needed with Moor 1.6 - see the changelog for details')
          QueryEngine operateOn}) {
    return (operateOn ?? this).customUpdate(
      'DELETE FROM services',
      variables: [],
      updates: {services},
    );
  }
}
