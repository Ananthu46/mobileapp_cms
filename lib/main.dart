import 'package:doctor_portal_cms/provider/auth/login_auth.dart';
import 'package:doctor_portal_cms/provider/doctor/doctor_order_provider.dart';
import 'package:doctor_portal_cms/provider/drug/drug_order_api_provider.dart';
import 'package:doctor_portal_cms/provider/ehr_provider/ehr_main_provider.dart';
import 'package:doctor_portal_cms/provider/nursing/nursing_patient_provider_Api.dart';
import 'package:doctor_portal_cms/screens/auth/login_screen.dart';
import 'package:doctor_portal_cms/screens/dashboard/dashbord_items.dart/doctors_dashbord.dart';
import './app/app_routes.dart';
import 'env/app_config.dart';
import 'env/config.dart';
import 'env/dev.dart';
import 'package:flutter/material.dart';
import 'dart:io';
import 'package:flutter/services.dart';
import 'app/app_theme.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'app/app_translations_delegate.dart';
import 'app/application.dart';
import 'package:provider/provider.dart';
import 'package:catcher/catcher_plugin.dart';

Catcher catcher;

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  CatcherOptions debugOptions =
      CatcherOptions(DialogReportMode(), [ConsoleHandler()]);

  //release configuration
  CatcherOptions releaseOptions = CatcherOptions(DialogReportMode(), [
    EmailManualHandler(["ananthuashokan46@gmail.com"])
  ]);

  //profile configuration
  CatcherOptions profileOptions = CatcherOptions(
    DialogReportMode(),
    [ConsoleHandler(), ToastHandler()],
    handlerTimeout: 10000,
  );

  catcher = Catcher(
      new AppConfig(config: Config.fromJson(config), child: MyApp()),
      debugConfig: debugOptions,
      releaseConfig: releaseOptions,
      profileConfig: profileOptions);

  SystemChrome.setPreferredOrientations(<DeviceOrientation>[
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]).then((_) => catcher);
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  AppTranslationsDelegate _newLocaleDelegate;

  @override
  void initState() {
    super.initState();
    _newLocaleDelegate = AppTranslationsDelegate(newLocale: null);
    application.onLocaleChanged = onLocaleChange;
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
      statusBarBrightness:
          Platform.isAndroid ? Brightness.dark : Brightness.light,
      systemNavigationBarColor: Colors.black,
      systemNavigationBarDividerColor: Colors.transparent,
      systemNavigationBarIconBrightness: Brightness.dark,
    ));
    return MultiProvider(
      providers: [
        // Provider<UserDao>(
        //   create: (context) => DoctorsPortalDatabase().userDao,
        //   child: SplashScreen(),
        // ),
        ChangeNotifierProvider(
          create: (ctx) => LoginAuth(),
        ),
        ChangeNotifierProvider(
          create: (ctx) => EHRMainProvider(),
        ),
        ChangeNotifierProvider(
          create: (ctx) => DrugOrderModalDoctor(),
        ),
        ChangeNotifierProvider(
          create: (ctx) => DrugPatientProvider(),
        ),
        ChangeNotifierProvider(
          create: (ctx) => NursingPatientProvider(),
        ),
      ],
      child: Consumer<LoginAuth>(
        builder: (ctx, auth, _) => MaterialApp(
          navigatorKey: Catcher.navigatorKey,
          title: 'Life HIS Doctor',
          debugShowCheckedModeBanner: false,
          theme: AppTheme.lightTheme,
          darkTheme: AppTheme.darkTheme,
          home: auth.isAuth
              ? DoctorsPortalDashbord()
              : FutureBuilder(
                  future: auth.tryAutoLogin(),
                  builder: (ctx, authResultSnapshot) => LoginInScreen(),
                ),
          routes: AppRouter.routes(),
          localizationsDelegates: [
            _newLocaleDelegate,
            const AppTranslationsDelegate(),
            //provides localised strings
            GlobalMaterialLocalizations.delegate,
            //provides RTL support
            GlobalWidgetsLocalizations.delegate,
          ],
          supportedLocales: application.supportedLocales(),
        ),
      ),
    );
  }

  void onLocaleChange(Locale locale) {
    setState(() {
      _newLocaleDelegate = AppTranslationsDelegate(newLocale: locale);
    });
  }
}
