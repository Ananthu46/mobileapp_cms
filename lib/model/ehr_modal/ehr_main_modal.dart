import 'dart:typed_data';

class EHRConsultationVisits {
  String visitId;
  String visit;
  String visitDate;
  List<EHRConsultationNotes> consultationNotes = [];
  EHRConsultationVisits(
      {this.visit, this.visitDate, this.visitId, this.consultationNotes});
  factory EHRConsultationVisits.fromJson(Map<dynamic, dynamic> json) {
    return EHRConsultationVisits(
      visit: json['visit'],
      visitDate: json['visitDate'].toString(),
      visitId: json['visitId'].toString(),
      consultationNotes: json['ehr_consultation'],
    );
  }

  Map<String, dynamic> toJson() => {
        'visitId': visitId,
        'visitDate': visitDate,
        'visit': visit,
      };
}

class EHRConsultationNotes {
  String soaptransactionId;
  String soaptext;
  String componentName;
  String visitId;
  EHRConsultationNotes(
      {this.soaptext,
      this.componentName,
      this.soaptransactionId,
      this.visitId});
  factory EHRConsultationNotes.fromJson(Map<dynamic, dynamic> json) {
    return EHRConsultationNotes(
      soaptext: json['soaptext'] == null ? " " : json['soaptext'],
      componentName: json['component_name'] == null
          ? " "
          : json['component_name'].toString(),
      soaptransactionId: json['soapTransactionId'] == null
          ? " "
          : json['soapTransactionId'].toString(),
      visitId: json['visitId'].toString(),
    );
  }
  Map<String, dynamic> toJson() => {
        'soapTransactionId': soaptransactionId,
        'component_name': componentName,
        'visit': soaptext,
        'visitId': visitId
      };
}

class EHRVitalVisits {
  String visitId;
  String visit;
  String enteredDate;
  EHRVitalVisits({this.visit, this.visitId, this.enteredDate});
  factory EHRVitalVisits.fromJson(Map<dynamic, dynamic> json) {
    return EHRVitalVisits(
      visit: json['visit'],
      visitId: json['visitId'].toString(),
      enteredDate: json['enteredTime'].toString(),
    );
  }

  Map<String, dynamic> toJson() => {
        'visitId': visitId,
        'visit': visit,
      };
}

class EHRVitalVisitsValue {
  String vitalSignId;
  String visitId;
  String parameterId;
  String currentValue;
  String parameterValue;
  String lowerrefRange;
  String upperrefRange;
  String enteredTime;
  String consultant;
  EHRVitalVisitsValue(
      {this.vitalSignId,
      this.visitId,
      this.parameterId,
      this.currentValue,
      this.lowerrefRange,
      this.parameterValue,
      this.upperrefRange,
      this.enteredTime,
      this.consultant});
  factory EHRVitalVisitsValue.fromJson(Map<dynamic, dynamic> json) {
    return EHRVitalVisitsValue(
      vitalSignId: json['vitalSignId'],
      visitId: json['visitId'],
      parameterId: json['parameterId'],
      currentValue: json['currentValue'],
      parameterValue: json['parameterValue'],
      lowerrefRange: json['lowerrefRange'],
      upperrefRange: json['upperrefRange'],
      enteredTime: json['enteredTime'],
      consultant: json['consultant'],
    );
  }
}

class EHRReferrals {
  String crossConsultationRequestId;
  String patientName;
  String departmentName;
  String currentValue;
  String empname;
  String reqDate;
  String mrno;
  String approvalStatus;
  String departmentId;
  String employeeId;
  String reqDepartmentName;
  String reqempname;
  String reqDepartmentId;
  String reqConsultantEmployeeId;
  String priority;
  String referalCount;
  String siteId;
  String lookupId;
  String age;
  String ageUnit;
  String patientAge;
  String dob;
  String opinionResult;
  String reqReason;
  String rejectReason;
  String opinionReason;
  EHRReferrals(
      {this.age,
      this.ageUnit,
      this.approvalStatus,
      this.crossConsultationRequestId,
      this.currentValue,
      this.departmentId,
      this.departmentName,
      this.dob,
      this.employeeId,
      this.empname,
      this.lookupId,
      this.mrno,
      this.opinionResult,
      this.patientAge,
      this.patientName,
      this.priority,
      this.referalCount,
      this.reqConsultantEmployeeId,
      this.reqDate,
      this.reqDepartmentId,
      this.reqDepartmentName,
      this.rejectReason,
      this.opinionReason,
      this.reqReason,
      this.reqempname,
      this.siteId});
  factory EHRReferrals.fromJson(Map<dynamic, dynamic> json) {
    return EHRReferrals(
      age: json['age'].toString(),
      ageUnit: json['ageUnit'].toString(),
      approvalStatus: json['approvalStatus'].toString(),
      crossConsultationRequestId: json['crossConsultationRequestId'].toString(),
      currentValue: json['currentValue'].toString(),
      departmentId: json['departmentId'].toString(),
      departmentName: json['departmentName'].toString(),
      dob: json['dob'].toString(),
      employeeId: json['employeeId'].toString(),
      empname: json['empname'].toString(),
      lookupId: json['lookupId'].toString(),
      mrno: json['mrno'].toString(),
      opinionResult: json['opinionResult'].toString(),
      patientAge: json['patientAge'].toString(),
      patientName: json['patientName'].toString(),
      priority: json['priority'].toString(),
      referalCount: json['referalCount'].toString(),
      reqConsultantEmployeeId: json['reqConsultantEmployeeId'].toString(),
      reqDate: json['reqDate'].toString(),
      reqDepartmentId: json['reqDepartmentId'].toString(),
      reqDepartmentName: json['reqDepartmentName'].toString(),
      rejectReason: json['rejectReason'].toString(),
      opinionReason: json['opinionReason'].toString(),
      reqReason: json['reqReason'].toString(),
      reqempname: json['reqempname'].toString(),
      siteId: json['siteId'].toString(),
    );
  }
}

class EHRLoadUploadDocuments {
  String documentName;
  String fileName;
  String uploadby;
  EHRLoadUploadDocuments({this.fileName, this.documentName, this.uploadby});
  factory EHRLoadUploadDocuments.fromJson(Map<dynamic, dynamic> json) {
    return EHRLoadUploadDocuments(
      fileName: json['fileName'],
      documentName: json['documentName'].toString(),
      uploadby: json['uploadby'].toString(),
    );
  }

  Map<String, dynamic> toJson() => {
        'documentName': documentName,
        'fileName': fileName,
        "uploadby": uploadby
      };
}

class EHRUploadDocuments {
  String documentName;
  String fileName;
  String bytes;
  String siteId;
  String patientId;
  String response;
  String consultant;
  String consultantId;
  String visitId;
  EHRUploadDocuments(
      {this.fileName,
      this.documentName,
      this.bytes,
      this.patientId,
      this.siteId,
      this.response,
      this.consultant,
      this.consultantId,
      this.visitId});
  factory EHRUploadDocuments.fromJson(Map<dynamic, dynamic> json) {
    return EHRUploadDocuments(
      response: json['Message'],
    );
  }

  Map<String, dynamic> toJson() => {
        'description': documentName,
        'fileName': fileName,
        "siteId": siteId,
        "patientId": patientId,
        "bytes": bytes,
        "consultant": consultant,
        "consultId": consultantId,
        "visitId": visitId
      };
}

class EHRUploadDocumentAPI {
  List<EHRUploadDocuments> uploadDoc;
  String response;
  EHRUploadDocumentAPI([this.uploadDoc]);
  Map toJson() {
    List<Map> uploadDoc = this.uploadDoc != null
        ? this.uploadDoc.map((i) => i.toJson()).toList()
        : null;
    return {
      'uploadDoc': uploadDoc,
    };
  }
}
