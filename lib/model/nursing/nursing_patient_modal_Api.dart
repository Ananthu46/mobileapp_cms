import 'package:flutter/foundation.dart';

class NursingPatientModal with ChangeNotifier {
  final int mrno;
  final String patientID;
  final String patientName;
  final int age;
  final String serviceCenter;
  final String doctorname;
  final String department;
  final String visitid;
  NursingPatientModal(
      {@required this.mrno,
      @required this.patientID,
      @required this.patientName,
      @required this.age,
      @required this.serviceCenter,
      @required this.doctorname,
      @required this.department,
      this.visitid});
}

class NursingOrderModal {
  String patientid;
  String date;
  String userid;
  String note;
  String visitid;
  String response;
  NursingOrderModal(
      {this.patientid,
      this.visitid,
      this.userid,
      this.date,
      this.note,
      this.response});
  Map<String, dynamic> toJson() => {
        'patient_id': patientid,
        'date': date,
        'userid': userid,
        'notes': note,
        'visit_id': visitid,
      };
  factory NursingOrderModal.fromJson(Map<dynamic, dynamic> json) {
    return NursingOrderModal(response: json['nursing_records']);
  }
}

class NursingOrderApi {
  List<NursingOrderModal> nursingOrders;

  NursingOrderApi([this.nursingOrders]);

  Map toJson() {
    List<Map> nursingOrders = this.nursingOrders != null
        ? this.nursingOrders.map((i) => i.toJson()).toList()
        : null;
    return {
      'Nursing_records': nursingOrders,
    };
  }
}

class PreviousNursingOrders {
  DateTime date;
  String username;
  String note;
  PreviousNursingOrders({this.date, this.note, this.username});
  Map<String, dynamic> toJson() => {
        'date': date,
        'username': username,
        'nots': note,
      };
  factory PreviousNursingOrders.fromJson(Map<dynamic, dynamic> json) {
    return PreviousNursingOrders(
        date: DateTime.parse(json['date']),
        note: json['notes'],
        username: json['user_name']);
  }
}
