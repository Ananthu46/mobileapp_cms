import 'package:flutter/foundation.dart';

class AuthUserResponse extends ChangeNotifier {
  String username;
  String otp;
  String status;

  AuthUserResponse({
    @required this.username,
    @required this.otp,
    @required this.status,
  });

  factory AuthUserResponse.fromJson(Map<String, dynamic> json) =>
      AuthUserResponse(
          username: json["username"], otp: json["otp"], status: json["status"]);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['username'] = this.username;
    data['otp'] = this.otp;
    data['status'] = this.status;
    return data;
  }
}

class LoginData {
  String connection;
  bool isAdmin;

  LoginData({this.connection, this.isAdmin});

  factory LoginData.fromJson(Map<dynamic, dynamic> json) {
    return LoginData(
      connection: json['connection'],
      isAdmin: json['isadmin'],
    );
  }
}

class LoginEmployeeData {
  String token;
  String loginid;
  String username;
  String userid;
  String employeeid;
  String employeename;
  String email;
  String departmentid;
  String servicecenter;
  String serviceid;
  String gender;
  String qualification;
  String department;
  LoginEmployeeData(
      {this.departmentid,
      this.email,
      this.employeeid,
      this.employeename,
      this.loginid,
      this.token,
      this.userid,
      this.username,
      this.gender,
      this.qualification,
      this.serviceid,
      this.department});

  factory LoginEmployeeData.fromJson(Map<dynamic, dynamic> json) {
    return LoginEmployeeData(
      token: json['token'],
      departmentid: json['departmentid'],
      email: json['email'],
      employeeid: json['employeeid'],
      employeename: json['employeename'],
      loginid: json['loginid'],
      userid: json['userid'],
      username: json['username'],
      serviceid: json['locId'],
      gender: json['gender'],
      qualification: json['qualification'],
      department: json['departmentName'],
    );
  }
}

class DepartmentData {
  String deptId;
  String departmentName;

  DepartmentData({
    this.deptId,
    this.departmentName,
  });

  factory DepartmentData.fromJson(Map<dynamic, dynamic> json) {
    return DepartmentData(
      deptId: json['dep_id'],
      departmentName: json['dep_name'],
    );
  }
}

class DeptLocation {
  String locId;
  String locName;

  DeptLocation({this.locId, this.locName});

  factory DeptLocation.fromJson(Map<dynamic, dynamic> json) {
    return DeptLocation(
      locId: json['loc_id'],
      locName: json['loc_name'],
    );
  }
}
