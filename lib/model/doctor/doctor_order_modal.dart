import 'package:doctor_portal_cms/model/drug/drug_order_api_modal.dart';
import 'package:flutter/material.dart';

class DoctorsMedicalHistory {
  final String medicalSoapNoteItems;
  DoctorsMedicalHistory({@required this.medicalSoapNoteItems});
}

class DoctorsOpPatients {
  String queueno;
  String patientName;
  String visitId;
  String age;
  String ageUnit;
  String gender;
  String mrn;
  String patientId;
  String encounterId;
  String userid;
  DoctorsOpPatients(
      {this.queueno,
      this.patientName,
      this.visitId,
      this.age,
      this.ageUnit,
      this.gender,
      this.mrn,
      this.patientId,
      this.encounterId,
      this.userid});
  factory DoctorsOpPatients.fromJson(Map<dynamic, dynamic> json) {
    return DoctorsOpPatients(
      queueno: json['queue_no'],
      visitId: json['visit_id'],
      age: json['age'],
      ageUnit: json[''],
      encounterId: json['encounter_id'],
      gender: json['gender'],
      mrn: json['mrn'],
      patientId: json['patient_id'],
      patientName: json['patient_name'],
    );
  }
  Map<String, dynamic> toJson() => {
        'queue_no': queueno,
        'patient_name': patientName,
        'visit_id': visitId,
        'age': age,
        'age_unit': ageUnit,
        'gender': gender,
        'mrn': mrn,
        'patient_id': patientId,
        'encounter_id': encounterId,
      };
}

class DoctorsIpPatients {
  String patientAdmissionId;
  String patientName;
  String visitId;
  String age;
  // String ageUnit;
  String gender;
  String genderid;
  String mrn;
  String patientId;
  String userid;
  String location;
  String encounterId;
  DoctorsIpPatients(
      {this.patientAdmissionId,
      this.patientName,
      this.visitId,
      this.age,
      this.location,
      this.gender,
      this.mrn,
      this.patientId,
      this.userid,
      this.genderid,
      this.encounterId});
  factory DoctorsIpPatients.fromJson(Map<dynamic, dynamic> json) {
    return DoctorsIpPatients(
      patientAdmissionId: json['patient_admission_id'],
      visitId: json['visit_id'],
      age: json['age'],
      location: json['location'],
      gender: json['gender'],
      mrn: json['mrn'],
      patientId: json['patient_id'],
      patientName: json['patient_name'],
      genderid: json['genderid'],
      encounterId: json['encounterid'],
    );
  }
  Map<String, dynamic> toJson() => {
        'patient_admission_id': patientAdmissionId,
        'patient_name': patientName,
        'visit_id': visitId,
        'age': age,
        'location': location,
        'gender': gender,
        'mrn': mrn,
        'patient_id': patientId,
        'genderid': genderid
      };
}

class DoctorsSoapNoteDetials {
  String componentId;
  String typeName;
  String componentName;
  String typeId;
  String textid;
  String textvalue;
  String transactionId;
  bool edit = false;
  String response;
  String onChangeValue;
  DoctorsSoapNoteDetials(
      {this.componentId,
      this.componentName,
      this.typeId,
      this.typeName,
      this.textid,
      this.textvalue,
      this.transactionId,
      this.edit,
      this.response,
      this.onChangeValue});
  factory DoctorsSoapNoteDetials.fromJson(Map<dynamic, dynamic> json) {
    return DoctorsSoapNoteDetials(
      componentId: json['component_id'],
      typeName: json['type_name'],
      componentName: json['component_name'],
      typeId: json['type_id'],
      textid: json['text_id'],
      textvalue: json['text_value'],
      transactionId: json['transactionid'],
      edit: false,
    );
  }
  Map<String, dynamic> toJson() => {
        'id': componentId,
        'type_name': typeName,
        'component_name': componentName,
        'type_id': typeId,
        'text': textvalue,
        'textId': textid,
        'transactionId': transactionId
      };
}

// class SoapNotesOrders {
//   String componentId;
//   String text;
//   String textId;
//   String response;
//   String transactionI
//   SoapNotesOrders({this.componentId, this.text, this.response, this.textId});
//   factory SoapNotesOrders.fromJson(Map<dynamic, dynamic> json) {
//     return SoapNotesOrders(
//       response: json['response'],
//     );
//   }
//   Map<String, dynamic> toJson() =>
//       {'id': componentId, 'text': text, 'textId': textId};
// }

class SaveProvisionalDiagnosis {
  String text;
  String visitId;
  String response;
  SaveProvisionalDiagnosis({this.visitId, this.text, this.response});
  factory SaveProvisionalDiagnosis.fromJson(Map<dynamic, dynamic> json) {
    return SaveProvisionalDiagnosis(
      response: json['response'],
    );
  }
  Map<String, dynamic> toJson() => {'text': text, 'visitId': visitId};
}

class SaveSoapNoteApi {
  List<DoctorsSoapNoteDetials> savesoap;
  List<SaveSoapPatientDetials> patientdetials;
  SaveSoapNoteApi([this.savesoap, this.patientdetials]);

  Map toJson() {
    List<Map> saveSoap = this.savesoap != null
        ? this.savesoap.map((i) => i.toJson()).toList()
        : null;
    List<Map> patientdetials = this.patientdetials != null
        ? this.patientdetials.map((i) => i.toJson()).toList()
        : null;
    return {
      'soapnotes': saveSoap,
      'patientdetails': patientdetials,
    };
  }
}

class SaveProvisionalDiagnosisApi {
  List<SaveProvisionalDiagnosis> saveProvisional;
  SaveProvisionalDiagnosisApi([this.saveProvisional]);
  Map toJson() {
    List<Map> saveProvisional = this.saveProvisional != null
        ? this.saveProvisional.map((i) => i.toJson()).toList()
        : null;
    return {
      'provisional': saveProvisional,
    };
  }
}

class SaveGeneralInformation {
  List<GeneralInfoDetials> savegeneralInformation;
  SaveGeneralInformation([this.savegeneralInformation]);

  Map toJson() {
    List<Map> savegeneralInformation = this.savegeneralInformation != null
        ? this.savegeneralInformation.map((i) => i.toJson()).toList()
        : null;
    return {
      'general_info': savegeneralInformation,
    };
  }
}

class SaveVitalSign {
  List<VitalSignsDetials> saveVital;
  List<SaveSoapPatientDetials> patientIddetials;
  List<SelectedDepartment> departmentDetials;
  SaveVitalSign(
      [this.saveVital, this.departmentDetials, this.patientIddetials]);

  Map toJson() {
    List<Map> saveVital = this.saveVital != null
        ? this.saveVital.map((i) => i.toJson()).toList()
        : null;
    List<Map> patientIddetials = this.patientIddetials != null
        ? this.patientIddetials.map((i) => i.toJson()).toList()
        : null;
    List<Map> departmentDetials = this.departmentDetials != null
        ? this.departmentDetials.map((i) => i.toJson()).toList()
        : null;
    return {
      'vital_sign': saveVital,
      'Patient_detials': patientIddetials,
      'department_detials': departmentDetials,
    };
  }
}

class SaveSoapPatientDetials {
  String patientId;
  String visitId;
  String userId;
  String mrn;
  String encounterId;
  String transactionId;
  SaveSoapPatientDetials(
      {this.patientId,
      this.transactionId,
      this.userId,
      this.visitId,
      this.encounterId,
      this.mrn});
  factory SaveSoapPatientDetials.fromJson(Map<dynamic, dynamic> json) {
    return SaveSoapPatientDetials(
      transactionId: json['soapnote_id'],
    );
  }
  Map<String, dynamic> toJson() => {
        'patientId': patientId,
        'visitId': visitId,
        'userid': userId,
        'transationId': transactionId,
        'encounterId': encounterId,
        'mrn': mrn
      };
}

// class SaveSoapIPPatientDetials {
//   String patientId;
//   String visitId;
//   String userId;
//   String mrn;
//   String transactionId;
//   SaveSoapIPPatientDetials(
//       {this.patientId,
//       this.transactionId,
//       this.userId,
//       this.visitId,
//       this.mrn});
//   factory SaveSoapIPPatientDetials.fromJson(Map<dynamic, dynamic> json) {
//     return SaveSoapIPPatientDetials(
//       transactionId: json['soapnote_id'],
//     );
//   }
//   Map<String, dynamic> toJson() => {
//         'patientId': patientId,
//         'visitId': visitId,
//         'userid': userId,
//         'transationId': transactionId,
//         'mrn': mrn
//       };
// }

class DoctorsSoapTextDetials {
  String componentId;
  String textValue;
  String textId;
  String response;
  DoctorsSoapTextDetials(
      {this.componentId, this.textValue, this.textId, this.response});
  factory DoctorsSoapTextDetials.fromJson(Map<dynamic, dynamic> json) {
    return DoctorsSoapTextDetials(
      componentId: json['component_id'].toString(),
      textValue: json['text_value'].toString(),
      textId: json['text_id'].toString(),
    );
  }
  Map<String, dynamic> toJson() => {
        'component_id': componentId,
        'text_value': textValue,
        'text_id': textId,
      };
}

class DoctorProvisionalDiagnosis {
  String provisional;

  DoctorProvisionalDiagnosis({this.provisional});
  factory DoctorProvisionalDiagnosis.fromJson(Map<dynamic, dynamic> json) {
    return DoctorProvisionalDiagnosis(
      provisional: json['provisional'].toString(),
    );
  }
}

class TransationId {
  String transation;

  TransationId({this.transation});
  factory TransationId.fromJson(Map<dynamic, dynamic> json) {
    return TransationId(
      transation: json['transactionId'].toString(),
    );
  }
}

class IPOPpatientsCount {
  String ipcount;
  String opcount;
  String referal;
  String encountered;
  String appointments;
  String followup;
  String monthToDate;
  String error;
  IPOPpatientsCount(
      {this.ipcount,
      this.opcount,
      this.appointments,
      this.encountered,
      this.followup,
      this.monthToDate,
      this.referal,
      this.error});
  factory IPOPpatientsCount.fromJson(Map<dynamic, dynamic> json) {
    return IPOPpatientsCount(
      ipcount: json['ipcount'].toString(),
      opcount: json['opcount'].toString(),
      appointments: json['appointments'].toString(),
      encountered: json['encountered'].toString(),
      followup: json['followup'].toString(),
      monthToDate: json['monthtodate'].toString(),
      referal: json['referal'].toString(),
      error: json['error'].toString(),
    );
  }
}

class GeneralInfoDetials {
  String generalInfo;
  String infoId;
  String patientid;
  String userid;
  String response;
  GeneralInfoDetials(
      {this.generalInfo,
      this.infoId,
      this.patientid,
      this.userid,
      this.response});
  factory GeneralInfoDetials.fromJson(Map<dynamic, dynamic> json) {
    return GeneralInfoDetials(
      generalInfo: json['general_info'],
      infoId: json['info_id'].toString(),
    );
  }
  Map<String, dynamic> toJson() => {
        'geninfo': generalInfo,
        'geninfoid': infoId,
        'patient_id': patientid,
        'userid': userid
      };
}

class VitalSignsDetials {
  String parameterId;
  String value;
  String remarks;
  String controlType;
  String referenceFrom;
  String referenceto;
  String vitalSignId;
  String response;
  String uom;
  VitalSignsDetials(
      {this.parameterId,
      this.remarks,
      this.value,
      this.response,
      this.controlType,
      this.referenceFrom,
      this.referenceto,
      this.vitalSignId,
      this.uom});
  factory VitalSignsDetials.fromJson(Map<dynamic, dynamic> json) {
    return VitalSignsDetials(
      response: json['vital_sign'],
    );
  }
  Map<String, dynamic> toJson() => {
        'parameterId': parameterId,
        'remarks': remarks,
        'value': value,
        'controlType': controlType,
        'referenceFrom': referenceFrom,
        'referenceto': referenceto,
        'vitalSignId': vitalSignId,
        'uom': uom
      };
}

class AllergyType {
  String id;
  String value;
  AllergyType({this.id, this.value});
}

class AllergyOnset {
  String id;
  String value;
  AllergyOnset({this.id, this.value});
}

class AllergyOnsetType {
  String id;
  String value;
  AllergyOnsetType({this.id, this.value});
}

class AllergyStatus {
  String id;
  String value;
  AllergyStatus({this.id, this.value});
}

class ExistingAllergies {
  String status;
  String allergyCategory;
  String allergicTo;
  String allergyId;
  String visit;
  String patientVisitId;
  ExistingAllergies(
      {this.allergicTo,
      this.allergyCategory,
      this.allergyId,
      this.status,
      this.visit,
      this.patientVisitId});
  factory ExistingAllergies.fromJson(Map<dynamic, dynamic> json) {
    return ExistingAllergies(
      allergicTo: json['Allergic_To'],
      allergyCategory: json['Allery_Category'],
      allergyId: json['Allergy_Id'].toString(),
      status: json['Status'],
      visit: json['visitId'].toString(),
      patientVisitId: json['patient_visitId'],
    );
  }
}

class Response {
  String resp;
  Response({this.resp});
  factory Response.fromJson(Map<dynamic, dynamic> json) {
    return Response(
      resp: json['Drug_Order'],
    );
  }
}

class AllergicdropdownItems {
  String lookupId;
  String lookupValue;
  String allergicToName;
  String allergicToId;
  AllergicdropdownItems(
      {this.allergicToId,
      this.allergicToName,
      this.lookupId,
      this.lookupValue});
  factory AllergicdropdownItems.fromJson(Map<dynamic, dynamic> json) {
    return AllergicdropdownItems(
      lookupId: json['lookup_id'],
      lookupValue: json['lookup_value'],
      allergicToName: json['allergicto_name'].toString(),
      allergicToId: json['allergicto_id'],
    );
  }
  static List<AllergicdropdownItems> fromJsonList(List list) {
    if (list == null) return null;
    return list.map((item) => AllergicdropdownItems.fromJson(item)).toList();
  }
}

class AllergyReactions {
  String lookupid;
  String lookupvalue;
  String lookupcode;
  bool isSelected;
  AllergyReactions(
      {this.lookupid, this.lookupvalue, this.lookupcode, this.isSelected});
  factory AllergyReactions.fromJson(Map<dynamic, dynamic> json) {
    return AllergyReactions(
      lookupid: json['lookup_id'],
      lookupvalue: json['lookup_value'],
      lookupcode: json['lookup_code'].toString(),
    );
  }
  Map<String, dynamic> toJson() => {
        'lookupId': lookupid,
      };
}

class AllergyReactionsTO {
  String allergyTypeId;
  String allergyDesc;
  String remarks;
  String response;
  bool isActive;
  AllergyReactionsTO(
      {this.allergyDesc,
      this.allergyTypeId,
      this.isActive,
      this.remarks,
      this.response});
  factory AllergyReactionsTO.fromJson(Map<dynamic, dynamic> json) {
    return AllergyReactionsTO(
      response: json['allergic_to'],
    );
  }
  Map<String, dynamic> toJson() => {
        'isActive': isActive,
        'remarks': remarks,
        'allergenDesc': allergyDesc,
        'allergicType': allergyTypeId,
      };
}

class AllergyReactionToSaveApi {
  List<AllergyReactionsTO> reaction;
  AllergyReactionToSaveApi([this.reaction]);

  Map toJson() {
    List<Map> reaction = this.reaction != null
        ? this.reaction.map((i) => i.toJson()).toList()
        : null;
    return {'allergic_to': reaction};
  }
}

class SaveAllergyDetialsData {
  String allergyType;
  String allergyTypeId;
  String allergytoValue;
  String allergytoId;
  String allergyStatus;
  String allergyStatusId;
  String onSetValue;
  String onSetId;
  String onSetTypeValue;
  String onSetTypeId;
  String onSetDate;
  bool knownAllergies;
  String remarks;
  SaveAllergyDetialsData(
      {this.allergyStatus,
      this.allergyStatusId,
      this.allergyType,
      this.allergyTypeId,
      this.allergytoId,
      this.allergytoValue,
      this.knownAllergies,
      this.onSetDate,
      this.onSetId,
      this.onSetValue,
      this.remarks,
      this.onSetTypeId,
      this.onSetTypeValue});
  // factory SaveAllergyDetialsData.fromJson(Map<dynamic, dynamic> json) {
  //   return SaveAllergyDetialsData(
  //     allergyStatus: json['general_info'],
  //     allergyType: json['general_info'],
  //     allergyTypeId: json['general_info'],
  //     allergytoId:json['general_info'],
  //     allergytoValue: json['general_info'],
  //     knownAllergies:json['general_info'],
  //     onSetDate: json['general_info'],
  //     onSetId: json['general_info'],
  //     onSetValue:json['general_info'] ,
  //     reactions:json['general_info'] ,
  //     remarks:json['general_info'] ,
  //   );
  // }
  Map<String, dynamic> toJson() => {
        'allergytype': allergyType,
        'allergytypeId': allergyTypeId,
        'allergytovalue': allergytoValue,
        'allergytoId': allergytoId,
        'onsetvalue': onSetValue,
        'onsetId': onSetId,
        'onsettypevalue': onSetTypeValue,
        'onSetTypeId': onSetTypeId,
        'onSetdate': onSetDate,
        'allergyStatus': allergyStatus,
        'allergyStatusId': allergyStatusId,
        'knownAllergies': knownAllergies,
        'remarks': remarks,
      };
}

class AllergyDetialsSaveApi {
  List<SaveAllergyDetialsData> allergyList;
  List<SaveSoapPatientDetials> patientIddetials;
  List<SelectedDepartment> departmentDetials;
  List<AllergyReactions> reaction;
  AllergyDetialsSaveApi(
      [this.allergyList,
      this.patientIddetials,
      this.departmentDetials,
      this.reaction]);

  Map toJson() {
    List<Map> allergyList = this.allergyList != null
        ? this.allergyList.map((i) => i.toJson()).toList()
        : null;
    List<Map> patientIddetials = this.patientIddetials != null
        ? this.patientIddetials.map((i) => i.toJson()).toList()
        : null;
    List<Map> departmentDetials = this.departmentDetials != null
        ? this.departmentDetials.map((i) => i.toJson()).toList()
        : null;
    List<Map> reaction = this.reaction != null
        ? this.reaction.map((i) => i.toJson()).toList()
        : null;
    return {
      'allergy_details': allergyList,
      'Patient_detials': patientIddetials,
      'department_detials': departmentDetials,
      'allergy_reaction': reaction
    };
  }
}

class AdmissonRequestSave {
  List<SaveAdmissionRequest> admissonreq;
  List<SaveSoapPatientDetials> patientIddetials;
  List<SelectedDepartment> departmentDetials;
  AdmissonRequestSave([
    this.admissonreq,
    this.patientIddetials,
    this.departmentDetials,
  ]);

  Map toJson() {
    List<Map> admissonreq = this.admissonreq != null
        ? this.admissonreq.map((i) => i.toJson()).toList()
        : null;
    List<Map> patientIddetials = this.patientIddetials != null
        ? this.patientIddetials.map((i) => i.toJson()).toList()
        : null;
    List<Map> departmentDetials = this.departmentDetials != null
        ? this.departmentDetials.map((i) => i.toJson()).toList()
        : null;
    return {
      'admissonreq': admissonreq,
      'Patient_detials': patientIddetials,
      'department_detials': departmentDetials,
    };
  }
}

class PreviousDiagnosis {
  String provisionalDiagnosis;
  String finalDiagnosis;
  String visitId;
  String response;
  PreviousDiagnosis(
      {this.provisionalDiagnosis,
      this.finalDiagnosis,
      this.response,
      this.visitId});
  factory PreviousDiagnosis.fromJson(Map<dynamic, dynamic> json) {
    return PreviousDiagnosis(
      provisionalDiagnosis: json['Provisional_Diagnosis'],
      finalDiagnosis: json['Final_Diagnosis'],
      response: json['diagnosis'],
    );
  }
  Map<String, dynamic> toJson() => {
        'provisional': provisionalDiagnosis,
        'finalDiagnosis': finalDiagnosis,
        'visitId': visitId
      };
}

class DiagnosisSaveApi {
  List<PreviousDiagnosis> diagnosis;
  DiagnosisSaveApi([this.diagnosis]);

  Map toJson() {
    List<Map> diagnosis = this.diagnosis != null
        ? this.diagnosis.map((i) => i.toJson()).toList()
        : null;
    return {'diagnosis': diagnosis};
  }
}

class LoadVitalSigns {
  String parameterid;
  String parametercode;
  String parametername;
  String visittype;
  String datatype;
  String uom;
  String unit;
  String refrangefrom;
  String refrangeto;
  LoadVitalSigns(
      {this.parameterid,
      this.parametercode,
      this.visittype,
      this.datatype,
      this.uom,
      this.unit,
      this.refrangefrom,
      this.refrangeto,
      this.parametername});
  factory LoadVitalSigns.fromJson(Map<dynamic, dynamic> json) {
    return LoadVitalSigns(
      parameterid: json['parameterid'],
      parametercode: json['parametercode'],
      visittype: json['visittype'],
      datatype: json['datatype'],
      uom: json['uom'],
      unit: json['unit'],
      refrangefrom: json['refrangefrom'],
      refrangeto: json['refrangeto'],
      parametername: json['parametername'],
    );
  }
  // Map<String, dynamic> toJson() => {
  //       'provisional_Diagnosis': provisionalDiagnosis,
  //       'final_Diagnosis': finalDiagnosis,
  //     };
}

class DoctorAdmissionBedType {
  String bedId;
  String bedType;
  DoctorAdmissionBedType({this.bedId, this.bedType});
  factory DoctorAdmissionBedType.fromJson(Map<dynamic, dynamic> json) {
    return DoctorAdmissionBedType(
      bedId: json['bed_id'],
      bedType: json['bed_type'],
    );
  }
  Map<String, dynamic> toJson() => {
        'bed_id': bedId,
        'bed_type': bedType,
      };
}

class DoctorAdmissionDepartmentlist {
  String deptId;
  String deptName;
  DoctorAdmissionDepartmentlist({this.deptId, this.deptName});
  factory DoctorAdmissionDepartmentlist.fromJson(Map<dynamic, dynamic> json) {
    return DoctorAdmissionDepartmentlist(
      deptId: json['dep_id'],
      deptName: json['dep_name'],
    );
  }
  Map<String, dynamic> toJson() => {
        'dep_id': deptId,
        'dep_name': deptName,
      };
}

class DoctorAdmissionPhonenumber {
  String phonenumber;
  DoctorAdmissionPhonenumber({this.phonenumber});
  factory DoctorAdmissionPhonenumber.fromJson(Map<dynamic, dynamic> json) {
    return DoctorAdmissionPhonenumber(
      phonenumber: json['phoneNo'],
    );
  }
  Map<String, dynamic> toJson() => {
        'phone_no': phonenumber,
      };
}

class AdmissionRequestDoctorsList {
  String doctId;
  String doctName;
  AdmissionRequestDoctorsList({this.doctId, this.doctName});
  factory AdmissionRequestDoctorsList.fromJson(Map<dynamic, dynamic> json) {
    return AdmissionRequestDoctorsList(
      doctId: json['doctors_id'],
      doctName: json['doctors_name'],
    );
  }
  Map<String, dynamic> toJson() => {
        'doctors_id': doctId,
        'doctors_name': doctName,
      };
}

class DoctorLabResults {
  String labServiceName;
  String labResultValue;
  String unit;
  String certifiedDate;
  String description;
  String valuetype;
  String visit;
  String orderDate;
  String resultStatus;
  String specimen;
  String remarks;
  String orderRemarks;
  String labResultId;
  String sampleid;
  String resultOutcome;
  String referenceRange;
  String orderId;
  DoctorLabResults(
      {this.labServiceName,
      this.labResultValue,
      this.certifiedDate,
      this.description,
      this.orderDate,
      this.orderRemarks,
      this.remarks,
      this.resultStatus,
      this.specimen,
      this.unit,
      this.valuetype,
      this.visit,
      this.sampleid,
      this.labResultId,
      this.resultOutcome,
      this.referenceRange,
      this.orderId});
  factory DoctorLabResults.fromJson(Map<dynamic, dynamic> json) {
    return DoctorLabResults(
      labServiceName: json['lab_service_name'],
      labResultValue: json['lab_result_value'],
      certifiedDate: json['certified_date'],
      description: json['description'],
      orderDate: json['ordered_date'],
      orderRemarks: json['order_remarks'],
      remarks: json['remarks'],
      resultStatus: json['result_status'],
      specimen: json['specimen'],
      unit: json['unit'],
      valuetype: json['valuetype'],
      visit: json['visit'],
      labResultId: json['lab_result_id'],
      sampleid: json['sample_id'],
      resultOutcome: json['result_outcome'],
      referenceRange: json['reference_range'],
      orderId: json['orderId'],
    );
  }
  Map<String, dynamic> toJson() => {
        'lab_service_name': labServiceName,
        'lab_result_value': labResultValue,
        'unit': unit,
        'certified_date': certifiedDate,
        'description': description,
        'valuetype': valuetype,
        'visit': visit,
        'ordered_date': orderDate,
        'result_status': resultStatus,
        'specimen': specimen,
        'remarks': remarks,
        'order_remarks': orderRemarks,
      };
}

class DoctorLabResultsParameters {
  final String labresultId;
  final String parameter;
  final String value;
  final String unit;
  final String reference;
  final String reportfalg;
  DoctorLabResultsParameters(
      {this.labresultId,
      this.parameter,
      this.value,
      this.unit,
      this.reference,
      this.reportfalg});
  factory DoctorLabResultsParameters.fromJson(Map<dynamic, dynamic> json) {
    return DoctorLabResultsParameters(
      labresultId: json['lab_result_id'],
      parameter: json['parameter'],
      unit: json['unit'],
      value: json['value'],
      reference: json['reference'],
      reportfalg: json['report_flag'],
    );
  }
}

class DoctorLabResultTemplate {
  final String templateId;
  final String templateResult;
  final String labResultId;
  final String createdBy;
  DoctorLabResultTemplate({
    this.templateId,
    this.templateResult,
    this.labResultId,
    this.createdBy,
  });
  factory DoctorLabResultTemplate.fromJson(Map<dynamic, dynamic> json) {
    return DoctorLabResultTemplate(
      templateId: json['lab_resultTemplateId'].toString(),
      templateResult: json['templateResult'].toString(),
      labResultId: json['lab_resultId'].toString(),
      createdBy: json['createdBy'].toString(),
    );
  }
}

class SaveAdmissionRequest {
  String deptId;
  String consultantId;
  String date;
  String selectedbedTypeId;
  String lengthOfStay;
  String attachBed;
  String mobileNo;
  String email;
  String attachBedId;
  SaveAdmissionRequest(
      {this.attachBed,
      this.consultantId,
      this.date,
      this.deptId,
      this.email,
      this.lengthOfStay,
      this.mobileNo,
      this.selectedbedTypeId,
      this.attachBedId});
  Map<String, dynamic> toJson() => {
        'attachBed': attachBed,
        'consultantId': consultantId,
        'date': date,
        'deptId': deptId,
        'email': email,
        'lengthOfStay': lengthOfStay,
        'mobileNo': mobileNo,
        'selectedbedTypeId': selectedbedTypeId,
        'attachBedId': attachBedId
      };
}

class MealsListData {
  MealsListData({
    this.imagePath = '',
    this.titleTxt = '',
    this.startColor = '',
    this.endColor = '',
    this.meals,
    this.kacl = 0,
  });

  String imagePath;
  String titleTxt;
  String startColor;
  String endColor;
  List<String> meals;
  int kacl;

  static List<MealsListData> tabIconsList = <MealsListData>[
    MealsListData(
      imagePath: 'assets/fitness_app/breakfast.png',
      titleTxt: 'Breakfast',
      kacl: 525,
      meals: <String>['Bread,', 'Peanut butter,', 'Apple'],
      startColor: '#FA7D82',
      endColor: '#FFB295',
    ),
    MealsListData(
      imagePath: 'assets/fitness_app/lunch.png',
      titleTxt: 'Lunch',
      kacl: 602,
      meals: <String>['Salmon,', 'Mixed veggies,', 'Avocado'],
      startColor: '#738AE6',
      endColor: '#5C5EDD',
    ),
    MealsListData(
      imagePath: 'assets/fitness_app/snack.png',
      titleTxt: 'Snack',
      kacl: 0,
      meals: <String>['Recommend:', '800 kcal'],
      startColor: '#FE95B6',
      endColor: '#FF5287',
    ),
    MealsListData(
      imagePath: 'assets/fitness_app/dinner.png',
      titleTxt: 'Dinner',
      kacl: 0,
      meals: <String>['Recommend:', '703 kcal'],
      startColor: '#6F72CA',
      endColor: '#1E1466',
    ),
  ];
}
