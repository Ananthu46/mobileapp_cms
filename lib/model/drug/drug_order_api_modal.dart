import 'dart:async';
import 'package:doctor_portal_cms/model/doctor/doctor_order_modal.dart';
import 'package:flutter/cupertino.dart';

class Debouncer {
  final int milliseconds;
  VoidCallback action;
  Timer _timer;

  Debouncer({this.milliseconds});

  run(VoidCallback action) {
    if (null != _timer) {
      _timer.cancel();
    }
    _timer = Timer(Duration(milliseconds: milliseconds), action);
  }
}

class IpPatientDetials {
  String billingtype;
  String roomno;
  String ward;
  String patientname;
  String age;
  String gender;
  String admissionno;
  String mrn;
  String admittingdoctor;
  String patientid;
  String admissiondate;
  String visitid;

  IpPatientDetials(
      {this.patientname,
      this.admissiondate,
      this.admissionno,
      this.admittingdoctor,
      this.age,
      this.billingtype,
      this.gender,
      this.mrn,
      this.patientid,
      this.roomno,
      this.ward,
      this.visitid});

  factory IpPatientDetials.fromJson(Map<dynamic, dynamic> json) {
    return IpPatientDetials(
      billingtype: json['billing_type'],
      roomno: json['room_no'],
      ward: json['ward'],
      patientname: json['patient_name'],
      age: json['age'],
      gender: json['gender'],
      admissionno: json['admission_no'],
      mrn: json['mrn'],
      admittingdoctor: json['Admitting_Doctor'],
      admissiondate: json['admission_date'],
      patientid: json['patient_id'],
      visitid: json['visit_id'],
    );
  }
}

class DrugServiceOrderModal {
  String serviceId;
  String serviceName;
  String serviceCode;
  int quantity;
  DrugServiceOrderModal({
    @required this.serviceId,
    @required this.serviceName,
    @required this.serviceCode,
    int quantity,
  });
  factory DrugServiceOrderModal.fromJson(Map<dynamic, dynamic> json) {
    return DrugServiceOrderModal(
      serviceId: json['service_id'],
      serviceName: json['service_name'],
      serviceCode: json['service_code'],
      quantity: 0,
    );
  }
}

class ServiceModalCount {
  String serviceCount;
  String serviceName;
  ServiceModalCount({this.serviceCount, this.serviceName});
}

class PharmacyStores {
  String storeId;
  String storeName;
  PharmacyStores({
    @required this.storeId,
    @required this.storeName,
  });
  factory PharmacyStores.fromJson(Map<dynamic, dynamic> json) {
    return PharmacyStores(
      storeId: json['store_id'],
      storeName: json['store_name'],
    );
  }
}

class PharmacyMedicineFrequency {
  String frequencyName;
  double frequencyValue;
  int frequencyId;
  double frequencyType;
  PharmacyMedicineFrequency({
    this.frequencyName,
    this.frequencyValue,
    this.frequencyId,
    this.frequencyType,
  });
  factory PharmacyMedicineFrequency.fromJson(Map<dynamic, dynamic> json) {
    return PharmacyMedicineFrequency(
      frequencyName: json['frequency_name'],
      frequencyValue: double.parse(json['frequency_value'].toString()),
      frequencyId: int.parse(json['frequency_id'].toString()),
      frequencyType: double.parse(json['frequency_type'].toString()),
    );
  }
}

class PharmacyMedicinesModal {
  String categoryName;
  String genericId;
  String itemname;
  String itemId;
  String categoryId;
  String genericName;
  String frequencyName;

  PharmacyMedicinesModal({
    this.categoryId,
    this.categoryName,
    this.genericId,
    this.genericName,
    this.itemId,
    this.itemname,
    this.frequencyName,
  });
  factory PharmacyMedicinesModal.fromJson(Map<dynamic, dynamic> json) {
    return PharmacyMedicinesModal(
      categoryId: json['category_id'],
      categoryName: json['category_name'],
      genericId: json['generic_id'],
      genericName: json['generic_name'],
      itemId: json['item_id'],
      itemname: json['item_name'],
    );
  }
}

class DrugPharmacyDetialsList {
  final String storeId;
  final String itemid;
  final String drugDetialId;
  final String genericId;
  final String categoryId;
  final String pharmacyname;
  final String frequency;
  final int frequencyId;
  int noOfdays;
  double totalQty;
  DrugPharmacyDetialsList(
      {@required this.storeId,
      this.itemid,
      this.drugDetialId,
      this.frequencyId,
      this.genericId,
      this.categoryId,
      @required this.pharmacyname,
      @required this.frequency,
      @required this.noOfdays,
      @required this.totalQty});
  Map<String, dynamic> toJson() => {
        'storeId': storeId,
        'item_id': itemid,
        'drug_detial_id': drugDetialId,
        'genericId': genericId,
        'categoryId': categoryId,
        'name': pharmacyname,
        'frequency': frequency,
        'frequencyId': frequencyId,
        'no_of_days': noOfdays,
        'totalQty': totalQty
      };
}

class PreviousPharmacyOrderModal {
  String drugorderid;
  String storeid;
  String drugdetailId;
  double totalQty;
  String frequencyName;
  String itemname;
  String itemId;
  String strength;
  String visitId;
  double quantity;
  double noOfDays;
  int frequencyId;
  String genericId;
  String categoryId;
  bool renewOrder;
  bool itemAdded;
  PharmacyMedicinesModal pharmacy; //for repeat order

  PreviousPharmacyOrderModal(
      {this.storeid,
      this.drugdetailId,
      this.totalQty,
      this.frequencyName,
      this.itemname,
      this.itemId,
      this.strength,
      this.visitId,
      this.quantity,
      this.noOfDays,
      this.frequencyId,
      this.categoryId,
      this.genericId,
      this.drugorderid,
      this.renewOrder,
      this.pharmacy,
      this.itemAdded});
  factory PreviousPharmacyOrderModal.fromJson(Map<dynamic, dynamic> json) {
    return PreviousPharmacyOrderModal(
      drugorderid: json['drugorderid'].toString(),
      drugdetailId: json['drugdetail_id'].toString(),
      frequencyId: int.parse(json['frequency_id']),
      frequencyName: json['freqency_name'].toString(),
      itemId: json['item_id'].toString(),
      itemname: json['item_name'].toString(),
      noOfDays: double.parse(json['no_days']),
      quantity: double.parse(json['quantity']),
      strength: json['strength'].toString(),
      totalQty: double.parse(json['total_qty']),
      visitId: json['visit_id'].toString(),
    );
  }
  Map<String, dynamic> toJson() => {
        'storeid': storeid,
        'drugdetailId': drugdetailId,
        'totalQty': totalQty,
        'frequencyName': frequencyName,
        'itemname': itemname,
        'itemId': itemId,
        'strength': strength,
        'frequencyId': frequencyId,
        'visitId': visitId,
        'quantity': quantity,
        'noOfDays': noOfDays,
        'genericId': genericId,
        'categoryId': categoryId
      };
}

class PharmacydetialsApi {
  List<PreviousPharmacyOrderModal> pharmacylist;
  List<SelectedDepartment> departmentData;
  List<SaveSoapPatientDetials> patientIddetials;
  List<DrugServicesDetialsList> serviceList;

  PharmacydetialsApi(
      [this.pharmacylist,
      this.departmentData,
      this.patientIddetials,
      this.serviceList]);

  Map toJson() {
    List<Map> pharmacylist = this.pharmacylist != null
        ? this.pharmacylist.map((i) => i.toJson()).toList()
        : null;
    List<Map> departmentData = this.departmentData != null
        ? this.departmentData.map((i) => i.toJson()).toList()
        : null;
    List<Map> patientIddetials = this.patientIddetials != null
        ? this.patientIddetials.map((i) => i.toJson()).toList()
        : null;
    List<Map> serviceList = this.serviceList != null
        ? this.serviceList.map((i) => i.toJson()).toList()
        : null;
    return {
      'Pharmacy_Detials': pharmacylist,
      'Department_data': departmentData,
      'Patient_detials': patientIddetials,
      'Services_Detial': serviceList,
    };
  }
}

class ServicesDetialsApi {
  List<DrugServicesDetialsList> serviceList;
  List<SelectedDepartment> departmentData;
  List<SaveSoapPatientDetials> patientIddetials;
  List<PreviousPharmacyOrderModal> pharmacylist;
  ServicesDetialsApi(
      [this.serviceList,
      this.departmentData,
      this.patientIddetials,
      this.pharmacylist]);

  Map toJson() {
    List<Map> serviceList = this.serviceList != null
        ? this.serviceList.map((i) => i.toJson()).toList()
        : null;
    List<Map> departmentData = this.departmentData != null
        ? this.departmentData.map((i) => i.toJson()).toList()
        : null;
    List<Map> patientIddetials = this.patientIddetials != null
        ? this.patientIddetials.map((i) => i.toJson()).toList()
        : null;
    List<Map> pharmacylist = this.pharmacylist != null
        ? this.pharmacylist.map((i) => i.toJson()).toList()
        : null;
    return {
      'Services_Detial': serviceList,
      'Department_data': departmentData,
      'Patient_detials': patientIddetials,
      'Pharmacy_Detials': pharmacylist,
    };
  }
}

class DrugServicesDetialsList {
  final String serviceId;
  final String serviceCode;
  final String serviceName;
  final int quantity;

  DrugServicesDetialsList({
    this.serviceId,
    this.serviceCode,
    @required this.serviceName,
    @required this.quantity,
  });
  Map<String, dynamic> toJson() => {
        'service_id': serviceId,
        'service_code': serviceCode,
        'servicename': serviceName,
        'quantity': quantity,
      };
}

class SelectedPatientdetials {
  final String patientId;
  final String visitId;

  SelectedPatientdetials({
    @required this.patientId,
    this.visitId, //set required
  });
  Map<String, dynamic> toJson() => {
        'patientId': patientId,
        'visitId': visitId,
      };
}

class SelectedDepartment {
  final String selectedBranch;
  final String selectedBranchId;
  final String selectedDepartment;
  final String selectedDepartmentId;
  final String selectedlocation;
  final String selectedlocationId;
  final String consultantId;
  SelectedDepartment(
      {this.selectedBranch,
      this.selectedBranchId,
      this.selectedDepartment,
      this.selectedDepartmentId,
      this.selectedlocation,
      this.selectedlocationId,
      this.consultantId});
  Map<String, dynamic> toJson() => {
        'selectedBranch': selectedBranch,
        'selectedbranchId': selectedBranchId,
        'selectdepartment': selectedDepartment,
        'selectDepartmentId': selectedDepartmentId,
        'selectlocation': selectedlocation,
        'selectLocationId': selectedlocationId,
        'consultantId': consultantId
      };
}

class PharmacyMapItems {
  String pharmacyName;
}
