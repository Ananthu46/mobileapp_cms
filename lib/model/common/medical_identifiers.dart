class AgeUnitYears {
  String ageunit = '1';
  String ageValue = 'Years';
}

class AgeUnitMonths {
  String ageunit = '2';
  String ageValue = 'Months';
}

class AgeUnitDays {
  String ageunit = '3';
  String ageValue = 'Days';
}
