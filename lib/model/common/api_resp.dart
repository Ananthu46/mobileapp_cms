import 'package:flutter/widgets.dart';

class ApiResponse extends ChangeNotifier{
  final bool status;
  final String msg;
  final dynamic response;
  ApiResponse(this.status, {this.msg = "success", this.response});
}