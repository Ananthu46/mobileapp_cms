import 'package:json_annotation/json_annotation.dart';

part 'config.g.dart';

@JsonSerializable(createToJson: false)
class Config {
  final String env;
  final bool production;
  final String apiKey;
  final String baseApi;
  final bool allowRootedDevices;

  Config({this.env, this.production, this.apiKey,this.baseApi,this.allowRootedDevices});

  factory Config.fromJson(Map<String, dynamic> json) => _$ConfigFromJson(json);
}