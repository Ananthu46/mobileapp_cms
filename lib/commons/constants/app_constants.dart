
class AppConstants {
  AppConstants._();

  // receiveTimeout
  static const int receiveTimeout = 5000;

  // connectTimeout
  static const int connectionTimeout = 3000;

  // booking endpoints
  static const String AUTH_REGISTER_API = '';
  static const String AUTH_VALIDATE_OTP = '';
  static const String GET_STATIC_FORM_DATA = '';
  static const String FORM_SUBMIT_API = '';

  //SHARED PREFERENCE DATA
  static const String AUTH_TOKEN = "AUTH_TOKEN";

  //AUDIENCE TYPE
  static const int AUDIENCE_TYPE_HOSPITAL = 1;
  static const int AUDIENCE_TYPE_PHARMACY = 2;

  //PASS CODE TYPE
  static const int PASS_CODE_TYPE_ACCESS = 201;
  static const int PASS_CODE_TYPE_FORGET_PASSWORD = 202;
  static const int PASS_CODE_TYPE_PASSWORD = 203;

  //LOGIN TYPE
  static const int LOGIN_TYPE_GUEST = 0;
  static const int LOGIN_TYPE_MOBILE = 1;
  static const int LOGIN_TYPE_EMAIL = 2;
  static const int LOGIN_TYPE_FACEBOOK = 3;
  static const int LOGIN_TYPE_GOOGLE = 4;

  //DEVICE TYPE

  static const int DEVICE_TYPE_WEB = 1;
  static const int DEVICE_TYPE_ANDROID = 2;
  static const int DEVICE_TYPE_IOS = 3;
}