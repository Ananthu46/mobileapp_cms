import 'package:doctor_portal_cms/model/common/api_resp.dart';
import 'package:dio/dio.dart';
import '../env/config.dart';
import '../network/interceptor/LoggingInterceptor.dart';

class RestClient {
  static Config _config;
  RestClient(Config config) {
    _config = config;
  }

  static Dio _dio;

  static Dio getDio() {
    if (_dio == null || _dio.options.baseUrl.isEmpty) {
      BaseOptions options = new BaseOptions(
          baseUrl: _config.baseApi,
          connectTimeout: 30000,
          receiveTimeout: 30000);
      _dio = new Dio(options)
        ..interceptors.add(LoggingInterceptor(requestBody: true));
      return _dio;
    } else {
      return _dio;
    }
  }

  Future<ApiResponse> getApiResponse(String endPoint,
      {Map<String, dynamic> headers,
      Map<String, dynamic> queryParameters}) async {
    try {
      Response response = await getDio().get(endPoint,
          options: Options(headers: headers), queryParameters: queryParameters);
      return ApiResponse(true, response: response);
    } catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
      return ApiResponse(false, msg: "$error");
    }
  }

  Future<ApiResponse> getPostApiResponse(String endPoint,
      {Map<String, dynamic> headers, Map<String, dynamic> data}) async {
    try {
      var response = await getDio()
          .post(endPoint, options: Options(headers: headers), data: data);
      return ApiResponse(true, response: response, msg: "success");
    } catch (error, stacktrace) {
      try {
        return ApiResponse(false,
            msg: "Sorry Something went wrong" + error.toString());
      } catch (e) {
        return ApiResponse(false,
            msg: "Sorry Something went wrong." + error.toString());
      }
    }
  }
}

/*class ApiResponse {
  final bool status;
  final String msg;
  final Response response;
  ApiResponse(this.status, {this.msg = "success", this.response});
}*/
