import 'package:doctor_portal_cms/app/size_config.dart';
import 'package:flutter/material.dart';

class GridItemContainerWidget extends StatelessWidget {
  final String caption;
  final IconData icon;
  final Function onTap;
  final Color color;
  GridItemContainerWidget({this.caption, this.icon, this.onTap, this.color});
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return new GestureDetector(
      onTap: onTap,
      child: Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
              color: Colors.grey,
              blurRadius: 1.0,
              spreadRadius: 1.0,
              offset: Offset(
                1.0,
                1.0,
              ),
            )
          ], color: Colors.white, borderRadius: BorderRadius.circular(1)),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  icon,
                  size: 60 * SizeConfig.getScaleFactor(),
                  color: color,
                ),
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Text(
                    caption,
                    textAlign: TextAlign.center,
                  ),
                )
              ],
            ),
          )),
    );
  }
}
