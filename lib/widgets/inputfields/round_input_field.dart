import 'package:doctor_portal_cms/app/app_theme.dart';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:doctor_portal_cms/app/size_config.dart';

class RoundedCornerInput extends StatefulWidget {
  final String hintText;
  final String defaultText;
  final bool isSecureInput;
  final int textLength;
  final TextEditingController textController;
  final IconData prefix_icon;
  final TextInputType keyboard_type;
  final Function validator;
  final bool clearState;
  RoundedCornerInput(
      {this.hintText,
      this.isSecureInput,
      this.textController,
      this.defaultText,
      this.prefix_icon,
      this.keyboard_type = TextInputType.text,
      this.textLength = 0,
      this.validator,
      this.clearState = false});

  @override
  _RoundedCornerInputState createState() => _RoundedCornerInputState();
}

class _RoundedCornerInputState extends State<RoundedCornerInput> {
  bool _isEncrypted = false;
  String _errorTxt = null;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _isEncrypted = this.widget.isSecureInput;
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is removed from the
    // widget tree.
    if (widget.clearState) {
      widget.textController.clear();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    void _toggle() {
      setState(() {
        _isEncrypted = !_isEncrypted;
      });
    }

    return TextField(
      obscureText: _isEncrypted,
      controller: widget.textController,
      keyboardType: widget.keyboard_type,
      cursorColor: Colors.greenAccent,
      inputFormatters: widget.textLength > 0
          ? [
              new LengthLimitingTextInputFormatter(widget.textLength),
            ]
          : null,
      onChanged: (value) {
        //widget.validator!=null?
        setState(() {
          print('value::::' + value);
          _errorTxt = widget.validator(value);
          print('_errorTxt::::' + _errorTxt.toString());
        });
      },
      decoration: InputDecoration(
        contentPadding: EdgeInsets.only(
            top: SizeConfig.getScaleFactor() * 15,
            bottom: SizeConfig.getScaleFactor() * 15,
            left: SizeConfig.getScaleFactor() * 20,
            right: SizeConfig.getScaleFactor() * 20),
        labelStyle: TextStyle(color: AppTheme.nearlyBlack),
        labelText: this.widget.hintText,
        errorStyle: TextStyle(color: AppTheme.redAppTheme),
        errorText: _errorTxt,
        filled: true,
        fillColor: Color(0xFFFBFAFF),
        prefixIcon: widget.prefix_icon != null
            ? IconButton(
                padding: EdgeInsets.only(left: 5, right: 5),
                color: AppTheme.grey,
                icon: Icon(
                  widget.prefix_icon,
                  size: 20,
                ),
                onPressed: () {})
            : null,
        suffixIcon: this.widget.isSecureInput
            ? IconButton(
                onPressed: _toggle,
                icon: _isEncrypted
                    ? Icon(
                        Icons.lock,
                        color: Colors.grey.shade700,
                      )
                    : Icon(
                        Icons.no_encryption,
                        color: Colors.grey.shade700,
                      ),
                iconSize: 20,
              )
            : null,
        hintStyle: TextStyle(color: Theme.of(context).textTheme.caption.color),
        border: OutlineInputBorder(
          borderSide: BorderSide(
              color: Theme.of(context).colorScheme.primary, width: 1),
          borderRadius: BorderRadius.circular(10),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(
              color: Theme.of(context).colorScheme.primary, width: 1),
          borderRadius: BorderRadius.circular(10),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(
              color: Theme.of(context).colorScheme.primary, width: 1),
          borderRadius: BorderRadius.circular(10),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderSide:
              BorderSide(color: Theme.of(context).colorScheme.error, width: 1),
          borderRadius: BorderRadius.circular(10),
        ),
        errorBorder: OutlineInputBorder(
          borderSide:
              BorderSide(color: Theme.of(context).colorScheme.error, width: 1),
          borderRadius: BorderRadius.circular(10),
        ),
      ),
      style: TextStyle(
          fontSize: SizeConfig.getScaleFactor() * 18,
          color: AppTheme.nearlyBlack),
    );
  }
}
