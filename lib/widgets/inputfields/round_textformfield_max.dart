import 'package:flutter/material.dart';

//** @author Ananthu */
//**obscuretext must be given */
class RoundTextFormFieldMaxLineWidget extends StatefulWidget {
  final EdgeInsets scrollpadding;
  final String labelText;
  final String hinttext;
  final TextInputType keyBoardType;
  final Function onSaved;
  final Widget prefixIcon;
  final Widget suffixIcon;
  final String prefixText;
  final TextEditingController controller;
  final bool isSecureInput;
  final Function onEditingComplete;
  final Function onChanged;
  final Function validator;
  final FocusNode focusNode;
  final FocusNode focusScope;
  RoundTextFormFieldMaxLineWidget(
      {this.validator,
      this.onSaved,
      this.labelText,
      this.hinttext,
      this.isSecureInput,
      this.controller,
      this.focusNode,
      this.prefixIcon,
      this.suffixIcon,
      this.prefixText,
      this.focusScope,
      this.keyBoardType,
      this.onEditingComplete,
      this.onChanged,
      this.scrollpadding});

  @override
  _RoundTextFormFieldMaxLineWidgetState createState() =>
      _RoundTextFormFieldMaxLineWidgetState();
}

class _RoundTextFormFieldMaxLineWidgetState
    extends State<RoundTextFormFieldMaxLineWidget> {
  bool _obscureText = false;
  @override
  void initState() {
    super.initState();
    _obscureText = this.widget.isSecureInput;
  }

  @override
  Widget build(BuildContext context) {
    void _passwordVisible() {
      setState(() {
        _obscureText = !_obscureText;
      });
    }

    return
        //  Material(
        //   elevation: 4,
        //   shadowColor: Theme.of(context).primaryColor,
        //   clipBehavior: Clip.antiAlias,
        //   borderRadius: const BorderRadius.all(
        //     const Radius.circular(10.0),
        //   ),
        //   child:
        TextFormField(
      scrollPadding: widget.scrollpadding,
      controller: widget.controller,
      decoration: InputDecoration(
          contentPadding: const EdgeInsets.symmetric(horizontal: 15),
          focusedBorder: OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(5.0),
            ),
            borderSide:
                BorderSide(color: Theme.of(context).primaryColor, width: 2),
          ),
          border: OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(5.0),
            ),
            borderSide: BorderSide.none,
          ),
          disabledBorder: OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(5.0),
            ),
            borderSide:
                BorderSide(color: Theme.of(context).primaryColor, width: 2),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(5.0),
            ),
            borderSide:
                BorderSide(color: Theme.of(context).primaryColor, width: 2),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(5.0),
            ),
            borderSide:
                BorderSide(color: Theme.of(context).primaryColor, width: 2),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(5.0),
            ),
            borderSide:
                BorderSide(color: Theme.of(context).primaryColor, width: 2),
          ),
          errorStyle: TextStyle(color: Colors.red),
          // suffixIcon: widget.suffixIcon,
          suffixIcon: this.widget.isSecureInput
              ? IconButton(
                  onPressed: _passwordVisible,
                  icon: _obscureText
                      ? Icon(
                          Icons.visibility,
                          color: Colors.grey.shade700,
                        )
                      : Icon(
                          Icons.visibility_off_sharp,
                          color: Colors.grey.shade700,
                        ),
                  iconSize: 20,
                )
              : null,
          prefixIcon: widget.prefixIcon,
          prefixText: widget.prefixText,
          filled: true,
          hintText: widget.hinttext,
          labelText: widget.labelText,
          fillColor: Colors.white),
      keyboardType: widget.keyBoardType,
      textAlignVertical: TextAlignVertical.center,
      obscureText: _obscureText,
      onSaved: widget.onSaved,
      focusNode: widget.focusNode,
      onEditingComplete: widget.onEditingComplete,
      onFieldSubmitted: (value) {
        FocusScope.of(context).requestFocus(widget.focusScope);
      },
      onChanged: widget.onChanged,
      enableSuggestions: true,
      validator: widget.validator,
      maxLines: 5,
      minLines: 1,
      // ),
    );
  }
}
