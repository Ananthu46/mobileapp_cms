import 'package:flutter/material.dart';

class SquareTextFieldLarge extends StatelessWidget {
  final String text;
  final TextInputType keyboardType;
  final Function onchanged;
  SquareTextFieldLarge({this.text, this.keyboardType, this.onchanged});

  @override
  Widget build(BuildContext context) {
    return new TextField(
      textAlign: TextAlign.center,
      textAlignVertical: TextAlignVertical.center,
      keyboardType: keyboardType,
      controller: TextEditingController(text: text),
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(10),
        enabledBorder: OutlineInputBorder(
          borderSide:
              BorderSide(color: Theme.of(context).primaryColor, width: 2),
        ),
        errorBorder: OutlineInputBorder(
          borderSide:
              BorderSide(color: Theme.of(context).primaryColor, width: 2),
        ),
        disabledBorder: OutlineInputBorder(
          borderSide:
              BorderSide(color: Theme.of(context).primaryColor, width: 2),
        ),
        // contentPadding: EdgeInsets.only(left: 5),
        focusedBorder: OutlineInputBorder(
          borderSide:
              BorderSide(color: Theme.of(context).primaryColor, width: 2),
        ),
        filled: true,
        fillColor: Colors.black12,
      ),
      onChanged: (onchanged),
    );
  }
}
