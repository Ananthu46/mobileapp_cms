import 'package:flutter/material.dart';

class RoundTextFormFieldSearch extends StatefulWidget {
  final EdgeInsets scrollpadding;
  final String labelText;
  final String hinttext;
  final TextInputType keyBoardType;
  final Function onSaved;
  final Widget prefixIcon;
  final Widget suffixIcon;
  final String prefixText;
  final TextEditingController controller;
  final bool isSecureInput;
  final Function onEditingComplete;
  final Function onChanged;
  final Function validator;
  final FocusNode focusNode;
  final FocusNode focusScope;
  RoundTextFormFieldSearch(
      {this.validator,
      this.onSaved,
      this.labelText,
      this.hinttext,
      this.isSecureInput,
      this.controller,
      this.focusNode,
      this.prefixIcon,
      this.suffixIcon,
      this.prefixText,
      this.focusScope,
      this.keyBoardType,
      this.onEditingComplete,
      this.onChanged,
      this.scrollpadding});

  @override
  _RoundTextFormFieldSearchState createState() =>
      _RoundTextFormFieldSearchState();
}

class _RoundTextFormFieldSearchState extends State<RoundTextFormFieldSearch> {
  bool _obscureText = false;
  @override
  void initState() {
    super.initState();
    _obscureText = this.widget.isSecureInput;
  }

  @override
  Widget build(BuildContext context) {
    void _passwordVisible() {
      setState(() {
        _obscureText = !_obscureText;
      });
    }

    return
        //  Material(
        //   elevation: 4,
        //   shadowColor: Theme.of(context).primaryColor,
        //   clipBehavior: Clip.antiAlias,
        //   borderRadius: const BorderRadius.all(
        //     const Radius.circular(10.0),
        //   ),
        //   child:
        TextFormField(
      style: TextStyle(
          color: Colors.black.withOpacity(0.7), fontWeight: FontWeight.w200),
      scrollPadding: widget.scrollpadding,
      controller: widget.controller,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.only(left: 30),
          focusedBorder: OutlineInputBorder(
              borderRadius: const BorderRadius.all(
                const Radius.circular(10.0),
              ),
              borderSide: BorderSide.none

              // BorderSide(color: Theme.of(context).primaryColor, width: 2),
              ),
          border: OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(10.0),
            ),
            borderSide: BorderSide.none,
          ),
          disabledBorder: OutlineInputBorder(
              borderRadius: const BorderRadius.all(
                const Radius.circular(10.0),
              ),
              borderSide: BorderSide.none
              // BorderSide(color: Theme.of(context).primaryColor, width: 2),
              ),
          enabledBorder: OutlineInputBorder(
              borderRadius: const BorderRadius.all(
                const Radius.circular(10.0),
              ),
              borderSide: BorderSide.none
              // borderSide:
              //     BorderSide(color: Theme.of(context).primaryColor, width: 2),
              ),
          errorBorder: OutlineInputBorder(
              borderRadius: const BorderRadius.all(
                const Radius.circular(10.0),
              ),
              borderSide: BorderSide.none
              // borderSide:
              //     BorderSide(color: Theme.of(context).primaryColor, width: 2),
              ),
          focusedErrorBorder: OutlineInputBorder(
              borderRadius: const BorderRadius.all(
                const Radius.circular(10.0),
              ),
              borderSide: BorderSide.none
              // borderSide:
              //     BorderSide(color: Theme.of(context).primaryColor, width: 2),
              ),
          errorStyle: TextStyle(color: Colors.red),
          // suffixIcon: widget.suffixIcon,
          suffixIcon: this.widget.isSecureInput
              ? IconButton(
                  onPressed: _passwordVisible,
                  icon: _obscureText
                      ? Icon(
                          Icons.visibility,
                          color: Colors.grey.shade700,
                        )
                      : Icon(
                          Icons.visibility_off_sharp,
                          color: Colors.grey.shade700,
                        ),
                  iconSize: 20,
                )
              : null,
          prefixIcon: widget.prefixIcon,
          prefixText: widget.prefixText,
          filled: true,
          hintStyle: TextStyle(color: Colors.black.withOpacity(0.7)),
          hintText: widget.hinttext,
          labelText: widget.labelText,
          fillColor: Colors.grey.withOpacity(0.3)),
      keyboardType: widget.keyBoardType,
      textAlignVertical: TextAlignVertical.center,
      obscureText: _obscureText,
      onSaved: widget.onSaved,
      focusNode: widget.focusNode,
      onEditingComplete: widget.onEditingComplete,
      onFieldSubmitted: (value) {
        FocusScope.of(context).requestFocus(widget.focusScope);
      },
      onChanged: widget.onChanged,
      enableSuggestions: true,
      validator: widget.validator,
      // ),
    );
  }
}
