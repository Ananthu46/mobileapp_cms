import 'package:flutter/material.dart';

class RoundFlatButton extends StatelessWidget {
  final Function _onpressed;
  final String _buttonName;
  RoundFlatButton(this._onpressed, this._buttonName);
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return new ButtonTheme(
      minWidth: deviceSize.width * 0.5,
      child: RaisedButton(
        elevation: 5,
        child: Text(
          '$_buttonName',
          style: Theme.of(context).primaryTextTheme.button,
        ),
        onPressed: _onpressed,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30),
        ),
        padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 8.0),
      ),
    );
  }
}
