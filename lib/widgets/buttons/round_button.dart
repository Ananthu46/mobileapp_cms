import 'package:flutter/material.dart';
import '../../app/size_config.dart';

class RoundButton extends StatelessWidget {
  final Function _onPressAction;
  final String _title;
  final Color textColor;
  final Color buttonColor;
  final bool isEnabled;

  RoundButton(this._onPressAction, this._title,
      {this.textColor = Colors.white, this.buttonColor, this.isEnabled = true});

  @override
  Widget build(BuildContext context) {
    return new InkWell(
      onTap: _onPressAction,
      child: (new Container(
        width: SizeConfig.getScaleFactor() * 320.0,
        height: SizeConfig.getScaleFactor() * 50.0,
        alignment: FractionalOffset.center,
        decoration: new BoxDecoration(
            color: isEnabled
                ? buttonColor != null
                    ? buttonColor
                    : Theme.of(context).buttonTheme.colorScheme.primary
                : Theme.of(context).disabledColor.withOpacity(0.1),
            borderRadius: new BorderRadius.all(const Radius.circular(30.0)),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 3,
                blurRadius: 3,
                offset: Offset(0, 4), // changes position of shadow
              ),
            ]),
        child: new Text(
          _title,
          style: new TextStyle(
            color: isEnabled ? textColor : textColor.withOpacity(0.5),
            fontSize: SizeConfig.getScaleFactor() * 20.0,
            fontWeight: FontWeight.w300,
            letterSpacing: SizeConfig.getScaleFactor() * 0.3,
          ),
        ),
      )),
    );
  }
}
