// Copyright 2017 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'dart:collection';
import 'package:doctor_portal_cms/model/drug/drug_order_api_modal.dart';
import 'package:doctor_portal_cms/provider/drug/drug_order_api_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

const Duration _kExpand = Duration(milliseconds: 200);
HashMap<String, bool> incrementAddPharmacy;
HashMap<String, int> pharmacynameMap;
HashMap<String, String> pharmacyGenericName;
HashMap<String, String> pharmacyFrequencyMap;
HashMap<String, int> pharmacyFrequencyID;
HashMap<String, double> totalquantitypharmacy;
HashMap<String, int> noOfDaysPharmacy;

/// A single-line [ListTile] with a trailing button that expands or collapses
/// the tile to reveal or hide the [children].
///
/// This widget is typically used with [ListView] to create an
/// "expand / collapse" list entry. When used with scrolling widgets like
/// [ListView], a unique [PageStorageKey] must be specified to enable the
/// [ExpansionTileOnButton] to save and restore its expanded state when it is scrolled
/// in and out of view.
///
/// See also:
///
///  * [ListTile], useful for creating expansion tile [children] when the
///    expansion tile represents a sublist.
///  * The "Expand/collapse" section of
///    <https://material.io/guidelines/components/lists-controls.html>.
class ExpansionTileOnButton extends StatefulWidget {
  /// Creates a single-line [ListTile] with a trailing button that expands or collapses
  /// the tile to reveal or hide the [children]. The [initiallyExpanded] property must
  /// be non-null.
  const ExpansionTileOnButton({
    Key key,
    this.headerBackgroundColor,
    this.leading,
    @required this.title,
    this.backgroundColor,
    this.iconColor,
    this.onExpansionChanged,
    this.children = const <Widget>[],
    this.trailing,
    this.initiallyExpanded = false,
    this.itemName,
    this.selectedStore,
    this.pharmacy,
  })  : assert(initiallyExpanded != null),
        super(key: key);

  /// A widget to display before the title.
  ///
  /// Typically a [CircleAvatar] widget.
  final Widget leading;
  final String itemName;
  final PharmacyMedicinesModal pharmacy;
  final PharmacyStores selectedStore;

  /// The primary content of the list item.
  ///
  /// Typically a [Text] widget.
  final Widget title;

  /// Called when the tile expands or collapses.
  ///
  /// When the tile starts expanding, this function is called with the value
  /// true. When the tile starts collapsing, this function is called with
  /// the value false.
  final ValueChanged<bool> onExpansionChanged;

  /// The widgets that are displayed when the tile expands.
  ///
  /// Typically [ListTile] widgets.
  final List<Widget> children;

  /// The color to display behind the sublist when expanded.
  final Color backgroundColor;

  /// The color to display the background of the header.
  final Color headerBackgroundColor;

  /// The color to display the icon of the header.
  final Color iconColor;

  /// A widget to display instead of a rotating arrow icon.
  final Widget trailing;

  /// Specifies if the list tile is initially expanded (true) or collapsed (false, the default).
  final bool initiallyExpanded;

  @override
  _ExpansionTileOnButtonState createState() => _ExpansionTileOnButtonState();
}

class _ExpansionTileOnButtonState extends State<ExpansionTileOnButton>
    with SingleTickerProviderStateMixin {
  static final Animatable<double> _easeOutTween =
      CurveTween(curve: Curves.easeOut);
  static final Animatable<double> _easeInTween =
      CurveTween(curve: Curves.easeIn);
  static final Animatable<double> _halfTween =
      Tween<double>(begin: 0.0, end: 0.5);

  final ColorTween _borderColorTween = ColorTween();
  final ColorTween _headerColorTween = ColorTween();
  final ColorTween _iconColorTween = ColorTween();
  final ColorTween _backgroundColorTween = ColorTween();

  AnimationController _controller;
  Animation<double> _iconTurns;
  Animation<double> _heightFactor;
  Animation<Color> _borderColor;
  Animation<Color> _headerColor;
  Animation<Color> _iconColor;
  Animation<Color> _backgroundColor;

  bool _isExpanded = false;

  @override
  void initState() {
    super.initState();
    pharmacynameMap = Provider.of<DrugPatientProvider>(context, listen: false)
        .pharmacynameMap;
    incrementAddPharmacy =
        Provider.of<DrugPatientProvider>(context, listen: false)
            .incrementAddPharmacy;
    pharmacyGenericName =
        Provider.of<DrugPatientProvider>(context, listen: false)
            .pharmacyGenericName;
    pharmacyFrequencyMap =
        Provider.of<DrugPatientProvider>(context, listen: false)
            .pharmacyFrequencyMap;
    pharmacyFrequencyID =
        Provider.of<DrugPatientProvider>(context, listen: false)
            .pharmacyFrequencyID;
    totalquantitypharmacy =
        Provider.of<DrugPatientProvider>(context, listen: false)
            .totalquantitypharmacy;
    noOfDaysPharmacy = Provider.of<DrugPatientProvider>(context, listen: false)
        .noOfDaysPharmacy;
    _controller = AnimationController(duration: _kExpand, vsync: this);
    _heightFactor = _controller.drive(_easeInTween);
    _iconTurns = _controller.drive(_halfTween.chain(_easeInTween));
    _borderColor = _controller.drive(_borderColorTween.chain(_easeOutTween));
    _headerColor = _controller.drive(_headerColorTween.chain(_easeInTween));
    _iconColor = _controller.drive(_iconColorTween.chain(_easeInTween));
    _backgroundColor =
        _controller.drive(_backgroundColorTween.chain(_easeOutTween));

    _isExpanded =
        PageStorage.of(context)?.readState(context) ?? widget.initiallyExpanded;
    if (_isExpanded) _controller.value = 1.0;
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void onTapAdd(PharmacyMedicinesModal pharmacy, selectedFrequencyName,
      frequencyId, noOfDays, total) {
    Provider.of<DrugPatientProvider>(context, listen: false).addPharmacyList(
      widget.selectedStore.storeId,
      pharmacy,
      '',
      selectedFrequencyName,
      frequencyId,
      noOfDays,
      total,
    );
    if (!pharmacynameMap.containsKey(pharmacy.itemname)) {
      setState(() {
        pharmacynameMap.putIfAbsent(pharmacy.itemname, () => 1);
        pharmacyGenericName.putIfAbsent(
            pharmacy.itemname, () => pharmacy.genericName);
        incrementAddPharmacy.putIfAbsent(pharmacy.itemname, () => true);
      });
    }
  }

  _onTapMinus(pharmacyName, count) {
    if (count < 2) {
      setState(() {
        pharmacyFrequencyID.removeWhere((key, value) => key == pharmacyName);
        pharmacyFrequencyMap.removeWhere((key, value) => key == pharmacyName);
        pharmacynameMap.removeWhere((key, value) => key == pharmacyName);
        pharmacyGenericName.removeWhere((key, value) => key == pharmacyName);
        incrementAddPharmacy.update(pharmacyName, (value) => false);
        totalquantitypharmacy.removeWhere((key, value) => key == pharmacyName);
        //  totalquantitypharmacy.update(pharmacyName, (value) => 0);
      });
      Provider.of<DrugPatientProvider>(context, listen: false)
          .onTapdeletePharmacy(pharmacyName);
    } else {
      setState(() {
        pharmacynameMap.update(pharmacyName, (value) {
          setState(() {
            value--;
          });
          return value;
        });
      });
    }
  }

  // void _handleTap() {
  //   setState(() {
  //     _isExpanded = !_isExpanded;
  //     if (_isExpanded) {
  //       _controller.forward();
  //     } else {
  //       _controller.reverse().then<void>((void value) {
  //         if (!mounted) return;
  //         setState(() {
  //           // Rebuild without widget.children.
  //         });
  //       });
  //     }
  //     PageStorage.of(context)?.writeState(context, _isExpanded);
  //   });
  //   if (widget.onExpansionChanged != null)
  //     widget.onExpansionChanged(_isExpanded);
  // }

  void _collapse() {
    setState(() {
      _isExpanded = false;
      if (_isExpanded) {
        _controller.forward();
      } else {
        _controller.reverse().then<void>((void value) {
          if (!mounted) return;
          setState(() {
            // Rebuild without widget.children.
          });
        });
      }
      PageStorage.of(context)?.writeState(context, _isExpanded);
    });
    if (widget.onExpansionChanged != null)
      widget.onExpansionChanged(_isExpanded);
  }

  void _expandTile() {
    setState(() {
      _isExpanded = true;
      if (_isExpanded) {
        _controller.forward();
      }
      PageStorage.of(context)?.writeState(context, _isExpanded);
    });
    if (widget.onExpansionChanged != null)
      widget.onExpansionChanged(_isExpanded);
  }

  Widget _buildChildren(BuildContext context, Widget child) {
    final Color borderSideColor = _borderColor.value ?? Colors.transparent;
    final Color titleColor = _headerColor.value;

    return Container(
      decoration: BoxDecoration(
          color: _backgroundColor.value ?? Colors.transparent,
          border: Border(
            top: BorderSide(color: borderSideColor),
            bottom: BorderSide(color: borderSideColor),
          )),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          IconTheme.merge(
            data: IconThemeData(color: _iconColor.value),
            child: Container(
              color: widget.headerBackgroundColor ?? Colors.black,
              child: ListTile(
                onTap: _isExpanded ? _collapse : _expandTile,
                leading: widget.leading,
                title: DefaultTextStyle(
                  style: Theme.of(context)
                      .textTheme
                      .subhead
                      .copyWith(color: titleColor),
                  child: widget.title,
                ),
                trailing: incrementAddPharmacy.containsKey(widget.itemName) &&
                        incrementAddPharmacy.containsValue(true) &&
                        pharmacynameMap[widget.itemName] != null
                    ? Container(
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.grey)),
                        height: 30,
                        width: 85,
                        child: Padding(
                            padding: const EdgeInsets.all(2.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                InkWell(
                                    child: Text(
                                      'Delete',
                                      style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.red),
                                    ),
                                    onTap: () {
                                      _collapse();
                                      _onTapMinus(widget.itemName,
                                          pharmacynameMap[widget.itemName]);
                                    }),
                              ],
                            )))
                    : InkWell(
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey)),
                          height: 30,
                          width: 85,
                          child: Padding(
                            padding: const EdgeInsets.all(2.0),
                            child: Center(
                                child: Text(
                              'ADD',
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold),
                            )),
                          ),
                        ),
                        onTap: () {
                          _expandTile();
                          onTapAdd(
                              widget.pharmacy,
                              pharmacyFrequencyMap[widget.itemName],
                              pharmacyFrequencyID[widget.itemName],
                              noOfDaysPharmacy[widget.itemName],
                              totalquantitypharmacy[widget.itemName]);
                        },
                      ),
              ),
            ),
          ),
          ClipRect(
            child: Align(
              heightFactor: _heightFactor.value,
              child: child,
            ),
          ),
        ],
      ),
    );
  }

  @override
  void didChangeDependencies() {
    final ThemeData theme = Theme.of(context);
    _borderColorTween..end = theme.dividerColor;
    _headerColorTween
      ..begin = theme.textTheme.subhead.color
      ..end = theme.accentColor;
    _iconColorTween
      ..begin = theme.unselectedWidgetColor
      ..end = theme.accentColor;
    _backgroundColorTween..end = widget.backgroundColor;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final bool closed = !_isExpanded && _controller.isDismissed;
    return AnimatedBuilder(
      animation: _controller.view,
      builder: _buildChildren,
      child: closed
          ? null
          : Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: widget.children),
    );
  }
}
