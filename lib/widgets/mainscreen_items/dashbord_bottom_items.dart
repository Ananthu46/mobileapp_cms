import 'package:doctor_portal_cms/app/app_theme.dart';
import 'package:flutter/material.dart';

class DashbordBottomItemsWidget extends StatefulWidget {
  const DashbordBottomItemsWidget(
      {Key key,
      this.mainScreenAnimationController,
      this.mainScreenAnimation,
      this.animation})
      : super(key: key);

  final AnimationController mainScreenAnimationController;
  final Animation<dynamic> mainScreenAnimation;
  final Animation<double> animation;
  @override
  _DashbordBottomItemsWidgetState createState() =>
      _DashbordBottomItemsWidgetState();
}

class _DashbordBottomItemsWidgetState extends State<DashbordBottomItemsWidget>
    with TickerProviderStateMixin {
  AnimationController animationController;
  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);

    super.initState();
  }

  Future<bool> getData() async {
    await Future<dynamic>.delayed(const Duration(milliseconds: 50));
    return true;
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: widget.mainScreenAnimationController,
      builder: (BuildContext context, Widget child) {
        return FadeTransition(
            opacity: widget.mainScreenAnimation,
            child: Transform(
                transform: Matrix4.translationValues(
                    0.0, 30 * (1.0 - widget.mainScreenAnimation.value), 0.0),
                child: Container(
                    height: 216,
                    width: double.infinity,
                    child: ListView(children: [
                      AnimatedBuilder(
                        animation: animationController,
                        builder: (BuildContext context, Widget child) {
                          return FadeTransition(
                            opacity: widget.animation,
                            child: Transform(
                              transform: Matrix4.translationValues(
                                  100 * (1.0 - widget.animation.value),
                                  0.0,
                                  0.0),
                              child: SizedBox(
                                width: 130,
                                child: Stack(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          top: 32,
                                          left: 8,
                                          right: 8,
                                          bottom: 16),
                                      child: Container(
                                        decoration: BoxDecoration(
                                          boxShadow: <BoxShadow>[
                                            BoxShadow(
                                                color: HexColor('#FFB295')
                                                    .withOpacity(0.6),
                                                offset: const Offset(1.1, 4.0),
                                                blurRadius: 8.0),
                                          ],
                                          gradient: LinearGradient(
                                            colors: <HexColor>[
                                              HexColor('#FA7D82'),
                                              HexColor('#FFB295'),
                                            ],
                                            begin: Alignment.topLeft,
                                            end: Alignment.bottomRight,
                                          ),
                                          borderRadius: const BorderRadius.only(
                                            bottomRight: Radius.circular(8.0),
                                            bottomLeft: Radius.circular(8.0),
                                            topLeft: Radius.circular(8.0),
                                            topRight: Radius.circular(54.0),
                                          ),
                                        ),
                                        child: Padding(
                                          padding: const EdgeInsets.only(
                                              top: 54,
                                              left: 16,
                                              right: 16,
                                              bottom: 8),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text(
                                                '',
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                  fontFamily:
                                                      DoctorsDashbordTheme
                                                          .fontName,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                  letterSpacing: 0.2,
                                                  color: DoctorsDashbordTheme
                                                      .white,
                                                ),
                                              ),
                                              Expanded(
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 8, bottom: 8),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      Text(
                                                        '',
                                                        style: TextStyle(
                                                          fontFamily:
                                                              DoctorsDashbordTheme
                                                                  .fontName,
                                                          fontWeight:
                                                              FontWeight.w500,
                                                          fontSize: 10,
                                                          letterSpacing: 0.2,
                                                          color:
                                                              DoctorsDashbordTheme
                                                                  .white,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: <Widget>[
                                                  Text(
                                                    '',
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                      fontFamily:
                                                          DoctorsDashbordTheme
                                                              .fontName,
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      fontSize: 24,
                                                      letterSpacing: 0.2,
                                                      color:
                                                          DoctorsDashbordTheme
                                                              .white,
                                                    ),
                                                  ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            left: 4, bottom: 3),
                                                    child: Text(
                                                      'kcal',
                                                      style: TextStyle(
                                                        fontFamily:
                                                            DoctorsDashbordTheme
                                                                .fontName,
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        fontSize: 10,
                                                        letterSpacing: 0.2,
                                                        color:
                                                            DoctorsDashbordTheme
                                                                .white,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      top: 0,
                                      left: 0,
                                      child: Container(
                                        width: 84,
                                        height: 84,
                                        decoration: BoxDecoration(
                                          color: DoctorsDashbordTheme
                                              .nearlyWhite
                                              .withOpacity(0.2),
                                          shape: BoxShape.circle,
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      top: 0,
                                      left: 8,
                                      child: SizedBox(
                                        width: 80,
                                        height: 80,
                                        // child: Image.asset(mealsListData.imagePath),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          );
                        },
                      )
                    ]))));
      },
    );
  }
}
