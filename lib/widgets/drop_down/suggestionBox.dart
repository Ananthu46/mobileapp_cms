import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';

class SuggestionBoxWidget extends StatefulWidget {
  final TextEditingController controller;
  final FutureOr<Iterable<dynamic>> Function(String) suggestionsCallback;
  final Function(dynamic) onSuggestionSelected;
  final Function onsaved;
  final Widget Function(BuildContext, dynamic) itemBuilder;
  final dynamic Function(BuildContext, Widget, AnimationController)
      transitionBuilder;
  final String Function(String) validator;
  final Widget suffixIcon;
  SuggestionBoxWidget(
      {@required this.controller,
      @required this.suggestionsCallback,
      @required this.onSuggestionSelected,
      @required @required this.onsaved,
      @required this.itemBuilder,
      @required this.transitionBuilder,
      @required this.validator,
      this.suffixIcon});

  @override
  _SuggestionBoxWidgetState createState() => _SuggestionBoxWidgetState();
}

class _SuggestionBoxWidgetState extends State<SuggestionBoxWidget> {
  @override
  Widget build(BuildContext context) {
    return TypeAheadFormField(
      getImmediateSuggestions: false,
      hideOnEmpty: true,
      suggestionsBoxDecoration: SuggestionsBoxDecoration(hasScrollbar: true),
      textFieldConfiguration: TextFieldConfiguration(
        scrollPadding: const EdgeInsets.only(bottom: 250.0),
        controller: widget.controller,
        decoration: InputDecoration(
          suffixIcon: widget.suffixIcon,
          // icon: Icon(Icons.search),
          labelText: 'search',
          labelStyle: TextStyle(color: Theme.of(context).hintColor),
          focusedErrorBorder: OutlineInputBorder(
            borderSide:
                BorderSide(color: Theme.of(context).primaryColor, width: 2),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide:
                BorderSide(color: Theme.of(context).primaryColor, width: 2),
          ),
          errorBorder: OutlineInputBorder(
            borderSide:
                BorderSide(color: Theme.of(context).primaryColor, width: 2),
          ),
          disabledBorder: OutlineInputBorder(
            borderSide:
                BorderSide(color: Theme.of(context).primaryColor, width: 2),
          ),
          focusedBorder: OutlineInputBorder(
              borderSide:
                  BorderSide(color: Theme.of(context).primaryColor, width: 2)),
          contentPadding: EdgeInsets.only(left: 10),
          errorStyle: TextStyle(color: Colors.red),
        ),
      ),
      suggestionsCallback: widget.suggestionsCallback,
      itemBuilder: widget.itemBuilder,
      transitionBuilder: widget.transitionBuilder,
      onSuggestionSelected: widget.onSuggestionSelected,
      validator: widget.validator,
      // validator: (value) {
      //   if (value.isEmpty) {
      //     return '* field is required';
      //   }
      //   return null;
      // },
      onSaved: widget.onsaved,
    );
  }
}
