import 'package:flutter/material.dart';

class DropdownWidget extends StatelessWidget {
  final Function onChanged;
  final String value;
  final List<DropdownMenuItem<dynamic>> items;
  DropdownWidget({this.onChanged, this.items, this.value});
  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField(
      value: value,
      hint: Text('--Select--'),
      decoration: InputDecoration(
        enabledBorder: OutlineInputBorder(
          borderSide:
              BorderSide(color: Theme.of(context).primaryColor, width: 2),
        ),
        errorBorder: OutlineInputBorder(
          borderSide:
              BorderSide(color: Theme.of(context).primaryColor, width: 2),
        ),
        disabledBorder: OutlineInputBorder(
          borderSide:
              BorderSide(color: Theme.of(context).primaryColor, width: 2),
        ),
        contentPadding: EdgeInsets.only(left: 10),
        errorStyle: TextStyle(color: Colors.red),
      ),
      isExpanded: true,
      onChanged: onChanged,
      items: items,
    );
  }
}
