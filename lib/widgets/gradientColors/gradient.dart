import 'package:flutter/cupertino.dart';

class GradientColors {
  // ************************blue*****************************
  static const List<Color> blue = [
    Color.fromRGBO(31, 179, 237, 1),
    Color.fromRGBO(17, 106, 197, 1)
  ];
  // ************************blue*****************************
  // ************************pink*****************************
  static const List<Color> pink = [
    Color.fromRGBO(240, 19, 77, 1),
    Color.fromRGBO(228, 0, 124, 1)
  ];
  // ************************pink*****************************

  // ************************orange*****************************
  static const List<Color> orange = [
    Color.fromRGBO(255, 190, 32, 1),
    Color.fromRGBO(251, 112, 71, 1)
  ];
  // ************************orange*****************************

  // ************************indigo*****************************
  static const List<Color> indigo = [
    Color.fromRGBO(117, 0, 149, 1),
    Color.fromRGBO(88, 0, 112, 0.76)
  ];
  // ************************indigo*****************************

  // ************************white*****************************
  static const List<Color> white = [
    Color.fromRGBO(255, 255, 255, 1),
    Color.fromRGBO(234, 236, 255, 1)
  ];
  // ************************white*****************************

  // ************************black*****************************
  static const List<Color> black = [Color(0xFF090909), Color(0xDD202020)];
  // ************************black*****************************

  // ************************piggyPink*****************************
  static const List<Color> piggyPink = [Color(0xFFee9ca7), Color(0xFFffdde1)];
  // ************************piggyPink*****************************

  // ************************coolBlues*****************************
  static const List<Color> coolBlues = [Color(0xFF2193b0), Color(0xFF6dd5ed)];
  // ************************coolBlues*****************************

  // ************************eveningSunshine*****************************
  static const List<Color> eveningSunshine = [
    Color(0xFFb92b27),
    Color(0xFF1565C0)
  ];
  // ************************eveningSunshine*****************************

  // ************************darkOcean*****************************
  static const List<Color> darkOcean = [Color(0xFF373B44), Color(0xFF4286f4)];
  // ************************eveningSunshine*****************************

  // ************************gradeGrey*****************************
  static const List<Color> gradeGrey = [Color(0xFFbdc3c7), Color(0xFF2c3e50)];
  // ************************gradeGrey*****************************

  // ************************dimBlue*****************************
  static const List<Color> dimBlue = [Color(0xFF00416A), Color(0xFFE4E5E6)];
  // ************************dimBlue*****************************

  // ************************ver*****************************
  static const List<Color> ver = [Color(0xFFFFE000), Color(0xFF799F0C)];
  // ************************ver*****************************

  // ************************lightBlue*****************************
  static const List<Color> lightBlue = [Color(0xFF4364F7), Color(0xFF6FB1FC)];
  // ************************lightBlue*****************************

  // ************************lightGreen*****************************
  static const List<Color> lightGreen = [Color(0xFF799F0C), Color(0xFFACBB78)];
  // ************************lightGreen*****************************

  // ************************mango*****************************
  static const List<Color> mango = [Color(0xFFffe259), Color(0xFFffa751)];
  // ************************mango*****************************

  // ************************royalBlue*****************************
  static const List<Color> royalBlue = [Color(0xFF536976), Color(0xFF292E49)];
  // ************************royalBlue*****************************

  // ************************skyLine*****************************
  static const List<Color> skyLine = [Color(0xFF1488CC), Color(0xFF2B32B2)];
  // ************************skyLine*****************************

  // ************************darkPink*****************************
  static const List<Color> darkPink = [Color(0xFFec008c), Color(0xFFfc6767)];
  // ************************darkPink*****************************

  // ************************purplePink*****************************
  static const List<Color> purplePink = [Color(0xFFcc2b5e), Color(0xFF753a88)];
  // ************************purplePink*****************************

  // ************************skyBlue*****************************
  static const List<Color> skyBlue = [Color(0xFF2193b0), Color(0xFF6dd5ed)];
  // ************************skyBlue*****************************

  // ************************seaBlue*****************************
  static const List<Color> seaBlue = [Color(0xFF2b5876), Color(0xFF4e4376)];
  // ************************seaBlue*****************************

  // ************************noontoDusk*****************************
  static const List<Color> noontoDusk = [
    Color(0xFFff6e7f),
    Color(0xFFbfe9ff),
  ];
  // ************************noontoDusk*****************************

  // ************************red*****************************
  static const List<Color> red = [Color(0xFFe52d27), Color(0xFFb31217)];
  // ************************red*****************************

  // ************************lightBrown*****************************
  static const List<Color> lightBrown = [Color(0xFF603813), Color(0xFFb29f94)];
  // ************************lightBrown*****************************

  // ************************harmonicEnergy*****************************
  static const List<Color> harmonicEnergy = [
    Color(0xFF16A085),
    Color(0xFFF4D03F)
  ];
  // ************************harmonicEnergy*****************************

  // ************************radish*****************************
  static const List<Color> radish = [Color(0xFFD31027), Color(0xFFEA384D)];
  // ************************radish*****************************

  // ************************sunny*****************************
  static const List<Color> sunny = [Color(0xFFEDE574), Color(0xFFE1F5C4)];
  // ************************sunny*****************************

  // ************************teal*****************************
  static const List<Color> teal = [Color(0xFF02AAB0), Color(0xFF00CDAC)];
  // ************************teal*****************************

  // ************************purple*****************************
  static const List<Color> purple = [Color(0xFFDA22FF), Color(0xFF9733EE)];
  // ************************purple*****************************

  // ************************green*****************************
  static const List<Color> green = [Color(0xFF348F50), Color(0xFF56B4D3)];
  // ************************green*****************************

  // ************************yellow*****************************
  static const List<Color> yellow = [Color(0xFFF09819), Color(0xFFEDDE5D)];
  // ************************yellow*****************************

  // ************************orangePink*****************************
  static const List<Color> orangePink = [Color(0xFFFF512F), Color(0xFFDD2476)];
  // ************************orangePink*****************************

  // ************************aqua*****************************
  static const List<Color> aqua = [Color(0xFF1A2980), Color(0xFF26D0CE)];
  // ************************aqua*****************************

// ************************sunrise*****************************
  static const List<Color> sunrise = [Color(0xFFFF512F), Color(0xFFF09819)];
  // ************************sunrise*****************************

  // ************************cherry*****************************
  static const List<Color> cherry = [Color(0xFFEB3349), Color(0xFFF45C43)];
  // ************************cherry*****************************

  // ************************mojito*****************************
  static const List<Color> mojito = [Color(0xFF1D976C), Color(0xFF93F9B9)];
  // ************************mojito*****************************

// ************************juicyOrange*****************************
  static const List<Color> juicyOrange = [Color(0xFFFF8008), Color(0xFFFFC837)];
  // ************************juicyOrange*****************************

  // ************************mirage*****************************
  static const List<Color> mirage = [Color(0xFF16222A), Color(0xFF3A6073)];
  // ************************mirage*****************************
  // ************************violet*****************************
  static const List<Color> violet = [Color(0xFF4776E6), Color(0xFF8E54E9)];
  // ************************violet*****************************

  // ************************lightBlack*****************************
  static const List<Color> lightBlack = [Color(0xFF232526), Color(0xFF414345)];
  // ************************lightBlack*****************************

  // ************************facebookMessenger*****************************
  static const List<Color> facebookMessenger = [
    Color(0xFF00c6ff),
    Color(0xFF0072ff)
  ];
  // ************************facebookMessenger*****************************

  // ************************winter*****************************
  static const List<Color> winter = [Color(0xFFe6dada), Color(0xFF274046)];
  // ************************winter*****************************

  // ************************cloud*****************************
  static const List<Color> cloud = [Color(0xFFece9e6), Color(0xFFffffff)];
  // ************************cloud*****************************

// ************************grey*****************************
  static const List<Color> grey = [Color(0xFF3e5151), Color(0x993e5151)];
  // ************************grey*****************************

  // ************************beautifulGreen*****************************
  static const List<Color> beautifulGreen = [
    Color(0xFF11998e),
    Color(0xFF38ef7d)
  ];
  // ************************beautifulGreen*****************************
}

class MoreGradientColors {
  // ************************lunada*****************************
  static const List<Color> countValues = [
    Color(0xFF00AB9E),
    Color(0xFF00887E),
    Color(0xFF00AB9E),
  ];
  // ************************lunada*****************************

  // ************************hazel*****************************
  static const List<Color> count = [
    Color(0xFF00776E),
    Color(0xFF00887E),
    Color(0xFF00887E),
    Color(0xFF00554F)
  ];
  // ************************hazel*****************************

  // ************************darkSkyBlue*****************************
  static const List<Color> darkSkyBlue = [
    Color(0xFF1FA2FF),
    Color(0xFF12D8FA),
    Color(0xFFA6FFCB)
  ];
  // ************************darkSkyBlue*****************************

  // ************************instagram*****************************
  static const List<Color> instagram = [
    Color(0xFF833ab4),
    Color(0xFFfd1d1d),
    Color(0xFFfcb045)
  ];
  // ************************instagram*****************************

  // ************************jShine*****************************
  static const List<Color> jShine = [
    Color(0xFF12c2e9),
    Color(0xFF471ed),
    Color(0xFFf64f59)
  ];
  // ************************jShine*****************************

  // ************************coolSky*****************************
  static const List<Color> coolSky = [
    Color(0xFF2980b9),
    Color(0xFF6dd5fa),
    Color(0xFFffffff)
  ];
  // ************************coolSky*****************************

// ************************azureLane*****************************
  static const List<Color> azureLane = [
    Color(0xFF7f7fd5),
    Color(0xFF86a8e7),
    Color(0xFF91eae4)
  ];
  // ************************azureLane*****************************

  // ************************orangePinkBlue*****************************
  static const List<Color> orangePinkTeal = [
    Color(0xFF40e0d0),
    Color(0xFFff8c00),
    Color(0xFFff0080)
  ];
  // ************************orangePinkBlue*****************************

  static const List<Color> appointments = [
    Color(0xFF483D8B),
    Color(0xFF7F0020),
    Color(0xFF000000)
  ];
  static const List<Color> encounter = [
    Color(0xFFFF0040),
    Color(0xFF7F0020),
    Color(0xFF000000)
  ];
  static const List<Color> referal = [
    Color(0xFFFF0040),
    Color(0xFF7F0020),
    Color(0xFF000000)
  ];
  static const List<Color> followup = [
    Color(0xFFFF0040),
    Color(0xFF7F0020),
    Color(0xFF000000)
  ];
  static const List<Color> month = [
    Color(0xFFFF0040),
    Color(0xFF7F0020),
    Color(0xFF000000)
  ];
}
