import 'dart:typed_data';

import 'package:doctor_portal_cms/app/size_config.dart';
import 'package:doctor_portal_cms/model/ehr_modal/ehr_main_modal.dart';
import 'package:doctor_portal_cms/provider/ehr_provider/ehr_main_provider.dart';
import 'package:doctor_portal_cms/widgets/inputfields/rouund_textfield_search.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:file_picker/file_picker.dart';

class EHRUploadDocumentScreen extends StatefulWidget {
  static const routeName = 'ehr-upload-screen';

  @override
  _EHRUploadDocumentScreenState createState() =>
      _EHRUploadDocumentScreenState();
}

class _EHRUploadDocumentScreenState extends State<EHRUploadDocumentScreen> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _controller = new TextEditingController();
  @override
  void initState() {
    Provider.of<EHRMainProvider>(context, listen: false).loaduploadDocument();
    super.initState();
  }

  bool _uploadDocument = false;

  void uploadDocument() {
    setState(() {
      _uploadDocument = true;
    });
  }

  void cancel() {
    setState(() {
      _uploadDocument = false;
    });
  }

  String _filename;
  Uint8List _fileUploadData;
  PlatformFile file;
  void _openFileExplorer() async {
    FilePickerResult result =
        await FilePicker.platform.pickFiles(withData: true);
    if (result != null) {
      file = result.files.first;
      setState(() {
        _fileUploadData = file.bytes;
        _filename = file.name;
      });
      print(file.name);
      print(file.bytes);
      print(file.size);
      print(file.extension);
      print(file.path);
    }
  }

  String _documentName;
  bool _isLoading = false;
  upload() async {
    _formKey.currentState.save();
    setState(() {
      _isLoading = true;
    });
    print(_fileUploadData);
    await Provider.of<EHRMainProvider>(context, listen: false)
        .uploadmedDocuments(
            bytes: _fileUploadData,
            docname: _documentName,
            fileName: _filename);
    String responseData = Provider.of<EHRMainProvider>(context, listen: false)
        .uploadmedDocument[0]
        .response;
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        backgroundColor: Colors.red,
        content: Text(responseData),
      ),
    );
    await Provider.of<EHRMainProvider>(context, listen: false)
        .loaduploadDocument();
    _controller.clear();
    setState(() {
      _filename = "";
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
        color: Theme.of(context).scaffoldBackgroundColor,
        child: SafeArea(
            child: Scaffold(
                resizeToAvoidBottomInset: false,
                body: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              IconButton(
                                  icon: Icon(Icons.arrow_back_ios,
                                      color: Colors.pink),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  }),
                              Text(
                                'Health Data',
                                style: TextStyle(color: Colors.pink),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 8 * SizeConfig.getScaleFactor(),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20),
                            child: Text(
                              'Upload Documents',
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.bold),
                            ),
                          ),
                          SizedBox(
                            height: 10 * SizeConfig.getScaleFactor(),
                          ),
                          if (!_uploadDocument)
                            ElevatedButton(
                                onPressed: uploadDocument,
                                child: Text('Upload New Document')),
                          if (_uploadDocument)
                            Form(
                              key: _formKey,
                              child: Card(
                                child: Padding(
                                  padding: const EdgeInsets.all(20.0),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text('Document Name :'),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Container(
                                        height: 40,
                                        child: RoundTextFormFieldSearch(
                                          controller: _controller,
                                          onSaved: (value) {
                                            _documentName = value;
                                          },
                                          isSecureInput: false,
                                          scrollpadding: EdgeInsets.all(0),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Text('Upload File :'),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: [
                                          IconButton(
                                              icon: Icon(
                                                Icons.drive_folder_upload,
                                                color: Colors.pink,
                                              ),
                                              onPressed: _openFileExplorer),
                                          AbsorbPointer(
                                            absorbing: true,
                                            child: Container(
                                              width: 200,
                                              height: 40,
                                              child: RoundTextFormFieldSearch(
                                                controller:
                                                    TextEditingController(
                                                        text: _filename),
                                                isSecureInput: false,
                                                scrollpadding:
                                                    EdgeInsets.all(0),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: [
                                          _isLoading
                                              ? CircularProgressIndicator()
                                              : ElevatedButton(
                                                  onPressed: upload,
                                                  child: Text('Upload')),
                                          SizedBox(
                                            width: 20,
                                          ),
                                          ElevatedButton(
                                              onPressed: cancel,
                                              child: Text('Cancel'))
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          Expanded(
                              child: Consumer<EHRMainProvider>(
                                  builder: (ctx, upload, _) => ListView.builder(
                                      padding: EdgeInsets.only(
                                        top: 0 * SizeConfig.getScaleFactor(),
                                      ),
                                      shrinkWrap: true,
                                      itemCount:
                                          upload.loaduploadDocuments.length,
                                      itemBuilder: (context, index) {
                                        EHRLoadUploadDocuments up =
                                            upload.loaduploadDocuments[index];

                                        return Card(
                                          child: Column(
                                            children: [
                                              _TextView(
                                                head: 'Document Name :',
                                                value: up.documentName,
                                              ),
                                              _TextView(
                                                head: 'File Name :',
                                                value: up.fileName,
                                              ),
                                              _TextView(
                                                head: 'Upload By :',
                                                value: up.uploadby,
                                              ),
                                            ],
                                          ),
                                        );
                                      })))
                        ])))));
  }
}

class _TextView extends StatelessWidget {
  final String head;
  final String value;
  _TextView({this.head, this.value});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: [
          Text(
            head,
            style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontWeight: FontWeight.bold),
          ),
          SizedBox(
            width: 20,
          ),
          SizedBox(width: 200, child: Text(value)),
        ],
      ),
    );
  }
}
