import 'package:doctor_portal_cms/app/size_config.dart';
import 'package:doctor_portal_cms/model/ehr_modal/ehr_main_modal.dart';
import 'package:doctor_portal_cms/provider/ehr_provider/ehr_main_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EHRReferralsScreen extends StatefulWidget {
  static const routeName = 'ehr-referrals';

  @override
  _EHRReferralsScreenState createState() => _EHRReferralsScreenState();
}

class _EHRReferralsScreenState extends State<EHRReferralsScreen> {
  @override
  void initState() {
    Provider.of<EHRMainProvider>(context, listen: false).loadreferral();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      color: Theme.of(context).scaffoldBackgroundColor,
      child: SafeArea(
        child: Scaffold(
          body: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      IconButton(
                          icon: Icon(Icons.arrow_back_ios, color: Colors.pink),
                          onPressed: () {
                            Navigator.of(context).pop();
                          }),
                      Text(
                        'Health Data',
                        style: TextStyle(color: Colors.pink),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 8 * SizeConfig.getScaleFactor(),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: Text(
                      'Referrals',
                      style:
                          TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                    ),
                  ),
                  SizedBox(
                    height: 10 * SizeConfig.getScaleFactor(),
                  ),
                  SizedBox(
                    height: 20 * SizeConfig.getScaleFactor(),
                  ),
                  SizedBox(
                    height: 15 * SizeConfig.getScaleFactor(),
                  ),
                  Expanded(
                      child: Consumer<EHRMainProvider>(
                          builder: (ctx, referal, _) => ListView.builder(
                              padding: EdgeInsets.only(
                                top: 0 * SizeConfig.getScaleFactor(),
                              ),
                              shrinkWrap: true,
                              itemCount: referal.loadreferrals.length,
                              itemBuilder: (context, index) {
                                EHRReferrals ref = referal.loadreferrals[index];

                                return Card(
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          children: [
                                            TextView(
                                              head: 'From Department :',
                                              value: ref.reqDepartmentName,
                                            ),
                                          ],
                                        ),
                                        TextView(
                                          head: 'From Consultant :',
                                          value: ref.reqempname,
                                        ),
                                        TextView(
                                          head: 'To Department :',
                                          value: ref.departmentName,
                                        ),
                                        TextView(
                                          head: 'To Consultant :',
                                          value: ref.empname,
                                        ),
                                        TextView(
                                          head: 'Referral Count :',
                                          value: ref.referalCount,
                                        ),
                                        TextView(
                                          head: 'Requested Date :',
                                          value: ref.reqDate,
                                        ),
                                        TextView(
                                          head: 'Approval Status :',
                                          value: ref.approvalStatus,
                                        ),
                                        TextView(
                                          head: 'Priority :',
                                          value: ref.priority,
                                        ),
                                        TextView(
                                          head: 'Reason :',
                                          value: ref.reqReason,
                                        ),
                                        TextView(
                                          head: 'Reason for Rejection :',
                                          value: ref.rejectReason,
                                        ),
                                        TextView(
                                          head: 'Opinion :',
                                          value: ref.opinionResult,
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              })))
                ]),
          ),
        ),
      ),
    );
  }
}

class TextView extends StatelessWidget {
  final String head;
  final String value;
  TextView({this.head, this.value});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Row(
          children: [
            Text(
              head,
              style: TextStyle(
                  color: Theme.of(context).primaryColor,
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(
              width: 20,
            ),
            Text(
              value,
            )
          ],
        ),
      ],
    );
  }
}
