import 'package:doctor_portal_cms/app/size_config.dart';
import 'package:doctor_portal_cms/model/doctor/doctor_order_modal.dart';
import 'package:doctor_portal_cms/provider/doctor/doctor_order_provider.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class EHRLabresultsScreen extends StatefulWidget {
  static const routeName = 'ehr-labresult';

  @override
  _EHRLabresultsScreenState createState() => _EHRLabresultsScreenState();
}

class _EHRLabresultsScreenState extends State<EHRLabresultsScreen> {
  bool _loaded = false;

  @override
  void initState() {
    Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .loadAlllabResults();
    super.initState();
  }

  Future<void> _refreshProducts(BuildContext context, labresultId) async {
    await Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .loadlabResultsParameters(labresultId);
    setState(() {
      _loaded = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
        body: Container(
            color: Theme.of(context).scaffoldBackgroundColor,
            child: SafeArea(
                child: Scaffold(
                    body: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                IconButton(
                                    icon: Icon(Icons.arrow_back_ios,
                                        color: Colors.pink),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    }),
                                Text(
                                  'Health Data',
                                  style: TextStyle(color: Colors.pink),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 8 * SizeConfig.getScaleFactor(),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 20),
                              child: Text(
                                'Lab Results',
                                style: TextStyle(
                                    fontSize: 22, fontWeight: FontWeight.bold),
                              ),
                            ),
                            SizedBox(
                              height: 10 * SizeConfig.getScaleFactor(),
                            ),

                            SizedBox(
                              height: 20 * SizeConfig.getScaleFactor(),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 10),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  SizedBox(
                                    width: 120 * SizeConfig.getScaleFactor(),
                                    child: Text(
                                      "Test",
                                      style: TextStyle(
                                          fontSize:
                                              18 * SizeConfig.getScaleFactor(),
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  _TextBoxWidget(
                                    text: "Result",
                                    width: 150 * SizeConfig.getScaleFactor(),
                                  ),
                                  _TextBoxWidget(
                                    text: "Unit",
                                    width: 70 * SizeConfig.getScaleFactor(),
                                  ),
                                  // _TextBoxWidget(
                                  //   text: "Reference Range",
                                  //   width: 90 * SizeConfig.getScaleFactor(),
                                  // ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 15 * SizeConfig.getScaleFactor(),
                            ),
                            // FutureBuilder(
                            //     future: _refreshProducts(context),
                            //     builder: (ctx, snapshot) => snapshot.connectionState ==
                            //             ConnectionState.waiting
                            //         ? Center(
                            //             child: CircularProgressIndicator(),
                            //           )
                            //         :
                            Expanded(
                              child: Consumer<DrugOrderModalDoctor>(
                                  builder: (ctx, labresults, _) =>
                                      ListView.builder(
                                          padding: EdgeInsets.only(
                                            top:
                                                0 * SizeConfig.getScaleFactor(),
                                          ),
                                          shrinkWrap: true,
                                          itemCount:
                                              labresults.loadLabResults.length,
                                          itemBuilder: (context, index) {
                                            DoctorLabResults result = labresults
                                                .loadLabResults[index];
                                            print(result.description);
                                            return labresults.loadLabResults
                                                        .isEmpty ||
                                                    result.sampleid == null
                                                ? Container()
                                                : Card(
                                                    elevation: 4,
                                                    child: Padding(
                                                      padding: const EdgeInsets
                                                              .symmetric(
                                                          horizontal: 10),
                                                      child: result
                                                                  .description ==
                                                              "PARAMETER"
                                                          ? FutureBuilder(
                                                              future: _loaded
                                                                  ? null
                                                                  : _refreshProducts(
                                                                      context,
                                                                      result
                                                                          .labResultId),
                                                              builder: (ctx,
                                                                      snapshot) =>
                                                                  snapshot.connectionState ==
                                                                          ConnectionState
                                                                              .waiting
                                                                      ? Center(
                                                                          child:
                                                                              CircularProgressIndicator(),
                                                                        )
                                                                      : Column(
                                                                          children: [
                                                                            SizedBox(
                                                                              height: 10 * SizeConfig.getScaleFactor(),
                                                                            ),
                                                                            Row(
                                                                              mainAxisAlignment: MainAxisAlignment.start,
                                                                              children: [
                                                                                SizedBox(
                                                                                  width: 250 * SizeConfig.getScaleFactor(),
                                                                                  child: Text(
                                                                                    result.labServiceName,
                                                                                    style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 16 * SizeConfig.getScaleFactor(), fontWeight: FontWeight.bold),
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                            Consumer<DrugOrderModalDoctor>(
                                                                                builder: (ctx, parameter, _) => ListView.builder(
                                                                                    physics: ScrollPhysics(),
                                                                                    padding: EdgeInsets.only(top: 0),
                                                                                    shrinkWrap: true,
                                                                                    itemCount: labresults.loadLabResultsParameter.length,
                                                                                    itemBuilder: (context, index) {
                                                                                      DoctorLabResultsParameters para = parameter.loadLabResultsParameter[index];
                                                                                      print(para.labresultId);
                                                                                      return Padding(
                                                                                        padding: const EdgeInsets.symmetric(vertical: 10),
                                                                                        child: Row(
                                                                                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                          children: [
                                                                                            SizedBox(
                                                                                              width: 120 * SizeConfig.getScaleFactor(),
                                                                                              child: Text(
                                                                                                para.parameter,
                                                                                                style: TextStyle(fontSize: 15 * SizeConfig.getScaleFactor(), fontWeight: FontWeight.bold),
                                                                                              ),
                                                                                            ),
                                                                                            SizedBox(
                                                                                              width: 80 * SizeConfig.getScaleFactor(),
                                                                                              child: Row(
                                                                                                mainAxisSize: MainAxisSize.min,
                                                                                                children: [
                                                                                                  Text(
                                                                                                    para.value,
                                                                                                    textAlign: TextAlign.center,
                                                                                                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: Colors.brown),
                                                                                                  ),
                                                                                                  // para.reportfalg == null
                                                                                                  // ||
                                                                                                  // para.reportfalg.isEmpty
                                                                                                  //     ? Container()
                                                                                                  //     :
                                                                                                  para.reportfalg == "L" ? Image.asset('assets/images/local_image/green.png') : Image.asset('assets/images/local_image/red.png')
                                                                                                ],
                                                                                              ),
                                                                                            ),
                                                                                            // _TextBoxWidgetResultValue(
                                                                                            //   text: para
                                                                                            //       .value,
                                                                                            //   width: 80 *
                                                                                            //       SizeConfig
                                                                                            //           .getScaleFactor(),
                                                                                            // ),
                                                                                            // para.reportfalg ==
                                                                                            //             null ||
                                                                                            //         para.reportfalg
                                                                                            //             .isEmpty
                                                                                            //     ? Container()
                                                                                            //     : para.reportfalg ==
                                                                                            //             "L"
                                                                                            //         ? Image.asset(
                                                                                            //             'assets/images/local_image/green.png')
                                                                                            //         : Image.asset(
                                                                                            //             'assets/images/local_image/red.png'),
                                                                                            _TextBoxWidgetValue(
                                                                                              text: para.unit,
                                                                                              width: 60 * SizeConfig.getScaleFactor(),
                                                                                            ),
                                                                                            _TextBoxWidgetValue(
                                                                                              text: para.reference,
                                                                                              width: 90 * SizeConfig.getScaleFactor(),
                                                                                            ),
                                                                                          ],
                                                                                        ),
                                                                                      );
                                                                                    })),
                                                                            SizedBox(
                                                                              height: 10 * SizeConfig.getScaleFactor(),
                                                                            ),
                                                                            Row(
                                                                              mainAxisAlignment: MainAxisAlignment.start,
                                                                              children: [
                                                                                _TextBoxWidgetItemsValue(
                                                                                  text: "Sample id :",
                                                                                  width: 80 * SizeConfig.getScaleFactor(),
                                                                                ),
                                                                                _TextBoxWidgetItemsValue(
                                                                                  text: result.sampleid,
                                                                                  width: 80 * SizeConfig.getScaleFactor(),
                                                                                ),
                                                                                _TextBoxWidgetItemsValue(
                                                                                  text: result.visit,
                                                                                  width: 80 * SizeConfig.getScaleFactor(),
                                                                                ),
                                                                                SizedBox(
                                                                                  width: 80 * SizeConfig.getScaleFactor(),
                                                                                  child: Text(result.resultStatus, style: TextStyle(color: Colors.red, fontSize: 16 * SizeConfig.getScaleFactor(), fontWeight: FontWeight.bold)),
                                                                                )
                                                                              ],
                                                                            ),
                                                                            SizedBox(
                                                                              height: 10 * SizeConfig.getScaleFactor(),
                                                                            ),
                                                                            Row(
                                                                              mainAxisAlignment: MainAxisAlignment.start,
                                                                              children: [
                                                                                _TextBoxWidgetItemsValue(
                                                                                  text: "Ordered on :",
                                                                                  width: 80 * SizeConfig.getScaleFactor(),
                                                                                ),
                                                                                _TextBoxWidgetItemsValue(
                                                                                  text: DateFormat('dd-MM-yyyy').format(DateTime.parse(result.orderDate)),
                                                                                  width: 120 * SizeConfig.getScaleFactor(),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                            SizedBox(
                                                                              height: 10 * SizeConfig.getScaleFactor(),
                                                                            ),
                                                                          ],
                                                                          // ),
                                                                        ))
                                                          : Column(
                                                              children: [
                                                                SizedBox(
                                                                  height: 15 *
                                                                      SizeConfig
                                                                          .getScaleFactor(),
                                                                ),
                                                                Row(
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .spaceEvenly,
                                                                  children: [
                                                                    SizedBox(
                                                                      width: 120 *
                                                                          SizeConfig
                                                                              .getScaleFactor(),
                                                                      child:
                                                                          Text(
                                                                        result
                                                                            .labServiceName,
                                                                        style: TextStyle(
                                                                            fontSize:
                                                                                16 * SizeConfig.getScaleFactor(),
                                                                            fontWeight: FontWeight.bold),
                                                                      ),
                                                                    ),
                                                                    SizedBox(
                                                                      width: 5 *
                                                                          SizeConfig
                                                                              .getScaleFactor(),
                                                                    ),
                                                                    SizedBox(
                                                                      width: 150 *
                                                                          SizeConfig
                                                                              .getScaleFactor(),
                                                                      child:
                                                                          Row(
                                                                        mainAxisSize:
                                                                            MainAxisSize.min,
                                                                        children: [
                                                                          Text(
                                                                            result.labResultValue,
                                                                            textAlign:
                                                                                TextAlign.center,
                                                                            style: TextStyle(
                                                                                fontSize: 16 * SizeConfig.getScaleFactor(),
                                                                                fontWeight: FontWeight.w500,
                                                                                color: Colors.brown),
                                                                          ),
                                                                          result.resultOutcome == null || result.resultOutcome.isEmpty
                                                                              ? Container()
                                                                              : result.resultOutcome == "L"
                                                                                  ? Image.asset('assets/images/local_image/green.png')
                                                                                  : Image.asset('assets/images/local_image/red.png')
                                                                        ],
                                                                      ),
                                                                    ),
                                                                    result.unit
                                                                            .isNotEmpty
                                                                        ? _TextBoxWidgetValue(
                                                                            text:
                                                                                result.unit,
                                                                            width:
                                                                                70 * SizeConfig.getScaleFactor(),
                                                                          )
                                                                        : _TextBoxWidgetValue(
                                                                            text:
                                                                                "-",
                                                                            width:
                                                                                80 * SizeConfig.getScaleFactor(),
                                                                          ),
                                                                  ],
                                                                ),
                                                                SizedBox(
                                                                  height: 10 *
                                                                      SizeConfig
                                                                          .getScaleFactor(),
                                                                ),
                                                                Row(
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .start,
                                                                  children: [
                                                                    _TextBoxWidgetItemsValue(
                                                                      text:
                                                                          "Sample id :",
                                                                      width: 80 *
                                                                          SizeConfig
                                                                              .getScaleFactor(),
                                                                    ),
                                                                    _TextBoxWidgetItemsValue(
                                                                      text: result
                                                                          .sampleid,
                                                                      width: 80 *
                                                                          SizeConfig
                                                                              .getScaleFactor(),
                                                                    ),
                                                                    _TextBoxWidgetItemsValue(
                                                                      text: result
                                                                          .visit,
                                                                      width: 80 *
                                                                          SizeConfig
                                                                              .getScaleFactor(),
                                                                    ),
                                                                    _TextBoxWidgetItemsValue(
                                                                      text:
                                                                          "Status :",
                                                                      width: 70 *
                                                                          SizeConfig
                                                                              .getScaleFactor(),
                                                                    ),
                                                                    SizedBox(
                                                                      width: 45 *
                                                                          SizeConfig
                                                                              .getScaleFactor(),
                                                                      child: Text(
                                                                          result
                                                                              .resultStatus,
                                                                          style: TextStyle(
                                                                              color: Colors.red,
                                                                              fontSize: 16,
                                                                              fontWeight: FontWeight.bold)),
                                                                    )
                                                                  ],
                                                                ),
                                                                SizedBox(
                                                                  height: 10 *
                                                                      SizeConfig
                                                                          .getScaleFactor(),
                                                                ),
                                                                Row(
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .start,
                                                                  children: [
                                                                    _TextBoxWidgetItemsValue(
                                                                      text:
                                                                          "Ordered on :",
                                                                      width: 80 *
                                                                          SizeConfig
                                                                              .getScaleFactor(),
                                                                    ),
                                                                    _TextBoxWidgetItemsValue(
                                                                      text: DateFormat(
                                                                              'dd-MM-yyyy')
                                                                          .format(
                                                                              DateTime.parse(result.orderDate)),
                                                                      width: 120 *
                                                                          SizeConfig
                                                                              .getScaleFactor(),
                                                                    ),
                                                                    result.referenceRange
                                                                            .isEmpty
                                                                        ? _TextBoxWidgetValue(
                                                                            text:
                                                                                "-",
                                                                            width:
                                                                                70 * SizeConfig.getScaleFactor(),
                                                                          )
                                                                        : _TextBoxWidgetValue(
                                                                            text:
                                                                                result.referenceRange,
                                                                            width:
                                                                                70 * SizeConfig.getScaleFactor(),
                                                                          )
                                                                  ],
                                                                ),
                                                                SizedBox(
                                                                  height: 10 *
                                                                      SizeConfig
                                                                          .getScaleFactor(),
                                                                ),
                                                              ],
                                                            ),
                                                    ),
                                                  );
                                          })),
                              // )
                            ),
                          ],
                        ))))));
  }
}

class _TextBoxWidget extends StatelessWidget {
  final String text;
  final double width;
  final double height;
  _TextBoxWidget({this.height, this.text, this.width});
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: Text(
        text,
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
      ),
    );
  }
}

class _TextBoxWidgetValue extends StatelessWidget {
  final String text;
  final double width;
  final double height;
  _TextBoxWidgetValue({this.height, this.text, this.width});
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: Text(
        text,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 16,
        ),
      ),
    );
  }
}

class _TextBoxWidgetItemsValue extends StatelessWidget {
  final String text;
  final double width;
  final double height;
  _TextBoxWidgetItemsValue({this.height, this.text, this.width});
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: Text(
        text,
      ),
    );
  }
}
