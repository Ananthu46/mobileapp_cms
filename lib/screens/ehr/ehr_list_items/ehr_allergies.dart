import 'package:doctor_portal_cms/app/size_config.dart';
import 'package:doctor_portal_cms/model/doctor/doctor_order_modal.dart';
import 'package:doctor_portal_cms/provider/doctor/doctor_order_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EHRAllergiesScreen extends StatefulWidget {
  static const routeName = 'ehr-allergy';

  @override
  _EHRAllergiesScreenState createState() => _EHRAllergiesScreenState();
}

class _EHRAllergiesScreenState extends State<EHRAllergiesScreen> {
  @override
  void initState() {
    Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .loadExistingAllergies();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
        body: Container(
            color: Theme.of(context).scaffoldBackgroundColor,
            child: SafeArea(
                child: Scaffold(
                    body: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        IconButton(
                            icon:
                                Icon(Icons.arrow_back_ios, color: Colors.pink),
                            onPressed: () {
                              Navigator.of(context).pop();
                            }),
                        Text(
                          'Health Data',
                          style: TextStyle(color: Colors.pink),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 8 * SizeConfig.getScaleFactor(),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child: Text(
                        'Allergy Recording',
                        style: TextStyle(
                            fontSize: 22, fontWeight: FontWeight.bold),
                      ),
                    ),
                    SizedBox(
                      height: 10 * SizeConfig.getScaleFactor(),
                    ),
                    Expanded(
                        child: Consumer<DrugOrderModalDoctor>(
                            builder: (ctx, allergy, _) => SingleChildScrollView(
                                child: ListView.builder(
                                    physics: ScrollPhysics(),
                                    padding: EdgeInsets.only(top: 0),
                                    shrinkWrap: true,
                                    itemCount: allergy
                                        .loadExistingAllergiesList.length,
                                    itemBuilder: (context, index) {
                                      ExistingAllergies existingAllergy =
                                          allergy
                                              .loadExistingAllergiesList[index];
                                      return allergy
                                              .loadExistingAllergiesList.isEmpty
                                          ? CircularProgressIndicator()
                                          : Card(
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      existingAllergy
                                                          .patientVisitId,
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          color: Theme.of(
                                                                  context)
                                                              .primaryColor),
                                                    ),
                                                    SizedBox(
                                                      height: 10,
                                                    ),
                                                    Row(
                                                      children: [
                                                        SizedBox(
                                                          width: 150,
                                                          child: Text(
                                                            'Allergy Category :',
                                                            style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                            ),
                                                          ),
                                                        ),
                                                        Text(existingAllergy
                                                            .allergyCategory),
                                                      ],
                                                    ),
                                                    SizedBox(
                                                      height: 5,
                                                    ),
                                                    Row(
                                                      children: [
                                                        SizedBox(
                                                          width: 150,
                                                          child: Text(
                                                            'Allergic To :',
                                                            style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                            ),
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          width: 200,
                                                          child: Text(
                                                              existingAllergy
                                                                  .allergicTo),
                                                        ),
                                                      ],
                                                    ),
                                                    SizedBox(
                                                      height: 5,
                                                    ),
                                                    Row(
                                                      children: [
                                                        SizedBox(
                                                          width: 150,
                                                          child: Text(
                                                            'Status :',
                                                            style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                            ),
                                                          ),
                                                        ),
                                                        Text(existingAllergy
                                                            .status),
                                                      ],
                                                    )
                                                  ],
                                                ),
                                              ),
                                            );
                                    }))))
                  ]),
            )))));
  }
}
