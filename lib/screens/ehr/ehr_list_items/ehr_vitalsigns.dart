import 'package:doctor_portal_cms/app/size_config.dart';
import 'package:doctor_portal_cms/model/ehr_modal/ehr_main_modal.dart';
import 'package:doctor_portal_cms/provider/ehr_provider/ehr_main_provider.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class EHRVitalsignsScreen extends StatefulWidget {
  static const routeName = 'ehr-vital';

  @override
  _EHRVitalsignsScreenState createState() => _EHRVitalsignsScreenState();
}

class _EHRVitalsignsScreenState extends State<EHRVitalsignsScreen> {
  @override
  void initState() {
    Provider.of<EHRMainProvider>(context, listen: false).loadEHRVitalSigns();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    SizeConfig().init(context);
    return Container(
        color: Theme.of(context).scaffoldBackgroundColor,
        child: SafeArea(
            child: Scaffold(
                body: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              IconButton(
                                  icon: Icon(Icons.arrow_back_ios,
                                      color: Colors.pink),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  }),
                              Text(
                                'Health Data',
                                style: TextStyle(color: Colors.pink),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 8 * SizeConfig.getScaleFactor(),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20),
                            child: Text(
                              'Vital Signs',
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.bold),
                            ),
                          ),
                          SizedBox(
                            height: 10 * SizeConfig.getScaleFactor(),
                          ),
                          Expanded(
                              child: Consumer<EHRMainProvider>(
                            builder: (context, vitalVisits, _) =>
                                ListView.builder(
                                    physics: ScrollPhysics(),
                                    padding: EdgeInsets.only(top: 0),
                                    shrinkWrap: true,
                                    itemCount:
                                        vitalVisits.loadEHRvitalvisits.length,
                                    itemBuilder: (context, index) {
                                      EHRVitalVisits ehr =
                                          vitalVisits.loadEHRvitalvisits[index];
                                      String date = DateFormat('dd-MM-yyyy')
                                          .format(
                                              DateTime.parse(ehr.enteredDate));
                                      return Column(
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: [
                                                Text(
                                                  '${ehr.visit}',
                                                  style: TextStyle(
                                                      color: Theme.of(context)
                                                          .primaryColor,
                                                      fontSize: 18,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                SizedBox(
                                                  width: 20,
                                                ),
                                                Text(
                                                  date,
                                                  style: TextStyle(
                                                      color: Colors.grey,
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            height: 5,
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Consumer<EHRMainProvider>(
                                                builder: (context, vitalSigns,
                                                        _) =>
                                                    ListView.builder(
                                                        physics:
                                                            ScrollPhysics(),
                                                        padding:
                                                            EdgeInsets.only(
                                                                top: 0),
                                                        shrinkWrap: true,
                                                        itemCount: vitalSigns
                                                            .loadEhrVitalValues
                                                            .length,
                                                        itemBuilder:
                                                            (context, i) {
                                                          EHRVitalVisitsValue
                                                              value = vitalSigns
                                                                  .loadEhrVitalValues[i];
                                                          return value.enteredTime ==
                                                                      ehr
                                                                          .enteredDate &&
                                                                  value.visitId ==
                                                                      ehr.visitId
                                                              ? Column(
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .start,
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .start,
                                                                  children: [
                                                                    Container(
                                                                      width: double
                                                                          .infinity,
                                                                      color: Colors
                                                                          .pink
                                                                          .withOpacity(
                                                                              0.2),
                                                                      child:
                                                                          Padding(
                                                                        padding:
                                                                            const EdgeInsets.all(5.0),
                                                                        child:
                                                                            Text(
                                                                          value
                                                                              .parameterValue,
                                                                          style:
                                                                              TextStyle(fontWeight: FontWeight.bold),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    SizedBox(
                                                                      height: 5,
                                                                    ),
                                                                    SizedBox(
                                                                      width:
                                                                          deviceSize.width -
                                                                              20,
                                                                      child:
                                                                          Padding(
                                                                        padding:
                                                                            const EdgeInsets.symmetric(horizontal: 5),
                                                                        child:
                                                                            Text(
                                                                          value
                                                                              .currentValue,
                                                                          style:
                                                                              TextStyle(fontWeight: FontWeight.w500),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    SizedBox(
                                                                      height: 5,
                                                                    ),
                                                                  ],
                                                                )
                                                              : Container();
                                                        })),
                                          )
                                        ],
                                      );
                                    }),
                          ))
                        ])))));
  }
}
