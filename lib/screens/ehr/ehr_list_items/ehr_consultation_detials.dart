import 'package:doctor_portal_cms/app/size_config.dart';
import 'package:doctor_portal_cms/model/ehr_modal/ehr_main_modal.dart';
import 'package:doctor_portal_cms/provider/ehr_provider/ehr_main_provider.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class EHRConsultationDetialsScreen extends StatefulWidget {
  static const routeName = 'ehr-consultation-detials';

  @override
  _EHRConsultationDetialsScreenState createState() =>
      _EHRConsultationDetialsScreenState();
}

class _EHRConsultationDetialsScreenState
    extends State<EHRConsultationDetialsScreen> {
  @override
  void initState() {
    Provider.of<EHRMainProvider>(context, listen: false).loadEHRConsultation();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    final deviceSize = MediaQuery.of(context).size;
    return Container(
        color: Theme.of(context).scaffoldBackgroundColor,
        child: SafeArea(
            child: Scaffold(
                body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    IconButton(
                        icon: Icon(Icons.arrow_back_ios, color: Colors.pink),
                        onPressed: () {
                          Navigator.of(context).pop();
                        }),
                    Text(
                      'Health Data',
                      style: TextStyle(color: Colors.pink),
                    )
                  ],
                ),
                SizedBox(
                  height: 8 * SizeConfig.getScaleFactor(),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: Text(
                    'Consultation Detials',
                    style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(
                  height: 10 * SizeConfig.getScaleFactor(),
                ),
                Expanded(
                    child: Consumer<EHRMainProvider>(
                  builder: (context, consultationDetials, _) =>
                      ListView.builder(
                          physics: ScrollPhysics(),
                          padding: EdgeInsets.only(top: 0),
                          shrinkWrap: true,
                          itemCount: consultationDetials.loadEHRVisits.length,
                          itemBuilder: (context, index) {
                            EHRConsultationVisits ehr =
                                consultationDetials.loadEHRVisits[index];
                            String date = DateFormat('dd-MM-yyyy')
                                .format(DateTime.parse(ehr.visitDate));
                            return Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        '${ehr.visit}',
                                        style: TextStyle(
                                            color:
                                                Theme.of(context).primaryColor,
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      SizedBox(
                                        width: 20,
                                      ),
                                      Text(
                                        '${date}',
                                        style: TextStyle(
                                            color:
                                                Theme.of(context).primaryColor,
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ),

                                SizedBox(
                                  height: 5,
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Consumer<EHRMainProvider>(
                                      builder: (context, consultationNotes,
                                              _) =>
                                          ListView.builder(
                                              physics: ScrollPhysics(),
                                              padding: EdgeInsets.only(top: 0),
                                              shrinkWrap: true,
                                              itemCount: consultationNotes
                                                  .loadEHRConsultations.length,
                                              itemBuilder: (context, i) {
                                                EHRConsultationNotes consult =
                                                    consultationNotes
                                                        .loadEHRConsultations[i];
                                                return consult.visitId ==
                                                        ehr.visitId
                                                    ? Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                        children: [
                                                          Container(
                                                            width:
                                                                double.infinity,
                                                            color: Colors.pink
                                                                .withOpacity(
                                                                    0.2),
                                                            child: Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                      .all(5.0),
                                                              child: Text(
                                                                consult
                                                                    .componentName,
                                                                style: TextStyle(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold),
                                                              ),
                                                            ),
                                                          ),
                                                          SizedBox(
                                                            height: 5,
                                                          ),
                                                          SizedBox(
                                                            width: deviceSize
                                                                    .width -
                                                                20,
                                                            child: Padding(
                                                              padding: const EdgeInsets
                                                                      .symmetric(
                                                                  horizontal:
                                                                      5),
                                                              child: Text(
                                                                consult
                                                                    .soaptext,
                                                                style: TextStyle(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w500),
                                                              ),
                                                            ),
                                                          ),
                                                          SizedBox(
                                                            height: 5,
                                                          ),
                                                        ],
                                                      )
                                                    : Container();
                                              })),
                                )
                                // ListView.builder(
                                //     itemBuilder: (context, i) =>
                                //         Text(ehr.consultationNotes[i].visitId))
                                // Consumer<EHRMainProvider>(
                                //     builder: (context, consultationNotes, _) =>
                                //         ListView.builder(
                                //             physics: ScrollPhysics(),
                                //             padding: EdgeInsets.only(top: 0),
                                //             shrinkWrap: true,
                                //             itemCount: consultationDetials
                                //                 .loadEHRConsultationsNote[
                                //                     consultationDetials
                                //                         .loadEHRVisits[index]
                                //                         .visitId]
                                //                 .length,
                                //             itemBuilder: (context, i) {
                                //               // EHRConsultationVisits ehr =
                                //               //     consultationDetials.loadEHRConsultationsNote.keys.toList()[index].;
                                //               // String date = DateFormat('dd-MM-yyyy')
                                //               //     .format(DateTime.parse(ehr.visitDate));
                                //               EHRConsultationNotes notes =
                                //                   consultationNotes
                                //                           .loadEHRConsultationsNote[
                                //                       consultationDetials
                                //                           .loadEHRVisits[index]
                                //                           .visitId][i];
                                //               return Row(
                                //                 children: [
                                //                   Text(notes.soaptransactionId),
                                //                   Text(consultationNotes
                                //                       .loadEHRConsultationsNote[
                                //                           consultationDetials
                                //                               .loadEHRVisits[
                                //                                   index]
                                //                               .visitId][i]
                                //                       .soaptext),
                                //                   Text(consultationNotes
                                //                       .loadEHRConsultationsNote[
                                //                           consultationDetials
                                //                               .loadEHRVisits[
                                //                                   index]
                                //                               .visitId][i]
                                //                       .componentName),
                                //                 ],
                                //               );
                                //             })),
                              ],
                            );
                          }),
                ))
              ]),
        ))));
  }
}
