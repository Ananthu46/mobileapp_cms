import 'package:doctor_portal_cms/app/size_config.dart';
import 'package:doctor_portal_cms/screens/doctor/doctors_soap_notes.dart';
import 'package:doctor_portal_cms/screens/ehr/ehr_list_items/ehr_allergies.dart';
import 'package:doctor_portal_cms/screens/ehr/ehr_list_items/ehr_consultation_detials.dart';
import 'package:doctor_portal_cms/screens/ehr/ehr_list_items/ehr_labresult.dart';
import 'package:doctor_portal_cms/screens/ehr/ehr_list_items/ehr_referrals.dart';
import 'package:doctor_portal_cms/screens/ehr/ehr_list_items/ehr_uploadDocument.dart';
import 'package:doctor_portal_cms/screens/ehr/ehr_list_items/ehr_vitalsigns.dart';
import 'package:doctor_portal_cms/widgets/inputfields/rouund_textfield_search.dart';
import 'package:flutter/material.dart';

class ElectronicHealthRecordScreen extends StatelessWidget {
  static const routeName = "electronic-health-record";
  final TextEditingController searchControllerPharmacy =
      new TextEditingController();
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      color: Theme.of(context).scaffoldBackgroundColor,
      child: SafeArea(
        child: Scaffold(
          body: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ListView(
              children: [
                Row(
                  children: [
                    IconButton(
                        icon: Icon(Icons.arrow_back_ios, color: Colors.pink),
                        onPressed: () {
                          Navigator.of(context).pop();
                        }),
                    Text(
                      'Health Data',
                      style: TextStyle(color: Colors.pink),
                    )
                  ],
                ),
                SizedBox(
                  height: 8 * SizeConfig.getScaleFactor(),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: Text(
                    'Health Records',
                    style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(
                  height: 10 * SizeConfig.getScaleFactor(),
                ),
                Container(
                  height: 50 * SizeConfig.getScaleFactor(),
                  child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: RoundTextFormFieldSearch(
                        scrollpadding: const EdgeInsets.only(bottom: 50.0),
                        isSecureInput: false,
                        controller: searchControllerPharmacy,
                        prefixIcon: Icon(Icons.search),
                        hinttext: 'Search',
                      )),
                ),
                SizedBox(
                  height: 10 * SizeConfig.getScaleFactor(),
                ),
                _ListTileItems(
                    onTap: () {
                      Navigator.of(context)
                          .pushNamed(EHRConsultationDetialsScreen.routeName);
                    },
                    text: 'Consultation Detials',
                    image: Image.asset('assets/icons/consultation_icon.png')),
                _Divider(),
                _ListTileItems(
                    onTap: () {
                      Navigator.of(context)
                          .pushNamed(EHRAllergiesScreen.routeName);
                    },
                    text: 'Allergies',
                    image: Image.asset('assets/icons/allergy_icon.png')),
                _Divider(),
                _ListTileItems(
                    onTap: () {
                      Navigator.of(context)
                          .pushNamed(EHRLabresultsScreen.routeName);
                    },
                    text: 'Lab Results',
                    image: Image.asset('assets/icons/labresults_icon.png')),
                _Divider(),
                _ListTileItems(
                    onTap: () {
                      Navigator.of(context)
                          .pushNamed(EHRVitalsignsScreen.routeName);
                    },
                    text: 'Vital Signs',
                    image: Image.asset('assets/icons/vitals_icon.png')),
                _Divider(),
                _ListTileItems(
                    onTap: () {
                      Navigator.of(context)
                          .pushNamed(EHRReferralsScreen.routeName);
                    },
                    text: 'Referrels',
                    image: Image.asset('assets/icons/referral_icon.png')),
                _Divider(),
                _ListTileItems(
                    onTap: () {
                      Navigator.of(context)
                          .pushNamed(EHRUploadDocumentScreen.routeName);
                    },
                    text: 'Upload Documents',
                    image: Image.asset('assets/icons/upload_icon.png')),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _ListTileItems extends StatelessWidget {
  final String text;
  final Widget image;
  final void Function() onTap;
  _ListTileItems({this.text, this.image, this.onTap});
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return GestureDetector(
      onTap: onTap,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 0, 5, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(
                      width: 30 * SizeConfig.getScaleFactor(),
                      child: image,
                    ),
                    SizedBox(
                      width: 20 * SizeConfig.getScaleFactor(),
                    ),
                    Text(
                      text,
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
              IconButton(
                  icon: Icon(
                    Icons.arrow_forward_ios,
                    color: Colors.black.withOpacity(0.5),
                  ),
                  onPressed: onTap)
            ],
          ),
        ],
      ),
    );

    // ListTile(
    //   isThreeLine: false,
    //   title: Text(
    //     text,
    //     style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
    //   ),
    //   trailing: Icon(
    //     Icons.arrow_forward_ios,
    //     color: Colors.grey.withOpacity(0.8),
    //   ),
    // );
  }
}

class _Divider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 50 * SizeConfig.getScaleFactor()),
      child: SizedBox(
        child: Container(
          height: 0.3,
          color: Colors.black.withOpacity(0.7),
        ),
        width: double.infinity,
      ),
    );
  }
}
