import 'dart:collection';
import 'package:doctor_portal_cms/model/drug/drug_order_api_modal.dart';
import 'package:doctor_portal_cms/provider/drug/drug_order_api_provider.dart';
import 'package:doctor_portal_cms/screens/view_orders/view_service_orders.dart';
import 'package:flutter/material.dart';
import 'package:doctor_portal_cms/app/size_config.dart';
// import 'package:doctor_portal_cms/screens/drug_order/drug_api_response.dart';
import 'package:doctor_portal_cms/widgets/inputfields/round_textformfield.dart';
import 'package:provider/provider.dart';

class DrugOrderServices extends StatefulWidget {
  final String mrn;
  final String patientId;
  final String patientName;
  final String age;
  final String gender;
  final String service;

  DrugOrderServices(
      {@required this.mrn,
      @required this.patientId,
      @required this.patientName,
      @required this.age,
      @required this.gender,
      @required this.service});
  @override
  _DrugOrderServicesState createState() => _DrugOrderServicesState();
}

class _DrugOrderServicesState extends State<DrugOrderServices> {
  String selectedBranchvalue;
  final _debouncer = Debouncer(milliseconds: 500);
  TextEditingController searchController = new TextEditingController();
  Future<void> _refreshProducts(BuildContext context) async {
    await Provider.of<DrugPatientProvider>(context, listen: false)
        .loadDrugservices(searchController.text);
  }

  HashMap<String, int> hashMap;
  HashMap<String, bool> incrementAdd;
  @override
  void initState() {
    hashMap = Provider.of<DrugPatientProvider>(context, listen: false).hashMap;
    incrementAdd =
        Provider.of<DrugPatientProvider>(context, listen: false).incrementAdd;
    searchController.addListener(() {
      _debouncer.run(() {
        if (mounted)
          Provider.of<DrugPatientProvider>(context, listen: false)
              .loadDrugservices(searchController.text);
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    searchController.dispose();
    super.dispose();
  }

  bool check = false;

  int value = 0;
  void onTapAdd(serviceName, serviceId, serviceCode) {
    Provider.of<DrugPatientProvider>(context, listen: false)
        .addServicesList(serviceName, serviceId, serviceCode, value);
    value = value++;
    if (!hashMap.containsKey(serviceName)) {
      setState(() {
        hashMap.putIfAbsent(serviceName, () => 1);
        incrementAdd.putIfAbsent(serviceName, () => true);
      });
    }
  }

  _onTapMinus(serviceName, serviceId, serviceCode, count) {
    if (count < 2) {
      setState(() {
        hashMap.removeWhere((key, value) => key == serviceName);
        incrementAdd.update(serviceName, (value) => false);
      });
      Provider.of<DrugPatientProvider>(context, listen: false)
          .onTapdelete(serviceName);
    } else {
      setState(() {
        hashMap.update(serviceName, (value) {
          setState(() {
            value--;
          });
          return value;
        });
      });
      Provider.of<DrugPatientProvider>(context, listen: false)
          .onTapMinus(serviceName, serviceId, serviceCode, value);
    }
  }

  _onTapPlus(serviceName, serviceId, serviceCode) {
    Provider.of<DrugPatientProvider>(context, listen: false)
        .addServicesList(serviceName, serviceId, serviceCode, value);
    hashMap.update(serviceName, (value) {
      setState(() {
        value++;
      });
      return value;
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: Column(
        // shrinkWrap: true,
        children: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Material(
                elevation: 8,
                shadowColor: Colors.blue,
                clipBehavior: Clip.antiAlias,
                borderRadius: const BorderRadius.all(
                  const Radius.circular(10.0),
                ),
                child: RoundTextFormFieldWidget(
                  scrollpadding: const EdgeInsets.only(bottom: 600.0),
                  isSecureInput: false,
                  controller: searchController,
                  prefixIcon: Icon(Icons.search),
                  hinttext: 'Search',
                )),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Padding(
                padding: const EdgeInsets.all(8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    SizedBox(),
                    SizedBox(
                      width: 10,
                    ),
                    RaisedButton(
                      color: Theme.of(context).primaryColor,
                      textColor: Theme.of(context).buttonColor,
                      onPressed: () {
                        Navigator.of(context).pushNamed(
                            ViewServiceOrdersScreen.routeName,
                            arguments: {
                              'mrn': '${widget.mrn}',
                              'patientID': '${widget.patientId}',
                              'patientname': '${widget.patientName}',
                              'age': '${widget.age}',
                              'gender': '${widget.gender}',
                              'service': '${widget.service}'
                            });
                      },
                      child: Text('View Selected'),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Expanded(
            child: ListView(
              children: [
                RefreshIndicator(
                  onRefresh: () => _refreshProducts(context),
                  child: Consumer<DrugPatientProvider>(
                    builder: (ctx, service, _) => SingleChildScrollView(
                      child: ListView.builder(
                          physics: ScrollPhysics(),
                          padding: EdgeInsets.only(top: 0),
                          shrinkWrap: true,
                          itemCount: service.drugServiceOrder.length,
                          itemBuilder: (context, index) {
                            DrugServiceOrderModal services =
                                service.drugServiceOrder[index];

                            return Card(
                              child: ListTile(
                                title: Text('${services.serviceName}'),
                                trailing: incrementAdd.containsKey(
                                            services.serviceName) &&
                                        incrementAdd.containsValue(true) &&
                                        hashMap[services.serviceName] != null
                                    ? Container(
                                        decoration: BoxDecoration(
                                            border:
                                                Border.all(color: Colors.grey)),
                                        height: 30,
                                        width: 85,
                                        child: Padding(
                                            padding: const EdgeInsets.all(2.0),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: [
                                                InkWell(
                                                  child: Text(
                                                    '-',
                                                    style: TextStyle(
                                                        fontSize: 25,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  onTap: () => _onTapMinus(
                                                      services.serviceName,
                                                      services.serviceId,
                                                      services.serviceCode,
                                                      hashMap[services
                                                          .serviceName]),
                                                ),
                                                Center(
                                                  child: Text(
                                                    '${hashMap[services.serviceName]}',
                                                    style: TextStyle(
                                                        fontSize: 18,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                ),
                                                InkWell(
                                                    onTap: () => _onTapPlus(
                                                        services.serviceName,
                                                        services.serviceId,
                                                        services.serviceCode),
                                                    child: Text(
                                                      '+',
                                                      style: TextStyle(
                                                          fontSize: 20,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    )),
                                              ],
                                            )))
                                    : InkWell(
                                        child: Container(
                                          decoration: BoxDecoration(
                                              border: Border.all(
                                                  color: Colors.grey)),
                                          height: 30,
                                          width: 85,
                                          child: Padding(
                                            padding: const EdgeInsets.all(2.0),
                                            child: Center(
                                                child: Text(
                                              'ADD',
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.bold),
                                            )),
                                          ),
                                        ),
                                        onTap: () => onTapAdd(
                                            services.serviceName,
                                            services.serviceId,
                                            services.serviceCode),
                                      ),
                              ),
                            );
                          }),
                    ),
                  ),
                  //   ),
                  //     ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
