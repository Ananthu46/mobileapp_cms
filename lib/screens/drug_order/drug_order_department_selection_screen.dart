// import 'package:flutter/material.dart';
// import 'package:doctor_portal_cms/app/size_config.dart';

// class DrugOrderDepartmentScreen extends StatelessWidget {
//   static const routeName = 'drug-departemt-selection';
//   @override
//   Widget build(BuildContext context) {
//     final deviceSize = MediaQuery.of(context).size;
//     return Scaffold(
//         body: Center(
//       child: Container(
//         child: Material(
//           elevation: 8,
//           shadowColor: Colors.blue,
//           clipBehavior: Clip.antiAlias,
//           borderRadius: const BorderRadius.all(
//             const Radius.circular(30.0),
//           ),
//           child: SizedBox(
//             width: deviceSize.width * 0.8,
//             height: deviceSize.height * 0.5,
//             child: Card(
//               child: Padding(
//                 padding: const EdgeInsets.all(20.0),
//                 child: Column(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   crossAxisAlignment: CrossAxisAlignment.center,
//                   children: [
//                     Text(
//                       'Please Select a department to Proceed!',
//                       style: TextStyle(
//                           color: Theme.of(context).primaryColor,
//                           fontSize: 20,
//                           fontWeight: FontWeight.bold),
//                     ),
//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       children: [
//                         Text('Branch'),
//                         SizedBox(
//                           width: 200,
//                           child: DropdownButtonFormField(
//                             decoration: InputDecoration(
//                                 contentPadding: EdgeInsets.only(left: 10)),
//                             isExpanded: true,
//                             items: [
//                               DropdownMenuItem(child: Text('')),
//                               DropdownMenuItem(child: Text('')),
//                               DropdownMenuItem(child: Text('')),
//                               DropdownMenuItem(child: Text(''))
//                             ],
//                             onChanged: (value) {},
//                           ),
//                         ),
//                       ],
//                     ),
//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       children: [
//                         Text('Department'),
//                         SizedBox(
//                           width: 200,
//                           child: DropdownButtonFormField(
//                             decoration: InputDecoration(
//                                 contentPadding: EdgeInsets.only(left: 10)),
//                             isExpanded: true,
//                             items: [
//                               DropdownMenuItem(child: Text('')),
//                               DropdownMenuItem(child: Text('')),
//                               DropdownMenuItem(child: Text('')),
//                               DropdownMenuItem(child: Text(''))
//                             ],
//                             onChanged: (value) {},
//                           ),
//                         ),
//                       ],
//                     ),
//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       children: [
//                         Text('Location'),
//                         SizedBox(
//                           width: 200,
//                           child: DropdownButtonFormField(
//                             decoration: InputDecoration(
//                                 contentPadding: EdgeInsets.only(left: 10)),
//                             isExpanded: true,
//                             items: [
//                               DropdownMenuItem(child: Text('')),
//                               DropdownMenuItem(child: Text('')),
//                               DropdownMenuItem(child: Text('')),
//                               DropdownMenuItem(child: Text(''))
//                             ],
//                             onChanged: (value) {},
//                           ),
//                         ),
//                       ],
//                     ),
//                     SizedBox(
//                       height: 20,
//                     ),
//                     FlatButton(
//                       autofocus: true,
//                       clipBehavior: Clip.hardEdge,
//                       onPressed: () {},
//                       child: Text(
//                         'Confirm',
//                         style: TextStyle(color: Colors.white),
//                       ),
//                       shape: RoundedRectangleBorder(
//                         borderRadius: BorderRadius.circular(30),
//                       ),
//                       minWidth: 250 * SizeConfig.getScaleFactor(),
//                       padding:
//                           EdgeInsets.symmetric(horizontal: 30.0, vertical: 8.0),
//                       color: Colors.blue,
//                     ),
//                   ],
//                 ),
//               ),
//             ),
//           ),
//         ),
//       ),
//     ));
//   }
// }
