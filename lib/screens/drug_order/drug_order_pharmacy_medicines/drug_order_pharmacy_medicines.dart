import 'dart:collection';
import 'package:doctor_portal_cms/model/drug/drug_order_api_modal.dart';
import 'package:doctor_portal_cms/provider/drug/drug_order_api_provider.dart';
import 'package:doctor_portal_cms/screens/previous_orders/pharmacy_previous_order_screen.dart';
import 'package:doctor_portal_cms/screens/view_orders/view_pharmacy_orders.dart';
import 'package:doctor_portal_cms/widgets/expansion_widget/expandOntrail_expansionWidget.dart';
import 'package:doctor_portal_cms/widgets/inputfields/round_textformfield.dart';
import 'package:doctor_portal_cms/widgets/inputfields/squareTextField.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:provider/provider.dart';

class DrugOrderPharmacyMedicineScreen extends StatefulWidget {
  final String mrn;
  final String patientId;
  final String patientName;
  final String age;
  final String gender;
  final String pharmacy;
  DrugOrderPharmacyMedicineScreen(
      {@required this.mrn,
      @required this.patientId,
      @required this.patientName,
      @required this.age,
      @required this.gender,
      @required this.pharmacy});
  @override
  _DrugOrderPharmacyMedicineScreenState createState() =>
      _DrugOrderPharmacyMedicineScreenState();
}

class _DrugOrderPharmacyMedicineScreenState
    extends State<DrugOrderPharmacyMedicineScreen> {
  final GlobalKey expansionTile = new GlobalKey();
  String selectedBranchvalue;
  PharmacyStores selectedstore;
  PharmacyMedicineFrequency selectedFrequency;
  // String selectedStoreId;
  int selectedFrequencyId;
  double selectedFrequencyValue;
  bool tabletSelected = false;
  bool capsuleSelected = false;
  bool syrupselected = false;
  bool dropSelected = false;
  bool suppositorySelected = false;
  bool injectionSelected = false;
  String noOfDays = '0';
  double totalqty = 0;
  TextEditingController searchControllerPharmacy = new TextEditingController();
  TextEditingController textController = new TextEditingController();
  Future<void> _refreshProducts(BuildContext context) async {
    await Provider.of<DrugPatientProvider>(context, listen: false)
        .loadPharmacyMedicines(
            selectedstore.storeId, searchControllerPharmacy.text);
  }

  Map<String, PreviousPharmacyOrderModal> drugPharmacyListprovider;

  @override
  void initState() {
    Provider.of<DrugPatientProvider>(context, listen: false)
        .clearPharmacyitems();
    var deptDetials = Provider.of<DrugPatientProvider>(context, listen: false)
        .selectedDepartmentdetials;

    print(deptDetials);
    print(deptDetials.isEmpty);
    if (deptDetials.isEmpty) {
      Provider.of<DrugPatientProvider>(context, listen: false)
          .loadLocIdstores();
    } else {
      Provider.of<DrugPatientProvider>(context, listen: false).loadstores();
    }

    Provider.of<DrugPatientProvider>(context, listen: false).loadFrequency();
    drugPharmacyListprovider =
        Provider.of<DrugPatientProvider>(context, listen: false)
            .drugpharmacylist;
    pharmacynameMap = Provider.of<DrugPatientProvider>(context, listen: false)
        .pharmacynameMap;
    noOfDaysPharmacy = Provider.of<DrugPatientProvider>(context, listen: false)
        .noOfDaysPharmacy;
    incrementAddPharmacy =
        Provider.of<DrugPatientProvider>(context, listen: false)
            .incrementAddPharmacy;
    pharmacyGenericName =
        Provider.of<DrugPatientProvider>(context, listen: false)
            .pharmacyGenericName;
    pharmacyFrequencyMap =
        Provider.of<DrugPatientProvider>(context, listen: false)
            .pharmacyFrequencyMap;
    pharmacyFrequencyID =
        Provider.of<DrugPatientProvider>(context, listen: false)
            .pharmacyFrequencyID;
    totalquantitypharmacy =
        Provider.of<DrugPatientProvider>(context, listen: false)
            .totalquantitypharmacy;
    selectedStoreData();
    searchControllerPharmacy.addListener(() async {
      await Provider.of<DrugPatientProvider>(context, listen: false)
          .loadPharmacyMedicines(
              selectedstore.storeId, searchControllerPharmacy.text);
    });
    super.initState();
  }

  selectedStoreData() async {
    await Future.delayed(Duration(milliseconds: 500), () {
      if (mounted)
        setState(() {
          selectedstore =
              Provider.of<DrugPatientProvider>(context, listen: false)
                  .pharmacyStore[0];
        });
    });
  }

  bool check = false;
  HashMap<String, int> pharmacynameMap;
  HashMap<String, bool> incrementAddPharmacy;
  HashMap<String, String> pharmacyGenericName;
  HashMap<String, String> pharmacyFrequencyMap;
  HashMap<String, int> pharmacyFrequencyID;
  HashMap<String, int> noOfDaysPharmacy;
  HashMap<String, double> totalquantitypharmacy;

  int selectedDays = 0;
  void onTapAdd(PharmacyMedicinesModal pharmacy, selectedFrequencyName,
      frequencyId, noOfDays, total) {
    Provider.of<DrugPatientProvider>(context, listen: false).addPharmacyList(
      selectedstore.storeId,
      pharmacy,
      '',
      selectedFrequencyName,
      frequencyId,
      noOfDays,
      total,
    );
    if (!pharmacynameMap.containsKey(pharmacy.itemname)) {
      setState(() {
        pharmacynameMap.putIfAbsent(pharmacy.itemname, () => 1);
        pharmacyGenericName.putIfAbsent(
            pharmacy.itemname, () => pharmacy.genericName);
        incrementAddPharmacy.putIfAbsent(pharmacy.itemname, () => true);
      });
    }
  }

  bool _onLongPressAdd = false;
  bool _onLongPressSubract = false;
  onAddNoOfdays(
    PharmacyMedicinesModal pharmacy,
  ) {
    if (!noOfDaysPharmacy.containsKey(pharmacy.itemname)) {
      setState(() {
        pharmacynameMap.putIfAbsent(pharmacy.itemname, () => 1);
        incrementAddPharmacy.putIfAbsent(pharmacy.itemname, () => true);
        noOfDaysPharmacy.putIfAbsent(pharmacy.itemname, () => 1);
      });
      Provider.of<DrugPatientProvider>(context, listen: false)
          .addNoOfDaysPharmacy(pharmacy, 1, selectedstore.storeId);
    } else {
      noOfDaysPharmacy.update(pharmacy.itemname, (selectedDays) {
        setState(() {
          selectedDays++;
        });
        Provider.of<DrugPatientProvider>(context, listen: false)
            .addNoOfDaysPharmacy(pharmacy, selectedDays, selectedstore.storeId);
        return selectedDays;
      });
    }
  }

  onAddNoOfDaysByKeyboard(PharmacyMedicinesModal pharmacy, noOfDays) {
    if (noOfDays != null) {
      if (!noOfDaysPharmacy.containsKey(pharmacy.itemname)) {
        setState(() {
          pharmacynameMap.putIfAbsent(pharmacy.itemname, () => noOfDays);
          incrementAddPharmacy.putIfAbsent(pharmacy.itemname, () => true);
          noOfDaysPharmacy.putIfAbsent(pharmacy.itemname, () => 1);
        });
        Provider.of<DrugPatientProvider>(context, listen: false)
            .addNoOfDaysPharmacy(pharmacy, noOfDays, selectedstore.storeId);
      } else {
        noOfDaysPharmacy.update(pharmacy.itemname, (selectedDays) {
          setState(() {
            selectedDays = noOfDays;
          });
          Provider.of<DrugPatientProvider>(context, listen: false)
              .addNoOfDaysPharmacy(
                  pharmacy, selectedDays, selectedstore.storeId);
          return selectedDays;
        });
      }
    }
  }

  onSubractNoOfDays(pharmacyName, count) {
    if (count == null) {
      return;
    } else if (count < 1) {
      setState(() {
        noOfDaysPharmacy.update(pharmacyName, (selectedDays) {
          setState(() {
            selectedDays = selectedDays;
          });
          return selectedDays;
        });
      });
    } else {
      setState(() {
        noOfDaysPharmacy.update(
            pharmacyName, (selectedDays) => selectedDays - 1);
      });
    }
  }

  onChangeFrequency(PharmacyMedicinesModal pharmacy, selectedFrequencyName,
      frequencyId, noofdays, total) {
    Provider.of<DrugPatientProvider>(context, listen: false).addPharmacyList(
      selectedstore.storeId,
      pharmacy,
      '',
      selectedFrequencyName,
      frequencyId,
      noofdays,
      total,
    );
    if (!pharmacyFrequencyID.containsKey(pharmacy.itemname)) {
      pharmacyFrequencyID.putIfAbsent(pharmacy.itemname, () => frequencyId);
      pharmacyFrequencyMap.putIfAbsent(
          pharmacy.itemname, () => selectedFrequencyName);
    }
  }

  onChangeTotalQuantity(PharmacyMedicinesModal pharmacy, nodays, freq) {
    double _newQty;

    if (!totalquantitypharmacy.containsKey(pharmacy.itemname)) {
      if (nodays == null && freq == null) {
        _newQty = 0;
      } else if (nodays == null) {
        _newQty = freq.toDouble();
      } else if (freq == null) {
        _newQty = 0;
      } else {
        setState(() {
          _newQty = freq * nodays;
        });
      }
      totalquantitypharmacy.putIfAbsent(pharmacy.itemname, () => _newQty);
    } else {
      if (nodays == null && freq == null) {
        _newQty = 0;
      } else if (nodays == null) {
        _newQty = freq.toDouble();
      } else if (freq == null) {
        _newQty = 0;
      } else {
        setState(() {
          _newQty = freq * nodays;
        });
      }
      totalquantitypharmacy.update(pharmacy.itemname, (value) {
        setState(() {
          value = _newQty;
        });
        return value;
      });
    }
    Provider.of<DrugPatientProvider>(context, listen: false)
        .addtotalQtyPharmacy(
      pharmacy,
      _newQty,
      selectedstore.storeId,
    );
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Container(
      color: Theme.of(context).primaryColor,
      child: SafeArea(
        child: Scaffold(
          body: Column(
            children: [
              Container(
                  height: 50,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Text('Store :'),
                            SizedBox(
                              width: 20,
                            ),
                            Container(
                              width: deviceSize.width * 0.7,
                              child: Consumer<DrugPatientProvider>(
                                builder: (ctx, stores, _) => stores
                                                .pharmacyStore.length <
                                            1 ||
                                        selectedstore == null
                                    ? LinearProgressIndicator()
                                    : DropdownButtonFormField<PharmacyStores>(
                                        hint: Text('--Select--'),
                                        decoration: InputDecoration(
                                          enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .primaryColor,
                                                width: 2),
                                          ),
                                          errorBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .primaryColor,
                                                width: 2),
                                          ),
                                          disabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .primaryColor,
                                                width: 2),
                                          ),
                                          contentPadding:
                                              EdgeInsets.only(left: 10),
                                          errorStyle:
                                              TextStyle(color: Colors.red),
                                        ),
                                        isExpanded: true,
                                        value: selectedstore,
                                        onChanged: (PharmacyStores newValue) {
                                          setState(() {
                                            selectedstore = newValue;
                                          });
                                        },
                                        items: stores.pharmacyStore
                                            .map((storepharmacy) {
                                          // selectedStoreId =
                                          //     selectedstore.storeId;

                                          return DropdownMenuItem(
                                            child: new Text(
                                                storepharmacy.storeName),
                                            value: storepharmacy,
                                          );
                                        }).toList(),
                                      ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  )),
              Container(
                  height: 50,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          RaisedButton(
                            color: Theme.of(context).primaryColor,
                            textColor: Theme.of(context).buttonColor,
                            onPressed: () {
                              Navigator.of(context).pushNamed(
                                  PreviousOrderScreen.routeName,
                                  arguments: {
                                    'mrn': '${widget.mrn}',
                                    'patientID': '${widget.patientId}',
                                    'patientname': '${widget.patientName}',
                                    'age': '${widget.age}',
                                    'gender': '${widget.gender}',
                                    'pharmacy': '${widget.pharmacy}',
                                    'store': '${selectedstore.storeId}'
                                  });
                            },
                            child: Text('Previous Orders'),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          RaisedButton(
                            color: Theme.of(context).primaryColor,
                            textColor: Theme.of(context).buttonColor,
                            onPressed: () {
                              Navigator.of(context).pushNamed(
                                  ViewPharmacyOrdersScreen.routeName,
                                  arguments: {
                                    'mrn': '${widget.mrn}',
                                    'patientID': '${widget.patientId}',
                                    'patientname': '${widget.patientName}',
                                    'age': '${widget.age}',
                                    'gender': '${widget.gender}',
                                    'pharmacy': '${widget.pharmacy}'
                                  });
                            },
                            child: Text('View Selected'),
                          ),
                        ]),
                  )),
              Container(
                height: 60,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Material(
                      elevation: 8,
                      shadowColor: Colors.blue,
                      clipBehavior: Clip.antiAlias,
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(10.0),
                      ),
                      child: RoundTextFormFieldWidget(
                        scrollpadding: const EdgeInsets.only(bottom: 600.0),
                        isSecureInput: false,
                        controller: searchControllerPharmacy,
                        prefixIcon: Icon(Icons.search),
                        hinttext: 'Search',
                      )),
                ),
              ),
              Expanded(
                child: ListView(children: [
                  RefreshIndicator(
                    onRefresh: () => _refreshProducts(context),
                    child: Consumer<DrugPatientProvider>(
                      builder: (ctx, pharmacyitems, _) => SingleChildScrollView(
                        child: ListView.builder(
                            physics: ScrollPhysics(),
                            padding: EdgeInsets.only(top: 0),
                            shrinkWrap: true,
                            itemCount: pharmacyitems.pharmacyMedicines.length,
                            itemBuilder: (context, index) {
                              PharmacyMedicinesModal pharmacy =
                                  pharmacyitems.pharmacyMedicines[index];
                              return Card(
                                child: ExpansionTileOnButton(
                                  headerBackgroundColor:
                                      Theme.of(context).scaffoldBackgroundColor,
                                  title: Text('${pharmacy.itemname}'),
                                  itemName: pharmacy.itemname,
                                  pharmacy: pharmacy,
                                  selectedStore: selectedstore,
                                  children: [
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Wrap(
                                      direction: Axis.horizontal,
                                      crossAxisAlignment:
                                          WrapCrossAlignment.start,
                                      runAlignment: WrapAlignment.center,
                                      alignment: WrapAlignment.start,
                                      spacing: 5,
                                      runSpacing: 5,
                                      children: [
                                        Container(
                                          width: double.infinity,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(10),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              Text(
                                                'Freq: ',
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                              SizedBox(
                                                width: 10,
                                              ),
                                              SizedBox(
                                                height: 30,
                                                width: 150,
                                                child: pharmacyFrequencyMap[
                                                            pharmacy
                                                                .itemname] ==
                                                        null
                                                    ? Consumer<
                                                        DrugPatientProvider>(
                                                        builder: (ctx,
                                                                frequency, _) =>
                                                            DropdownButtonFormField(
                                                          hint: Text(
                                                              '--Select--'),
                                                          decoration:
                                                              InputDecoration(
                                                            enabledBorder:
                                                                OutlineInputBorder(
                                                              borderSide: BorderSide(
                                                                  color: Theme.of(
                                                                          context)
                                                                      .primaryColor,
                                                                  width: 2),
                                                            ),
                                                            errorBorder:
                                                                OutlineInputBorder(
                                                              borderSide: BorderSide(
                                                                  color: Theme.of(
                                                                          context)
                                                                      .primaryColor,
                                                                  width: 2),
                                                            ),
                                                            disabledBorder:
                                                                OutlineInputBorder(
                                                              borderSide: BorderSide(
                                                                  color: Theme.of(
                                                                          context)
                                                                      .primaryColor,
                                                                  width: 2),
                                                            ),
                                                            contentPadding:
                                                                EdgeInsets.only(
                                                                    left: 10),
                                                            errorStyle:
                                                                TextStyle(
                                                                    color: Colors
                                                                        .red),
                                                          ),
                                                          isExpanded: true,
                                                          // value:
                                                          //     '',
                                                          onChanged:
                                                              (newValue) {
                                                            setState(() {
                                                              pharmacyFrequencyMap[
                                                                      pharmacy
                                                                          .itemname] =
                                                                  newValue;
                                                              onChangeFrequency(
                                                                  pharmacy,
                                                                  pharmacyFrequencyMap[
                                                                      pharmacy
                                                                          .itemname],
                                                                  selectedFrequencyId,
                                                                  noOfDaysPharmacy[
                                                                      pharmacy
                                                                          .itemname],
                                                                  totalquantitypharmacy[
                                                                      pharmacy
                                                                          .itemname]);
                                                              onTapAdd(
                                                                  pharmacy,
                                                                  pharmacyFrequencyMap[
                                                                      pharmacy
                                                                          .itemname],
                                                                  selectedFrequencyId,
                                                                  noOfDaysPharmacy[
                                                                      pharmacy
                                                                          .itemname],
                                                                  totalquantitypharmacy[
                                                                      pharmacy
                                                                          .itemname]);
                                                              onChangeTotalQuantity(
                                                                  pharmacy,
                                                                  noOfDaysPharmacy[
                                                                      pharmacy
                                                                          .itemname],
                                                                  selectedFrequencyValue);
                                                            });
                                                          },
                                                          items: frequency
                                                              .pharmacyfrequency
                                                              .map(
                                                                  (frequncyList) {
                                                            return DropdownMenuItem(
                                                              child: new Text(
                                                                  frequncyList
                                                                      .frequencyName),
                                                              value: frequncyList
                                                                  .frequencyName,
                                                              onTap: () {
                                                                setState(() {
                                                                  selectedFrequencyId =
                                                                      frequncyList
                                                                          .frequencyId;
                                                                  selectedFrequencyValue =
                                                                      frequncyList
                                                                          .frequencyValue;
                                                                });
                                                              },
                                                            );
                                                          }).toList(),
                                                          // validator: (value) {
                                                          //             if (value == null) {
                                                          //                _showErrorDialog();
                                                          //             }
                                                          //             return null;
                                                          //           },
                                                        ),
                                                      )
                                                    : Consumer<
                                                        DrugPatientProvider>(
                                                        builder: (ctx,
                                                                frequency, _) =>
                                                            DropdownButtonFormField(
                                                          hint: Text(
                                                              '--Select--'),
                                                          decoration:
                                                              InputDecoration(
                                                            enabledBorder:
                                                                OutlineInputBorder(
                                                              borderSide: BorderSide(
                                                                  color: Theme.of(
                                                                          context)
                                                                      .primaryColor,
                                                                  width: 2),
                                                            ),
                                                            errorBorder:
                                                                OutlineInputBorder(
                                                              borderSide: BorderSide(
                                                                  color: Theme.of(
                                                                          context)
                                                                      .primaryColor,
                                                                  width: 2),
                                                            ),
                                                            disabledBorder:
                                                                OutlineInputBorder(
                                                              borderSide: BorderSide(
                                                                  color: Theme.of(
                                                                          context)
                                                                      .primaryColor,
                                                                  width: 2),
                                                            ),
                                                            contentPadding:
                                                                EdgeInsets.only(
                                                                    left: 10),
                                                            errorStyle:
                                                                TextStyle(
                                                                    color: Colors
                                                                        .red),
                                                          ),
                                                          isExpanded: true,
                                                          value:
                                                              pharmacyFrequencyMap[
                                                                  pharmacy
                                                                      .itemname],
                                                          onChanged:
                                                              (newValue) {
                                                            setState(() {
                                                              pharmacyFrequencyMap[
                                                                      pharmacy
                                                                          .itemname] =
                                                                  newValue;
                                                              // selectedFrequency =
                                                              //     newValue;
                                                              onChangeTotalQuantity(
                                                                  pharmacy,
                                                                  noOfDaysPharmacy[
                                                                      pharmacy
                                                                          .itemname],
                                                                  selectedFrequencyValue);
                                                              onTapAdd(
                                                                  pharmacy,
                                                                  pharmacyFrequencyMap[
                                                                      pharmacy
                                                                          .itemname],
                                                                  selectedFrequencyId,
                                                                  noOfDaysPharmacy[
                                                                      pharmacy
                                                                          .itemname],
                                                                  totalquantitypharmacy[
                                                                      pharmacy
                                                                          .itemname]);
                                                            });
                                                          },
                                                          items: frequency
                                                              .pharmacyfrequency
                                                              .map(
                                                                  (frequncyList) {
                                                            return DropdownMenuItem(
                                                                child: new Text(
                                                                    frequncyList
                                                                        .frequencyName),
                                                                value: frequncyList
                                                                    .frequencyName,
                                                                onTap: () {
                                                                  setState(() {
                                                                    selectedFrequencyId =
                                                                        frequncyList
                                                                            .frequencyId;
                                                                    selectedFrequencyValue =
                                                                        frequncyList
                                                                            .frequencyValue;
                                                                  });
                                                                });
                                                          }).toList(),
                                                        ),
                                                      ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        SizedBox(
                                          width: 25,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 10),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              Text('No.Days :',
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold)),
                                              IconButton(
                                                icon: Icon(Ionicons
                                                    .md_remove_circle_outline),
                                                onPressed: () {
                                                  onSubractNoOfDays(
                                                      pharmacy.itemname,
                                                      noOfDaysPharmacy[
                                                          pharmacy.itemname]);
                                                  onChangeTotalQuantity(
                                                      pharmacy,
                                                      noOfDaysPharmacy[
                                                          pharmacy.itemname],
                                                      selectedFrequencyValue);
                                                },
                                              ),
                                              SizedBox(
                                                  width: 50,
                                                  height: 35,
                                                  child: SquareTextField(
                                                    text: noOfDaysPharmacy[pharmacy
                                                                    .itemname] ==
                                                                null ||
                                                            noOfDaysPharmacy[
                                                                    pharmacy
                                                                        .itemname] ==
                                                                0
                                                        ? noOfDays
                                                        : noOfDaysPharmacy[
                                                                pharmacy
                                                                    .itemname]
                                                            .toString(),
                                                    keyboardType:
                                                        TextInputType.number,
                                                    onchanged: (value) {
                                                      TextSelection.collapsed(
                                                          offset:
                                                              value.length + 1);
                                                      setState(() {
                                                        if (value.isNotEmpty) {
                                                          onAddNoOfDaysByKeyboard(
                                                              pharmacy,
                                                              int.parse(value));

                                                          onChangeTotalQuantity(
                                                              pharmacy,
                                                              int.parse(value),
                                                              selectedFrequencyValue);
                                                        }
                                                      });
                                                    },
                                                  )),
                                              IconButton(
                                                icon: Icon(Ionicons
                                                    .md_add_circle_outline),
                                                onPressed: () {
                                                  onAddNoOfdays(
                                                    pharmacy,
                                                  );
                                                  onChangeTotalQuantity(
                                                      pharmacy,
                                                      noOfDaysPharmacy[
                                                          pharmacy.itemname],
                                                      selectedFrequencyValue);
                                                },
                                              ),
                                            ],
                                          ),
                                        ),
                                        SizedBox(
                                          width: 25,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 10),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    children: [
                                                      Text('Total Qty :',
                                                          style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold)),
                                                      SizedBox(
                                                        width: 10,
                                                      ),
                                                      totalquantitypharmacy[
                                                                  pharmacy
                                                                      .itemname] ==
                                                              null
                                                          ? Text('0')
                                                          : Text(
                                                              '${totalquantitypharmacy[pharmacy.itemname]}'),
                                                    ],
                                                  )),
                                              SizedBox(
                                                width: 5,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                  ],
                                ),
                              );
                            }),
                      ),
                    ),
                    //     ),
                  ),
                ]),
              )
            ],
          ),
        ),
      ),
    );
  }
}
//
