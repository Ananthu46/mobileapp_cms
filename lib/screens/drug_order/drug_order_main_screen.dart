import 'package:doctor_portal_cms/screens/drug_order/drug_order_pharmacy_medicines/drug_order_pharmacy_medicines.dart';
import 'package:doctor_portal_cms/screens/nursing_orders/nusing_orders_screen.dart';
import 'package:flutter/material.dart';
import 'package:doctor_portal_cms/app/size_config.dart';
import 'package:doctor_portal_cms/screens/drug_order/drug_order_services.dart';
import 'package:flutter_icons/flutter_icons.dart';

class DrugOrderTabBar extends StatelessWidget {
  static const routeName = 'drug-tab';
  @override
  Widget build(BuildContext context) {
    // final deviceSize = MediaQuery.of(context).size;
    SizeConfig().init(context);
    Map arguments = ModalRoute.of(context).settings.arguments as Map;
    return Container(
        color: Theme.of(context).primaryColor,
        child: SafeArea(
          child: Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: AppBar(
              title: Text('Orders'),
              centerTitle: true,
            ),
            body: ListView(children: [
              Card(
                elevation: 4,
                child: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'MRNO: ${arguments['mrn']}',
                        style: TextStyle(),
                      ),
                      ListTile(
                        onTap: () {},
                        leading: CircleAvatar(
                          backgroundColor: Colors.black,
                          backgroundImage:
                              AssetImage('assets/images/local_image/dummy.jpg'),
                        ),
                        title: Text(
                          '${arguments['patientname']}',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        subtitle: Wrap(
                          direction: Axis.vertical,
                          children: [
                            Text(
                              '${arguments['age']} Years',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              '${arguments['gender']}',
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            // Text(
                            //   '${arguments['service']}',
                            // ),
                          ],
                        ),
                        trailing: IconButton(
                          icon: Icon(
                            FontAwesome5Solid.notes_medical,
                            color: Theme.of(context).primaryColor,
                          ),
                          onPressed: () {
                            Navigator.of(context).pushNamed(
                                NursingOrdersScreen.routeName,
                                arguments: {
                                  'mrn': '${arguments['mrn']}',
                                  'patientID': '${arguments['patientID']}',
                                  'patientname': '${arguments['patientname']}',
                                  'age': '${arguments['age']}',
                                  'gender': '${arguments['gender']}',
                                  'service': '${arguments['service']}',
                                  'visitid': '${arguments['visitid']}',
                                });
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              DefaultTabController(
                length: 2,
                initialIndex: 0,
                child: Column(children: [
                  Container(
                    width: double.infinity,
                    color: Theme.of(context).primaryColor,
                    child: TabBar(
                      isScrollable: false,
                      tabs: [
                        Tab(
                          text: 'Services',
                        ),
                        Tab(
                          text: 'Pharmacy',
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 590 * SizeConfig.getScaleFactor(),
                    child: TabBarView(
                      children: [
                        DrugOrderServices(
                          mrn: arguments['mrn'],
                          patientId: arguments['patientID'],
                          patientName: arguments['patientname'],
                          age: arguments['age'],
                          gender: arguments['gender'],
                          service: arguments['service'],
                        ),
                        DrugOrderPharmacyMedicineScreen(
                          mrn: arguments['mrn'],
                          patientId: arguments['patientID'],
                          patientName: arguments['patientname'],
                          age: arguments['age'],
                          gender: arguments['gender'],
                          pharmacy: arguments['service'],
                        ),
                      ],
                    ),
                  ),
                ]),
              ),
            ]),
          ),
        ));
  }
}
