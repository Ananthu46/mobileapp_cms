// import 'package:doctor_portal_cms/provider/drug/drug_order_api_provider.dart';
// import 'package:doctor_portal_cms/widgets/inputfields/round_textformfield_copy.dart';
// import 'package:flutter/material.dart';
// import 'package:doctor_portal_cms/screens/drug_order/drug_order_main_screen.dart';
// import 'package:provider/provider.dart';

// class DrugOrderPatientScreen extends StatefulWidget {
//   static const routeName = 'drugorder';
//   final String location;
//   DrugOrderPatientScreen({this.location});

//   @override
//   _DrugOrderPatientScreenState createState() => _DrugOrderPatientScreenState();
// }

// class _DrugOrderPatientScreenState extends State<DrugOrderPatientScreen> {
//   // bool _isLoading = false;
//   TextEditingController searchControllerPharmacy = new TextEditingController();
//   // Future<void> _refreshProducts(BuildContext context) async {
//   //   await Provider.of<DrugOrderProvider>(context, listen: false).loadpatients();
//   // }

//   Future<void> _refreshProducts(BuildContext context) async {
//     await Provider.of<DrugPatientProvider>(context, listen: false)
//         .loadIPpatientsApi(widget.location);
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         resizeToAvoidBottomInset: false,
//         appBar: AppBar(
//           title: Text('Patient List'),
//           centerTitle: true,
//         ),
//         body: RefreshIndicator(
//           onRefresh: () => _refreshProducts(context),
//           child: ListView(
//             children: [
//               Padding(
//                 padding: const EdgeInsets.all(8.0),
//                 child: Material(
//                     elevation: 8,
//                     shadowColor: Colors.blue,
//                     clipBehavior: Clip.antiAlias,
//                     borderRadius: const BorderRadius.all(
//                       const Radius.circular(30.0),
//                     ),
//                     child: RoundTextFormFieldWidget(
//                       isSecureInput: false,
//                       controller: searchControllerPharmacy,
//                       prefixIcon: Icon(Icons.search),
//                       hinttext: 'Search',
//                     )),
//               ),
//               RefreshIndicator(
//                   onRefresh: () => _refreshProducts(context),
//                   child: FutureBuilder(
//                       future: _refreshProducts(context),
//                       builder: (ctx, snapshot) => snapshot.connectionState ==
//                               ConnectionState.waiting
//                           ? Center(
//                               child: CircularProgressIndicator(),
//                             )
//                           : RefreshIndicator(
//                               onRefresh: () => _refreshProducts(context),
//                               child: Consumer<DrugPatientProvider>(
//                                 builder: (ctx, inpatients, _) =>
//                                     ListView.builder(
//                                   physics: ScrollPhysics(),
//                                   padding: EdgeInsets.only(top: 0),
//                                   shrinkWrap: true,
//                                   itemCount: inpatients.loadIPPatients.length,
//                                   itemBuilder: (context, index) {
//                                     return Card(
//                                       elevation: 4,
//                                       child: Padding(
//                                         padding: const EdgeInsets.all(5.0),
//                                         child: Column(
//                                           mainAxisAlignment:
//                                               MainAxisAlignment.center,
//                                           crossAxisAlignment:
//                                               CrossAxisAlignment.start,
//                                           children: [
//                                             Text(
//                                               'MRNO: ${inpatients.loadIPPatients[index].mrn}',
//                                             ),
//                                             ListTile(
//                                               onTap: () {
//                                                 Navigator.of(context).pushNamed(
//                                                     DrugOrderTabBar.routeName,
//                                                     arguments: {
//                                                       'mrn':
//                                                           '${inpatients.loadIPPatients[index].mrn}',
//                                                       'patientID':
//                                                           '${inpatients.loadIPPatients[index].patientid}',
//                                                       'patientname':
//                                                           '${inpatients.loadIPPatients[index].patientname}',
//                                                       'age':
//                                                           '${inpatients.loadIPPatients[index].age}',
//                                                       'dept':
//                                                           '${inpatients.loadIPPatients[index].ward}',
//                                                       'service':
//                                                           '${inpatients.loadIPPatients[index].admittingdoctor}',
//                                                       'visitid':
//                                                           '${inpatients.loadIPPatients[index].visitid}',
//                                                       'gender':
//                                                           '${inpatients.loadIPPatients[index].gender}',
//                                                     });
//                                                 Provider.of<DrugPatientProvider>(
//                                                         context,
//                                                         listen: false)
//                                                     .addSelectedpatientsDetials(
//                                                         inpatients
//                                                             .loadIPPatients[
//                                                                 index]
//                                                             .patientid,
//                                                         inpatients
//                                                             .loadIPPatients[
//                                                                 index]
//                                                             .visitid);
//                                               },
//                                               leading: CircleAvatar(
//                                                 backgroundColor: Colors.black,
//                                                 backgroundImage: AssetImage(
//                                                     'assets/images/local_image/dummy.jpg'),
//                                               ),
//                                               title: Text(
//                                                 '${inpatients.loadIPPatients[index].patientname}',
//                                                 style: TextStyle(
//                                                     fontWeight:
//                                                         FontWeight.bold),
//                                               ),
//                                               subtitle: Wrap(
//                                                 direction: Axis.vertical,
//                                                 children: [
//                                                   Text(
//                                                     '${inpatients.loadIPPatients[index].age} Years',
//                                                     style: TextStyle(
//                                                         fontWeight:
//                                                             FontWeight.bold),
//                                                   ),
//                                                   SizedBox(
//                                                     width: 10,
//                                                   ),
//                                                   Row(
//                                                     children: [
//                                                       Text(
//                                                         '${inpatients.loadIPPatients[index].roomno}'
//                                                         ' - '
//                                                         '${inpatients.loadIPPatients[index].ward}',
//                                                       ),
//                                                     ],
//                                                   ),
//                                                   SizedBox(
//                                                     width: 10,
//                                                   ),
//                                                   Text(
//                                                     '${inpatients.loadIPPatients[index].admittingdoctor}',
//                                                   ),
//                                                 ],
//                                               ),
//                                             ),
//                                           ],
//                                         ),
//                                       ),
//                                     );
//                                   },
//                                 ),
//                               ),
//                             ))),
//             ],
//           ),
//         ));
//   }
// }
