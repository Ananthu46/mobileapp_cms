import 'dart:collection';
import 'package:doctor_portal_cms/model/drug/drug_order_api_modal.dart';
import 'package:doctor_portal_cms/provider/drug/drug_order_api_provider.dart';
import 'package:doctor_portal_cms/widgets/buttons/round_flat_button.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';

class ViewPharmacyOrdersScreen extends StatefulWidget {
  static const routeName = 'view-pharmacy-orders';

  @override
  _ViewPharmacyOrdersScreenState createState() =>
      _ViewPharmacyOrdersScreenState();
}

class _ViewPharmacyOrdersScreenState extends State<ViewPharmacyOrdersScreen> {
  int selectedFrequencyId;
  double selectedFrequencyValue;
  bool _isLoading = false;
  _submit(context) async {
    setState(() {
      _isLoading = true;
    });
    await Provider.of<DrugPatientProvider>(context, listen: false)
        .sendPharmacyOrders(context);
    String resp = Provider.of<DrugPatientProvider>(context, listen: false)
        .responsedataAPI[0]
        .resp;
    if (resp == "Saved") {
      Provider.of<DrugPatientProvider>(context, listen: false)
          .clearallPharmacy();
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          backgroundColor: Colors.blue,
          content: const Text('Pharmacy Orders Saved'),
        ),
      );
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          backgroundColor: Colors.red,
          content: const Text('Pharmacy Not Orders Saved'),
        ),
      );
    }

    setState(() {
      _isLoading = false;
    });
  }

  HashMap<String, int> pharmacynameMap;

  HashMap<String, bool> incrementAddPharmacy;

  HashMap<String, String> pharmacyGenericName;

  HashMap<String, String> pharmacyFrequencyMap;

  HashMap<String, int> pharmacyFrequencyID;

  HashMap<String, int> noOfDaysPharmacy;

  HashMap<String, double> totalquantitypharmacy;
  @override
  void initState() {
    pharmacynameMap = Provider.of<DrugPatientProvider>(context, listen: false)
        .pharmacynameMap;
    incrementAddPharmacy =
        Provider.of<DrugPatientProvider>(context, listen: false)
            .incrementAddPharmacy;
    pharmacyGenericName =
        Provider.of<DrugPatientProvider>(context, listen: false)
            .pharmacyGenericName;
    pharmacyFrequencyMap =
        Provider.of<DrugPatientProvider>(context, listen: false)
            .pharmacyFrequencyMap;
    pharmacyFrequencyID =
        Provider.of<DrugPatientProvider>(context, listen: false)
            .pharmacyFrequencyID;
    noOfDaysPharmacy = Provider.of<DrugPatientProvider>(context, listen: false)
        .noOfDaysPharmacy;
    totalquantitypharmacy =
        Provider.of<DrugPatientProvider>(context, listen: false)
            .totalquantitypharmacy;
    super.initState();
  }

  void onTapAdd(storeid, PharmacyMedicinesModal pharmacy, selectedFrequencyName,
      frequencyId, noOfDays, total) {
    Provider.of<DrugPatientProvider>(context, listen: false).addPharmacyList(
      storeid,
      pharmacy,
      '',
      selectedFrequencyName,
      frequencyId,
      noOfDays,
      total,
    );
    if (!pharmacynameMap.containsKey(pharmacy.itemname)) {
      setState(() {
        pharmacynameMap.putIfAbsent(pharmacy.itemname, () => 1);
        pharmacyGenericName.putIfAbsent(
            pharmacy.itemname, () => pharmacy.genericName);
        incrementAddPharmacy.putIfAbsent(pharmacy.itemname, () => true);
      });
    }
  }

  onChangeTotalQuantity(
      storeid, PharmacyMedicinesModal pharmacy, nodays, freq) {
    double _newQty;
    if (pharmacy != null) {
      if (!totalquantitypharmacy.containsKey(pharmacy.itemname)) {
        if (nodays == null && freq == null) {
          _newQty = 0;
        } else if (nodays == null) {
          _newQty = freq.toDouble();
        } else if (freq == null) {
          _newQty = 0;
        } else {
          setState(() {
            _newQty = freq * nodays;
          });
        }
        totalquantitypharmacy.putIfAbsent(pharmacy.itemname, () => _newQty);
      } else {
        if (nodays == null && freq == null) {
          _newQty = 0;
        } else if (nodays == null) {
          _newQty = freq.toDouble();
        } else if (freq == null) {
          _newQty = 0;
        } else {
          setState(() {
            _newQty = freq * nodays;
          });
        }
        totalquantitypharmacy.update(pharmacy.itemname, (value) {
          setState(() {
            value = _newQty;
          });
          return value;
        });
      }
      Provider.of<DrugPatientProvider>(context, listen: false)
          .addtotalQtyPharmacy(
        pharmacy,
        _newQty,
        storeid,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    Map arguments = ModalRoute.of(context).settings.arguments as Map;
    return Scaffold(
        appBar: AppBar(
          title: Text('Pharmacy Order'),
          centerTitle: true,
        ),
        body: ListView(
          children: [
            Card(
              elevation: 4,
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'MRNO: ${arguments['mrn']}',
                    ),
                    ListTile(
                      onTap: () {},
                      leading: CircleAvatar(
                        backgroundColor: Colors.black,
                        backgroundImage:
                            AssetImage('assets/images/local_image/dummy.jpg'),
                      ),
                      title: Text(
                        '${arguments['patientname']}',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      subtitle: Wrap(
                        direction: Axis.vertical,
                        children: [
                          Text(
                            '${arguments['age']} Years',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            '${arguments['gender']}',
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          // Text(
                          //   '${arguments['pharmacy']}',
                          // ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: 1,
              width: double.infinity,
              color: Theme.of(context).primaryColor,
            ),
            Consumer<DrugPatientProvider>(
              builder: (ctx, druglistsPharmacy, _) => ListView.builder(
                physics: ScrollPhysics(),
                padding: EdgeInsets.only(top: 0),
                shrinkWrap: true,
                itemCount: druglistsPharmacy.drugpharmacylist.length,
                itemBuilder: (context, index) {
                  return Card(
                      elevation: 4,
                      child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Column(
                            children: [
                              ListTile(
                                title: Text(
                                  '${druglistsPharmacy.drugpharmacylist.keys.toList()[index]}',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                trailing: Container(
                                  width: 150,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Text('Quantity'),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          Text(
                                            '${druglistsPharmacy.drugpharmacylist.values.toList()[index].totalQty}',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ],
                                      ),
                                      IconButton(
                                        icon: Icon(
                                          Icons.delete,
                                          color: Colors.red,
                                        ),
                                        onPressed: () {
                                          Provider.of<DrugPatientProvider>(
                                                  context,
                                                  listen: false)
                                              .onTapdeletePharmacy(
                                                  druglistsPharmacy
                                                      .drugpharmacylist.keys
                                                      .toList()[index]);
                                          // Provider.of<DrugDetialsListItems>(context,
                                          //         listen: false)
                                          //     .onTapdelete(druglistsPharmacy
                                          //         .drugServicesList.keys
                                          //         .toList()[index]);
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 15, bottom: 10),
                                child: Row(
                                  children: [
                                    Text('Freq :  '),
                                    SizedBox(
                                      width: 150,
                                      height: 30,
                                      child: Consumer<DrugPatientProvider>(
                                        builder: (ctx, frequency, _) =>
                                            DropdownButtonFormField(
                                          hint: Text('--Select--'),
                                          decoration: InputDecoration(
                                            enabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Theme.of(context)
                                                      .primaryColor,
                                                  width: 2),
                                            ),
                                            errorBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Theme.of(context)
                                                      .primaryColor,
                                                  width: 2),
                                            ),
                                            disabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Theme.of(context)
                                                      .primaryColor,
                                                  width: 2),
                                            ),
                                            contentPadding:
                                                EdgeInsets.only(left: 10),
                                            errorStyle:
                                                TextStyle(color: Colors.red),
                                          ),
                                          isExpanded: true,
                                          value: pharmacyFrequencyMap[
                                              druglistsPharmacy
                                                  .drugpharmacylist.keys
                                                  .toList()[index]],
                                          onChanged: (newValue) {
                                            setState(() {
                                              pharmacyFrequencyMap[
                                                      druglistsPharmacy
                                                          .drugpharmacylist.keys
                                                          .toList()[index]] =
                                                  newValue;
                                              // selectedFrequency =
                                              //     newValue;
                                              onChangeTotalQuantity(
                                                  druglistsPharmacy
                                                      .drugpharmacylist.values
                                                      .toList()[index]
                                                      .storeid,
                                                  druglistsPharmacy
                                                      .drugpharmacylist.values
                                                      .toList()[index]
                                                      .pharmacy,
                                                  noOfDaysPharmacy[
                                                      druglistsPharmacy
                                                          .drugpharmacylist.keys
                                                          .toList()[index]],
                                                  selectedFrequencyValue);
                                              onTapAdd(
                                                  druglistsPharmacy
                                                      .drugpharmacylist.values
                                                      .toList()[index]
                                                      .storeid,
                                                  druglistsPharmacy
                                                      .drugpharmacylist.values
                                                      .toList()[index]
                                                      .pharmacy,
                                                  pharmacyFrequencyMap[
                                                      druglistsPharmacy
                                                          .drugpharmacylist.keys
                                                          .toList()[index]],
                                                  selectedFrequencyId,
                                                  noOfDaysPharmacy[
                                                      druglistsPharmacy
                                                          .drugpharmacylist.keys
                                                          .toList()[index]],
                                                  totalquantitypharmacy[
                                                      druglistsPharmacy
                                                          .drugpharmacylist.keys
                                                          .toList()[index]]);
                                            });
                                          },
                                          items: frequency.pharmacyfrequency
                                              .map((frequncyList) {
                                            return DropdownMenuItem(
                                                child: new Text(
                                                    frequncyList.frequencyName),
                                                value:
                                                    frequncyList.frequencyName,
                                                onTap: () {
                                                  setState(() {
                                                    selectedFrequencyId =
                                                        frequncyList
                                                            .frequencyId;
                                                    selectedFrequencyValue =
                                                        frequncyList
                                                            .frequencyValue;
                                                  });
                                                });
                                          }).toList(),
                                        ),
                                      ),
                                    ),
                                    // Text(
                                    //     '${druglistsPharmacy.drugpharmacylist.values.toList()[index].frequencyName}'),
                                  ],
                                ),
                              ),
                              Padding(
                                  padding: const EdgeInsets.only(
                                      left: 15, bottom: 10),
                                  child: druglistsPharmacy
                                          .drugpharmacylist.values
                                          .toList()[index]
                                          .frequencyName
                                          .isEmpty
                                      ? Text(
                                          'Frequency is Required',
                                          style: TextStyle(color: Colors.red),
                                        )
                                      : Container()),
                              // ListTile(
                              //   title: Row(
                              //     children: [
                              //       Text('Freq :'),
                              //       Text(
                              //           '${druglistsPharmacy.drugpharmacylist.values.toList()[index].frequency}')
                              //     ],
                              //   ),
                              //   )
                            ],
                          )));
                },
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                _isLoading
                    ? CircularProgressIndicator()
                    : RoundFlatButton(() => _submit(context), 'Save Orders'),
              ],
            ),
          ],
        ));
  }
}
