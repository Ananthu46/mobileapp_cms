import 'package:doctor_portal_cms/provider/drug/drug_order_api_provider.dart';
import 'package:doctor_portal_cms/widgets/buttons/round_flat_button.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';

class ViewServiceOrdersScreen extends StatefulWidget {
  static const routeName = 'view-service-orders';

  @override
  _ViewServiceOrdersScreenState createState() =>
      _ViewServiceOrdersScreenState();
}

class _ViewServiceOrdersScreenState extends State<ViewServiceOrdersScreen> {
  bool _isLoading = false;
  _submit(context) async {
    setState(() {
      _isLoading = true;
    });
    await Provider.of<DrugPatientProvider>(context, listen: false)
        .sendServiceorder(context);
    String resp = Provider.of<DrugPatientProvider>(context, listen: false)
        .responsedataAPI[0]
        .resp;
    print(resp);
    if (resp == "Saved") {
      Provider.of<DrugPatientProvider>(context, listen: false)
          .clearallServices();
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          backgroundColor: Colors.blue,
          content: const Text('Service Orders Saved'),
        ),
      );
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          backgroundColor: Colors.red,
          content: const Text('Service Not Orders Saved'),
        ),
      );
    }

    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    Map arguments = ModalRoute.of(context).settings.arguments as Map;
    return Scaffold(
        appBar: AppBar(
          title: Text('Service Order'),
          centerTitle: true,
        ),
        body: ListView(
          children: [
            Card(
              elevation: 4,
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'MRNO: ${arguments['mrn']}',
                    ),
                    ListTile(
                      onTap: () {},
                      leading: CircleAvatar(
                        backgroundColor: Colors.black,
                        backgroundImage:
                            AssetImage('assets/images/local_image/dummy.jpg'),
                      ),
                      title: Text(
                        '${arguments['patientname']}',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      subtitle: Wrap(
                        direction: Axis.vertical,
                        children: [
                          Text(
                            '${arguments['age']} Years',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            '${arguments['gender']}',
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            '${arguments['service']}',
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: 1,
              width: double.infinity,
              color: Theme.of(context).primaryColor,
            ),
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.end,
            //   children: [
            //     Padding(
            //       padding: const EdgeInsets.only(right: 15),
            //       child: Row(
            //         children: [
            //           RaisedButton(
            //             color: Theme.of(context).primaryColor,
            //             textColor: Theme.of(context).buttonColor,
            //             onPressed: () {
            //               Provider.of<DrugDetialsListItems>(context,
            //                       listen: false)
            //                   .clearall();
            //             },
            //             child: Text('Clear All'),
            //           ),
            //           RaisedButton(
            //             color: Theme.of(context).primaryColor,
            //             textColor: Theme.of(context).buttonColor,
            //             onPressed: () {
            //               Provider.of<DrugDetialsListItems>(context,
            //                       listen: false)
            //                   .clearall();
            //             },
            //             child: Text('Add New'),
            //           ),
            //         ],
            //       ),
            //     ),
            //   ],
            // ),
            Consumer<DrugPatientProvider>(
              builder: (ctx, druglists, _) => ListView.builder(
                physics: ScrollPhysics(),
                padding: EdgeInsets.only(top: 0),
                shrinkWrap: true,
                itemCount: druglists.drugServicesList.length,
                itemBuilder: (context, index) {
                  return Card(
                      elevation: 4,
                      child: Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: ListTile(
                          title: Text(
                            '${druglists.drugServicesList.keys.toList()[index]}',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          trailing: Container(
                            width: 150,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text('Quantity'),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      '${druglists.drugServicesList.values.toList()[index].quantity}',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                                IconButton(
                                  icon: Icon(
                                    Icons.delete,
                                    color: Colors.red,
                                  ),
                                  onPressed: () {
                                    Provider.of<DrugPatientProvider>(context,
                                            listen: false)
                                        .onTapdelete(druglists
                                            .drugServicesList.keys
                                            .toList()[index]);
                                  },
                                ),
                              ],
                            ),
                          ),
                        ),
                      ));
                },
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                _isLoading
                    ? CircularProgressIndicator()
                    : RoundFlatButton(() => _submit(context), 'Save Orders'),
              ],
            ),
          ],
        ));
  }
}
