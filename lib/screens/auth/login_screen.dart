import 'dart:ui';

import 'package:doctor_portal_cms/app/size_config.dart';
import 'package:doctor_portal_cms/provider/auth/login_auth.dart';
import 'package:doctor_portal_cms/widgets/inputfields/round_textformfield.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LoginInScreen extends StatefulWidget {
  @override
  _LoginInScreenState createState() => _LoginInScreenState();
}

class _LoginInScreenState extends State<LoginInScreen> {
  final _formKey = GlobalKey<FormState>();
  final globalScaffoldKey = GlobalKey<ScaffoldState>();

  Map<String, String> _authData = {
    'username': '',
    'password': '',
  };
  var _isLoading = false;
  Future<void> _submit(context) async {
    if (!_formKey.currentState.validate()) {
      // Invalid!
      return;
    }

    _formKey.currentState.save();
    setState(() {
      _isLoading = true;
    });
    // await Provider.of<PersonalDataProvider>(context, listen: false)
    //     .loadPersonalData(_authData['email']);
    try {
      await Provider.of<LoginAuth>(context, listen: false)
          .login(_authData['username'], _authData['password'], context);
    } catch (error) {
      print(error);
      setState(() {
        _isLoading = false;
      });
      const errorMessage =
          'Could not authenticate you. Please try again later.';
      _showErrorDialog(error, context);
    }

    if (this.mounted) {
      setState(() {
        _isLoading = false;
      });
    }
  }

  void _showErrorDialog(String message, context) {
    showDialog<void>(
      barrierDismissible: false,
      context: context,
      builder: (ctx) => AlertDialog(
        // title: Text('An Error Occurred!'),
        content: Text(message),
        actions: <Widget>[
          FlatButton(
            child: Text('Okay'),
            onPressed: () {
              Navigator.of(ctx).pop();
            },
          )
        ],
      ),
    );
  }

  final _passwordFocus = FocusNode();
  @override
  void dispose() {
    _passwordFocus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    SizeConfig().init(context);
    return
        //  Container(
        //     color: Colors.brown[300],
        //     child: SafeArea(
        //         child: MaterialApp(
        //       home:
        Scaffold(
      resizeToAvoidBottomInset: false,
      key: globalScaffoldKey,
      body: Stack(children: [
        Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/images/splash/cesa_splash_bg.jpg"),
                  fit: BoxFit.cover)),
          child: ClipRRect(
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
              child: Container(
                child: Scaffold(
                  backgroundColor: Colors.transparent,
                  body: SingleChildScrollView(
                    child: Container(
                      height: deviceSize.height * 0.9,
                      width: deviceSize.width,
                      child: SingleChildScrollView(
                        child: Column(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                margin: EdgeInsets.only(
                                    top: deviceSize.height * 0.15),
                                child: Form(
                                  key: _formKey,
                                  child: Column(children: [
                                    Center(
                                      child: Container(
                                        height:
                                            120 * SizeConfig.getScaleFactor(),
                                        width:
                                            300 * SizeConfig.getScaleFactor(),
                                        child: Image.asset(
                                            'assets/images/logo/chazikkat_logo.png'),
                                      ),
                                    ),
                                    // SizedBox(
                                    //   height: 120 * SizeConfig.getScaleFactor(),
                                    // ),
                                    Padding(
                                      padding: EdgeInsets.all(20),
                                      child: RoundTextFormFieldWidget(
                                        scrollpadding:
                                            const EdgeInsets.only(bottom: 0.0),
                                        isSecureInput: false,
                                        labelText: 'Username',
                                        validator: (value) {
                                          if (value.isEmpty ||
                                              value.length < 3) {
                                            return 'Invalid Username!';
                                          }
                                        },
                                        onFieldSubmitted: (value) {
                                          FocusScope.of(context)
                                              .requestFocus(_passwordFocus);
                                        },
                                        onSaved: (value) {
                                          _authData['username'] = value;
                                        },
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.all(20),
                                      child: RoundTextFormFieldWidget(
                                        focusNode: _passwordFocus,
                                        scrollpadding:
                                            const EdgeInsets.only(bottom: 0.0),
                                        isSecureInput: true,
                                        labelText: 'Password',
                                        validator: (value) {
                                          if (value.isEmpty ||
                                              value.length < 5) {
                                            return 'Invalid Password!';
                                          }
                                        },
                                        onSaved: (value) {
                                          _authData['password'] = value;
                                        },
                                        onFieldSubmitted: (value) {
                                          _submit(
                                              globalScaffoldKey.currentContext);
                                        },
                                      ),
                                    ),
                                    _isLoading == true
                                        ? CircularProgressIndicator()
                                        : FlatButton(
                                            autofocus: true,
                                            clipBehavior: Clip.hardEdge,
                                            onPressed: () => _submit(
                                                globalScaffoldKey
                                                    .currentContext),
                                            child: Text(
                                              'Login',
                                              style: TextStyle(
                                                  color: Colors.white),
                                            ),
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(30),
                                            ),
                                            minWidth: 250 *
                                                SizeConfig.getScaleFactor(),
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 30.0,
                                                vertical: 8.0),
                                            color:
                                                Theme.of(context).primaryColor),
                                  ]),
                                ),
                              ),
                            ]),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        Positioned(
          bottom: 0,
          left: 0,
          right: 0,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Developed by Camerinfolks Pvt Ltd',
                  style: TextStyle(
                    color: Colors.white.withOpacity(0.6),
                  )),
            ],
          ),
        ),
      ]),
      //   ),
      // )
      //  )
    );
  }
}
