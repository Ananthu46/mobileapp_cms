import 'package:doctor_portal_cms/model/drug/drug_order_api_modal.dart';
import 'package:doctor_portal_cms/provider/drug/drug_order_api_provider.dart';
import 'package:doctor_portal_cms/screens/view_orders/view_pharmacy_orders.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PreviousOrderScreen extends StatefulWidget {
  static const routeName = 'prevoius-order';

  @override
  _PreviousOrderScreenState createState() => _PreviousOrderScreenState();
}

class _PreviousOrderScreenState extends State<PreviousOrderScreen> {
  String patientId;
  Future<void> _refreshProducts(BuildContext context) async {
    Provider.of<DrugPatientProvider>(context, listen: false)
        .clearPreviousOrders();
    await Provider.of<DrugPatientProvider>(context, listen: false)
        .previousOrderPharmacy();
  }

  Map<String, PreviousPharmacyOrderModal> previousOrderList;
  @override
  void initState() {
    Provider.of<DrugPatientProvider>(context, listen: false)
        .clearPreviousOrders();
    Provider.of<DrugPatientProvider>(context, listen: false)
        .previousOrderPharmacy();
    super.initState();
  }

  bool _chechedvalue = false;
  Map<String, PreviousPharmacyOrderModal> _checkedPharmacyItem =
      new Map<String, PreviousPharmacyOrderModal>();
  void checboxSelectedPreviousOrder(
      PreviousPharmacyOrderModal item, value, store) {
    print(item.drugorderid);
    print(item.frequencyId);
    Map<String, PreviousPharmacyOrderModal> _drugpharmacylist =
        Provider.of<DrugPatientProvider>(context, listen: false)
            .drugpharmacylist;
    Map<String, PreviousPharmacyOrderModal> _previousOrderList =
        Provider.of<DrugPatientProvider>(context, listen: false)
            .previousOrderList;
    if (value == true) {
      if (_drugpharmacylist.containsKey(item.itemname) ||
          (_previousOrderList.containsValue(item.itemname))) {
        _showErrorDialog(context);
        return;
      }
      _checkedPharmacyItem.putIfAbsent(
          item.drugorderid, () => PreviousPharmacyOrderModal(renewOrder: true));
      Provider.of<DrugPatientProvider>(context, listen: false)
          .previousOrderAdd(item, store);
    } else {
      _checkedPharmacyItem.removeWhere((key, value) => key == item.drugorderid);
      Provider.of<DrugPatientProvider>(context, listen: false)
          .previousOrderDelete(item);
    }
  }

  void _showErrorDialog(context) {
    showDialog<void>(
      barrierDismissible: false,
      context: context,
      builder: (ctx) => AlertDialog(
        // title: Text('An Error Occurred!'),
        content: Text("Item already added"),
        actions: <Widget>[
          FlatButton(
            child: Text('Okay'),
            onPressed: () {
              Navigator.of(ctx).pop();
            },
          )
        ],
      ),
    );
  }

  void repeatOrder() {
    Map arguments = ModalRoute.of(context).settings.arguments as Map;
    Provider.of<DrugPatientProvider>(context, listen: false).addRepeatOrders();
    Navigator.of(context)
        .pushNamed(ViewPharmacyOrdersScreen.routeName, arguments: {
      'mrn': '${arguments['mrn']}',
      'patientID': '${arguments['patientID']}',
      'patientname': '${arguments['patientname']}',
      'age': '${arguments['age']}',
      'gender': '${arguments['gender']}',
      'pharmacy': '${arguments['pharmacy']}'
    });
  }

  @override
  Widget build(BuildContext context) {
    Map arguments = ModalRoute.of(context).settings.arguments as Map;
    return Scaffold(
        appBar: AppBar(),
        body: Column(
          children: [
            Card(
              elevation: 4,
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'MRNO: ${arguments['mrn']}',
                    ),
                    ListTile(
                      onTap: () {},
                      leading: CircleAvatar(
                        backgroundColor: Colors.black,
                        backgroundImage:
                            AssetImage('assets/images/local_image/dummy.jpg'),
                      ),
                      title: Text(
                        '${arguments['patientname']}',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      subtitle: Wrap(
                        direction: Axis.vertical,
                        children: [
                          Text(
                            '${arguments['age']} Years',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            '${arguments['gender']}',
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          // Text(
                          //   '${arguments['pharmacy']}',
                          // ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                RaisedButton(
                  color: Theme.of(context).primaryColor,
                  textColor: Theme.of(context).buttonColor,
                  onPressed: repeatOrder,
                  child: Text('Repeated Order'),
                ),
                SizedBox(
                  width: 10,
                ),
              ],
            ),
            Expanded(
              child: RefreshIndicator(
                onRefresh: () => _refreshProducts(context),
                child: Consumer<DrugPatientProvider>(
                  builder: (ctx, previousPharmacy, _) => SingleChildScrollView(
                    child: ListView.builder(
                        physics: ScrollPhysics(),
                        padding: EdgeInsets.only(top: 0),
                        shrinkWrap: true,
                        itemCount:
                            previousPharmacy.previousPharmacyOrder.length,
                        itemBuilder: (context, index) {
                          PreviousPharmacyOrderModal previousOrders =
                              previousPharmacy.previousPharmacyOrder[index];

                          return Card(
                            child: CheckboxListTile(
                              title: Text('${previousOrders.itemname}'),
                              value: _checkedPharmacyItem
                                      .containsKey(previousOrders.drugorderid)
                                  ? _checkedPharmacyItem[
                                          previousOrders.drugorderid]
                                      .renewOrder
                                  : _chechedvalue,
                              onChanged: (bool value) {
                                setState(() {
                                  checboxSelectedPreviousOrder(previousOrders,
                                      value, arguments['store']);
                                });
                              },
                              secondary: SizedBox(
                                width: 150,
                                child: Wrap(
                                  direction: Axis.horizontal,
                                  spacing: 5,
                                  runSpacing: 10,
                                  children: [
                                    Text('${previousOrders.frequencyName}'),
                                    SizedBox(
                                      width: 20,
                                    ),
                                  ],
                                ),
                              ),
                              controlAffinity: ListTileControlAffinity.leading,
                            ),
                          );
                        }),
                  ),
                  //   ),
                  //     ),
                ),
              ),
            )
          ],
        ));
  }
}
