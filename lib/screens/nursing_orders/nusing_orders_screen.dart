import 'package:date_time_picker/date_time_picker.dart';
import 'package:doctor_portal_cms/model/nursing/nursing_patient_modal_Api.dart';
import 'package:doctor_portal_cms/provider/nursing/nursing_patient_provider_Api.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:doctor_portal_cms/widgets/buttons/round_flat_button.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:doctor_portal_cms/widgets/expansion_widget/custom_expansion_widget.dart'
    as expansion;
import 'package:html/parser.dart';

class NursingOrdersScreen extends StatefulWidget {
  static const routeName = 'nursing-orders';

  @override
  _NursingOrdersScreenState createState() => _NursingOrdersScreenState();
}

class _NursingOrdersScreenState extends State<NursingOrdersScreen> {
  Future<void> _refreshProducts(BuildContext context) async {
    await Provider.of<NursingPatientProvider>(context, listen: false)
        .loadPreviousNursingNotes(patientId, visitId);
  }

  final _formKey = GlobalKey<FormState>();
  final _descriptionController = TextEditingController();
  String description = '';
  bool _isLoading = false;
  bool _validateSave = false;
  String patientId;
  String visitId;
  String date = DateTime.now().toString();
  void _submit(Map arguments, description, date) async {
    setState(() {
      _isLoading = true;
      _validateSave = false;
    });
    _formKey.currentState.save();
    await Provider.of<NursingPatientProvider>(context, listen: false)
        .addNursingOrder(arguments, description, date);
    await Provider.of<NursingPatientProvider>(context, listen: false)
        .sendNursingOrders();
    await Provider.of<NursingPatientProvider>(context, listen: false)
        .loadPreviousNursingNotes(patientId, visitId);
    List<NursingOrderModal> responseData =
        Provider.of<NursingPatientProvider>(context, listen: false)
            .responseDataApi;
    String res = responseData[responseData.length - 1].response;
    print(res);
    if (res == 'Saved') {
      await Fluttertoast.showToast(
          msg: "Saved",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 12.0);
    } else {
      setState(() {
        _validateSave = true;
      });
    }

    setState(() {
      _isLoading = false;
    });
  }

  @override
  void didChangeDependencies() {
    loadNursingNotes(context);
    super.didChangeDependencies();
  }

  loadNursingNotes(context) async {
    Map arguments = ModalRoute.of(context).settings.arguments as Map;
    patientId = arguments['patientID'];
    visitId = arguments['visitid'];
    print(patientId);
    Provider.of<NursingPatientProvider>(context, listen: false)
        .loadPreviousNursingNotes(patientId, visitId);
  }

  @override
  Widget build(BuildContext context) {
    Map arguments = ModalRoute.of(context).settings.arguments as Map;

    return Scaffold(
      appBar: AppBar(
        title: Text('Nursing Notes'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: RefreshIndicator(
          onRefresh: () => _refreshProducts(context),
          child: Column(
            children: [
              Column(
                children: [
                  Card(
                    elevation: 4,
                    child: Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'MRNO: ${arguments['mrn']}',
                          ),
                          ListTile(
                            onTap: () {},
                            leading: CircleAvatar(
                              backgroundColor: Colors.black,
                              backgroundImage: AssetImage(
                                  'assets/images/local_image/dummy.jpg'),
                            ),
                            title: Text(
                              '${arguments['patientname']}',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            subtitle: Wrap(
                              direction: Axis.vertical,
                              children: [
                                Text(
                                  '${arguments['age']} Years',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  '${arguments['gender']}',
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  '${arguments['service']}',
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Card(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Form(
                          key: _formKey,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              DateTimePicker(
                                  type: DateTimePickerType.dateTimeSeparate,
                                  dateMask: 'dd MMM, yyyy',
                                  initialValue: DateTime.now().toString(),
                                  firstDate: DateTime(2000),
                                  lastDate: DateTime(2100),
                                  icon: Icon(Icons.event),
                                  dateLabelText: 'Date',
                                  timeLabelText: "Hour",
                                  use24HourFormat: true,
                                  onChanged: (val) {
                                    setState(() {
                                      date = val;
                                      print(date);
                                    });
                                  },
                                  onSaved: (val) {
                                    setState(() {
                                      date = val;
                                    });
                                  }),
                              SizedBox(
                                height: 20,
                              ),
                              Text(
                                'Notes:',
                                style: Theme.of(context).textTheme.headline6,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              TextFormField(
                                controller: _descriptionController,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.all(20),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: const BorderRadius.all(
                                      const Radius.circular(1.0),
                                    ),
                                    borderSide: BorderSide(
                                        color: Colors.blue, width: 2),
                                  ),
                                  border: OutlineInputBorder(
                                    borderRadius: const BorderRadius.all(
                                      const Radius.circular(1.0),
                                    ),
                                    borderSide: BorderSide(
                                        color: Colors.blue, width: 2),
                                  ),
                                  disabledBorder: OutlineInputBorder(
                                    borderRadius: const BorderRadius.all(
                                      const Radius.circular(1.0),
                                    ),
                                    borderSide: BorderSide(
                                        color: Colors.blue, width: 2),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: const BorderRadius.all(
                                      const Radius.circular(1.0),
                                    ),
                                    borderSide: BorderSide(
                                        color: Colors.blue, width: 2),
                                  ),
                                  errorBorder: OutlineInputBorder(
                                    borderRadius: const BorderRadius.all(
                                      const Radius.circular(1.0),
                                    ),
                                    borderSide: BorderSide(
                                        color: Colors.blue, width: 2),
                                  ),
                                  focusedErrorBorder: OutlineInputBorder(
                                    borderRadius: const BorderRadius.all(
                                      const Radius.circular(1.0),
                                    ),
                                    borderSide: BorderSide(
                                        color: Colors.blue, width: 2),
                                  ),
                                ),
                                maxLines: 5,
                                onChanged: (value) {
                                  description = value;
                                },
                                onSaved: (value) {
                                  description = value;
                                },
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              if (_validateSave)
                                Text(
                                  'An Error Occured, Note Cannot be saved',
                                  style: TextStyle(
                                      color: Theme.of(context).errorColor),
                                ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  _isLoading
                      ? CircularProgressIndicator()
                      : RoundFlatButton(
                          () => _submit(arguments, description, date),
                          'Submit'),
                  SizedBox(
                    height: 20,
                  ),
                ],
              ),
              FutureBuilder(
                  future: _refreshProducts(context),
                  builder: (ctx, snapshot) => snapshot.connectionState ==
                          ConnectionState.waiting
                      ? Center(
                          child: Container(),
                        )
                      : Consumer<NursingPatientProvider>(
                          builder: (ctx, previousNotes, _) => ListView.builder(
                              physics: ScrollPhysics(),
                              padding: EdgeInsets.only(top: 0),
                              shrinkWrap: true,
                              itemCount:
                                  previousNotes.loadpreviousNursingNotes.length,
                              itemBuilder: (context, index) {
                                PreviousNursingOrders notes = previousNotes
                                    .loadpreviousNursingNotes[index];
                                return Card(
                                  child: expansion.ExpansionTile(
                                    initiallyExpanded: true,
                                    headerBackgroundColor:
                                        Theme.of(context).primaryColor,
                                    title: Text(
                                      'Posted by  :'
                                      '${notes.username}',
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    trailing: Text(
                                      DateFormat('dd-MM-yyyy')
                                          .format(notes.date),
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Text(
                                              '${parse(parse(notes.note).body.text).documentElement.text}',
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                );
                              })))
            ],
          ),
        ),
      ),
    );
  }
}
