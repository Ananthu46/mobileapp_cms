import 'package:doctor_portal_cms/model/doctor/doctor_order_modal.dart';
import 'package:doctor_portal_cms/model/drug/drug_order_api_modal.dart';
import 'package:doctor_portal_cms/provider/doctor/doctor_order_provider.dart';
import 'package:doctor_portal_cms/provider/drug/drug_order_api_provider.dart';
import 'package:doctor_portal_cms/screens/doctor/doctors_select_items/admission_request_doctor/admission_request_screen.dart';
import 'package:doctor_portal_cms/screens/doctor/doctors_select_items/allergies_doctor/allergy_selection_screen.dart';
import 'package:doctor_portal_cms/screens/doctor/doctors_select_items/diagnosis_doctor/diagnosis_selection_screen.dart';
import 'package:doctor_portal_cms/screens/doctor/doctors_select_items/lab_results/labresults.dart';
import 'package:doctor_portal_cms/screens/doctor/doctors_select_items/vital_sign_doctor/vital_sign_doctor_screen.dart';
import 'package:doctor_portal_cms/screens/drug_order/drug_order_main_screen.dart';
import 'package:doctor_portal_cms/screens/ehr/electronic_health_record_screen.dart';
import 'package:flutter/material.dart';
import 'package:doctor_portal_cms/app/size_config.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:provider/provider.dart';

class DoctorsSoapNotes extends StatefulWidget {
  static const routeName = 'soapnotes';

  @override
  _DoctorsSoapNotesState createState() => _DoctorsSoapNotesState();
}

class _DoctorsSoapNotesState extends State<DoctorsSoapNotes> {
  final _formKey = GlobalKey<FormState>();
  Map<String, DoctorsSoapTextDetials> loadSoapTextDetials = {};
  // String _provisionalDiagnosisText = "";
  bool _editSoapNotes = false;
  List<SelectedDepartment> deptDetials = [];
  TextEditingController _descriptionController = new TextEditingController();
  @override
  void initState() {
    super.initState();
    deptDetials = Provider.of<DrugPatientProvider>(context, listen: false)
        .selectedDepartmentdetials;
    _editSoapNotes = false;
    _editProvisionalDiagnosis = false;
    print(deptDetials.isEmpty);
    if (deptDetials.isEmpty) {
      Provider.of<DrugPatientProvider>(context, listen: false)
          .loadLocIdstores();
    } else {
      Provider.of<DrugPatientProvider>(context, listen: false).loadstores();
    }
  }

  Future<void> _refreshProducts(BuildContext context) async {
    print(_editProvisionalDiagnosis);
    print(_editSoapNotes);
    if (_editProvisionalDiagnosis || _editSoapNotes) {
      return null;
    } else {
      await loadSoapNoteitems();
    }
  }

  loadSoapNoteitems() async {
    Map arguments = ModalRoute.of(context).settings.arguments as Map;
    await Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .loadSoapNoteDetialslist(arguments['visitid']);
    // loadSoapTextDetials =
    //     Provider.of<DrugOrderModalDoctor>(context, listen: false)
    //         .loadSoapTextDetials;
  }

  _saveSoapNpotes(DoctorsSoapNoteDetials soap) async {
    setState(() {
      soap.edit = false;
    });
    _formKey.currentState.save();
    await Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .sendSoapNotes(soap);
  }

  _saveProvisionalDiagnosis(text) async {
    Map arguments = ModalRoute.of(context).settings.arguments as Map;
    setState(() {
      _editProvisionalDiagnosis = false;
    });
    _formKey.currentState.save();
    await Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .sendProvisionalDiagnosis(text, arguments['visitid']);
  }

  bool _editProvisionalDiagnosis = false;

  bool viewAllergies = false;
  bool viewDiagnosis = false;
  bool viewAdmissionRequest = false;
  bool viewSoapNotes = true;
  bool viewGeneralInformation = false;
  bool viewVitalSign = false;
  bool viewlabreports = false;
  addSoapNoteDescription(id, value) {
    if (loadSoapTextDetials.containsKey(id)) {
      setState(() {
        loadSoapTextDetials.update(
            id,
            (v) => DoctorsSoapTextDetials(
                componentId: v.componentId,
                textValue: value,
                textId: v.textId));
      });
    } else {
      setState(() {
        loadSoapTextDetials.putIfAbsent(
            id,
            () => DoctorsSoapTextDetials(
                componentId: id, textValue: value, textId: ""));
      });
    }
  }

  _viewSoapNotesScreen() {
    setState(() {
      viewAllergies = false;
      viewAdmissionRequest = false;
      viewDiagnosis = false;
      viewSoapNotes = true;
      viewGeneralInformation = false;
      viewVitalSign = false;
      viewlabreports = false;
    });
  }

  _viewAllergyScreen() {
    setState(() {
      viewAllergies = true;
      viewSoapNotes = false;
      viewAdmissionRequest = false;
      viewDiagnosis = false;
      viewGeneralInformation = false;
      viewVitalSign = false;
      viewlabreports = false;
    });
  }

  _viewDiagnosisScreen() {
    setState(() {
      viewAllergies = false;
      viewSoapNotes = false;
      viewAdmissionRequest = false;
      viewDiagnosis = true;
      viewGeneralInformation = false;
      viewVitalSign = false;
      viewlabreports = false;
    });
  }

  _viewAdmissionRequest() {
    setState(() {
      viewAllergies = false;
      viewSoapNotes = false;
      viewAdmissionRequest = true;
      viewDiagnosis = false;
      viewGeneralInformation = false;
      viewVitalSign = false;
      viewlabreports = false;
    });
  }

  _viewVitalSign() {
    setState(() {
      viewAllergies = false;
      viewSoapNotes = false;
      viewAdmissionRequest = false;
      viewDiagnosis = false;
      viewGeneralInformation = false;
      viewVitalSign = true;
      viewlabreports = false;
    });
  }

  _viewGeneralInformation() {
    setState(() {
      viewAllergies = false;
      viewSoapNotes = false;
      viewAdmissionRequest = false;
      viewDiagnosis = false;
      viewGeneralInformation = true;
      viewVitalSign = false;
      viewlabreports = false;
    });
  }

  _viewLabResults() {
    setState(() {
      viewSoapNotes = false;
      viewAllergies = false;
      viewAdmissionRequest = false;
      viewDiagnosis = false;
      viewGeneralInformation = false;
      viewVitalSign = false;
      viewlabreports = true;
    });
  }

  TextEditingController _textEditingController = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    SizeConfig().init(context);
    Map arguments = ModalRoute.of(context).settings.arguments as Map;
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Container(
          color: Theme.of(context).primaryColor,
          child: SafeArea(
            child: Scaffold(
              // appBar: AppBar(
              //   title: Text(
              //     'Medical Records',
              //     style: TextStyle(fontWeight: FontWeight.bold),
              //   ),
              //   centerTitle: true,
              // ),
              body: Form(
                key: _formKey,
                child: ListView(children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Text("Medical Records"),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Card(
                    elevation: 4,
                    child: Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'MRNO: ${arguments['mrn']}',
                          ),
                          ListTile(
                            onTap: () {},
                            leading: CircleAvatar(
                              backgroundColor: Colors.black,
                              backgroundImage: arguments['gender'] == 'MALE'
                                  ? AssetImage(
                                      'assets/images/local_image/male.jpg')
                                  : AssetImage(
                                      'assets/images/local_image/female.jpg'),
                            ),
                            title: Text(
                              '${arguments['patientname']}',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            trailing: Container(
                              width: 100,
                              child: Row(
                                children: [
                                  IconButton(
                                    icon: Icon(
                                      // FontAwesome.,
                                      AntDesign.book,
                                      color: Theme.of(context).primaryColor,
                                    ),
                                    onPressed: () {
                                      Navigator.of(context).pushNamed(
                                          ElectronicHealthRecordScreen
                                              .routeName);
                                    },
                                  ),
                                  IconButton(
                                    icon: Icon(
                                      AntDesign.addfile,
                                      color: Theme.of(context).primaryColor,
                                    ),
                                    onPressed: () {
                                      Navigator.of(context).pushNamed(
                                          DrugOrderTabBar.routeName,
                                          arguments: {
                                            'patientID':
                                                '${arguments['patientID']}',
                                            'patientname':
                                                '${arguments['patientname']}',
                                            'age': '${arguments['age']}',
                                            'gender': '${arguments['gender']}',
                                            'service':
                                                '${arguments['visitid']}',
                                            'mrn': '${arguments['mrn']}',
                                            'visitid':
                                                '${arguments['visitid']}',
                                          });
                                    },
                                  ),
                                ],
                              ),
                            ),
                            subtitle: Wrap(
                              direction: Axis.vertical,
                              children: [
                                Text(
                                  '${arguments['age']} Years',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  '${arguments['gender']}',
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Center(
                    child: deviceSize.width < 575 * SizeConfig.getScaleFactor()
                        ? SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 10),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Column(
                                    children: [
                                      IconButton(
                                        icon: viewSoapNotes
                                            ? Icon(
                                                FontAwesome.history,
                                                color: Theme.of(context)
                                                    .primaryColor,
                                              )
                                            : Icon(FontAwesome.history),
                                        onPressed: _viewSoapNotesScreen,
                                        tooltip: 'Medical History',
                                      ),
                                      viewSoapNotes
                                          ? Text(
                                              'Medical History',
                                              style: TextStyle(
                                                color: Theme.of(context)
                                                    .primaryColor,
                                              ),
                                            )
                                          : Text('Medical History'),
                                    ],
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Column(
                                    children: [
                                      IconButton(
                                        icon: viewlabreports
                                            ? Icon(
                                                FontAwesome.newspaper_o,
                                                color: Theme.of(context)
                                                    .primaryColor,
                                              )
                                            : Icon(FontAwesome.newspaper_o),
                                        onPressed: _viewLabResults,
                                        tooltip: 'Lab Results',
                                      ),
                                      viewlabreports
                                          ? Text(
                                              'Lab Results',
                                              style: TextStyle(
                                                color: Theme.of(context)
                                                    .primaryColor,
                                              ),
                                            )
                                          : Text('Lab Results'),
                                    ],
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Column(
                                    children: [
                                      Tooltip(
                                        message: 'Allergies',
                                        child: IconButton(
                                          icon: viewAllergies
                                              ? Icon(
                                                  FontAwesome5Solid.allergies,
                                                  color: Theme.of(context)
                                                      .primaryColor,
                                                )
                                              : Icon(
                                                  FontAwesome5Solid.allergies),
                                          onPressed: _viewAllergyScreen,
                                        ),
                                      ),
                                      viewAllergies
                                          ? Text(
                                              'Allergies',
                                              style: TextStyle(
                                                color: Theme.of(context)
                                                    .primaryColor,
                                              ),
                                            )
                                          : Text('Allergies'),
                                    ],
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Column(
                                    children: [
                                      Tooltip(
                                        message: 'Diagnosis',
                                        child: IconButton(
                                          icon: viewDiagnosis
                                              ? Icon(
                                                  FontAwesome5Solid
                                                      .laptop_medical,
                                                  color: Theme.of(context)
                                                      .primaryColor)
                                              : Icon(
                                                  FontAwesome5Solid
                                                      .laptop_medical,
                                                ),
                                          onPressed: _viewDiagnosisScreen,
                                        ),
                                      ),
                                      viewDiagnosis
                                          ? Text(
                                              'Diagnosis',
                                              style: TextStyle(
                                                color: Theme.of(context)
                                                    .primaryColor,
                                              ),
                                            )
                                          : Text('Diagnosis'),
                                    ],
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Column(
                                    children: [
                                      IconButton(
                                        icon: viewAdmissionRequest
                                            ? Icon(FontAwesome.newspaper_o,
                                                color: Theme.of(context)
                                                    .primaryColor)
                                            : Icon(FontAwesome.newspaper_o),
                                        onPressed: _viewAdmissionRequest,
                                        tooltip: 'Admission Request',
                                      ),
                                      viewAdmissionRequest
                                          ? Text(
                                              'Admission Request',
                                              style: TextStyle(
                                                color: Theme.of(context)
                                                    .primaryColor,
                                              ),
                                            )
                                          : Text('Admission Request'),
                                    ],
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Column(
                                    children: [
                                      Tooltip(
                                        message: 'General Information',
                                        child: IconButton(
                                          icon: viewGeneralInformation
                                              ? Icon(
                                                  Ionicons.ios_paper,
                                                  color: Theme.of(context)
                                                      .primaryColor,
                                                )
                                              : Icon(Ionicons.ios_paper),
                                          onPressed: _viewGeneralInformation,
                                        ),
                                      ),
                                      viewGeneralInformation
                                          ? Text(
                                              'General Information',
                                              style: TextStyle(
                                                color: Theme.of(context)
                                                    .primaryColor,
                                              ),
                                            )
                                          : Text('General Information'),
                                    ],
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Column(
                                    children: [
                                      Tooltip(
                                        message: 'Vital Signs',
                                        child: IconButton(
                                          icon: viewVitalSign
                                              ? Icon(
                                                  Ionicons.md_analytics,
                                                  color: Theme.of(context)
                                                      .primaryColor,
                                                )
                                              : Icon(Ionicons.md_analytics),
                                          onPressed: _viewVitalSign,
                                        ),
                                      ),
                                      viewVitalSign
                                          ? Text(
                                              'Vital Signs',
                                              style: TextStyle(
                                                color: Theme.of(context)
                                                    .primaryColor,
                                              ),
                                            )
                                          : Text('Vital Signs'),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          )
                        : Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Column(
                                children: [
                                  IconButton(
                                    icon: viewSoapNotes
                                        ? Icon(
                                            FontAwesome.history,
                                            color:
                                                Theme.of(context).primaryColor,
                                          )
                                        : Icon(FontAwesome.history),
                                    onPressed: _viewSoapNotesScreen,
                                    tooltip: 'Medical History',
                                  ),
                                  viewSoapNotes
                                      ? Text(
                                          'Medical History',
                                          style: TextStyle(
                                            color:
                                                Theme.of(context).primaryColor,
                                          ),
                                        )
                                      : Text('Medical History'),
                                ],
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Column(
                                children: [
                                  IconButton(
                                    icon: viewlabreports
                                        ? Icon(
                                            FontAwesome.newspaper_o,
                                            color:
                                                Theme.of(context).primaryColor,
                                          )
                                        : Icon(FontAwesome.newspaper_o),
                                    onPressed: _viewLabResults,
                                    tooltip: 'Lab Results',
                                  ),
                                  viewlabreports
                                      ? Text(
                                          'Lab Results',
                                          style: TextStyle(
                                            color:
                                                Theme.of(context).primaryColor,
                                          ),
                                        )
                                      : Text('Lab Results'),
                                ],
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Column(
                                children: [
                                  Tooltip(
                                    message: 'Allergies',
                                    child: IconButton(
                                      icon: viewAllergies
                                          ? Icon(
                                              FontAwesome5Solid.allergies,
                                              color: Theme.of(context)
                                                  .primaryColor,
                                            )
                                          : Icon(FontAwesome5Solid.allergies),
                                      onPressed: _viewAllergyScreen,
                                    ),
                                  ),
                                  viewAllergies
                                      ? Text(
                                          'Allergies',
                                          style: TextStyle(
                                            color:
                                                Theme.of(context).primaryColor,
                                          ),
                                        )
                                      : Text('Allergies'),
                                ],
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Column(
                                children: [
                                  Tooltip(
                                    message: 'Diagnosis',
                                    child: IconButton(
                                      icon: viewDiagnosis
                                          ? Icon(
                                              FontAwesome5Solid.laptop_medical,
                                              color: Theme.of(context)
                                                  .primaryColor)
                                          : Icon(
                                              FontAwesome5Solid.laptop_medical,
                                            ),
                                      onPressed: _viewDiagnosisScreen,
                                    ),
                                  ),
                                  viewDiagnosis
                                      ? Text(
                                          'Diagnosis',
                                          style: TextStyle(
                                            color:
                                                Theme.of(context).primaryColor,
                                          ),
                                        )
                                      : Text('Diagnosis'),
                                ],
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Column(
                                children: [
                                  IconButton(
                                    icon: viewAdmissionRequest
                                        ? Icon(FontAwesome.newspaper_o,
                                            color:
                                                Theme.of(context).primaryColor)
                                        : Icon(FontAwesome.newspaper_o),
                                    onPressed: _viewAdmissionRequest,
                                    tooltip: 'Admission Request',
                                  ),
                                  viewAdmissionRequest
                                      ? Text(
                                          'Admission Request',
                                          style: TextStyle(
                                            color:
                                                Theme.of(context).primaryColor,
                                          ),
                                        )
                                      : Text('Admission Request'),
                                ],
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Column(
                                children: [
                                  Tooltip(
                                    message: 'General Information',
                                    child: IconButton(
                                      icon: viewGeneralInformation
                                          ? Icon(
                                              Ionicons.ios_paper,
                                              color: Theme.of(context)
                                                  .primaryColor,
                                            )
                                          : Icon(Ionicons.ios_paper),
                                      onPressed: _viewGeneralInformation,
                                    ),
                                  ),
                                  viewGeneralInformation
                                      ? Text(
                                          'General Information',
                                          style: TextStyle(
                                            color:
                                                Theme.of(context).primaryColor,
                                          ),
                                        )
                                      : Text('General Information'),
                                ],
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Column(
                                children: [
                                  Tooltip(
                                    message: 'Vital Signs',
                                    child: IconButton(
                                      icon: viewVitalSign
                                          ? Icon(
                                              Ionicons.md_analytics,
                                              color: Theme.of(context)
                                                  .primaryColor,
                                            )
                                          : Icon(Ionicons.md_analytics),
                                      onPressed: _viewVitalSign,
                                    ),
                                  ),
                                  viewVitalSign
                                      ? Text(
                                          'Vital Signs',
                                          style: TextStyle(
                                            color:
                                                Theme.of(context).primaryColor,
                                          ),
                                        )
                                      : Text('Vital Signs'),
                                ],
                              ),
                            ],
                          ),
                  ),
                  if (viewAllergies)
                    Container(
                      height:
                          deviceSize.height + 190 * SizeConfig.getScaleFactor(),
                      child: AllergySelectionscreen(
                        patientid: arguments['patientID'],
                      ),
                    ),
                  if (viewlabreports)
                    Container(
                      height: deviceSize.height * 0.6 +
                          39 * SizeConfig.getScaleFactor(),
                      child: LabResultsDoctor(
                        patientid: arguments['patientID'],
                        visitid: arguments['visitid'],
                      ),
                    ),
                  // if (viewDiagnosis)
                  // Container(
                  //   height: deviceSize.height *
                  //       0.68 *
                  //       SizeConfig.getScaleFactor(),
                  //   child: DiagnosisSelectionScreen(arguments['visitid']),
                  // ),
                  if (viewAdmissionRequest)
                    Container(
                      height:
                          deviceSize.height + 75 * SizeConfig.getScaleFactor(),
                      child: AdmissionRequestScreen(
                        patientId: arguments['patientID'],
                        visitId: arguments['visitid'],
                      ),
                    ),
                  // if (viewGeneralInformation)
                  //   Container(
                  //     height: deviceSize.height * 0.8,
                  //     child: GeneralInformationScreen(),
                  //   ),
                  if (viewVitalSign)
                    Container(
                      height: deviceSize.height * 0.8,
                      child: VitalSignDoctor(),
                    ),

                  // if (viewSoapNotes)
                  //   Padding(
                  //     padding: const EdgeInsets.all(8.0),
                  //     child: Row(
                  //       mainAxisAlignment: MainAxisAlignment.end,
                  //       children: [
                  //         RaisedButton(
                  //           color: Theme.of(context).primaryColor,
                  //           textColor: Theme.of(context).buttonColor,
                  //           onPressed: _saveSoapNpotes,
                  //           child: Text('Save SOAP Detials'),
                  //         ),
                  //       ],
                  //     ),
                  //   ),
                  if (viewSoapNotes)
                    Consumer<DrugOrderModalDoctor>(
                      builder: (ctx, provisionalDiagnosis, _) => Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'PROVISIONAL DIAGNOSIS',
                                  style: TextStyle(
                                      color: Theme.of(context).primaryColor,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18),
                                ),
                                _editProvisionalDiagnosis
                                    ? Center(
                                        child: ElevatedButton(
                                          child: Text("Save"),
                                          onPressed: () =>
                                              _saveProvisionalDiagnosis(
                                                  _descriptionController.text),
                                        ),
                                      )
                                    : IconButton(
                                        icon: Icon(
                                          Icons.edit_outlined,
                                          size: 18,
                                          color: Theme.of(context).primaryColor,
                                        ),
                                        onPressed: () {
                                          setState(() {
                                            _descriptionController
                                                .text = provisionalDiagnosis
                                                        .provisionalDiagnosisText
                                                        .isEmpty ||
                                                    provisionalDiagnosis
                                                            .provisionalDiagnosisText[
                                                                0]
                                                            .provisional ==
                                                        "null"
                                                ? ""
                                                : provisionalDiagnosis
                                                    .provisionalDiagnosisText[0]
                                                    .provisional;

                                            _editProvisionalDiagnosis = true;
                                          });
                                        })
                              ],
                            ),
                            _editProvisionalDiagnosis && viewSoapNotes
                                ? TextFormField(
                                    scrollPadding:
                                        const EdgeInsets.only(bottom: 100.0),
                                    decoration: InputDecoration(
                                      contentPadding: EdgeInsets.all(20),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: const BorderRadius.all(
                                          const Radius.circular(1.0),
                                        ),
                                        borderSide: BorderSide(
                                            color:
                                                Theme.of(context).primaryColor,
                                            width: 2),
                                      ),
                                      border: OutlineInputBorder(
                                        borderRadius: const BorderRadius.all(
                                          const Radius.circular(1.0),
                                        ),
                                        borderSide: BorderSide(
                                            color:
                                                Theme.of(context).primaryColor,
                                            width: 2),
                                      ),
                                      disabledBorder: OutlineInputBorder(
                                        borderRadius: const BorderRadius.all(
                                          const Radius.circular(1.0),
                                        ),
                                        borderSide: BorderSide(
                                            color:
                                                Theme.of(context).primaryColor,
                                            width: 2),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: const BorderRadius.all(
                                          const Radius.circular(1.0),
                                        ),
                                        borderSide: BorderSide(
                                            color:
                                                Theme.of(context).primaryColor,
                                            width: 2),
                                      ),
                                      errorBorder: OutlineInputBorder(
                                        borderRadius: const BorderRadius.all(
                                          const Radius.circular(1.0),
                                        ),
                                        borderSide: BorderSide(
                                            color:
                                                Theme.of(context).primaryColor,
                                            width: 2),
                                      ),
                                      focusedErrorBorder: OutlineInputBorder(
                                        borderRadius: const BorderRadius.all(
                                          const Radius.circular(1.0),
                                        ),
                                        borderSide: BorderSide(
                                            color:
                                                Theme.of(context).primaryColor,
                                            width: 2),
                                      ),
                                    ),
                                    minLines: 2,
                                    maxLines: 5,
                                    controller: _descriptionController,
                                    onSaved: (value) {},
                                  )
                                : Text(provisionalDiagnosis
                                            .provisionalDiagnosisText.isEmpty ||
                                        provisionalDiagnosis
                                                .provisionalDiagnosisText[0]
                                                .provisional ==
                                            "null"
                                    ? ""
                                    : provisionalDiagnosis
                                        .provisionalDiagnosisText[0]
                                        .provisional),
                          ],
                        ),
                      ),
                    ),
                  if (viewSoapNotes)
                    FutureBuilder(
                        future: _editProvisionalDiagnosis || _editSoapNotes
                            ? null
                            : _refreshProducts(context),
                        builder: (ctx, snapshot) => snapshot.connectionState ==
                                ConnectionState.waiting
                            ? Center(
                                child: CircularProgressIndicator(),
                              )
                            : Consumer<DrugOrderModalDoctor>(
                                builder: (ctx, soapnotes, _) =>
                                    ListView.builder(
                                        physics: ScrollPhysics(),
                                        padding: EdgeInsets.only(top: 0),
                                        shrinkWrap: true,
                                        itemCount: soapnotes
                                            .loadSoapNoteDetials.length,
                                        itemBuilder: (context, index) {
                                          DoctorsSoapNoteDetials soap =
                                              soapnotes
                                                  .loadSoapNoteDetials[index];
                                          return Padding(
                                            padding: const EdgeInsets.all(15),
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Text(
                                                      soap.componentName,
                                                      style: TextStyle(
                                                          color:
                                                              Theme.of(context)
                                                                  .primaryColor,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: 18),
                                                    ),
                                                    soap.edit
                                                        ? Center(
                                                            child:
                                                                ElevatedButton(
                                                              child:
                                                                  Text("Save"),
                                                              onPressed: () =>
                                                                  _saveSoapNpotes(
                                                                      soap),
                                                            ),
                                                          )
                                                        : IconButton(
                                                            icon: Icon(
                                                              Icons
                                                                  .edit_outlined,
                                                              size: 15,
                                                              color: Theme.of(
                                                                      context)
                                                                  .primaryColor,
                                                            ),
                                                            onPressed: () {
                                                              setState(() {
                                                                _editSoapNotes =
                                                                    true;
                                                                soap.edit =
                                                                    true;
                                                              });
                                                            })
                                                  ],
                                                ),
                                                SizedBox(
                                                  height: 5,
                                                ),
                                                soap.edit
                                                    ? TextFormField(
                                                        scrollPadding:
                                                            const EdgeInsets
                                                                    .only(
                                                                bottom: 120.0),
                                                        decoration:
                                                            InputDecoration(
                                                          contentPadding:
                                                              EdgeInsets.all(
                                                                  20),
                                                          focusedBorder:
                                                              OutlineInputBorder(
                                                            borderRadius:
                                                                const BorderRadius
                                                                    .all(
                                                              const Radius
                                                                      .circular(
                                                                  1.0),
                                                            ),
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColor,
                                                                width: 2),
                                                          ),
                                                          border:
                                                              OutlineInputBorder(
                                                            borderRadius:
                                                                const BorderRadius
                                                                    .all(
                                                              const Radius
                                                                      .circular(
                                                                  1.0),
                                                            ),
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColor,
                                                                width: 2),
                                                          ),
                                                          disabledBorder:
                                                              OutlineInputBorder(
                                                            borderRadius:
                                                                const BorderRadius
                                                                    .all(
                                                              const Radius
                                                                      .circular(
                                                                  1.0),
                                                            ),
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColor,
                                                                width: 2),
                                                          ),
                                                          enabledBorder:
                                                              OutlineInputBorder(
                                                            borderRadius:
                                                                const BorderRadius
                                                                    .all(
                                                              const Radius
                                                                      .circular(
                                                                  1.0),
                                                            ),
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColor,
                                                                width: 2),
                                                          ),
                                                          errorBorder:
                                                              OutlineInputBorder(
                                                            borderRadius:
                                                                const BorderRadius
                                                                    .all(
                                                              const Radius
                                                                      .circular(
                                                                  1.0),
                                                            ),
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColor,
                                                                width: 2),
                                                          ),
                                                          focusedErrorBorder:
                                                              OutlineInputBorder(
                                                            borderRadius:
                                                                const BorderRadius
                                                                    .all(
                                                              const Radius
                                                                      .circular(
                                                                  1.0),
                                                            ),
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColor,
                                                                width: 2),
                                                          ),
                                                        ),
                                                        minLines: 3,
                                                        maxLines: 6,
                                                        onChanged: (value) {
                                                          soap.textvalue =
                                                              value;
                                                        },
                                                        onEditingComplete: () {
                                                          print(soap
                                                              .onChangeValue);
                                                          setState(() {});
                                                          soap.textvalue = soap
                                                              .onChangeValue;
                                                        },
                                                        controller:
                                                            new TextEditingController(
                                                                text: soap
                                                                    .textvalue),
                                                      )
                                                    : Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .fromLTRB(
                                                                5, 0, 20, 0),
                                                        child: Text(
                                                          soap.textvalue,
                                                          style: TextStyle(
                                                              fontSize: 15,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500),
                                                        ),
                                                      ),
                                              ],
                                            ),
                                          );
                                        }))),

                  // Expanded(
                  //   child: ListView(
                  //     children: [Text("hi")],
                  //   ),
                  // ),
                  // RefreshIndicator(
                  //   onRefresh: () => _refreshProducts(context),
                  //   child: Column(children: [
                  //     Expanded(
                  //         child: FutureBuilder(
                  //             future: _refreshProducts(context),
                  //             builder: (ctx, snapshot) => snapshot
                  //                         .connectionState ==
                  //                     ConnectionState.waiting
                  //                 ? Center(
                  //                     child: CircularProgressIndicator(),
                  //                   )
                  //                 : Consumer<DrugOrderModalDoctor>(
                  //                     builder: (ctx, soapnotes, _) =>
                  //                         ListView.builder(
                  //                             physics: ScrollPhysics(),
                  //                             padding:
                  //                                 EdgeInsets.only(top: 0),
                  //                             shrinkWrap: true,
                  //                             itemCount: soapnotes
                  //                                 .loadSoapNoteDetials.length,
                  //                             itemBuilder: (context, index) {
                  //                               DoctorsSoapNoteDetials soap =
                  //                                   soapnotes
                  //                                           .loadSoapNoteDetials[
                  //                                       index];
                  //                               return Column(
                  //                                 children: [
                  //                                   Text(
                  //                                     soap.componentName,
                  //                                   ),
                  //                                   Text(soap.textvalue),
                  //                                 ],
                  //                               );
                  //                             })))),
                  //   ]),
                  // ),
                ]),
                // FutureBuilder(
                //   future: _refreshProducts(context),
                //   builder: (ctx, snapshot) => snapshot.connectionState ==
                //           ConnectionState.waiting
                //       ? Center(
                //           child: CircularProgressIndicator(),
                //         )
                //       : Column(children: [
                //           Text(
                //             'Provisional Diagnosis',
                //           ),
                //           Text(_provisionalDiagnosisText == null
                //               ? ""
                //               : _provisionalDiagnosisText),
                //           Expanded(
                //               child: Consumer<DrugOrderModalDoctor>(
                //                   builder: (ctx, soapnotes, _) =>
                //                       SingleChildScrollView(
                //                           child: ListView.builder(
                //                               physics: ScrollPhysics(),
                //                               padding:
                //                                   EdgeInsets.only(top: 0),
                //                               shrinkWrap: true,
                //                               itemCount: soapnotes
                //                                   .loadSoapNoteDetials
                //                                   .length,
                //                               itemBuilder:
                //                                   (context, index) {
                //                                 DoctorsSoapNoteDetials
                //                                     soap = soapnotes
                //                                             .loadSoapNoteDetials[
                //                                         index];
                //                                 return soapnotes
                //                                         .loadSoapNoteDetials
                //                                         .isEmpty
                //                                     ? CircularProgressIndicator()
                //                                     : Column(
                //                                         children: [
                //                                           Text(
                //                                             soap.componentName,
                //                                           ),
                //                                           Text(soap
                //                                               .textvalue),
                //                                         ],
                //                                       );
                //                               })))),
                //           Padding(
                //             padding: EdgeInsets.only(
                //                 bottom: MediaQuery.of(context)
                //                     .viewInsets
                //                     .bottom),
                //           ),
                //         ]),

                // if (viewSoapNotes)
                //   expansion.ExpansionTile(
                //     initiallyExpanded: true,
                //     headerBackgroundColor: Theme.of(context).primaryColor,
                //     title: Text(
                //       'Provisional Diagnosis',
                //       style: TextStyle(color: Colors.white),
                //     ),
                //     // trailing: SizedBox(
                //     //   width: 80,
                //     //   child: GestureDetector(
                //     //     onTap: _showProvisionalDiagnosisAdd,
                //     //     child: Row(
                //     //       children: [
                //     //         Text(
                //     //           'ADD',
                //     //           style: TextStyle(
                //     //               color: Colors.white,
                //     //               fontWeight: FontWeight.bold),
                //     //         ),
                //     //         Icon(
                //     //           Ionicons.ios_add_circle_outline,
                //     //           color: Colors.white,
                //     //         ),
                //     //       ],
                //     //     ),
                //     //   ),
                //     // ),
                //     children: [
                //       Padding(
                //         padding: const EdgeInsets.all(8.0),
                //         child: TextFormField(
                //           controller: _descriptionController,
                //           decoration: InputDecoration(
                //             contentPadding: EdgeInsets.all(20),
                //             focusedBorder: OutlineInputBorder(
                //               borderRadius: const BorderRadius.all(
                //                 const Radius.circular(1.0),
                //               ),
                //               borderSide:
                //                   BorderSide(color: Theme.of(context).primaryColor, width: 2),
                //             ),
                //             border: OutlineInputBorder(
                //               borderRadius: const BorderRadius.all(
                //                 const Radius.circular(1.0),
                //               ),
                //               borderSide:
                //                   BorderSide(color: Theme.of(context).primaryColor, width: 2),
                //             ),
                //             disabledBorder: OutlineInputBorder(
                //               borderRadius: const BorderRadius.all(
                //                 const Radius.circular(1.0),
                //               ),
                //               borderSide:
                //                   BorderSide(color: Theme.of(context).primaryColor, width: 2),
                //             ),
                //             enabledBorder: OutlineInputBorder(
                //               borderRadius: const BorderRadius.all(
                //                 const Radius.circular(1.0),
                //               ),
                //               borderSide:
                //                   BorderSide(color: Theme.of(context).primaryColor, width: 2),
                //             ),
                //             errorBorder: OutlineInputBorder(
                //               borderRadius: const BorderRadius.all(
                //                 const Radius.circular(1.0),
                //               ),
                //               borderSide:
                //                   BorderSide(color: Theme.of(context).primaryColor, width: 2),
                //             ),
                //             focusedErrorBorder: OutlineInputBorder(
                //               borderRadius: const BorderRadius.all(
                //                 const Radius.circular(1.0),
                //               ),
                //               borderSide:
                //                   BorderSide(color: Theme.of(context).primaryColor, width: 2),
                //             ),
                //           ),
                //           minLines: 3,
                //           maxLines: 10,
                //           onChanged: (value) {
                //             setState(() {
                //               _descriptionController.text = value;
                //             });
                //           },
                //           onSaved: (value) {
                //             Provider.of<DrugOrderModalDoctor>(context,
                //                     listen: false)
                //                 .provisionalDiagnosis(
                //               _descriptionController.text,
                //             );
                //           },
                //         ),
                //       ),
                //     ],
                //   ),
                // if (viewSoapNotes)
                //   Consumer<DrugOrderModalDoctor>(
                //       builder: (ctx, soapnotes, _) => SingleChildScrollView(
                //             child: ListView.builder(
                //                 physics: ScrollPhysics(),
                //                 padding: EdgeInsets.only(top: 0),
                //                 shrinkWrap: true,
                //                 itemCount:
                //                     soapnotes.loadSoapNoteDetials.length,
                //                 itemBuilder: (context, index) {
                //                   DoctorsSoapNoteDetials soap =
                //                       soapnotes.loadSoapNoteDetials[index];
                //                   return expansion.ExpansionTile(
                //                     initiallyExpanded: index == 0,
                //                     headerBackgroundColor:
                //                         Theme.of(context).primaryColor,
                //                     title: Text(
                //                       '${soap.componentName}',
                //                       style: TextStyle(color: Colors.white),
                //                     ),
                //                     // trailing: SizedBox(
                //                     //   width: 80,
                //                     //   child: GestureDetector(
                //                     //     onTap: _showCheifComplaintsAdd,
                //                     //     child: Row(
                //                     //       children: [
                //                     //         Text(
                //                     //           'ADD',
                //                     //           style: TextStyle(
                //                     //               color: Colors.white,
                //                     //               fontWeight: FontWeight.bold),
                //                     //         ),
                //                     //         Icon(
                //                     //           Ionicons.ios_add_circle_outline,
                //                     //           color: Colors.white,
                //                     //         ),
                //                     //       ],
                //                     //     ),
                //                     //   ),
                //                     // ),
                //                     children: [
                //                       Padding(
                //                         padding: const EdgeInsets.all(8.0),
                //                         child: TextFormField(
                //                           // controller: TextEditingController(
                //                           //     text: loadSoapTextDetials[
                //                           //                 soap.componentId] ==
                //                           //             null
                //                           //         ? ""
                //                           //         : loadSoapTextDetials[
                //                           //                 soap.componentId]
                //                           //             .textValue),
                //                           decoration: InputDecoration(
                //                             contentPadding:
                //                                 EdgeInsets.all(20),
                //                             focusedBorder: OutlineInputBorder(
                //                               borderRadius:
                //                                   const BorderRadius.all(
                //                                 const Radius.circular(1.0),
                //                               ),
                //                               borderSide: BorderSide(
                //                                   color: Theme.of(context).primaryColor,
                //                                   width: 2),
                //                             ),
                //                             border: OutlineInputBorder(
                //                               borderRadius:
                //                                   const BorderRadius.all(
                //                                 const Radius.circular(1.0),
                //                               ),
                //                               borderSide: BorderSide(
                //                                   color: Theme.of(context).primaryColor,
                //                                   width: 2),
                //                             ),
                //                             disabledBorder:
                //                                 OutlineInputBorder(
                //                               borderRadius:
                //                                   const BorderRadius.all(
                //                                 const Radius.circular(1.0),
                //                               ),
                //                               borderSide: BorderSide(
                //                                   color: Theme.of(context).primaryColor,
                //                                   width: 2),
                //                             ),
                //                             enabledBorder: OutlineInputBorder(
                //                               borderRadius:
                //                                   const BorderRadius.all(
                //                                 const Radius.circular(1.0),
                //                               ),
                //                               borderSide: BorderSide(
                //                                   color: Theme.of(context).primaryColor,
                //                                   width: 2),
                //                             ),
                //                             errorBorder: OutlineInputBorder(
                //                               borderRadius:
                //                                   const BorderRadius.all(
                //                                 const Radius.circular(1.0),
                //                               ),
                //                               borderSide: BorderSide(
                //                                   color: Theme.of(context).primaryColor,
                //                                   width: 2),
                //                             ),
                //                             focusedErrorBorder:
                //                                 OutlineInputBorder(
                //                               borderRadius:
                //                                   const BorderRadius.all(
                //                                 const Radius.circular(1.0),
                //                               ),
                //                               borderSide: BorderSide(
                //                                   color: Theme.of(context).primaryColor,
                //                                   width: 2),
                //                             ),
                //                           ),
                //                           minLines: 3,
                //                           maxLines: 10,
                //                           // onEditingComplete: (){

                //                           // },
                //                           onSaved: (value) {
                //                             Provider.of<DrugOrderModalDoctor>(
                //                                     context,
                //                                     listen: false)
                //                                 .addNewSoapNote(
                //                               soap.componentId,
                //                               loadSoapTextDetials[
                //                                   soap.componentId],
                //                             );
                //                           },
                //                           onChanged: (value) {
                //                             if (value != null) {
                //                               addSoapNoteDescription(
                //                                   soap.componentId, value);
                //                               // loadSoapTextDetials[
                //                               //         soap.componentId]
                //                               //     .textValue = value;
                //                             }
                //                           },
                //                         ),
                //                       ),
                //                     ],
                //                   );
                //                 }),
                //           )),
                // if (viewSoapNotes)
                //   SizedBox(
                //     height: 120,
                //   ),
                // )
              ),
            ),
          )),
    );
  }
}
