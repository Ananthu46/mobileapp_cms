import 'package:date_time_picker/date_time_picker.dart';
import 'package:doctor_portal_cms/app/size_config.dart';
import 'package:doctor_portal_cms/provider/doctor/doctor_order_provider.dart';
import 'package:doctor_portal_cms/screens/doctor/doctors_select_items/allergies_doctor/allergy_reaction_popup.dart';
import 'package:doctor_portal_cms/screens/doctor/doctors_select_items/allergies_doctor/existing_allergies.dart';
import 'package:doctor_portal_cms/screens/doctor/doctors_select_items/allergies_doctor/new_allergyTo_popup.dart';
import 'package:doctor_portal_cms/widgets/buttons/round_flat_button.dart';
import 'package:doctor_portal_cms/widgets/drop_down/dropdown.dart';
import 'package:doctor_portal_cms/widgets/drop_down/suggestionBox.dart';
import 'package:doctor_portal_cms/widgets/popup_layout/popup_layout_box.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:doctor_portal_cms/widgets/expansion_widget/custom_expansion_widget.dart'
    as expansion;
import 'package:provider/provider.dart';

class AllergySelectionscreen extends StatefulWidget {
  static const routeName = 'allergy-screen';
  final String patientid;
  AllergySelectionscreen({this.patientid});
  @override
  _AllergySelectionscreenState createState() => _AllergySelectionscreenState();
}

class _AllergySelectionscreenState extends State<AllergySelectionscreen> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _typeAheadController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  // TextEditingController searchControllerPharmacy = new TextEditingController();
  // final List<DropdownMenuItem> items = [];
  String selectedValue;
  // Map<String, String> selectedValueMap = Map();
  String selectAllergyType;
  String selectAllergyTypeId = '0';
  String allergicToValue;
  String allergicToId;
  String selectOnSet;
  String selectOnSetId;
  String selectOnSetType;
  String selectOnSetTypeId;
  String selectAllergyStatus;
  String selectAllergyStatusId;
  bool checkBoxValue = false;
  String filter = '';
  String date;
  String description;
  // String reaction;
  bool _isLoading = false;
  bool _validator = false;
  showPopup(
    BuildContext context, {
    BuildContext popupContext,
  }) {
    Navigator.push(
      context,
      PopupLayout(
          top: 30,
          left: 30,
          right: 30,
          bottom: 10,
          child: AllergyreactionAddScreen()),
    );
  }

  showAllergyToPopup(BuildContext context, {BuildContext popupContext}) {
    Navigator.push(
      context,
      PopupLayout(
          top: 30,
          left: 30,
          right: 30,
          bottom: 10,
          child: AllergyTOAddScreen()),
    );
  }

  void _saveData() async {
    setState(() {
      _validator = false;
    });
    if (!_formKey.currentState.validate()) {
      // Invalid!
      return;
    }
    if (_validator) {
      return;
    }
    setState(() {
      _isLoading = true;
    });
    _formKey.currentState.save();
    if (selectAllergyStatusId == null) {
      setState(() {
        selectAllergyStatus = "Active";
        selectAllergyStatusId = "471";
      });
    }
    print(selectOnSetId);
    print(selectOnSetTypeId);
    await Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .addToSaveAllergyList(
      patientId: widget.patientid,
      selectAllergyType: selectAllergyType,
      selectAllergyTypeId: selectAllergyTypeId,
      allergicToId: allergicToId,
      allergicToValue: allergicToValue,
      checkBoxValue: checkBoxValue,
      date: date,
      description: description,
      selectAllergyStatus: selectAllergyStatus,
      selectAllergyStatusId: selectAllergyStatusId,
      selectOnSet: selectOnSet,
      selectOnSetId: selectOnSetId,
      selectOnSetType: selectOnSetType,
      selectOnSetTypeId: selectOnSetTypeId,
    );

    await Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .saveAllergyDetials(context);
    String response = Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .responsedata[0]
        .resp;
    if (response == "Saved") {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          backgroundColor: Colors.blue,
          content: const Text('Allergy Recordings Saved Sucessfully'),
        ),
      );
      Provider.of<DrugOrderModalDoctor>(context, listen: false)
          .clearAllAllergyData();

      setState(() {
        _isLoading = false;
        description = null;
        // reaction = null;
        selectAllergyType = null;
        selectAllergyTypeId = '0';
        allergicToValue = null;
        allergicToId = null;
        selectOnSet = null;
        selectOnSetId = null;
        selectOnSetType = null;
        selectOnSetTypeId = null;
        selectAllergyStatus = null;
        selectAllergyStatusId = null;
        checkBoxValue = false;
        _typeAheadController.text = '';
        _descriptionController.text = '';
      });
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          backgroundColor: Colors.red,
          content: const Text('Allergy Recordings Not saved'),
        ),
      );
    }
    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    SizeConfig().init(context);
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Scaffold(
        body: ListView(
          physics: ScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 8, right: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  RaisedButton(
                    color: Theme.of(context).primaryColor,
                    textColor: Theme.of(context).buttonColor,
                    onPressed: () {
                      Navigator.of(context).pushNamed(
                          ExistingAllergiesScreen.routeName,
                          arguments: {
                            'patientID': widget.patientid,
                          });
                    },
                    child: Text('Existing Allergies'),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: deviceSize.height + 115 * SizeConfig.getScaleFactor(),
                width: double.infinity,
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey),
                  color: Colors.white,
                ),
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            SizedBox(
                              width: 100 * SizeConfig.getScaleFactor(),
                              height: 40 * SizeConfig.getScaleFactor(),
                              child: Row(
                                children: [
                                  Text(
                                    'Allergy Type',
                                    style: TextStyle(
                                        color: Theme.of(context).primaryColor,
                                        fontWeight: FontWeight.bold),
                                    // style: Theme.of(context).textTheme.subtitle1,
                                  ),
                                  Text(
                                    '*',
                                    style: TextStyle(
                                        color: Theme.of(context).errorColor,
                                        fontWeight: FontWeight.bold),
                                    // style: Theme.of(context).textTheme.subtitle1,
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              width: 30 * SizeConfig.getScaleFactor(),
                            ),
                            SizedBox(
                              width: 200 * SizeConfig.getScaleFactor(),
                              height: 40 * SizeConfig.getScaleFactor(),
                              child: Consumer<DrugOrderModalDoctor>(
                                builder: (ctx, allergy, _) => DropdownWidget(
                                  value: selectAllergyType,
                                  items: allergy.allergyType
                                      .map((allergyTypevalue) {
                                    return DropdownMenuItem(
                                      child: new Text(allergyTypevalue.value),
                                      value: allergyTypevalue.value,
                                      onTap: () {
                                        setState(() {
                                          selectAllergyType =
                                              allergyTypevalue.value;
                                          selectAllergyTypeId =
                                              allergyTypevalue.id;
                                        });
                                      },
                                    );
                                  }).toList(),
                                  onChanged: (value) {
                                    setState(() {
                                      selectAllergyType = value;
                                    });
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10 * SizeConfig.getScaleFactor(),
                        ),
                        Flexible(
                          fit: FlexFit.loose,
                          child: Row(
                            children: [
                              SizedBox(
                                width: 100 * SizeConfig.getScaleFactor(),
                                height: 40 * SizeConfig.getScaleFactor(),
                                child: Row(
                                  children: [
                                    Text(
                                      'Allergy To',
                                      style: TextStyle(
                                          color: Theme.of(context).primaryColor,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      '*',
                                      style: TextStyle(
                                          color: Theme.of(context).errorColor,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                width: 30 * SizeConfig.getScaleFactor(),
                              ),
                              SizedBox(
                                width: 200 * SizeConfig.getScaleFactor(),
                                height: 40 * SizeConfig.getScaleFactor(),
                                child: SuggestionBoxWidget(
                                  suffixIcon: IconButton(
                                    onPressed: () {
                                      setState(() {
                                        this._typeAheadController.text = "";
                                        this._typeAheadController =
                                            new TextEditingController();
                                      });
                                    },
                                    icon: Icon(Icons.delete),
                                  ),
                                  controller: this._typeAheadController,
                                  onSuggestionSelected: (suggestion) {
                                    this._typeAheadController.text =
                                        suggestion['allergicto_name'];
                                    allergicToId = suggestion['allergicto_id'];
                                  },
                                  suggestionsCallback: (pattern) async {
                                    return await DrugOrderModalDoctor
                                        .loadAllergyToListItems(
                                            selectAllergyTypeId, pattern);
                                  },
                                  itemBuilder: (context, suggestion) {
                                    return ListTile(
                                      title:
                                          Text(suggestion['allergicto_name']),
                                    );
                                  },
                                  transitionBuilder:
                                      (context, suggestionsBox, controller) {
                                    return suggestionsBox;
                                  },
                                  onsaved: (value) {
                                    allergicToValue = value;
                                  },
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      setState(() {
                                        _validator = true;
                                      });
                                    }
                                    return;
                                  },
                                ),
                              ),
                              Container(
                                // color: Colors.blue,
                                child: SizedBox(
                                  width: 30 * SizeConfig.getScaleFactor(),
                                  child: IconButton(
                                    iconSize: 20 * SizeConfig.getScaleFactor(),
                                    onPressed: () {
                                      print('object');
                                      showAllergyToPopup(context);
                                    },
                                    icon: Icon(
                                      Icons.add_circle_rounded,
                                      color: Theme.of(context).primaryColor,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        if (_validator)
                          Row(
                            children: [
                              SizedBox(
                                width: 100 * SizeConfig.getScaleFactor(),
                                height: 20 * SizeConfig.getScaleFactor(),
                              ),
                              SizedBox(
                                width: 30 * SizeConfig.getScaleFactor(),
                              ),
                              SizedBox(
                                width: 200 * SizeConfig.getScaleFactor(),
                                height: 20 * SizeConfig.getScaleFactor(),
                                child: Text(
                                  '* field is requred',
                                  style: TextStyle(
                                      color: Theme.of(context).errorColor),
                                ),
                              )
                            ],
                          ),
                        SizedBox(
                          height: 10 * SizeConfig.getScaleFactor(),
                        ),
                        Row(
                          children: [
                            SizedBox(
                              width: 100 * SizeConfig.getScaleFactor(),
                              height: 40 * SizeConfig.getScaleFactor(),
                              child: Row(
                                children: [
                                  Text(
                                    'On Set',
                                    style: TextStyle(
                                        color: Theme.of(context).primaryColor,
                                        fontWeight: FontWeight.bold),
                                    // style: Theme.of(context).textTheme.subtitle1,
                                  ),
                                  Text(
                                    '*',
                                    style: TextStyle(
                                        color: Theme.of(context).errorColor,
                                        fontWeight: FontWeight.bold),
                                    // style: Theme.of(context).textTheme.subtitle1,
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              width: 30 * SizeConfig.getScaleFactor(),
                            ),
                            SizedBox(
                              width: 200 * SizeConfig.getScaleFactor(),
                              height: 40 * SizeConfig.getScaleFactor(),
                              child: Consumer<DrugOrderModalDoctor>(
                                builder: (ctx, allergy, _) => DropdownWidget(
                                  value: selectOnSet,
                                  items:
                                      allergy.allergyOnset.map((allergyOnset) {
                                    return DropdownMenuItem(
                                      child: new Text(allergyOnset.value),
                                      value: allergyOnset.value,
                                      onTap: () {
                                        selectOnSetId = allergyOnset.id;
                                        selectOnSet = allergyOnset.value;
                                        print(selectOnSetId);
                                      },
                                    );
                                  }).toList(),
                                  onChanged: (value) {
                                    setState(() {
                                      selectOnSet = value;
                                    });
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            SizedBox(
                              width: 100 * SizeConfig.getScaleFactor(),
                              height: 40 * SizeConfig.getScaleFactor(),
                              child: Row(
                                children: [
                                  Text(
                                    'OnSet Type',
                                    style: TextStyle(
                                        color: Theme.of(context).primaryColor,
                                        fontWeight: FontWeight.bold),
                                    // style: Theme.of(context).textTheme.subtitle1,
                                  ),
                                  Text(
                                    '*',
                                    style: TextStyle(
                                        color: Theme.of(context).errorColor,
                                        fontWeight: FontWeight.bold),
                                    // style: Theme.of(context).textTheme.subtitle1,
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              width: 30 * SizeConfig.getScaleFactor(),
                            ),
                            SizedBox(
                              width: 200 * SizeConfig.getScaleFactor(),
                              height: 40 * SizeConfig.getScaleFactor(),
                              child: Consumer<DrugOrderModalDoctor>(
                                builder: (ctx, allergy, _) => DropdownWidget(
                                  value: selectOnSetType,
                                  items: allergy.allergyOnsetType
                                      .map((allergyOnsetType) {
                                    return DropdownMenuItem(
                                      child: new Text(allergyOnsetType.value),
                                      value: allergyOnsetType.value,
                                      onTap: () {
                                        selectOnSetType =
                                            allergyOnsetType.value;
                                        selectOnSetTypeId = allergyOnsetType.id;
                                        print(selectOnSetTypeId);
                                      },
                                    );
                                  }).toList(),
                                  onChanged: (value) {
                                    setState(() {
                                      selectOnSetType = value;
                                    });
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10 * SizeConfig.getScaleFactor(),
                        ),
                        SizedBox(
                          width: 100 * SizeConfig.getScaleFactor(),
                          child: Text(
                            'OnSet Date',
                            style: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontWeight: FontWeight.bold),
                            // style: Theme.of(context).textTheme.subtitle1,
                          ),
                        ),
                        SizedBox(
                          height: 10 * SizeConfig.getScaleFactor(),
                        ),
                        SizedBox(
                          width: deviceSize.width *
                              0.4 *
                              SizeConfig.getScaleFactor(),
                          child: DateTimePicker(
                              type: DateTimePickerType.date,
                              dateMask: 'dd MMM, yyyy',
                              initialValue: DateTime.now().toString(),
                              firstDate: DateTime(2000),
                              lastDate: DateTime(2100),
                              icon: Icon(
                                Icons.event,
                                color: Theme.of(context).primaryColor,
                              ),
                              timeLabelText: "Hour",
                              // use24HourFormat: false,

                              // selectableDayPredicate: (date) {
                              //   if (date.weekday == 6 || date.weekday == 7) {
                              //     return false;
                              //   }
                              //   return true;
                              // },
                              onChanged: (val) {
                                setState(() {
                                  date = val;
                                });
                              },
                              onSaved: (val) {
                                setState(() {
                                  date = val;
                                });
                              }),
                        ),
                        SizedBox(
                          height: 10 * SizeConfig.getScaleFactor(),
                        ),
                        Row(
                          children: [
                            SizedBox(
                              width: 100 * SizeConfig.getScaleFactor(),
                              height: 40 * SizeConfig.getScaleFactor(),
                              child: Text(
                                'Allergy Status',
                                style: TextStyle(
                                    color: Theme.of(context).primaryColor,
                                    fontWeight: FontWeight.bold),
                                // style: Theme.of(context).textTheme.subtitle1,
                              ),
                            ),
                            SizedBox(
                              width: 30 * SizeConfig.getScaleFactor(),
                            ),
                            SizedBox(
                              width: 200 * SizeConfig.getScaleFactor(),
                              height: 40 * SizeConfig.getScaleFactor(),
                              child: Consumer<DrugOrderModalDoctor>(
                                builder: (ctx, allergy, _) => DropdownWidget(
                                  value: allergy.allergyStatus[0].value,
                                  items: allergy.allergyStatus
                                      .map((allergyStatus) {
                                    return DropdownMenuItem(
                                      child: new Text(allergyStatus.value),
                                      value: allergyStatus.value,
                                      onTap: () {
                                        selectAllergyStatus =
                                            allergyStatus.value;
                                        selectAllergyStatusId =
                                            allergyStatus.id;
                                        print(selectAllergyStatusId);
                                      },
                                    );
                                  }).toList(),
                                  onChanged: (value) {
                                    setState(() {
                                      selectAllergyStatus = value;
                                    });
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10 * SizeConfig.getScaleFactor(),
                        ),
                        Row(
                          children: [
                            Checkbox(
                              activeColor: Theme.of(context).primaryColor,
                              value: checkBoxValue,
                              onChanged: (bool value) {
                                setState(() {
                                  checkBoxValue = value;
                                });
                              },
                            ),
                            Text(
                              'No Known Allergies',

                              // style: Theme.of(context).textTheme.subtitle1,
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10),
                          child: Text(
                            'Remarks',
                            style: Theme.of(context).textTheme.subtitle1,
                          ),
                        ),
                        TextFormField(
                          scrollPadding: const EdgeInsets.only(bottom: 120),
                          controller: _descriptionController,
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(20),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(1.0),
                              ),
                              borderSide: BorderSide(width: 1),
                            ),
                            border: OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(1.0),
                              ),
                              borderSide: BorderSide(width: 1),
                            ),
                            disabledBorder: OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(1.0),
                              ),
                              borderSide: BorderSide(width: 1),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(1.0),
                              ),
                              borderSide: BorderSide(width: 1),
                            ),
                            errorBorder: OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(1.0),
                              ),
                              borderSide: BorderSide(width: 1),
                            ),
                            focusedErrorBorder: OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(1.0),
                              ),
                              borderSide: BorderSide(width: 1),
                            ),
                          ),
                          minLines: 2,
                          maxLines: 5,
                          onSaved: (value) {
                            description = value;
                          },
                        ),
                        SizedBox(
                          height: 10 * SizeConfig.getScaleFactor(),
                        ),
                        expansion.ExpansionTile(
                          initiallyExpanded: true,
                          headerBackgroundColor: Theme.of(context).primaryColor,
                          title: Text(
                            'Reactions',
                            style: TextStyle(color: Colors.white),
                          ),
                          trailing: SizedBox(
                            width: 90 * SizeConfig.getScaleFactor(),
                            child: OutlinedButton(
                              onPressed: () {
                                showPopup(context);
                              },
                              child: Row(
                                children: [
                                  Text(
                                    'ADD',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Icon(
                                    Ionicons.ios_add_circle_outline,
                                    color: Colors.white,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Consumer<DrugOrderModalDoctor>(
                                builder: (ctx, data, _) => TextFormField(
                                  controller: TextEditingController(
                                      text:
                                          data.addedReactionValues.join('\n')),
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.all(20),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: const BorderRadius.all(
                                        const Radius.circular(1.0),
                                      ),
                                      borderSide: BorderSide(width: 1),
                                    ),
                                    border: OutlineInputBorder(
                                      borderRadius: const BorderRadius.all(
                                        const Radius.circular(1.0),
                                      ),
                                      borderSide: BorderSide(width: 1),
                                    ),
                                    disabledBorder: OutlineInputBorder(
                                      borderRadius: const BorderRadius.all(
                                        const Radius.circular(1.0),
                                      ),
                                      borderSide: BorderSide(width: 1),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: const BorderRadius.all(
                                        const Radius.circular(1.0),
                                      ),
                                      borderSide: BorderSide(width: 1),
                                    ),
                                    errorBorder: OutlineInputBorder(
                                      borderRadius: const BorderRadius.all(
                                        const Radius.circular(1.0),
                                      ),
                                      borderSide: BorderSide(width: 1),
                                    ),
                                    focusedErrorBorder: OutlineInputBorder(
                                      borderRadius: const BorderRadius.all(
                                        const Radius.circular(1.0),
                                      ),
                                      borderSide: BorderSide(width: 1),
                                    ),
                                  ),
                                  minLines: 2,
                                  maxLines: 4,
                                  // onChanged: (value) {
                                  //   reaction = value;
                                  //   TextSelection.collapsed(
                                  //       offset: value.length + 1);
                                  // },
                                  // onSaved: (value) {
                                  //   reaction = value;
                                  // },
                                ),
                              ),
                            )
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            _isLoading
                                ? CircularProgressIndicator()
                                : RoundFlatButton(_saveData, 'Save Allergy'),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
