import 'package:doctor_portal_cms/app/size_config.dart';
import 'package:doctor_portal_cms/model/doctor/doctor_order_modal.dart';
import 'package:doctor_portal_cms/provider/doctor/doctor_order_provider.dart';
import 'package:doctor_portal_cms/widgets/drop_down/dropdown.dart';
import 'package:doctor_portal_cms/widgets/inputfields/round_textformfield_max.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';

class AllergyTOAddScreen extends StatefulWidget {
  @override
  _AllergyTOAddScreenState createState() => _AllergyTOAddScreenState();
}

class _AllergyTOAddScreenState extends State<AllergyTOAddScreen> {
  String selectAllergyType;
  String selectAllergyTypeId = '0';
  bool checkBoxValue = true;
  String description = "";
  TextEditingController _allergyNameController = new TextEditingController();
  TextEditingController _allergyRemarksController = new TextEditingController();
  void allergicTo(
      {String allergyDesc,
      String allergicTypeId,
      String remarks,
      bool isActive}) async {
    await Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .saveAllergyTO(
            allergicTypeId: allergicTypeId,
            allergyDesc: allergyDesc,
            isActive: isActive,
            remarks: remarks)
        .then((AllergyReactionsTO value) {
      if (value.response == "  Saved  ") {
        Fluttertoast.showToast(
            msg: "Saved",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIos: 1,
            backgroundColor: Colors.teal,
            textColor: Colors.white,
            fontSize: 12.0);
      } else {
        Fluttertoast.showToast(
            msg: " Request cannot be Completed ",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIos: 1,
            backgroundColor: Colors.teal,
            textColor: Colors.white,
            fontSize: 12.0);
      }
      print(value.response);
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      resizeToAvoidBottomInset: true,
      floatingActionButton: Container(
        width: 30 * SizeConfig.getScaleFactor(),
        height: 30 * SizeConfig.getScaleFactor(),
        child: FloatingActionButton(
          child: Icon(Icons.close),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      appBar: AppBar(),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: ListView(
          children: [
            _HeaderTextMandatory(
              text: "Allergy Name",
              widget: SizedBox(
                height: 40 * SizeConfig.getScaleFactor(),
                child: RoundTextFormFieldMaxLineWidget(
                  scrollpadding: const EdgeInsets.only(bottom: 120.0),
                  isSecureInput: false,
                  controller: _allergyNameController,
                ),
              ),
            ),
            _HeaderTextMandatory(
              text: "Allergy Type",
              widget: SizedBox(
                width: 180 * SizeConfig.getScaleFactor(),
                height: 35 * SizeConfig.getScaleFactor(),
                child: Consumer<DrugOrderModalDoctor>(
                  builder: (ctx, allergy, _) => DropdownWidget(
                    value: selectAllergyType,
                    items: allergy.allergyType.map((allergyTypevalue) {
                      return DropdownMenuItem(
                        child: new Text(allergyTypevalue.value),
                        value: allergyTypevalue.value,
                        onTap: () {
                          setState(() {
                            selectAllergyType = allergyTypevalue.value;
                            selectAllergyTypeId = allergyTypevalue.id;
                          });
                        },
                      );
                    }).toList(),
                    onChanged: (value) {
                      setState(() {
                        selectAllergyType = value;
                      });
                    },
                  ),
                ),
              ),
            ),
            // _HeaderText(
            //   text: "Remarks",
            //   widget: SizedBox(
            //     height: 40 * SizeConfig.getScaleFactor(),
            //     child: RoundTextFormFieldMaxLineWidget(
            //       scrollpadding: const EdgeInsets.only(bottom: 120.0),
            //       isSecureInput: false,
            //       controller: _allergyRemarksController,
            //     ),
            //   ),
            // ),
            _HeaderText(
              text: "Remarks",
              widget: SizedBox(
                // height: 40 * SizeConfig.getScaleFactor(),
                child: TextFormField(
                  scrollPadding: const EdgeInsets.only(bottom: 120),
                  controller: _allergyRemarksController,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(20),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(1.0),
                      ),
                      borderSide: BorderSide(width: 1),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(1.0),
                      ),
                      borderSide: BorderSide(width: 1),
                    ),
                    disabledBorder: OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(1.0),
                      ),
                      borderSide: BorderSide(width: 1),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(1.0),
                      ),
                      borderSide: BorderSide(width: 1),
                    ),
                    errorBorder: OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(1.0),
                      ),
                      borderSide: BorderSide(width: 1),
                    ),
                    focusedErrorBorder: OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(1.0),
                      ),
                      borderSide: BorderSide(width: 1),
                    ),
                  ),
                  minLines: 2,
                  maxLines: 5,
                  onSaved: (value) {
                    description = value;
                  },
                ),
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(
                        width: 130 * SizeConfig.getScaleFactor(),
                        child: Text(
                          "Active",
                          style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontSize: 16,
                              fontWeight: FontWeight.bold),
                        )),
                    SizedBox(
                      width: 20 * SizeConfig.getScaleFactor(),
                      child: Checkbox(
                        activeColor: Theme.of(context).primaryColor,
                        value: checkBoxValue,
                        onChanged: (bool value) {
                          setState(() {
                            checkBoxValue = value;
                          });
                        },
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 20 * SizeConfig.getScaleFactor(),
                ),
                Center(
                    child: SizedBox(
                        width: 150 * SizeConfig.getScaleFactor(),
                        child: ElevatedButton(
                            onPressed: () => allergicTo(
                                allergyDesc: _allergyNameController.text,
                                allergicTypeId: selectAllergyTypeId,
                                remarks: description,
                                isActive: checkBoxValue),
                            child: Text('Save'))))
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class _HeaderText extends StatelessWidget {
  final String text;
  final Widget widget;
  _HeaderText({this.text, this.widget});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
                width: 130 * SizeConfig.getScaleFactor(),
                child: Text(
                  text,
                  style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontSize: 16,
                      fontWeight: FontWeight.bold),
                )),
            SizedBox(
              width: 170 * SizeConfig.getScaleFactor(),
              child: widget,
            )
          ],
        ),
        SizedBox(
          height: 20 * SizeConfig.getScaleFactor(),
        ),
      ],
    );
  }
}

class _HeaderTextMandatory extends StatelessWidget {
  final String text;
  final Widget widget;
  _HeaderTextMandatory({this.text, this.widget});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
                width: 130 * SizeConfig.getScaleFactor(),
                child: Row(
                  children: [
                    Text(
                      text,
                      style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      "*",
                      style: TextStyle(
                          color: Theme.of(context).errorColor,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                )),
            SizedBox(
              width: 170 * SizeConfig.getScaleFactor(),
              child: widget,
            )
          ],
        ),
        SizedBox(
          height: 20 * SizeConfig.getScaleFactor(),
        ),
      ],
    );
  }
}
