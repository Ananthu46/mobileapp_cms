import 'package:doctor_portal_cms/app/size_config.dart';
import 'package:doctor_portal_cms/model/doctor/doctor_order_modal.dart';
import 'package:doctor_portal_cms/provider/doctor/doctor_order_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ExistingAllergiesScreen extends StatefulWidget {
  static const routeName = 'existing-allergies';

  @override
  _ExistingAllergiesScreenState createState() =>
      _ExistingAllergiesScreenState();
}

class _ExistingAllergiesScreenState extends State<ExistingAllergiesScreen> {
  String patientId;
  @override
  void initState() {
    Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .loadExistingAllergies();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // final deviceSize = MediaQuery.of(context).size;
    SizeConfig().init(context);
    return Scaffold(
        appBar: AppBar(
          title: Text('Existing Allergies'),
          centerTitle: true,
        ),
        body: Column(
          children: [
            Card(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 70 * SizeConfig.getScaleFactor(),
                      child: Center(child: Text('Visit no')),
                    ),
                    Container(
                      width: 105 * SizeConfig.getScaleFactor(),
                      child: Center(child: Text('Allergy Category')),
                    ),
                    Container(
                      width: 110 * SizeConfig.getScaleFactor(),
                      child: Center(child: Text('Allergic To')),
                    ),
                    Container(
                      width: 50 * SizeConfig.getScaleFactor(),
                      child: Center(child: Text('Status')),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              child: Consumer<DrugOrderModalDoctor>(
                  builder: (ctx, allergy, _) => SingleChildScrollView(
                      child: ListView.builder(
                          physics: ScrollPhysics(),
                          padding: EdgeInsets.only(top: 0),
                          shrinkWrap: true,
                          itemCount: allergy.loadExistingAllergiesList.length,
                          itemBuilder: (context, index) {
                            ExistingAllergies existingAllergy =
                                allergy.loadExistingAllergiesList[index];
                            return Card(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      width: 70 * SizeConfig.getScaleFactor(),
                                      child: Center(
                                          child: Text(
                                              '${existingAllergy.allergyId}')),
                                    ),
                                    Container(
                                      width: 105 * SizeConfig.getScaleFactor(),
                                      child: Center(
                                        child: Text(
                                            '${existingAllergy.allergyCategory}'),
                                      ),
                                    ),
                                    Container(
                                      width: 110 * SizeConfig.getScaleFactor(),
                                      child: Center(
                                          child: Text(
                                              '${existingAllergy.allergicTo}')),
                                    ),
                                    Container(
                                      width: 50 * SizeConfig.getScaleFactor(),
                                      child: Center(
                                          child: Text(
                                              '${existingAllergy.status}')),
                                    ),
                                  ],
                                ),
                              ),
                            );
                          }))),
            )
          ],
        ));
  }
}
