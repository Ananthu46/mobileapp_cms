import 'package:doctor_portal_cms/app/size_config.dart';
import 'package:doctor_portal_cms/model/doctor/doctor_order_modal.dart';
import 'package:doctor_portal_cms/provider/doctor/doctor_order_provider.dart';
import 'package:doctor_portal_cms/widgets/popup_layout/popup_content.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AllergyreactionAddScreen extends StatefulWidget {
  @override
  _AllergyreactionAddScreenState createState() =>
      _AllergyreactionAddScreenState();
}

class _AllergyreactionAddScreenState extends State<AllergyreactionAddScreen> {
  @override
  void initState() {
    Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .loadAllergyReactions();
    _checkedAllergyItem =
        Provider.of<DrugOrderModalDoctor>(context, listen: false)
            .checkedAllergyItem;
    super.initState();
  }

  bool _chechedvalue = false;
  Map<String, AllergyReactions> _checkedAllergyItem = {};
  void checboxSelectedPreviousOrder(AllergyReactions item, value) {
    if (value == true) {
      _checkedAllergyItem.putIfAbsent(
          item.lookupid, () => AllergyReactions(isSelected: true));
      Provider.of<DrugOrderModalDoctor>(context, listen: false)
          .allergyReactionAdd(item);
    } else {
      _checkedAllergyItem.removeWhere((key, value) => key == item.lookupid);
      Provider.of<DrugOrderModalDoctor>(context, listen: false)
          .allergyReactionDelete(item);
    }
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return PopupContent(
        content: Scaffold(
            floatingActionButton: Container(
              width: 30 * SizeConfig.getScaleFactor(),
              height: 30 * SizeConfig.getScaleFactor(),
              child: FloatingActionButton(
                child: Icon(Icons.close),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ),
            appBar: AppBar(
              title: Text('Reactions'),
              centerTitle: true,
              leading: new Builder(builder: (context) {
                return IconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () {
                    try {
                      Navigator.pop(context); //close the popup
                    } catch (e) {
                      print(e);
                    }
                  },
                );
              }),
              brightness: Brightness.light,
            ),
            resizeToAvoidBottomInset: false,
            body: Consumer<DrugOrderModalDoctor>(
              builder: (ctx, reaction, _) => ListView.builder(
                  physics: ScrollPhysics(),
                  padding: EdgeInsets.only(top: 0),
                  shrinkWrap: true,
                  itemCount: reaction.loadAllergyReaction.length,
                  itemBuilder: (context, index) {
                    AllergyReactions reactions =
                        reaction.loadAllergyReaction[index];
                    return CheckboxListTile(
                      title: Text('${reactions.lookupvalue}'),
                      value: _checkedAllergyItem.containsKey(reactions.lookupid)
                          ? _checkedAllergyItem[reactions.lookupid].isSelected
                          : _chechedvalue,
                      onChanged: (bool value) {
                        setState(() {
                          checboxSelectedPreviousOrder(reactions, value);
                        });
                      },
                      controlAffinity: ListTileControlAffinity.leading,
                    );
                  }),
            )));
  }
}
