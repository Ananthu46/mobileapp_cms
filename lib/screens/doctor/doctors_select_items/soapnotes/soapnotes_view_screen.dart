import 'package:doctor_portal_cms/model/doctor/doctor_order_modal.dart';
import 'package:doctor_portal_cms/model/drug/drug_order_api_modal.dart';
import 'package:doctor_portal_cms/provider/auth/login_auth.dart';
import 'package:doctor_portal_cms/provider/doctor/doctor_order_provider.dart';
import 'package:doctor_portal_cms/provider/drug/drug_order_api_provider.dart';
import 'package:doctor_portal_cms/screens/auth/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SoapNotesViewScreen extends StatefulWidget {
  @override
  _SoapNotesViewScreenState createState() => _SoapNotesViewScreenState();
}

class _SoapNotesViewScreenState extends State<SoapNotesViewScreen> {
  final _formKey = GlobalKey<FormState>();
  Map<String, DoctorsSoapTextDetials> loadSoapTextDetials = {};
  // String _provisionalDiagnosisText = "";
  bool _editSoapNotes = false;
  List<SelectedDepartment> deptDetials = [];
  TextEditingController _descriptionController = new TextEditingController();
  @override
  void initState() {
    super.initState();
    deptDetials = Provider.of<DrugPatientProvider>(context, listen: false)
        .selectedDepartmentdetials;
    _editSoapNotes = false;
    _editProvisionalDiagnosis = false;
    print(deptDetials.isEmpty);
    if (deptDetials.isEmpty) {
      Provider.of<DrugPatientProvider>(context, listen: false)
          .loadLocIdstores();
    } else {
      Provider.of<DrugPatientProvider>(context, listen: false).loadstores();
    }
  }

  Future<void> _refreshProducts(BuildContext context) async {
    print(_editProvisionalDiagnosis);
    print(_editSoapNotes);
    if (_editProvisionalDiagnosis || _editSoapNotes) {
      return null;
    } else {
      await loadSoapNoteitems();
    }
  }

  loadSoapNoteitems() async {
    Map arguments = ModalRoute.of(context).settings.arguments as Map;
    await Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .loadSoapNoteDetialslist(arguments['visitid']);
  }

  _saveSoapNpotes(DoctorsSoapNoteDetials soap) async {
    setState(() {
      soap.edit = false;
    });
    if (soap.textid.isNotEmpty || soap.textvalue.isNotEmpty) {
      _formKey.currentState.save();
      await Provider.of<DrugOrderModalDoctor>(context, listen: false)
          .sendSoapNotes(soap);
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          backgroundColor: Colors.blue,
          content: const Text('Saved Sucessfully'),
        ),
      );
    }
  }

  _saveSoapNpotesWithoutSnackbar(DoctorsSoapNoteDetials soap) async {
    if (soap.textid.isNotEmpty || soap.textvalue.isNotEmpty) {
      _formKey.currentState.save();
      await Provider.of<DrugOrderModalDoctor>(context, listen: false)
          .sendSoapNotes(soap);
    }
    setState(() {
      soap.edit = false;
    });

    // ScaffoldMessenger.of(context).showSnackBar(
    //   SnackBar(
    //     backgroundColor: Colors.blue,
    //     content: const Text('Saved Sucessfully'),
    //   ),
    // );
  }

  _saveProvisionalDiagnosis(String text) async {
    Map arguments = ModalRoute.of(context).settings.arguments as Map;
    setState(() {
      _editProvisionalDiagnosis = false;
    });
    _formKey.currentState.save();
    await Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .sendProvisionalDiagnosis(text, arguments['visitid']);
    // ScaffoldMessenger.of(context).showSnackBar(
    //   SnackBar(
    //     backgroundColor: Colors.blue,
    //     content: const Text('Saved Sucessfully'),
    //   ),
    // );
    await loadSoapNoteitems();
  }

  bool _editProvisionalDiagnosis = false;
  bool viewAllergies = false;
  bool viewDiagnosis = false;
  bool viewAdmissionRequest = false;
  // bool viewSoapNotes = true;
  bool viewGeneralInformation = false;
  bool viewVitalSign = false;
  bool viewlabreports = false;
  addSoapNoteDescription(id, value) {
    if (loadSoapTextDetials.containsKey(id)) {
      setState(() {
        loadSoapTextDetials.update(
            id,
            (v) => DoctorsSoapTextDetials(
                componentId: v.componentId,
                textValue: value,
                textId: v.textId));
      });
    } else {
      setState(() {
        loadSoapTextDetials.putIfAbsent(
            id,
            () => DoctorsSoapTextDetials(
                componentId: id, textValue: value, textId: ""));
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LoginAuth>(
        builder: (ctx, auth, _) => auth.isAuth
            ? Scaffold(
                body: Form(
                key: _formKey,
                child: ListView(
                  children: [
                    Consumer<DrugOrderModalDoctor>(
                      builder: (ctx, provisionalDiagnosis, _) => Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    if (_editProvisionalDiagnosis) {
                                      _saveProvisionalDiagnosis(
                                          _descriptionController.text);
                                    } else {
                                      setState(() {
                                        _descriptionController
                                            .text = provisionalDiagnosis
                                                    .provisionalDiagnosisText
                                                    .isEmpty ||
                                                provisionalDiagnosis
                                                        .provisionalDiagnosisText[
                                                            0]
                                                        .provisional ==
                                                    "null"
                                            ? ""
                                            : provisionalDiagnosis
                                                .provisionalDiagnosisText[0]
                                                .provisional;

                                        _editProvisionalDiagnosis = true;
                                      });
                                    }
                                  },
                                  child: Text(
                                    'PROVISIONAL DIAGNOSIS',
                                    style: TextStyle(
                                        color: Theme.of(context).primaryColor,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18),
                                  ),
                                ),
                                _editProvisionalDiagnosis
                                    ? Center(
                                        child: ElevatedButton(
                                          child: Text("Save"),
                                          onPressed: () =>
                                              _saveProvisionalDiagnosis(
                                                  _descriptionController.text),
                                        ),
                                      )
                                    : IconButton(
                                        icon: Icon(
                                          Icons.edit_outlined,
                                          size: 18,
                                          color: Theme.of(context).primaryColor,
                                        ),
                                        onPressed: () {
                                          setState(() {
                                            _descriptionController
                                                .text = provisionalDiagnosis
                                                        .provisionalDiagnosisText
                                                        .isEmpty ||
                                                    provisionalDiagnosis
                                                            .provisionalDiagnosisText[
                                                                0]
                                                            .provisional ==
                                                        "null"
                                                ? ""
                                                : provisionalDiagnosis
                                                    .provisionalDiagnosisText[0]
                                                    .provisional;

                                            _editProvisionalDiagnosis = true;
                                          });
                                        })
                              ],
                            ),
                            _editProvisionalDiagnosis
                                ? TextFormField(
                                    scrollPadding:
                                        const EdgeInsets.only(bottom: 100.0),
                                    decoration: InputDecoration(
                                      contentPadding: EdgeInsets.all(20),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: const BorderRadius.all(
                                          const Radius.circular(1.0),
                                        ),
                                        borderSide: BorderSide(
                                            color:
                                                Theme.of(context).primaryColor,
                                            width: 2),
                                      ),
                                      border: OutlineInputBorder(
                                        borderRadius: const BorderRadius.all(
                                          const Radius.circular(1.0),
                                        ),
                                        borderSide: BorderSide(
                                            color:
                                                Theme.of(context).primaryColor,
                                            width: 2),
                                      ),
                                      disabledBorder: OutlineInputBorder(
                                        borderRadius: const BorderRadius.all(
                                          const Radius.circular(1.0),
                                        ),
                                        borderSide: BorderSide(
                                            color:
                                                Theme.of(context).primaryColor,
                                            width: 2),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: const BorderRadius.all(
                                          const Radius.circular(1.0),
                                        ),
                                        borderSide: BorderSide(
                                            color:
                                                Theme.of(context).primaryColor,
                                            width: 2),
                                      ),
                                      errorBorder: OutlineInputBorder(
                                        borderRadius: const BorderRadius.all(
                                          const Radius.circular(1.0),
                                        ),
                                        borderSide: BorderSide(
                                            color:
                                                Theme.of(context).primaryColor,
                                            width: 2),
                                      ),
                                      focusedErrorBorder: OutlineInputBorder(
                                        borderRadius: const BorderRadius.all(
                                          const Radius.circular(1.0),
                                        ),
                                        borderSide: BorderSide(
                                            color:
                                                Theme.of(context).primaryColor,
                                            width: 2),
                                      ),
                                    ),
                                    minLines: 2,
                                    maxLines: 5,
                                    controller: _descriptionController,
                                    onSaved: (value) {},
                                  )
                                : Text(provisionalDiagnosis
                                            .provisionalDiagnosisText.isEmpty ||
                                        provisionalDiagnosis
                                                .provisionalDiagnosisText[0]
                                                .provisional ==
                                            "null"
                                    ? ""
                                    : provisionalDiagnosis
                                        .provisionalDiagnosisText[0]
                                        .provisional),
                          ],
                        ),
                      ),
                    ),
                    FutureBuilder(
                        future: _editProvisionalDiagnosis || _editSoapNotes
                            ? null
                            : _refreshProducts(context),
                        builder: (ctx, snapshot) => snapshot.connectionState ==
                                ConnectionState.waiting
                            ? Center(
                                child: CircularProgressIndicator(),
                              )
                            : Consumer<DrugOrderModalDoctor>(
                                builder: (ctx, soapnotes, _) =>
                                    ListView.builder(
                                        physics: ScrollPhysics(),
                                        padding: EdgeInsets.only(top: 0),
                                        shrinkWrap: true,
                                        itemCount: soapnotes
                                            .loadSoapNoteDetials.length,
                                        itemBuilder: (context, index) {
                                          DoctorsSoapNoteDetials soap =
                                              soapnotes
                                                  .loadSoapNoteDetials[index];
                                          int _itemcount = soapnotes
                                              .loadSoapNoteDetials.length;

                                          return Padding(
                                            padding: const EdgeInsets.all(15),
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    GestureDetector(
                                                      onTap: () {
                                                        if (_editSoapNotes &&
                                                            soap.edit) {
                                                          _saveSoapNpotes(soap);
                                                        } else {
                                                          for (int i = 0;
                                                              i < _itemcount;
                                                              i++) {
                                                            if (i != index) {
                                                              setState(() {
                                                                soapnotes
                                                                    .loadSoapNoteDetials[
                                                                        i]
                                                                    .edit = false;
                                                                _saveSoapNpotesWithoutSnackbar(
                                                                    soapnotes
                                                                        .loadSoapNoteDetials[i]);
                                                              });
                                                            }
                                                          }

                                                          setState(() {
                                                            _editSoapNotes =
                                                                true;
                                                            soap.edit = true;
                                                          });
                                                        }
                                                      },
                                                      child: Text(
                                                        soap.componentName,
                                                        style: TextStyle(
                                                            color: Theme.of(
                                                                    context)
                                                                .primaryColor,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontSize: 18),
                                                      ),
                                                    ),
                                                    soap.edit
                                                        ? Center(
                                                            child:
                                                                ElevatedButton(
                                                              child:
                                                                  Text("Save"),
                                                              onPressed: () =>
                                                                  _saveSoapNpotes(
                                                                      soap),
                                                            ),
                                                          )
                                                        : IconButton(
                                                            icon: Icon(
                                                              Icons
                                                                  .edit_outlined,
                                                              size: 15,
                                                              color: Theme.of(
                                                                      context)
                                                                  .primaryColor,
                                                            ),
                                                            onPressed: () {
                                                              setState(() {
                                                                _editSoapNotes =
                                                                    true;
                                                                soap.edit =
                                                                    true;
                                                              });
                                                            })
                                                  ],
                                                ),
                                                SizedBox(
                                                  height: 5,
                                                ),
                                                soap.edit
                                                    ? TextFormField(
                                                        scrollPadding:
                                                            const EdgeInsets
                                                                    .only(
                                                                bottom: 120.0),
                                                        decoration:
                                                            InputDecoration(
                                                          contentPadding:
                                                              EdgeInsets.all(
                                                                  20),
                                                          focusedBorder:
                                                              OutlineInputBorder(
                                                            borderRadius:
                                                                const BorderRadius
                                                                    .all(
                                                              const Radius
                                                                      .circular(
                                                                  1.0),
                                                            ),
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColor,
                                                                width: 2),
                                                          ),
                                                          border:
                                                              OutlineInputBorder(
                                                            borderRadius:
                                                                const BorderRadius
                                                                    .all(
                                                              const Radius
                                                                      .circular(
                                                                  1.0),
                                                            ),
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColor,
                                                                width: 2),
                                                          ),
                                                          disabledBorder:
                                                              OutlineInputBorder(
                                                            borderRadius:
                                                                const BorderRadius
                                                                    .all(
                                                              const Radius
                                                                      .circular(
                                                                  1.0),
                                                            ),
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColor,
                                                                width: 2),
                                                          ),
                                                          enabledBorder:
                                                              OutlineInputBorder(
                                                            borderRadius:
                                                                const BorderRadius
                                                                    .all(
                                                              const Radius
                                                                      .circular(
                                                                  1.0),
                                                            ),
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColor,
                                                                width: 2),
                                                          ),
                                                          errorBorder:
                                                              OutlineInputBorder(
                                                            borderRadius:
                                                                const BorderRadius
                                                                    .all(
                                                              const Radius
                                                                      .circular(
                                                                  1.0),
                                                            ),
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColor,
                                                                width: 2),
                                                          ),
                                                          focusedErrorBorder:
                                                              OutlineInputBorder(
                                                            borderRadius:
                                                                const BorderRadius
                                                                    .all(
                                                              const Radius
                                                                      .circular(
                                                                  1.0),
                                                            ),
                                                            borderSide: BorderSide(
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColor,
                                                                width: 2),
                                                          ),
                                                        ),
                                                        minLines: 3,
                                                        maxLines: 6,
                                                        onChanged: (value) {
                                                          soap.textvalue =
                                                              value;
                                                        },
                                                        onEditingComplete: () {
                                                          print(soap
                                                              .onChangeValue);
                                                          setState(() {});
                                                          soap.textvalue = soap
                                                              .onChangeValue;
                                                        },
                                                        controller:
                                                            new TextEditingController(
                                                                text: soap
                                                                    .textvalue),
                                                      )
                                                    : Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .fromLTRB(
                                                                5, 0, 20, 0),
                                                        child: Text(
                                                          soap.textvalue,
                                                          style: TextStyle(
                                                              fontSize: 15,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500),
                                                        ),
                                                      ),
                                              ],
                                            ),
                                          );
                                        }))),
                  ],
                ),
              ))
            : LoginInScreen());
  }
}
