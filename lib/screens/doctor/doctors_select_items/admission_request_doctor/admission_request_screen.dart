import 'package:date_time_picker/date_time_picker.dart';
import 'package:doctor_portal_cms/model/doctor/doctor_order_modal.dart';
import 'package:doctor_portal_cms/provider/doctor/doctor_order_provider.dart';
import 'package:doctor_portal_cms/widgets/buttons/round_flat_button.dart';
import 'package:doctor_portal_cms/widgets/drop_down/suggestionBox.dart';
import 'package:doctor_portal_cms/widgets/inputfields/SquareTextFormFieldWidget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AdmissionRequestScreen extends StatefulWidget {
  static const routeName = 'admission-request-screen';
  final String patientId;
  final String visitId;
  AdmissionRequestScreen({this.patientId, this.visitId});
  @override
  _AdmissionRequestScreenState createState() => _AdmissionRequestScreenState();
}

class _AdmissionRequestScreenState extends State<AdmissionRequestScreen> {
  String selectDepartment = '-- Select --';
  String selectDoctor = '-- Select --';
  String preferedBedType = '-- Select --';
  final TextEditingController _typeAheadController = TextEditingController();
  List<DoctorAdmissionDepartmentlist> admissionRequestDeptList;
  List<AdmissionRequestDoctorsList> admissionRequestDoctorsList;
  List<DoctorAdmissionBedType> admissionRequestBedType;
  List<DoctorAdmissionPhonenumber> admissionRequestphoneNo;
  bool _isLoading = false;
  void initState() {
    loadAdmissionRequest();
    super.initState();
  }

  loadAdmissionRequest() async {
    setState(() {
      _isLoading = true;
    });
    await Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .loadAdmissionRequestInfo(widget.patientId);
    Future.delayed(Duration.zero, () {
      setState(() {
        admissionRequestDeptList =
            Provider.of<DrugOrderModalDoctor>(context, listen: false)
                .admissionRequestDeptList;
        admissionRequestDoctorsList =
            Provider.of<DrugOrderModalDoctor>(context, listen: false)
                .admissionRequestDoctorsList;
        admissionRequestBedType =
            Provider.of<DrugOrderModalDoctor>(context, listen: false)
                .admissionRequestBedType;
        admissionRequestphoneNo =
            Provider.of<DrugOrderModalDoctor>(context, listen: false)
                .admissionRequestphoneNo;
        _isLoading = false;
      });
    });
  }

  final _formKey = GlobalKey<FormState>();
  String deptId;
  String consultantId;
  String date;
  String selectedbedTypeId;
  String lengthOfStay;
  String attachBed;
  String attachBedId;
  String mobileNo;
  String email;

  _submit() async {
    if (!_formKey.currentState.validate()) {
      // Invalid!
      return;
    }
    setState(() {
      _isLoading = true;
    });
    _formKey.currentState.save();

    await Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .addToAdmissionRequest(
            attachBed: attachBed == null ? "" : attachBed,
            consultantId: consultantId == null ? "" : consultantId,
            date: date == null ? DateTime.now().toString() : date,
            deptId: deptId == null ? "" : deptId,
            email: email == null ? "" : email,
            lengthOfStay: lengthOfStay == null ? "" : lengthOfStay,
            mobileNo: mobileNo == null ? "" : mobileNo,
            patientId: widget.patientId,
            selectedbedTypeId:
                selectedbedTypeId == null ? "" : selectedbedTypeId,
            attachBedId: attachBedId == null ? "" : attachBedId);

    await Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .saveAdmissionRequestInfo(context);
    String response = Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .responsedata[0]
        .resp;
    if (response == "Saved") {
      Provider.of<DrugOrderModalDoctor>(context, listen: false)
          .clearAllAdmissionRequestInfo();
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          backgroundColor: Colors.blue,
          content: const Text('Admission Request Saved Sucessfully'),
        ),
      );
      setState(() {
        _typeAheadController.value = new TextEditingValue();
      });
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          backgroundColor: Colors.red,
          content: const Text('Request cannot be Completed'),
        ),
      );
    }
    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(15),
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      'Department',
                      style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontWeight: FontWeight.bold),
                      // style: Theme.of(context).textTheme.subtitle1,
                    ),
                    Text(
                      ' *',
                      style: TextStyle(
                          color: Theme.of(context).errorColor,
                          fontWeight: FontWeight.bold),
                      // style: Theme.of(context).textTheme.subtitle1,
                    ),
                  ],
                ),
                SizedBox(
                  width: deviceSize.width * 0.9,
                  child: Consumer<DrugOrderModalDoctor>(
                      builder: (ctx, dept, _) => DropdownButtonFormField<
                              DoctorAdmissionDepartmentlist>(
                            hint: Text('--Select--'),
                            decoration: InputDecoration(
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).primaryColor,
                                    width: 2),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).primaryColor,
                                    width: 2),
                              ),
                              disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).primaryColor,
                                    width: 2),
                              ),
                              contentPadding: EdgeInsets.only(left: 10),
                              errorStyle: TextStyle(color: Colors.red),
                            ),
                            isExpanded: true,
                            //  value:admissionRequestDeptList.contains('') ,
                            onChanged:
                                (DoctorAdmissionDepartmentlist newValue) {
                              setState(() {
                                deptId = newValue.deptId;
                              });
                            },
                            items:
                                dept.admissionRequestDeptList.map((deptList) {
                              // selectedStoreId =
                              //     selectedstore.storeId;

                              return DropdownMenuItem(
                                child: new Text(deptList.deptName),
                                value: deptList,
                              );
                            }).toList(),
                            validator: (value) {
                              print(value);
                              if (value == null) {
                                return '* field is required';
                              }
                              return null;
                            },
                          )),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Text(
                      'Requesting Doctor',
                      style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontWeight: FontWeight.bold),
                      // style: Theme.of(context).textTheme.subtitle1,
                    ),
                    Text(
                      ' *',
                      style: TextStyle(
                          color: Theme.of(context).errorColor,
                          fontWeight: FontWeight.bold),
                      // style: Theme.of(context).textTheme.subtitle1,
                    ),
                  ],
                ),
                SizedBox(
                  width: deviceSize.width * 0.9,
                  child: Consumer<DrugOrderModalDoctor>(
                      builder: (ctx, doctor, _) =>
                          DropdownButtonFormField<AdmissionRequestDoctorsList>(
                            hint: Text('--Select--'),
                            decoration: InputDecoration(
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).primaryColor,
                                    width: 2),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).primaryColor,
                                    width: 2),
                              ),
                              disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).primaryColor,
                                    width: 2),
                              ),
                              contentPadding: EdgeInsets.only(left: 10),
                              errorStyle: TextStyle(color: Colors.red),
                            ),
                            isExpanded: true,
                            //  value: ,
                            onChanged: (AdmissionRequestDoctorsList newValue) {
                              setState(() {
                                consultantId = newValue.doctId;
                              });
                            },
                            items: doctor.admissionRequestDoctorsList
                                .map((docList) {
                              return DropdownMenuItem(
                                child: new Text(docList.doctName),
                                value: docList,
                              );
                            }).toList(),
                            validator: (value) {
                              print(value);
                              if (value == null) {
                                return '* field is required';
                              }
                              return null;
                            },
                          )),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Text(
                      'Admission Required on',
                      style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontWeight: FontWeight.bold),
                      // style: Theme.of(context).textTheme.subtitle1,
                    ),
                    Text(
                      ' *',
                      style: TextStyle(
                          color: Theme.of(context).errorColor,
                          fontWeight: FontWeight.bold),
                      // style: Theme.of(context).textTheme.subtitle1,
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                SizedBox(
                  width: deviceSize.width * 0.4,
                  child: DateTimePicker(
                    type: DateTimePickerType.date,
                    dateMask: 'dd MMM, yyyy',
                    initialValue: DateTime.now().toString(),
                    firstDate: DateTime(2000),
                    lastDate: DateTime(2100),
                    icon: Icon(
                      Icons.event,
                      color: Theme.of(context).primaryColor,
                    ),
                    timeLabelText: "Hour",
                    // use24HourFormat: false,

                    // selectableDayPredicate: (date) {
                    //   if (date.weekday == 6 || date.weekday == 7) {
                    //     return false;
                    //   }
                    //   return true;
                    // },
                    onChanged: (val) {
                      setState(() {
                        date = date;
                      });
                    },
                    validator: (value) {
                      if (value == null) {
                        return '* field is required';
                      }
                      return null;
                    },
                    // onSaved: (val) {
                    //   date = val;
                    // }
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'Prefered Bedtype',
                  style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontWeight: FontWeight.bold),
                  // style: Theme.of(context).textTheme.subtitle1,
                ),
                SizedBox(
                  width: deviceSize.width * 0.9,
                  child: Consumer<DrugOrderModalDoctor>(
                      builder: (ctx, bedType, _) =>
                          DropdownButtonFormField<DoctorAdmissionBedType>(
                            hint: Text('--Select--'),
                            decoration: InputDecoration(
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).primaryColor,
                                    width: 2),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).primaryColor,
                                    width: 2),
                              ),
                              disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).primaryColor,
                                    width: 2),
                              ),
                              contentPadding: EdgeInsets.only(left: 10),
                              errorStyle: TextStyle(color: Colors.red),
                            ),
                            isExpanded: true,
                            //  value: ,
                            onChanged: (DoctorAdmissionBedType newValue) {
                              setState(() {
                                selectedbedTypeId = newValue.bedId;
                              });
                            },
                            items: bedType.admissionRequestBedType.map((bed) {
                              // selectedStoreId =
                              //     selectedstore.storeId;

                              return DropdownMenuItem(
                                child: new Text(bed.bedType),
                                value: bed,
                              );
                            }).toList(),
                          )),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Text(
                      'Estimate Length of Stay',
                      style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      ' *',
                      style: TextStyle(
                          color: Theme.of(context).errorColor,
                          fontWeight: FontWeight.bold),
                      // style: Theme.of(context).textTheme.subtitle1,
                    ),
                  ],
                ),
                SquareTextFormFieldWidget(
                  scrollpadding: const EdgeInsets.only(bottom: 120),
                  isSecureInput: false,
                  keyBoardType: TextInputType.number,
                  validator: (value) {
                    if (value.isEmpty) {
                      return '* field is required';
                    }
                    return null;
                  },
                  onChanged: (v) {
                    lengthOfStay = v;
                  },
                ),
                // TextFormField(
                //   keyboardType: TextInputType.number,
                //   onChanged: (v) {
                //     lengthOfStay = v;
                //   },
                //   decoration: InputDecoration(
                //     contentPadding: EdgeInsets.only(left: 10),
                //   ),

                // ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'Attach Bed',
                  style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontWeight: FontWeight.bold),
                  // style: Theme.of(context).textTheme.subtitle1,
                ),
                SizedBox(
                  height: 10,
                ),
                SuggestionBoxWidget(
                  controller: this._typeAheadController,
                  onSuggestionSelected: (suggestion) {
                    this._typeAheadController.text = suggestion['bed_no'];
                    attachBedId = suggestion['bed_id'];
                  },
                  suggestionsCallback: (pattern) async {
                    return await DrugOrderModalDoctor.loadBedwithBedtype(
                        pattern, selectedbedTypeId);
                  },
                  itemBuilder: (context, suggestion) {
                    return ListTile(
                      leading: SizedBox(
                          width: 60,
                          height: 40,
                          child: Image.asset(
                              'assets/images/local_image/free_bed.png')),
                      title: Text(suggestion['bed_no']),
                    );
                  },
                  transitionBuilder: (context, suggestionsBox, controller) {
                    return suggestionsBox;
                  },
                  onsaved: (value) {
                    attachBed = value;
                  },
                  validator: (value) {
                    return;
                  },
                  //  validator: (value) {
                  //   if (value.isEmpty) {
                  //     return '* field is required';
                  //   }
                  //   return null;
                  // },
                ),

                // Padding(
                //     padding: const EdgeInsets.all(10.0),
                //     child: RoundTextFormFieldWidget(
                //       scrollpadding: const EdgeInsets.only(bottom: 200.0),
                //       isSecureInput: false,
                //       controller: searchController,
                //       prefixIcon: Icon(Icons.search),
                //       hinttext: 'Search',
                //     )),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'Mobile Number',
                  style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontWeight: FontWeight.bold),
                  // style: Theme.of(context).textTheme.subtitle1,
                ),
                Consumer<DrugOrderModalDoctor>(
                  builder: (ctx, phno, _) => SquareTextFormFieldWidget(
                    scrollpadding: const EdgeInsets.only(bottom: 120),
                    controller: TextEditingController(
                        text: phno.admissionRequestphoneNo.isEmpty
                            ? ''
                            : phno.admissionRequestphoneNo[0].phonenumber),
                    isSecureInput: false,
                    keyBoardType: TextInputType.number,
                    validator: (value) {
                      if (value.isEmpty) {
                        return '* field is required';
                      }
                      return null;
                    },
                    onChanged: (v) {
                      setState(() {
                        mobileNo = v;
                      });
                    },
                    prefixText: '+',
                  ),
                  // TextField(
                  //       onChanged: (v) {
                  //         setState(() {
                  //           mobileNo = v;
                  //         });
                  //       },
                  //       controller: TextEditingController(
                  //           text: phno.admissionRequestphoneNo.isEmpty
                  //               ? ''
                  //               : phno
                  //                   .admissionRequestphoneNo[0].phonenumber),
                  //       keyboardType: TextInputType.number,
                  //       decoration: InputDecoration(
                  //         prefixText: '+',
                  //         contentPadding: EdgeInsets.only(left: 10),

                  //         // hintText: 'Name Of Medication',
                  //         //  hintStyle:
                  //         //     TextStyle(color: Theme.of(context).hintColor)
                  //       ),
                  //     )
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'Email',
                  style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontWeight: FontWeight.bold),
                  // style: Theme.of(context).textTheme.subtitle1,
                ),
                SquareTextFormFieldWidget(
                  scrollpadding: const EdgeInsets.only(bottom: 120),
                  isSecureInput: false,
                  keyBoardType: TextInputType.number,
                  onChanged: (v) {
                    email = v;
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    _isLoading
                        ? CircularProgressIndicator()
                        : RoundFlatButton(_submit, 'Save Admission Request'),
                  ],
                ),
                SizedBox(
                  height: 50,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
