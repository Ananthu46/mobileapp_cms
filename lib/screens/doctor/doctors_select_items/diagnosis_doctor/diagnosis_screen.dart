// import 'package:doctor_portal_cms/model/doctor/doctor_order_modal.dart';
// import 'package:doctor_portal_cms/provider/doctor/doctor_order_provider.dart';
// import 'package:doctor_portal_cms/widgets/zefyr/src/widgets/controller.dart';
// import 'package:doctor_portal_cms/widgets/zefyr/src/widgets/field.dart';
// import 'package:doctor_portal_cms/widgets/zefyr/zefyr.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/rendering.dart';
// import 'package:provider/provider.dart';
// import 'package:quill_delta/quill_delta.dart';
// import 'package:notustohtml/notustohtml.dart';

// class DoctorDiagnosisScreen extends StatefulWidget {
//   static const routeName = 'doc-diagnosis';

//   @override
//   _DoctorDiagnosisScreenState createState() => _DoctorDiagnosisScreenState();
// }

// class _DoctorDiagnosisScreenState extends State<DoctorDiagnosisScreen> {
//   GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
//   ZefyrController _provisionalController;
//   ZefyrController _finalController;
//   final FocusNode _ffocusNode = FocusNode();
//   final FocusNode _pfocusNode = FocusNode();
//   GeneralInfoDetials previousGeneralInformation;
//   bool _isLoading = false;
//   PreviousDiagnosis previousDiagnosis;
//   String selectBloodGroup = '-- Select --';
//   @override
//   void initState() {
//     loadPreviousDiagnosis();
//     super.initState();
//   }

//   loadPreviousDiagnosis() async {
//     setState(() {
//       _isLoading = true;
//     });
//     await Provider.of<DrugOrderModalDoctor>(context, listen: false)
//         .loadDiagnosisInfo()
//         .then((value) {
//       previousDiagnosis = value;
//       if (previousDiagnosis != null) {
//         Delta fdelta = NotusHtmlCodec()
//             .decode("<p>" + previousDiagnosis.finalDiagnosis + "</p>");
//         Delta pdelta = NotusHtmlCodec()
//             .decode("<p>" + previousDiagnosis.provisionalDiagnosis + "</p>");
//         NotusDocument.fromDelta(fdelta);
//         NotusDocument.fromDelta(pdelta);
//         _provisionalController =
//             ZefyrController(NotusDocument.fromDelta(fdelta));
//         _finalController = ZefyrController(NotusDocument.fromDelta(pdelta));
//       }
//     });
//     setState(() {
//       _isLoading = false;
//     });
//   }

//   // _saveGeneralInformation() async {
//   //   print(_provisionalController.document);
//   //   final html = NotusHtmlCodec().encode(_provisionalController.document.toDelta());
//   //   print(html);
//   //   if (html.isNotEmpty) {
//   //     await Provider.of<DrugOrderModalDoctor>(context, listen: false)
//   //         .addGeneralinformation(html, previousGeneralInformation);
//   //     await Provider.of<DrugOrderModalDoctor>(context, listen: false)
//   //         .saveGeneralInformationList()
//   //         .then((value) {
//   //       ScaffoldMessenger.of(context).showSnackBar(
//   //         SnackBar(
//   //           backgroundColor: Theme.of(context).primaryColorDark,
//   //           content: Text('$value'),
//   //         ),
//   //       );
//   //     });
//   //   }
//   // }
//   ScrollController _scrollController = new ScrollController();
//   Widget build(BuildContext context) {
//     final provisionalDiagnosisEditor = ZefyrField(
//       decoration: new InputDecoration(
//         fillColor: Colors.white,
//         border: InputBorder.none,
//         focusedBorder: InputBorder.none,
//         enabledBorder: InputBorder.none,
//         errorBorder: InputBorder.none,
//         disabledBorder: InputBorder.none,
//         focusedErrorBorder: InputBorder.none,
//         filled: true,
//         contentPadding: EdgeInsets.only(bottom: 10.0, left: 10.0, right: 10.0),
//       ),
//       height: 180.0, // set the editor's height
//       controller: _provisionalController,
//       focusNode: _ffocusNode,
//       autofocus: false,
//       physics: ClampingScrollPhysics(),
//     );
//     final finalDiagnosisEditor = ZefyrField(
//       decoration: new InputDecoration(
//         fillColor: Colors.white,
//         border: InputBorder.none,
//         focusedBorder: InputBorder.none,
//         enabledBorder: InputBorder.none,
//         errorBorder: InputBorder.none,
//         disabledBorder: InputBorder.none,
//         focusedErrorBorder: InputBorder.none,
//         filled: true,
//         contentPadding: EdgeInsets.only(bottom: 10.0, left: 10.0, right: 10.0),
//       ),
//       height: 180.0, // set the editor's height
//       controller: _finalController,
//       focusNode: _pfocusNode,
//       autofocus: false,
//       physics: ClampingScrollPhysics(),
//     );
//     final form = Column(
//       mainAxisAlignment: MainAxisAlignment.start,
//       crossAxisAlignment: CrossAxisAlignment.start,
//       children: <Widget>[
//         Padding(
//           padding: const EdgeInsets.all(8.0),
//           child: Text(
//             'Provisional Diagnosis',
//             style: TextStyle(
//                 color: Theme.of(context).primaryColor,
//                 fontWeight: FontWeight.bold),
//             // style: Theme.of(context).textTheme.subtitle1,
//           ),
//         ),
//         Container(
//             decoration: BoxDecoration(
//               border: Border.all(),
//               borderRadius: BorderRadius.circular(10),
//             ),
//             child: Padding(
//               padding: const EdgeInsets.all(8.0),
//               child: provisionalDiagnosisEditor,
//             )),
//         Padding(
//           padding: const EdgeInsets.all(8.0),
//           child: Text(
//             'Final Diagnosis',
//             style: TextStyle(
//                 color: Theme.of(context).primaryColor,
//                 fontWeight: FontWeight.bold),
//             // style: Theme.of(context).textTheme.subtitle1,
//           ),
//         ),
//         GestureDetector(
//           onTap: () {
//             print('h');
//             _scrollController.animateTo(
//               200,
//               curve: Curves.easeOut,
//               duration: const Duration(milliseconds: 100),
//             );
//           },
//           child: Container(
//               decoration: BoxDecoration(
//                 border: Border.all(),
//                 borderRadius: BorderRadius.circular(10),
//               ),
//               child: Padding(
//                 padding: const EdgeInsets.all(8.0),
//                 child: finalDiagnosisEditor,
//               )),
//         ),
//         Row(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: [
//             SizedBox(
//                 width: 200,
//                 child: ElevatedButton(onPressed: () {}, child: Text('Save'))),
//           ],
//         )
//       ],
//     );

//     return GestureDetector(
//       onTap: () {
//         FocusScope.of(context).requestFocus(new FocusNode());
//       },
//       child: Scaffold(
//         body: _isLoading
//             ? Center(child: CircularProgressIndicator())
//             : ZefyrScaffold(
//                 key: _scaffoldKey,
//                 child: SingleChildScrollView(
//                   child: Padding(
//                       padding: const EdgeInsets.symmetric(horizontal: 20),
//                       child: form),
//                 ),
//               ),
//       ),
//     );
//   }
// }
