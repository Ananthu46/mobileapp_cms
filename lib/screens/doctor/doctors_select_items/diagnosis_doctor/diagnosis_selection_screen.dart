import 'dart:io';

import 'package:doctor_portal_cms/provider/doctor/doctor_order_provider.dart';
import 'package:doctor_portal_cms/widgets/buttons/round_flat_button.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_summernote/flutter_summernote.dart';
import 'package:html/parser.dart';
import 'package:provider/provider.dart';
// import 'package:webview_flutter/webview_flutter.dart';
import 'package:doctor_portal_cms/model/doctor/doctor_order_modal.dart';

class DiagnosisSelectionScreen extends StatefulWidget {
  static const routeName = 'diagnosis-screen';
  final String visitid;
  DiagnosisSelectionScreen(this.visitid);
  @override
  _DiagnosisSelectionScreenState createState() =>
      _DiagnosisSelectionScreenState();
}

class _DiagnosisSelectionScreenState extends State<DiagnosisSelectionScreen> {
  final _formKey = GlobalKey<FormState>();
  // GlobalKey<FlutterSummernoteState> _keyEditor = GlobalKey();
  // GlobalKey<FlutterSummernoteState> _keyEditorFinal = GlobalKey();
//  String selectedDropdownItem = '';
  // bool checkBoxValue = false;
  bool _isLoading = false;
  String selectBloodGroup = '-- Select --';
  void initState() {
    // if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
    loadPreviousDiagnosis();
    super.initState();
  }

  loadPreviousDiagnosis() async {
    setState(() {
      _isLoading = true;
    });
    await Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .loadDiagnosisInfo(widget.visitid)
        .then((value) {
      setState(() {
        _provisionalController.text =
            _parseHtmlString(value[0].provisionalDiagnosis);
        _finalController.text = _parseHtmlString(value[0].finalDiagnosis);
        _isLoading = false;
      });
    });
  }

  _saveDiagnosis() async {
    _formKey.currentState.save();
    await Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .saveDiagnosis(
            finalDiagnosis: _finalDiagnosis,
            provisional: _provisionalDiagnosis,
            visitId: widget.visitid);
  }

  TextEditingController _provisionalController = new TextEditingController();
  TextEditingController _finalController = new TextEditingController();
  String _provisionalDiagnosis;
  String _finalDiagnosis;
  String _parseHtmlString(String htmlString) {
    final document = parse(htmlString);
    final String parsedString = parse(document.body.text).documentElement.text;

    return parsedString;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Scaffold(
          body: Form(
            key: _formKey,
            child: ListView(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    // height: 432,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey),
                      color: Colors.white,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Provisional Diagnosis',
                            style: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontWeight: FontWeight.bold),
                            // style: Theme.of(context).textTheme.subtitle1,
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          _isLoading
                              ? Center(
                                  child: SizedBox(
                                      height: 20,
                                      width: 20,
                                      child: CircularProgressIndicator()),
                                )
                              : _TextEditingFieldWidget(
                                  controller: _provisionalController,
                                  onSaved: (value) {
                                    _provisionalDiagnosis = value;
                                  },
                                ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            'Final Diagnosis',
                            style: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontWeight: FontWeight.bold),
                            // style: Theme.of(context).textTheme.subtitle1,
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          _isLoading
                              ? Container()
                              : _TextEditingFieldWidget(
                                  controller: _finalController,
                                  onSaved: (value) {
                                    _finalDiagnosis = value;
                                  },
                                ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              RoundFlatButton(_saveDiagnosis, 'Save'),
                            ],
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                bottom:
                                    MediaQuery.of(context).viewInsets.bottom),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}

class _TextEditingFieldWidget extends StatelessWidget {
  final TextEditingController controller;
  final void Function(String) onSaved;
  _TextEditingFieldWidget({this.controller, this.onSaved});
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      scrollPadding: const EdgeInsets.only(bottom: 120),
      controller: controller,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(20),
        focusedBorder: OutlineInputBorder(
          borderRadius: const BorderRadius.all(
            const Radius.circular(1.0),
          ),
          borderSide: BorderSide(width: 1),
        ),
        border: OutlineInputBorder(
          borderRadius: const BorderRadius.all(
            const Radius.circular(1.0),
          ),
          borderSide: BorderSide(width: 1),
        ),
        disabledBorder: OutlineInputBorder(
          borderRadius: const BorderRadius.all(
            const Radius.circular(1.0),
          ),
          borderSide: BorderSide(width: 1),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: const BorderRadius.all(
            const Radius.circular(1.0),
          ),
          borderSide: BorderSide(width: 1),
        ),
        errorBorder: OutlineInputBorder(
          borderRadius: const BorderRadius.all(
            const Radius.circular(1.0),
          ),
          borderSide: BorderSide(width: 1),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderRadius: const BorderRadius.all(
            const Radius.circular(1.0),
          ),
          borderSide: BorderSide(width: 1),
        ),
      ),
      minLines: 5,
      maxLines: 8,
      onSaved: onSaved,
    );
  }
}

  // @override
  // Widget build(BuildContext context) {
  //   return Scaffold(
  //     body: Form(
  //       child: ListView(
  //         children: [
  //           Padding(
  //             padding: const EdgeInsets.all(8.0),
  //             child: Container(
  //               height: 432,
  //               width: double.infinity,
  //               decoration: BoxDecoration(
  //                 border: Border.all(color: Colors.grey),
  //                 color: Colors.white,
  //               ),
  //               child: Padding(
  //                 padding: const EdgeInsets.all(10.0),
  //                 child: Column(
  //                   crossAxisAlignment: CrossAxisAlignment.start,
  //                   children: [
  //                     Text(
  //                       'Provisional Diagnosis',
  //                       style: TextStyle(
  //                           color: Theme.of(context).primaryColor,
  //                           fontWeight: FontWeight.bold),
  //                       // style: Theme.of(context).textTheme.subtitle1,
  //                     ),
  //                     SizedBox(
  //                       height: 10,
  //                     ),
  //                     _isLoading
  //                         ? Center(
  //                             child: SizedBox(
  //                                 height: 20,
  //                                 width: 20,
  //                                 child: CircularProgressIndicator()),
  //                           )
  //                         : Container(
  //                             height: 150,
  //                             child: FlutterSummernote(
  //                               showBottomToolbar: false,
  //                               // hint: "Your text here...",
  //                               value: previousDiagnosis != null
  //                                   ? previousDiagnosis[0].provisionalDiagnosis
  //                                   : "",
  //                               key: _keyEditor,
  //                               hasAttachment: false,
  //                               customToolbar: """
  //                   [
  //                     ['style', ['bold', 'italic', 'underline', 'clear']],
  //                     ['font', ['strikethrough', 'superscript', 'subscript']],
  //                     ['insert', ['', 'fontsize', 'color'],]
  //                   ]
  //                 """,
  //                             ),
  //                           ),
  //                     SizedBox(
  //                       height: 10,
  //                     ),
  //                     Text(
  //                       'Final Diagnosis',
  //                       style: TextStyle(
  //                           color: Theme.of(context).primaryColor,
  //                           fontWeight: FontWeight.bold),
  //                       // style: Theme.of(context).textTheme.subtitle1,
  //                     ),
  //                     SizedBox(
  //                       height: 10,
  //                     ),
  //                     _isLoading
  //                         ? Container()
  //                         : Container(
  //                             height: 150,
  //                             child: FlutterSummernote(
  //                               showBottomToolbar: false,
  //                               // hint: "Your text here...",
  //                               value: previousDiagnosis != null
  //                                   ? previousDiagnosis[0].finalDiagnosis
  //                                   : "",
  //                               key: _keyEditorFinal,
  //                               hasAttachment: false,
  //                               customToolbar: """
  //                   [
  //                     ['style', ['bold', 'italic', 'underline', 'clear']],
  //                     ['font', ['strikethrough', 'superscript', 'subscript']],
  //                     ['insert', ['', 'fontsize', 'color'],]
  //                   ]
  //                 """,
  //                             ),
  //                           ),
  //                     Row(
  //                       mainAxisAlignment: MainAxisAlignment.center,
  //                       children: [
  //                         RoundFlatButton(_saveDiagnosis, 'Save'),
  //                       ],
  //                     ),
  //                     Padding(
  //                       padding: EdgeInsets.only(
  //                           bottom: MediaQuery.of(context).viewInsets.bottom),
  //                     ),
  //                   ],
  //                 ),
  //               ),
  //             ),
  //           ),
  //         ],
  //       ),
  //     ),
  //   );
  // }

