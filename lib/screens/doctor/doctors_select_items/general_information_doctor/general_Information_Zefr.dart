// import 'package:doctor_portal_cms/model/doctor/doctor_order_modal.dart';
// import 'package:doctor_portal_cms/provider/doctor/doctor_order_provider.dart';
// import 'package:doctor_portal_cms/widgets/zefyr/src/widgets/controller.dart';
// import 'package:doctor_portal_cms/widgets/zefyr/src/widgets/field.dart';
// import 'package:doctor_portal_cms/widgets/zefyr/zefyr.dart';
// import 'package:flutter/material.dart';
// import 'package:provider/provider.dart';
// import 'package:quill_delta/quill_delta.dart';
// import 'package:notustohtml/notustohtml.dart';

// class GeneralInformationZefyr extends StatefulWidget {
//   static const routeName = 'general-information-zefr';

//   @override
//   _GeneralInformationZefyrState createState() =>
//       _GeneralInformationZefyrState();
// }

// class _GeneralInformationZefyrState extends State<GeneralInformationZefyr> {
//   ZefyrController _controller;
//   final FocusNode _focusNode = FocusNode();
//   GeneralInfoDetials previousGeneralInformation;
//   bool _isLoading = false;
//   @override
//   void initState() {
//     loadGeneralInformation();
//     super.initState();
//   }

//   void loadGeneralInformation() async {
//     setState(() {
//       _isLoading = true;
//     });
//     await Provider.of<DrugOrderModalDoctor>(context, listen: false)
//         .loadGeneralInformationDetials()
//         .then((value) {
//       previousGeneralInformation = value;
//       Delta delta = NotusHtmlCodec().decode(value.generalInfo);
//       NotusDocument.fromDelta(delta);
//       _controller = ZefyrController(NotusDocument.fromDelta(delta));
//       return;
//     });
//     setState(() {
//       _isLoading = false;
//     });
//   }

//   _saveGeneralInformation() async {
//     print(_controller.document);
//     final html = NotusHtmlCodec().encode(_controller.document.toDelta());
//     print(html);
//     if (html.isNotEmpty) {
//       await Provider.of<DrugOrderModalDoctor>(context, listen: false)
//           .addGeneralinformation(html, previousGeneralInformation);
//       await Provider.of<DrugOrderModalDoctor>(context, listen: false)
//           .saveGeneralInformationList()
//           .then((value) {
//         ScaffoldMessenger.of(context).showSnackBar(
//           SnackBar(
//             backgroundColor: Theme.of(context).primaryColorDark,
//             content: Text('$value'),
//           ),
//         );
//       });
//     }
//   }

//   Widget build(BuildContext context) {
//     final editor = ZefyrField(
//       decoration: InputDecoration(
//           border: InputBorder.none, contentPadding: EdgeInsets.only(left: 10)),
//       height: 200.0, // set the editor's height
//       controller: _controller,
//       focusNode: _focusNode,
//       autofocus: true,
//       physics: ClampingScrollPhysics(),
//     );
//     final form = ListView(
//       children: <Widget>[
//         Container(
//             decoration: BoxDecoration(
//               border: Border.all(),
//               borderRadius: BorderRadius.circular(10),
//             ),
//             child: Padding(
//               padding: const EdgeInsets.all(8.0),
//               child: editor,
//             )),
//         SizedBox(
//             width: 50,
//             child: ElevatedButton(
//                 onPressed: _saveGeneralInformation, child: Text('Save')))
//       ],
//     );

//     return Scaffold(
//       body: _isLoading
//           ? Center(child: CircularProgressIndicator())
//           : ZefyrScaffold(
//               child: Padding(padding: const EdgeInsets.all(8.0), child: form),
//             ),
//     );
//   }
// }
