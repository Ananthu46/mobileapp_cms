import 'package:doctor_portal_cms/model/doctor/doctor_order_modal.dart';
import 'package:doctor_portal_cms/provider/doctor/doctor_order_provider.dart';
import 'package:doctor_portal_cms/widgets/buttons/squarebutton.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class GeneralInformationScreen extends StatefulWidget {
  static const routeName = 'general-information';
  final String patientId;
  GeneralInformationScreen(this.patientId);

  @override
  _GeneralInformationScreenState createState() =>
      _GeneralInformationScreenState();
}

class _GeneralInformationScreenState extends State<GeneralInformationScreen> {
  final _formKey = GlobalKey<FormState>();
  final _scaffoldState = GlobalKey<ScaffoldState>();
  GeneralInfoDetials previousGeneralInformation;
  bool _isLoading = false;
  @override
  void initState() {
    loadGeneralInformation();
    super.initState();
  }

  loadGeneralInformation() async {
    setState(() {
      _isLoading = true;
    });
    await Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .loadGeneralInformationDetials(widget.patientId)
        .then((value) {
      if (mounted)
        setState(() {
          previousGeneralInformation =
              Provider.of<DrugOrderModalDoctor>(context, listen: false)
                  .currentGeneralinformation;
          _generalController.text = previousGeneralInformation.generalInfo;
          _isLoading = false;
        });
    });
  }

  TextEditingController _generalController = new TextEditingController();
  String _generalInfo;
  _saveGeneralInformation() async {
    _formKey.currentState.save();
    print(_generalInfo != null);
    if (_generalInfo != null) {
      if (_generalInfo.isNotEmpty) {
        await Provider.of<DrugOrderModalDoctor>(context, listen: false)
            .addGeneralinformation(
                _generalInfo, previousGeneralInformation, widget.patientId);
        await Provider.of<DrugOrderModalDoctor>(context, listen: false)
            .saveGeneralInformationList()
            .then((value) {
          List<GeneralInfoDetials> generalInformationResponse =
              Provider.of<DrugOrderModalDoctor>(context, listen: false)
                  .generalInformationResponse;
          String res = generalInformationResponse[0].response;
          if (res == 'error') {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                backgroundColor: Colors.red,
                content: Text('An Error occured, Can\'t Complete request'),
              ),
            );
          } else {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                backgroundColor: Colors.blue,
                content: Text('$res'),
              ),
            );
          }
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Scaffold(
            key: _scaffoldState,
            body: _isLoading
                ? Center(child: CircularProgressIndicator())
                : ListView(children: [
                    Form(
                        key: _formKey,
                        child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                                // height: 432,
                                width: double.infinity,
                                decoration: BoxDecoration(
                                  border: Border.all(color: Colors.grey),
                                  color: Colors.white,
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'General Information',
                                        style: TextStyle(
                                            color:
                                                Theme.of(context).primaryColor,
                                            fontWeight: FontWeight.bold),
                                        // style: Theme.of(context).textTheme.subtitle1,
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      _TextEditingFieldWidget(
                                        controller: _generalController,
                                        onSaved: (value) {
                                          _generalInfo = value;
                                        },
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          SizedBox(
                                            width: 10,
                                          ),
                                          SquareFlatButton(
                                              _saveGeneralInformation, 'Save'),
                                        ],
                                      ),
                                    ],
                                  ),
                                ))))
                  ])));
  }
}

class _TextEditingFieldWidget extends StatelessWidget {
  final TextEditingController controller;
  final void Function(String) onSaved;
  _TextEditingFieldWidget({this.controller, this.onSaved});
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      scrollPadding: const EdgeInsets.only(bottom: 120),
      controller: controller,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(20),
        focusedBorder: OutlineInputBorder(
          borderRadius: const BorderRadius.all(
            const Radius.circular(1.0),
          ),
          borderSide: BorderSide(width: 1),
        ),
        border: OutlineInputBorder(
          borderRadius: const BorderRadius.all(
            const Radius.circular(1.0),
          ),
          borderSide: BorderSide(width: 1),
        ),
        disabledBorder: OutlineInputBorder(
          borderRadius: const BorderRadius.all(
            const Radius.circular(1.0),
          ),
          borderSide: BorderSide(width: 1),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: const BorderRadius.all(
            const Radius.circular(1.0),
          ),
          borderSide: BorderSide(width: 1),
        ),
        errorBorder: OutlineInputBorder(
          borderRadius: const BorderRadius.all(
            const Radius.circular(1.0),
          ),
          borderSide: BorderSide(width: 1),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderRadius: const BorderRadius.all(
            const Radius.circular(1.0),
          ),
          borderSide: BorderSide(width: 1),
        ),
      ),
      minLines: 5,
      maxLines: 15,
      onSaved: onSaved,
    );
  }
}
