// import 'dart:io';

// import 'package:doctor_portal_cms/model/doctor/doctor_order_modal.dart';
// import 'package:doctor_portal_cms/provider/doctor/doctor_order_provider.dart';
// import 'package:doctor_portal_cms/widgets/buttons/squarebutton.dart';
// import 'package:flutter/material.dart';
// import 'package:provider/provider.dart';
// import 'package:flutter_summernote/flutter_summernote.dart';
// import 'package:webview_flutter/webview_flutter.dart';

// class GeneralInformationScreen extends StatefulWidget {
//   static const routeName = 'general-information';
//   final String patientId;
//   GeneralInformationScreen(this.patientId);

//   @override
//   _GeneralInformationScreenState createState() =>
//       _GeneralInformationScreenState();
// }

// class _GeneralInformationScreenState extends State<GeneralInformationScreen> {
//   final _formKey = GlobalKey<FormState>();
//   final _scaffoldState = GlobalKey<ScaffoldState>();
//   GlobalKey<FlutterSummernoteState> _keyEditor = GlobalKey();
//   GeneralInfoDetials previousGeneralInformation;
//   bool _isLoading = false;
//   @override
//   void initState() {
//     if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
//     loadGeneralInformation();
//     super.initState();
//   }

//   loadGeneralInformation() async {
//     setState(() {
//       _isLoading = true;
//     });
//     await Provider.of<DrugOrderModalDoctor>(context, listen: false)
//         .loadGeneralInformationDetials(widget.patientId);
//     Future.delayed(Duration.zero, () {
//       if (mounted)
//         setState(() {
//           previousGeneralInformation =
//               Provider.of<DrugOrderModalDoctor>(context, listen: false)
//                   .currentGeneralinformation;
//           _isLoading = false;
//         });
//     });
//   }

//   _saveGeneralInformation() async {
//     // _formKey.currentState.save();
//     final value = await _keyEditor.currentState.getText();
//     print(value.isNotEmpty);
//     if (value.isNotEmpty) {
//       await Provider.of<DrugOrderModalDoctor>(context, listen: false)
//           .addGeneralinformation(
//               value, previousGeneralInformation, widget.patientId);
//       await Provider.of<DrugOrderModalDoctor>(context, listen: false)
//           .saveGeneralInformationList();
//       List<GeneralInfoDetials> generalInformationResponse =
//           Provider.of<DrugOrderModalDoctor>(context, listen: false)
//               .generalInformationResponse;
//       String res = generalInformationResponse[0].response;
//       ScaffoldMessenger.of(context).showSnackBar(
//         SnackBar(
//           backgroundColor: Colors.red,
//           content: Text('$res'),
//         ),
//       );
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       key: _scaffoldState,
//       body: ListView(
//         children: [
//           Form(
//               key: _formKey,
//               child: _isLoading
//                   ? CircularProgressIndicator()
//                   : Container(
//                       height: 400,
//                       child: FlutterSummernote(
//                         showBottomToolbar: false,
//                         hint: previousGeneralInformation == null
//                             ? "Your text here..."
//                             : '',
//                         value: previousGeneralInformation == null
//                             ? ""
//                             : previousGeneralInformation.generalInfo,
//                         key: _keyEditor,
//                         hasAttachment: false,
//                         customToolbar: """
//                   [
//                     ['style', ['bold', 'italic', 'underline', 'clear']],
//                     ['font', ['strikethrough', 'superscript', 'subscript']],
//                     ['insert', ['', 'fontsize', 'color'],]
//                   ]
//                 """,
//                       ),
//                     )),
//           Row(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: [
//               SizedBox(
//                 width: 10,
//               ),
//               SquareFlatButton(_saveGeneralInformation, 'Save'),
//             ],
//           ),
//         ],
//       ),
//     );
//   }
// }
