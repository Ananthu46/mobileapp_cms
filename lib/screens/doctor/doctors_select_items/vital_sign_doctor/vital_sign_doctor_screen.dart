import 'package:doctor_portal_cms/model/doctor/doctor_order_modal.dart';
import 'package:doctor_portal_cms/provider/doctor/doctor_order_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class VitalSignDoctor extends StatefulWidget {
  static const routeName = 'vital-sign';

  @override
  _VitalSignDoctorState createState() => _VitalSignDoctorState();
}

class _VitalSignDoctorState extends State<VitalSignDoctor> {
  String pulserate;
  String systaholicpressure;
  String diastrolicPressure;
  String grbs;
  String weight;
  String height;
  String temp;
  String spo2;
  String pulserateRemarks;
  String systaholicpressureRemarks;
  String diastrolicPressureRemarks;
  String grbsRemarks;
  String weightRemarks;
  String heightRemarks;
  String tempRemarks;
  String spo2Remarks;
  // Map<String, VitalSignsDetials> saveVitalSign = {};
  _addVitalSigns(
      {String parameterId,
      String value,
      String remarks,
      String controlType,
      String refFrom,
      String refTo,
      String vitalSignId,
      String uom}) async {
    await Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .addvitalSigns(parameterId, value, remarks, controlType, refFrom, refTo,
            vitalSignId, uom);
    // saveVitalSign =
    //     Provider.of<DrugOrderModalDoctor>(context, listen: false).saveVitalSign;
  }

  _saveVitalSigns(BuildContext context) async {
    await Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .sendVitalSign(context);
    String response = Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .responsedata[0]
        .resp;
    if (response == "Saved") {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          backgroundColor: Colors.blue,
          content: const Text('Vital Signs Saved Sucessfully'),
        ),
      );
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          backgroundColor: Colors.red,
          content: const Text('Not Saved'),
        ),
      );
    }
  }

  List<LoadVitalSigns> loadVitalParameter = [];
  @override
  void initState() {
    Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .loadVitalParameters();
    loadVitalParameter =
        Provider.of<DrugOrderModalDoctor>(context, listen: false)
            .loadVitalParameter;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(mainAxisSize: MainAxisSize.max, children: [
      Padding(
        padding: const EdgeInsets.only(top: 8, right: 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            ElevatedButton(
              // color: Theme.of(context).primaryColor,
              // textColor: Theme.of(context).buttonColor,
              onPressed: () => _saveVitalSigns(context),
              child: Text('Save Vital Signs'),
            ),
          ],
        ),
      ),
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Table(
            //  defaultColumnWidth: FixedColumnWidth(130),
            defaultVerticalAlignment: TableCellVerticalAlignment.middle,
            border: TableBorder.all(color: Colors.transparent),
            children: [
              TableRow(
                  decoration: BoxDecoration(color: Colors.grey[300]),
                  children: [
                    Center(
                        heightFactor: 2,
                        child: Text(
                          'Vital Signs',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        )),
                    Center(
                        heightFactor: 2,
                        child: Text(
                          'Current Value',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        )),
                    Center(
                        heightFactor: 2,
                        child: Text(
                          'unit',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        )),
                  ]),
            ]),
      ),
      Expanded(
          child: Consumer<DrugOrderModalDoctor>(
              builder: (ctx, vitalparameter, _) => ListView.builder(
                  physics: ScrollPhysics(),
                  padding: EdgeInsets.only(top: 10),
                  shrinkWrap: true,
                  itemCount: vitalparameter.loadVitalParameter.length,
                  itemBuilder: (context, index) {
                    LoadVitalSigns vital =
                        vitalparameter.loadVitalParameter[index];
                    return Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: Column(
                          children: [
                            Table(
                                //  defaultColumnWidth: FixedColumnWidth(130),
                                defaultVerticalAlignment:
                                    TableCellVerticalAlignment.middle,
                                border: TableBorder.all(color: Colors.black),
                                children: [
                                  TableRow(
                                    children: [
                                      Center(
                                          child: Padding(
                                        padding: const EdgeInsets.all(3.0),
                                        child: Text(vital.parametername,
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold)),
                                      )),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Container(
                                            width: 60,
                                            height: 30,
                                            child: (_SquareTextField(
                                              onchanged: (v) {
                                                pulserate = v;
                                                _addVitalSigns(
                                                  parameterId:
                                                      vital.parameterid,
                                                  controlType: vital.datatype,
                                                  refFrom: vital.refrangefrom,
                                                  refTo: vital.refrangeto,
                                                  uom: vital.uom,
                                                  value: v,
                                                );
                                              },
                                              // text: saveVitalSign[
                                              //             vital.parameterid] ==
                                              //         null
                                              //     ? ""
                                              //     : saveVitalSign[
                                              //             vital.parameterid]
                                              //         .value,
                                            ))),
                                      ),
                                      Center(
                                          child: Padding(
                                        padding: const EdgeInsets.all(3.0),
                                        child: Text(vital.unit,
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold)),
                                      )),
                                    ],
                                  ),
                                ]),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                // mainAxisSize: MainAxisSize.min,
                                children: [
                                  Row(
                                    children: [
                                      Text('Reference Range : '),
                                      Text(
                                        vital.refrangefrom +
                                            ' - ' +
                                            vital.refrangeto,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            color: Colors.brown),
                                      ),
                                    ],
                                  ),
                                  _ExtendedTextField(onchanged: (v) {
                                    _addVitalSigns(
                                      parameterId: vital.parameterid,
                                      remarks: v,
                                      controlType: vital.datatype,
                                      refFrom: vital.refrangefrom,
                                      refTo: vital.refrangeto,
                                      uom: vital.uom,
                                    );
                                  })
                                ],
                              ),
                            )
                          ],
                        ));
                  })))
    ]);
  }
}

class _SquareTextField extends StatelessWidget {
  final String text;
  final TextInputType keyboardType;
  final Function onchanged;
  _SquareTextField({
    this.text,
    this.keyboardType,
    this.onchanged,
  });

  @override
  Widget build(BuildContext context) {
    return new TextField(
      // maxLines: 4,
      textAlign: TextAlign.center,
      textAlignVertical: TextAlignVertical.center,
      keyboardType: keyboardType,
      // controller: TextEditingController(text: text),
      decoration: InputDecoration(
        enabledBorder: OutlineInputBorder(
          borderSide:
              BorderSide(color: Theme.of(context).primaryColor, width: 0),
        ),
        errorBorder: OutlineInputBorder(
          borderSide:
              BorderSide(color: Theme.of(context).primaryColor, width: 0),
        ),
        disabledBorder: OutlineInputBorder(
          borderSide:
              BorderSide(color: Theme.of(context).primaryColor, width: 0),
        ),
        // contentPadding: EdgeInsets.only(left: 5),
        focusedBorder: OutlineInputBorder(
          borderSide:
              BorderSide(color: Theme.of(context).primaryColor, width: 0),
        ),

        filled: true,
        fillColor: Colors.black12,
      ),
      // maxLines: 6,
      // minLines: 3,
      onChanged: (onchanged),
    );
  }
}

class _ExtendedTextField extends StatelessWidget {
  final String text;
  final TextInputType keyboardType;
  final Function onchanged;
  _ExtendedTextField({
    this.text,
    this.keyboardType,
    this.onchanged,
  });

  @override
  Widget build(BuildContext context) {
    return new SizedBox(
      height: 60,
      width: 150,
      child: TextField(
        decoration: InputDecoration(
          enabledBorder: OutlineInputBorder(
            borderSide:
                BorderSide(color: Theme.of(context).primaryColor, width: 0),
          ),
          errorBorder: OutlineInputBorder(
              borderSide: BorderSide(
            color: Theme.of(context).primaryColor,
            width: 0,
          )),
          disabledBorder: OutlineInputBorder(
            borderSide:
                BorderSide(color: Theme.of(context).primaryColor, width: 0),
          ),
          contentPadding: EdgeInsets.only(left: 20),
          focusedBorder: OutlineInputBorder(
            borderSide:
                BorderSide(color: Theme.of(context).primaryColor, width: 0),
          ),
          filled: true,
          fillColor: Colors.transparent,
        ),
        maxLines: null,
        expands: true,
        onChanged: (onchanged),
      ),
    );
  }
}
    // Column(
    //   mainAxisSize: MainAxisSize.min,
    //   children: [
    //     Card(
    //       child: ExpansionTile(title: Text('DISCHARGE SUMMARY'), children: [
    //         Center(
    //           child: Container(
    //               color: Colors.white,
    //               padding: EdgeInsets.all(10.0),
    //               child: Column(
    //                 children: [
    //                   Table(
    //                       //  defaultColumnWidth: FixedColumnWidth(130),
    //                       defaultVerticalAlignment:
    //                           TableCellVerticalAlignment.middle,
    //                       border: TableBorder.all(color: Colors.transparent),
    //                       children: [
    //                         TableRow(
    //                             decoration:
    //                                 BoxDecoration(color: Colors.grey[300]),
    //                             children: [
    //                               Center(
    //                                   heightFactor: 2,
    //                                   child: Text(
    //                                     'Vital Signs',
    //                                     style: TextStyle(
    //                                         fontWeight: FontWeight.bold),
    //                                   )),
    //                               Center(
    //                                   heightFactor: 2,
    //                                   child: Text(
    //                                     'Current Value',
    //                                     style: TextStyle(
    //                                         fontWeight: FontWeight.bold),
    //                                   )),
    //                               Center(
    //                                   heightFactor: 2,
    //                                   child: Text(
    //                                     'unit',
    //                                     style: TextStyle(
    //                                         fontWeight: FontWeight.bold),
    //                                   )),
    //                             ]),
    //                       ]),
    //                   Consumer<DrugOrderModalDoctor>(
    //                       builder: (ctx, vitalparameter, _) => ListView.builder(
    //                           physics: ScrollPhysics(),
    //                           padding: EdgeInsets.only(top: 0),
    //                           shrinkWrap: true,
    //                           itemCount:
    //                               vitalparameter.loadVitalParameter.length,
    //                           itemBuilder: (context, index) {
    //                             LoadVitalSigns vital =
    //                                 vitalparameter.loadVitalParameter[index];
    //                             return Table(
    //                                 //  defaultColumnWidth: FixedColumnWidth(130),
    //                                 defaultVerticalAlignment:
    //                                     TableCellVerticalAlignment.middle,
    //                                 border:
    //                                     TableBorder.all(color: Colors.black),
    //                                 children: [
    //                                   TableRow(
    //                                     children: [
    //                                       Center(
    //                                           child: Padding(
    //                                         padding: const EdgeInsets.all(3.0),
    //                                         child: Text(vital.parametername,
    //                                             style: TextStyle(
    //                                                 fontWeight:
    //                                                     FontWeight.bold)),
    //                                       )),
    //                                       Center(
    //                                           child: Padding(
    //                                         padding: const EdgeInsets.all(3.0),
    //                                         child: Text(vital.parametername,
    //                                             style: TextStyle(
    //                                                 fontWeight:
    //                                                     FontWeight.bold)),
    //                                       )),
    //                                       Center(
    //                                           child: Padding(
    //                                         padding: const EdgeInsets.all(3.0),
    //                                         child: Text(vital.parametername,
    //                                             style: TextStyle(
    //                                                 fontWeight:
    //                                                     FontWeight.bold)),
    //                                       )),
    //                                       // TextButton(
    //                                       //   child: Text("View"),
    //                                       //   onPressed: () {
    //                                       //     Navigator.push(
    //                                       //       context,
    //                                       //       MaterialPageRoute(
    //                                       //           builder: (context) =>
    //                                       //               VisitSummaryHtml(
    //                                       //                 visitId: discharge
    //                                       //                     .visitid,
    //                                       //                 consultant:
    //                                       //                     discharge
    //                                       //                         .consultant,
    //                                       //                 patientId:
    //                                       //                     patientid,
    //                                       //               )),
    //                                       //     );
    //                                       //   },
    //                                       // ),
    //                                     ],
    //                                   ),
    //                                 ]);
    //                           }))
    //                 ],
    //               )),
    //           //  ),
    //         ),
    //       ]),
    //     ),
    //   ],
    // );
//   }
// }
        //  SingleChildScrollView(
        //   scrollDirection: Axis.horizontal,
        //   child:
//         Column(mainAxisSize: MainAxisSize.min, children: [
//       Container(
//           height: 40,
//           width: 800,
//           color: Theme.of(context).primaryColor,
//           child: Expanded(
//               child: ListView(scrollDirection: Axis.horizontal, children: [
//             Padding(
//               padding: const EdgeInsets.all(8.0),
//               child: Row(children: [
//                 SizedBox(
//                   width: 100,
//                   child: Text(
//                     'Vital Sign',
//                     style: TextStyle(
//                         fontSize: 14,
//                         fontWeight: FontWeight.bold,
//                         color: Theme.of(context).scaffoldBackgroundColor),
//                   ),
//                 ),
//                 SizedBox(
//                   width: 100,
//                   child: Text(
//                     'Current value',
//                     style: TextStyle(
//                         fontSize: 14,
//                         fontWeight: FontWeight.bold,
//                         color: Theme.of(context).scaffoldBackgroundColor),
//                   ),
//                 ),
//                 SizedBox(
//                   width: 20,
//                 ),
//                 SizedBox(
//                   width: 80,
//                   child: Text(
//                     'Unit',
//                     style: TextStyle(
//                         fontSize: 14,
//                         fontWeight: FontWeight.bold,
//                         color: Theme.of(context).scaffoldBackgroundColor),
//                   ),
//                 ),
//                 SizedBox(
//                   width: 110,
//                   child: Text(
//                     'Reference Range',
//                     style: TextStyle(
//                         fontSize: 14,
//                         fontWeight: FontWeight.bold,
//                         color: Theme.of(context).scaffoldBackgroundColor),
//                   ),
//                 ),
//                 SizedBox(
//                   width: 20,
//                 ),
//                 SizedBox(
//                   width: 80,
//                   child: Text(
//                     'Remarks',
//                     style: TextStyle(
//                         fontSize: 14,
//                         fontWeight: FontWeight.bold,
//                         color: Theme.of(context).scaffoldBackgroundColor),
//                   ),
//                 )
//               ]),
//             )
//           ])))
//     ]);

//     // Expanded(
//     //     child: ListView(scrollDirection: Axis.horizontal, children: [
//     //   Container(
//     //       width: double.infinity,
//     //       height: 35,
//     //       color: Theme.of(context).primaryColor,
//     //       child: Padding(
//     //         padding: const EdgeInsets.all(8.0),
//     //         child: Row(
//     //           children: [
//     //             SizedBox(
//     //               width: 100,
//     //               child: Text(
//     //                 'Vital Sign',
//     //                 style: TextStyle(
//     //                     fontSize: 14,
//     //                     fontWeight: FontWeight.bold,
//     //                     color: Theme.of(context).scaffoldBackgroundColor),
//     //               ),
//     //             ),
//     //             SizedBox(
//     //               width: 100,
//     //               child: Text(
//     //                 'Current value',
//     //                 style: TextStyle(
//     //                     fontSize: 14,
//     //                     fontWeight: FontWeight.bold,
//     //                     color: Theme.of(context).scaffoldBackgroundColor),
//     //               ),
//     //             ),
//     //             SizedBox(
//     //               width: 20,
//     //             ),
//     //             SizedBox(
//     //               width: 80,
//     //               child: Text(
//     //                 'Unit',
//     //                 style: TextStyle(
//     //                     fontSize: 14,
//     //                     fontWeight: FontWeight.bold,
//     //                     color: Theme.of(context).scaffoldBackgroundColor),
//     //               ),
//     //             ),
//     //             SizedBox(
//     //               width: 110,
//     //               child: Text(
//     //                 'Reference Range',
//     //                 style: TextStyle(
//     //                     fontSize: 14,
//     //                     fontWeight: FontWeight.bold,
//     //                     color: Theme.of(context).scaffoldBackgroundColor),
//     //               ),
//     //             ),
//     //             SizedBox(
//     //               width: 20,
//     //             ),
//     //             SizedBox(
//     //               width: 80,
//     //               child: Text(
//     //                 'Remarks',
//     //                 style: TextStyle(
//     //                     fontSize: 14,
//     //                     fontWeight: FontWeight.bold,
//     //                     color: Theme.of(context).scaffoldBackgroundColor),
//     //               ),
//     //             ),
//     //           ],
//     //         ),
//     //       )),
//     //       Expanded(
//     //           child: Consumer<DrugOrderModalDoctor>(
//     //               builder: (ctx, vitalparameter, _) => ListView.builder(
//     //                   // scrollDirection: Axis.horizontal,
//     //                   padding: EdgeInsets.only(
//     //                     top: 0 * SizeConfig.getScaleFactor(),
//     //                   ),
//     //                   shrinkWrap: true,
//     //                   itemCount: vitalparameter.loadVitalParameter.length,
//     //                   itemBuilder: (context, index) {
//     //                     LoadVitalSigns result =
//     //                         vitalparameter.loadVitalParameter[index];

//     //                     return vitalparameter.loadVitalParameter.isEmpty
//     //                         ? SizedBox()
//     //                         : Padding(
//     //                             padding: const EdgeInsets.all(8.0),
//     //                             child: Row(
//     //                               children: [
//     //                                 SizedBox(
//     //                                   width: 100,
//     //                                   child: Text(
//     //                                     'Vital Sign',
//     //                                     style: TextStyle(
//     //                                         fontSize: 14,
//     //                                         fontWeight: FontWeight.bold,
//     //                                         color: Theme.of(context)
//     //                                             .scaffoldBackgroundColor),
//     //                                   ),
//     //                                 ),
//     //                                 SizedBox(
//     //                                   width: 100,
//     //                                   child: Text(
//     //                                     'Current value',
//     //                                     style: TextStyle(
//     //                                         fontSize: 14,
//     //                                         fontWeight: FontWeight.bold,
//     //                                         color: Theme.of(context)
//     //                                             .scaffoldBackgroundColor),
//     //                                   ),
//     //                                 ),
//     //                                 SizedBox(
//     //                                   width: 20,
//     //                                 ),
//     //                                 SizedBox(
//     //                                   width: 80,
//     //                                   child: Text(
//     //                                     'Unit',
//     //                                     style: TextStyle(
//     //                                         fontSize: 14,
//     //                                         fontWeight: FontWeight.bold,
//     //                                         color: Theme.of(context)
//     //                                             .scaffoldBackgroundColor),
//     //                                   ),
//     //                                 ),
//     //                                 SizedBox(
//     //                                   width: 110,
//     //                                   child: Text(
//     //                                     'Reference Range',
//     //                                     style: TextStyle(
//     //                                         fontSize: 14,
//     //                                         fontWeight: FontWeight.bold,
//     //                                         color: Theme.of(context)
//     //                                             .scaffoldBackgroundColor),
//     //                                   ),
//     //                                 ),
//     //                                 SizedBox(
//     //                                   width: 20,
//     //                                 ),
//     //                                 SizedBox(
//     //                                   width: 80,
//     //                                   child: Text(
//     //                                     'Remarks',
//     //                                     style: TextStyle(
//     //                                         fontSize: 14,
//     //                                         fontWeight: FontWeight.bold,
//     //                                         color: Theme.of(context)
//     //                                             .scaffoldBackgroundColor),
//     //                                   ),
//     //                                 ),
//     //                               ],
//     //                             ),
//     //                           );

//     //                     // // Expanded(
//     //                     // //   child: ListView(
//     //                     // //     scrollDirection: Axis.horizontal,
//     //                     // //     children: [
//     //                     // //       DataTable(
//     //                     // //         columnSpacing: 20,
//     //                     // //         showCheckboxColumn: false,
//     //                     // //         headingRowColor: MaterialStateColor.resolveWith((states) =>
//     //                     // //             Theme.of(context).primaryColor), // <-- this is important
//     //                     // //         columns: [
//     //                     // //           DataColumn(
//     //                     // //               label: Text(
//     //                     // //             'Vital Sign',
//     //                     // //             style: TextStyle(
//     //                     // //                 color: Theme.of(context).scaffoldBackgroundColor),
//     //                     // //           )),
//     //                     // //           DataColumn(
//     //                     // //             label: Container(
//     //                     // //               width: 100,
//     //                     // //               child: Text('Current Value',
//     //                     // //                   style: TextStyle(
//     //                     // //                       color:
//     //                     // //                           Theme.of(context).scaffoldBackgroundColor)),
//     //                     // //             ),
//     //                     // //           ),
//     //                     // //           DataColumn(
//     //                     // //             label: Text('Unit',
//     //                     // //                 style: TextStyle(
//     //                     // //                     color: Theme.of(context).scaffoldBackgroundColor)),
//     //                     // //           ),
//     //                     // //           DataColumn(
//     //                     // //             label: Text('Reference Range',
//     //                     // //                 style: TextStyle(
//     //                     // //                     color: Theme.of(context).scaffoldBackgroundColor)),
//     //                     // //           ),
//     //                     // //           DataColumn(
//     //                     // //             label: Text('Remarks',
//     //                     // //                 style: TextStyle(
//     //                     // //                     color: Theme.of(context).scaffoldBackgroundColor)),
//     //                     // //           ),
//     //                     // //         ],
//     //                     // //         rows: [],
//     //                     // //       ),
//     //                     // //     ],
//     //                     // //   ),
//     //                     // // ),
//     //                     // Expanded(
//     //                     //     child: Consumer<DrugOrderModalDoctor>(
//     //                     //   builder: (ctx, vitalparameter, _) => ListView.builder(
//     //                     //       // scrollDirection: Axis.horizontal,
//     //                     //       padding: EdgeInsets.only(
//     //                     //         top: 0 * SizeConfig.getScaleFactor(),
//     //                     //       ),
//     //                     //       shrinkWrap: true,
//     //                     //       itemCount: vitalparameter.loadVitalParameter.length,
//     //                     //       itemBuilder: (context, index) {
//     //                     //         LoadVitalSigns result =
//     //                     //             vitalparameter.loadVitalParameter[index];

//     //                     //         return vitalparameter.loadVitalParameter.isEmpty
//     //                     //             ? SizedBox()
//     //                     //             : Padding(
//     //                     //                 padding: const EdgeInsets.all(8.0),
//     //                     //                 child: DataTable(
//     //                     //                     columnSpacing: 20,
//     //                     //                     showCheckboxColumn: false,
//     //                     //                     headingRowColor: MaterialStateColor.resolveWith(
//     //                     //                         (states) => Theme.of(context).primaryColor),
//     //                     //                     columns: [
//     //                     //                       DataColumn(
//     //                     //                           label: Text(
//     //                     //                         'Vital Sign',
//     //                     //                         style: TextStyle(
//     //                     //                             color: Theme.of(context)
//     //                     //                                 .scaffoldBackgroundColor),
//     //                     //                       )),
//     //                     //                       DataColumn(
//     //                     //                         label: Container(
//     //                     //                           width: 100,
//     //                     //                           child: Text('Current Value',
//     //                     //                               style: TextStyle(
//     //                     //                                   color: Theme.of(context)
//     //                     //                                       .scaffoldBackgroundColor)),
//     //                     //                         ),
//     //                     //                       ),
//     //                     //                       DataColumn(
//     //                     //                         label: Text('Unit',
//     //                     //                             style: TextStyle(
//     //                     //                                 color: Theme.of(context)
//     //                     //                                     .scaffoldBackgroundColor)),
//     //                     //                       ),
//     //                     //                       DataColumn(
//     //                     //                         label: Text('Reference Range',
//     //                     //                             style: TextStyle(
//     //                     //                                 color: Theme.of(context)
//     //                     //                                     .scaffoldBackgroundColor)),
//     //                     //                       ),
//     //                     //                       DataColumn(
//     //                     //                         label: Text('Remarks',
//     //                     //                             style: TextStyle(
//     //                     //                                 color: Theme.of(context)
//     //                     //                                     .scaffoldBackgroundColor)),
//     //                     //                       ),
//     //                     //                     ], // <-- this is important
//     //                     //                     // columns: [
//     //                     //                     //   DataColumn(
//     //                     //                     //       label: Text(
//     //                     //                     //     'Vital Sign',
//     //                     //                     //     style: TextStyle(
//     //                     //                     //         color: Theme.of(context).scaffoldBackgroundColor),
//     //                     //                     //   )),
//     //                     //                     //   DataColumn(
//     //                     //                     //     label: Container(
//     //                     //                     //       width: 100,
//     //                     //                     //       child: Text('Current Value',
//     //                     //                     //           style: TextStyle(
//     //                     //                     //               color: Theme.of(context)
//     //                     //                     //                   .scaffoldBackgroundColor)),
//     //                     //                     //     ),
//     //                     //                     //   ),
//     //                     //                     //   DataColumn(
//     //                     //                     //     label: Text('Unit',
//     //                     //                     //         style: TextStyle(
//     //                     //                     //             color:
//     //                     //                     //                 Theme.of(context).scaffoldBackgroundColor)),
//     //                     //                     //   ),
//     //                     //                     //   DataColumn(
//     //                     //                     //     label: Text('Reference Range',
//     //                     //                     //         style: TextStyle(
//     //                     //                     //             color:
//     //                     //                     //                 Theme.of(context).scaffoldBackgroundColor)),
//     //                     //                     //   ),
//     //                     //                     //   DataColumn(
//     //                     //                     //     label: Text('Remarks',
//     //                     //                     //         style: TextStyle(
//     //                     //                     //             color:
//     //                     //                     //                 Theme.of(context).scaffoldBackgroundColor)),
//     //                     //                     //   ),
//     //                     //                     // ],
//     //                     //                     rows: [
//     //                     //                       DataRow(
//     //                     //                         cells: [
//     //                     //                           DataCell(Container(
//     //                     //                               width: 100,
//     //                     //                               child: Text(result.parametername))),
//     //                     //                           DataCell(Container(
//     //                     //                               width: 60,
//     //                     //                               height: 30,
//     //                     //                               child: (SquareTextField(
//     //                     //                                 onchanged: (v) {
//     //                     //                                   pulserate = v;
//     //                     //                                 },
//     //                     //                                 text: pulserate,
//     //                     //                               )))),
//     //                     //                           DataCell(Container(
//     //                     //                               width: 100, child: Text(result.unit))),
//     //                     //                           DataCell(Container(
//     //                     //                               width: 100,
//     //                     //                               child: Text(result.refrangefrom +
//     //                     //                                   ' - ' +
//     //                     //                                   result.refrangeto))),
//     //                     //                           DataCell(
//     //                     //                             Container(
//     //                     //                                 width: 120,
//     //                     //                                 height: 40,
//     //                     //                                 child: (SquareTextFieldLarge(
//     //                     //                                   onchanged: (v) {
//     //                     //                                     pulserateRemarks = v;
//     //                     //                                   },
//     //                     //                                   text: pulserateRemarks,
//     //                     //                                 ))),
//     //                     //                           ),
//     //                     //                         ],
//     //                     //                       ),
//     //                     //                       // DataRow(
//     //                     //                       //   cells: [
//     //                     //                       //     DataCell(Container(
//     //                     //                       //         width: 100,
//     //                     //                       //         child: Text('SYSTAHOLIC PRESSURE'))),
//     //                     //                       //     DataCell(Container(
//     //                     //                       //         width: 60,
//     //                     //                       //         height: 30,
//     //                     //                       //         child: (SquareTextField(
//     //                     //                       //           onchanged: (v) {
//     //                     //                       //             systaholicpressure = v;
//     //                     //                       //           },
//     //                     //                       //           text: systaholicpressure,
//     //                     //                       //         )))),
//     //                     //                       //     DataCell(
//     //                     //                       //         Container(width: 100, child: Text('mmHg'))),
//     //                     //                       //     DataCell(Container(
//     //                     //                       //         width: 100, child: Text('0.0 - 0.0'))),
//     //                     //                       //     DataCell(
//     //                     //                       //       Container(
//     //                     //                       //           width: 120,
//     //                     //                       //           height: 40,
//     //                     //                       //           child: (SquareTextFieldLarge(
//     //                     //                       //             onchanged: (v) {
//     //                     //                       //               systaholicpressure = v;
//     //                     //                       //             },
//     //                     //                       //             text: systaholicpressure,
//     //                     //                       //           ))),
//     //                     //                       //     ),
//     //                     //                       //   ],
//     //                     //                       // ),
//     //                     //                       // DataRow(
//     //                     //                       //   cells: [
//     //                     //                       //     DataCell(Container(
//     //                     //                       //         width: 100, child: Text('DIASTOLIC PRESSURE'))),
//     //                     //                       //     DataCell(Container(
//     //                     //                       //         width: 60,
//     //                     //                       //         height: 30,
//     //                     //                       //         child: (SquareTextField(
//     //                     //                       //           onchanged: (v) {
//     //                     //                       //             diastrolicPressure = v;
//     //                     //                       //           },
//     //                     //                       //           text: diastrolicPressure,
//     //                     //                       //         )))),
//     //                     //                       //     DataCell(
//     //                     //                       //         Container(width: 100, child: Text('mmHg'))),
//     //                     //                       //     DataCell(Container(
//     //                     //                       //         width: 100, child: Text('0.0 - 0.0'))),
//     //                     //                       //     DataCell(
//     //                     //                       //       Container(
//     //                     //                       //           width: 120,
//     //                     //                       //           height: 40,
//     //                     //                       //           child: (SquareTextFieldLarge(
//     //                     //                       //             onchanged: (v) {
//     //                     //                       //               diastrolicPressureRemarks = v;
//     //                     //                       //             },
//     //                     //                       //             text: diastrolicPressureRemarks,
//     //                     //                       //           ))),
//     //                     //                       //     ),
//     //                     //                       //   ],
//     //                     //                       // ),
//     //                     //                       // DataRow(
//     //                     //                       //   cells: [
//     //                     //                       //     DataCell(
//     //                     //                       //       Container(width: 100, child: Text('GRBS')),
//     //                     //                       //     ),
//     //                     //                       //     DataCell(Container(
//     //                     //                       //         width: 60,
//     //                     //                       //         height: 30,
//     //                     //                       //         child: (SquareTextField(
//     //                     //                       //           onchanged: (v) {
//     //                     //                       //             grbs = v;
//     //                     //                       //           },
//     //                     //                       //           text: grbs,
//     //                     //                       //         )))),
//     //                     //                       //     DataCell(
//     //                     //                       //         Container(width: 100, child: Text('mg/dL'))),
//     //                     //                       //     DataCell(Container(
//     //                     //                       //         width: 100, child: Text('0.0 - 0.0'))),
//     //                     //                       //     DataCell(
//     //                     //                       //       Container(
//     //                     //                       //           width: 120,
//     //                     //                       //           height: 40,
//     //                     //                       //           child: (SquareTextFieldLarge(
//     //                     //                       //             onchanged: (v) {
//     //                     //                       //               grbsRemarks = v;
//     //                     //                       //             },
//     //                     //                       //             text: grbsRemarks,
//     //                     //                       //           ))),
//     //                     //                       //     ),
//     //                     //                       //   ],
//     //                     //                       // ),
//     //                     //                       // DataRow(
//     //                     //                       //   cells: [
//     //                     //                       //     DataCell(
//     //                     //                       //         Container(width: 100, child: Text('WEIGHT'))),
//     //                     //                       //     DataCell(Container(
//     //                     //                       //         width: 60,
//     //                     //                       //         height: 30,
//     //                     //                       //         child: (SquareTextField(
//     //                     //                       //           onchanged: (v) {
//     //                     //                       //             weight = v;
//     //                     //                       //           },
//     //                     //                       //           text: weight,
//     //                     //                       //         )))),
//     //                     //                       //     DataCell(Container(width: 100, child: Text('Kg'))),
//     //                     //                       //     DataCell(Container(
//     //                     //                       //         width: 100, child: Text('0.0 - 0.0'))),
//     //                     //                       //     DataCell(
//     //                     //                       //       Container(
//     //                     //                       //           width: 120,
//     //                     //                       //           height: 40,
//     //                     //                       //           child: (SquareTextFieldLarge(
//     //                     //                       //             onchanged: (v) {
//     //                     //                       //               weightRemarks = v;
//     //                     //                       //             },
//     //                     //                       //             text: weightRemarks,
//     //                     //                       //           ))),
//     //                     //                       //     ),
//     //                     //                       //   ],
//     //                     //                       // ),
//     //                     //                       // DataRow(
//     //                     //                       //   cells: [
//     //                     //                       //     DataCell(
//     //                     //                       //         Container(width: 100, child: Text('HEIGHT'))),
//     //                     //                       //     DataCell(Container(
//     //                     //                       //         width: 60,
//     //                     //                       //         height: 30,
//     //                     //                       //         child: (SquareTextField(
//     //                     //                       //           onchanged: (v) {
//     //                     //                       //             height = v;
//     //                     //                       //           },
//     //                     //                       //           text: height,
//     //                     //                       //         )))),
//     //                     //                       //     DataCell(Container(width: 100, child: Text('Cm'))),
//     //                     //                       //     DataCell(Container(
//     //                     //                       //         width: 100, child: Text('0.0 - 0.0'))),
//     //                     //                       //     DataCell(
//     //                     //                       //       Container(
//     //                     //                       //           width: 120,
//     //                     //                       //           height: 40,
//     //                     //                       //           child: (SquareTextFieldLarge(
//     //                     //                       //             onchanged: (v) {
//     //                     //                       //               heightRemarks = v;
//     //                     //                       //             },
//     //                     //                       //             text: heightRemarks,
//     //                     //                       //           ))),
//     //                     //                       //     ),
//     //                     //                       //   ],
//     //                     //                       // ),
//     //                     //                       // DataRow(
//     //                     //                       //   cells: [
//     //                     //                       //     DataCell(
//     //                     //                       //       Container(width: 100, child: Text('TEMP')),
//     //                     //                       //     ),
//     //                     //                       //     DataCell(Container(
//     //                     //                       //         width: 60,
//     //                     //                       //         height: 30,
//     //                     //                       //         child: (SquareTextField(
//     //                     //                       //           onchanged: (v) {
//     //                     //                       //             temp = v;
//     //                     //                       //           },
//     //                     //                       //           text: temp,
//     //                     //                       //         )))),
//     //                     //                       //     DataCell(Container(width: 100, child: Text(''))),
//     //                     //                       //     DataCell(Container(
//     //                     //                       //         width: 100, child: Text('97.8 - 99.1'))),
//     //                     //                       //     DataCell(
//     //                     //                       //       Container(
//     //                     //                       //           width: 120,
//     //                     //                       //           height: 40,
//     //                     //                       //           child: (SquareTextFieldLarge(
//     //                     //                       //             onchanged: (v) {
//     //                     //                       //               tempRemarks = v;
//     //                     //                       //             },
//     //                     //                       //             text: tempRemarks,
//     //                     //                       //           ))),
//     //                     //                       //     ),
//     //                     //                       //   ],
//     //                     //                       // ),
//     //                     //                       // DataRow(
//     //                     //                       //   cells: [
//     //                     //                       //     DataCell(
//     //                     //                       //       Container(width: 100, child: Text('SP02')),
//     //                     //                       //     ),
//     //                     //                       //     DataCell(Container(
//     //                     //                       //         width: 60,
//     //                     //                       //         height: 30,
//     //                     //                       //         child: (SquareTextField(
//     //                     //                       //           onchanged: (v) {
//     //                     //                       //             spo2 = v;
//     //                     //                       //           },
//     //                     //                       //           text: spo2,
//     //                     //                       //         )))),
//     //                     //                       //     DataCell(Container(width: 100, child: Text('%'))),
//     //                     //                       //     DataCell(Container(
//     //                     //                       //         width: 100, child: Text('0.0 - 0.0'))),
//     //                     //                       //     DataCell(
//     //                     //                       //       Container(
//     //                     //                       //           width: 120,
//     //                     //                       //           height: 40,
//     //                     //                       //           child: (SquareTextFieldLarge(
//     //                     //                       //             onchanged: (v) {
//     //                     //                       //               spo2Remarks = v;
//     //                     //                       //             },
//     //                     //                       //             text: spo2Remarks,
//     //                     //                       //           ))),
//     //                     //                       //     ),
//     //                     //                       //   ],
//     //                     //                       // ),
//     //                     //                     ]),
//     //                     //               );
//     //                     //         // ),
//     //                     //         // ),
//     //                     //       }),
//     //                     // )),
//     //                   }))),
//     //     ],
//     //   ),
//     // ),
//     // Row(
//     //   mainAxisAlignment: MainAxisAlignment.start,
//     //   children: [
//     //     SizedBox(
//     //       width: 10,
//     //     ),
//     //     SquareFlatButton(
//     //         () => _saveVitalSigns(context), 'Save Vital Sign'),
//     //   ],
//     // ),
//     // SizedBox(
//     //   height: 25,
//     // ),
//     // Padding(
//     //   padding: EdgeInsets.only(
//     //       bottom: MediaQuery.of(context).viewInsets.bottom),
//     // ),
//   }
// }
