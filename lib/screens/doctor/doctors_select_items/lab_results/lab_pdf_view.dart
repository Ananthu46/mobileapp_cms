import 'package:doctor_portal_cms/model/doctor/doctor_order_modal.dart';
import 'package:doctor_portal_cms/provider/doctor/doctor_order_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_plugin_pdf_viewer/flutter_plugin_pdf_viewer.dart';
import 'package:provider/provider.dart';

class LabPdfView extends StatefulWidget {
  static const routeName = 'labpdf-download';
  final DoctorLabResults result;
  LabPdfView({this.result});
  @override
  _LabPdfViewState createState() => _LabPdfViewState();
}

class _LabPdfViewState extends State<LabPdfView> {
  bool _isLoading = false;
  PDFDocument document;
  loadDocument(
    context,
  ) async {
    setState(() {
      _isLoading = true;
    });
    await Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .loadCultureLab(
      context: context,
      labresultid: widget.result.labResultId,
      orderno: widget.result.orderId,
      sampleid: widget.result.sampleid,
      visitid: widget.result.visit,
    );
    setState(() {
      document =
          Provider.of<DrugOrderModalDoctor>(context, listen: false).document;
      _isLoading = false;
    });
  }

  // int progress = 0;

  // ReceivePort _receivePort = ReceivePort();

  // static downloadingCallback(id, status, progress) {
  //   ///Looking up for a send port
  //   SendPort sendPort = IsolateNameServer.lookupPortByName("downloading");

  //   ///ssending the data
  //   sendPort.send([id, status, progress]);
  // }

  // @override
  // void initState() {
  //   // TODO: implement initState
  //   super.initState();

  //   ///register a send port for the other isolates
  //   IsolateNameServer.registerPortWithName(
  //       _receivePort.sendPort, "downloading");

  //   ///Listening for the data is comming other isolataes
  //   // _receivePort.listen((message) {
  //   //   setState(() {
  //   //     progress = message[2];
  //   //   });
  //   //
  //   //   print(progress);
  //   // });

  //   FlutterDownloader.registerCallback(downloadingCallback);
  // }

  // void _downloadLabreport() async {
  //   final status = await Permission.storage.request();
  //   // Directory downloadsDirectory= await getApplicationDocumentsDirectory();
  //   // var targetPath = await ExtStorage.getExternalStoragePublicDirectory(
  //   //     ExtStorage.DIRECTORY_DOWNLOADS);
  //   // Directory downloadsDirectory =
  //   //     await DownloadsPathProvider.downloadsDirectory;
  //   // Directory downloadsDirectory = await getApplicationDocumentsDirectory();
  //   if (status.isGranted) {
  //     var downloadurl;
  //     if ({widget.resulttypeid} == "29397") {
  //       downloadurl =
  //           '$IP_CONFIG/life/birtreports?_reportFormat=pdf&ReportName=cultureresult.rptdesign&__id=parameterPage&__masterpage=true&labresultid=${widget.labresultid}&patientId=${widget.patientid}&visitid=${widget.visitid}&sampleId=*${widget.sampleid}';
  //     } else if ({widget.resulttypeid} == "640") {
  //       downloadurl =
  //           '$IP_CONFIG/life/birtreports?_reportFormat=pdf&ReportName=TemplateReportServiceCenter.rptdesign&__id=parameterPage&__masterpage=true&orderIds=*${widget.orderno}&patientId=${widget.patientid}&visitid=${widget.visitid}&sampleId=*${widget.sampleid}&resultId=${widget.labresultid}';
  //     } else if ({widget.resulttypeid} == "1188") {
  //       downloadurl =
  //           '$IP_CONFIG/life/birtreports?_reportFormat=pdf&ReportName=semenAnalysysReport.rptdesign&__id=parameterPage&amp;__masterpage=true&patientId=${widget.patientid}&visitid=${widget.visitid}&sampleId=*${widget.sampleid}&resultId=${widget.labresultid}&generatedBy=2';
  //     } else if ({widget.resulttypeid} == "98") {
  //       downloadurl =
  //           '$IP_CONFIG/life/birtreports?_reportFormat=pdf&ReportName=csfAnalysisReport.rptdesign&__id=parameterPage&amp;__masterpage=true&orderIds=*${widget.orderno}&patientId=${widget.patientid}&visitid=${widget.visitid}&sampleId=*${widget.sampleid}&resultId=${widget.labresultid}&generatedBy=2';
  //     } else if ({widget.resulttypeid} == "1189") {
  //       downloadurl =
  //           '$IP_CONFIG/life/birtreports?_reportFormat=pdf&ReportName=CovidAntigenReportServiceCenterWise.rptdesign&__id=parameterPage&__masterpage=true&sampleId=*${widget.sampleid}&patientId=${widget.patientid}&visitid=${widget.visitid}&orderno=${widget.orderno}';
  //     } else {
  //       downloadurl =
  //           '$IP_CONFIG/life/birtreports?_reportFormat=pdf&ReportName=ResultProcessingReportServiceCenterWise.rptdesign&__id=parameterPage&__masterpage=true&sampleId=*${widget.sampleid}&patientId=${widget.patientid}&visitid=${widget.visitid}&orderno=${widget.orderno}';
  //     }
  //     print(downloadurl);
  //     Directory directory = await getExternalStorageDirectory();
  //     if (Platform.isAndroid) {
  //       String newPath = "";
  //       print(directory);
  //       List<String> paths = directory.path.split("/");
  //       for (int x = 1; x < paths.length; x++) {
  //         String folder = paths[x];
  //         if (folder != "Android") {
  //           newPath += "/" + folder;
  //         } else {
  //           break;
  //         }
  //       }

  //       newPath = newPath + "/Patient Portal";
  //       directory = Directory(newPath);
  //       if (!await directory.exists()) {
  //         await directory.create(recursive: true);
  //       }
  //     }

  //     print(directory);
  //     final taskId = await FlutterDownloader.enqueue(
  //       fileName: 'Lab-${widget.sampleid}.pdf',
  //       url: downloadurl, savedDir: directory.path,

  //       showNotification:
  //           true, // show download progress in status bar (for Android)
  //       openFileFromNotification:
  //           true, // click on notification to open downloaded file (for Android)
  //     );
  //     Fluttertoast.showToast(
  //         msg: "Downloading file",
  //         toastLength: Toast.LENGTH_SHORT,
  //         gravity: ToastGravity.BOTTOM,
  //         timeInSecForIos: 1,
  //         backgroundColor: Theme.of(context).primaryColor,
  //         textColor: Colors.white,
  //         fontSize: 12.0);
  //   } else {
  //     print("Permission Not Granted");
  //   }
  // }

  @override
  void didChangeDependencies() {
    loadDocument(
      context,
    );
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // backgroundColor: Theme.of(context).primaryColor,
        leading: BackButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        // title: const Text('  hai'),
        centerTitle: true,
        actions: [
          // Padding(
          //     padding: const EdgeInsets.only(right: 18.0),
          //     child: Center(
          //         child: TextButton(
          //             child:
          //                 Text('Save', style: TextStyle(color: Colors.white)),
          //             onPressed: _downloadLabreport)))
        ],
      ),
      body: Center(
          child: _isLoading
              ? Stack(
                  children: [
                    Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: 90,
                          ),
                          Text('Please wait while we generate the report'),
                        ],
                      ),
                    ),
                    // Center(
                    //   child: Image.asset('assets/loading/ajaxprocess.gif'),
                    // ),
                  ],
                )
              : PDFViewer(document: document)),
    );
  }
}
