import 'package:doctor_portal_cms/app/size_config.dart';
import 'package:doctor_portal_cms/model/doctor/doctor_order_modal.dart';
import 'package:doctor_portal_cms/provider/doctor/doctor_order_provider.dart';
import 'package:doctor_portal_cms/widgets/popup_layout/popup_content.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LabResultParameterPopup extends StatefulWidget {
  final String labresultId;
  final String labserviceName;
  LabResultParameterPopup({this.labresultId, this.labserviceName});
  @override
  _LabResultParameterPopupState createState() =>
      _LabResultParameterPopupState();
}

class _LabResultParameterPopupState extends State<LabResultParameterPopup> {
  @override
  void initState() {
    Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .loadlabResultsParameters(widget.labresultId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return PopupContent(
        content: Container(
      child: Scaffold(
          appBar: AppBar(
            title: Text('${widget.labserviceName}'),
            centerTitle: true,
            leading: new Builder(builder: (context) {
              return IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  try {
                    Navigator.pop(context); //close the popup
                  } catch (e) {
                    print(e);
                  }
                },
              );
            }),
            brightness: Brightness.light,
          ),
          resizeToAvoidBottomInset: false,
          body: Column(
            children: [
              SizedBox(
                height: 10 * SizeConfig.getScaleFactor(),
              ),
              // Row(
              //   mainAxisAlignment: MainAxisAlignment.center,
              //   children: [
              //     SizedBox(
              //       width: 250 * SizeConfig.getScaleFactor(),
              //       child: Text(
              //         widget.labserviceName,
              //         style: TextStyle(
              //             color: Theme.of(context).primaryColor,
              //             fontSize: 16 * SizeConfig.getScaleFactor(),
              //             fontWeight: FontWeight.bold),
              //       ),
              //     ),
              //   ],
              // ),
              Consumer<DrugOrderModalDoctor>(
                  builder: (ctx, parameter, _) => ListView.builder(
                      physics: ScrollPhysics(),
                      padding: EdgeInsets.only(top: 0),
                      shrinkWrap: true,
                      itemCount: parameter.loadLabResultsParameter.length,
                      itemBuilder: (context, index) {
                        print(parameter.loadLabResultsParameter.length);
                        DoctorLabResultsParameters para =
                            parameter.loadLabResultsParameter[index];
                        print(para.labresultId);
                        return parameter.loadLabResultsParameter.isEmpty
                            ? CircularProgressIndicator()
                            : Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 10),
                                child: Column(
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        SizedBox(
                                          width:
                                              120 * SizeConfig.getScaleFactor(),
                                          child: Text(
                                            para.parameter,
                                            style: TextStyle(
                                                fontSize: 15 *
                                                    SizeConfig.getScaleFactor(),
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                        SizedBox(
                                          width:
                                              80 * SizeConfig.getScaleFactor(),
                                          child: Row(
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              Text(
                                                para.value,
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    fontSize: 16,
                                                    fontWeight: FontWeight.w500,
                                                    color: Colors.brown),
                                              ),
                                              para.reportfalg == "L"
                                                  ? Image.asset(
                                                      'assets/images/local_image/green.png')
                                                  : Image.asset(
                                                      'assets/images/local_image/red.png'),
                                            ],
                                          ),
                                        ),
                                        _TextBoxWidgetValue(
                                          text: para.unit,
                                          width:
                                              60 * SizeConfig.getScaleFactor(),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        _TextBoxWidgetValue(
                                          text: "Reference  :",
                                          width:
                                              100 * SizeConfig.getScaleFactor(),
                                        ),
                                        _TextBoxWidgetValue(
                                          text: para.reference,
                                          width:
                                              60 * SizeConfig.getScaleFactor(),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              );
                      })),
              SizedBox(
                height: 10 * SizeConfig.getScaleFactor(),
              ),
              SizedBox(
                height: 10 * SizeConfig.getScaleFactor(),
              ),
            ],
            // ),
          )),
    ));
  }
}

class _TextBoxWidgetValue extends StatelessWidget {
  final String text;
  final double width;
  final double height;
  _TextBoxWidgetValue({this.height, this.text, this.width});
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: Text(
        text,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 12 * SizeConfig.getScaleFactor(),
        ),
      ),
    );
  }
}
