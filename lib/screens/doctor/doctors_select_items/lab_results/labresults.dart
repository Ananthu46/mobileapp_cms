import 'package:doctor_portal_cms/app/size_config.dart';
import 'package:doctor_portal_cms/model/doctor/doctor_order_modal.dart';
import 'package:doctor_portal_cms/provider/doctor/doctor_order_provider.dart';
import 'package:doctor_portal_cms/screens/doctor/doctors_select_items/lab_results/lab_pdf_view.dart';
import 'package:doctor_portal_cms/screens/doctor/doctors_select_items/lab_results/lab_result_parameter_popup.dart';
import 'package:doctor_portal_cms/screens/doctor/doctors_select_items/lab_results/lab_result_template_popup.dart';
import 'package:doctor_portal_cms/widgets/popup_layout/popup_layout_box.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class LabResultsDoctor extends StatefulWidget {
  final String patientid;
  final String visitid;
  LabResultsDoctor({this.patientid, this.visitid});

  @override
  _LabResultsDoctorState createState() => _LabResultsDoctorState();
}

class _LabResultsDoctorState extends State<LabResultsDoctor> {
  @override
  void initState() {
    Provider.of<DrugOrderModalDoctor>(context, listen: false).loadlabResults(
      widget.visitid,
      widget.patientid,
    );
    super.initState();
  }

  showPopupParameter(BuildContext context,
      {BuildContext popupContext, DoctorLabResults result}) {
    Navigator.push(
      context,
      PopupLayout(
          top: 30,
          left: 30,
          right: 30,
          bottom: 50,
          child: LabResultParameterPopup(
            labresultId: result.labResultId,
            labserviceName: result.labServiceName,
          )),
    );
  }

  showPopupTemplate(BuildContext context,
      {BuildContext popupContext, DoctorLabResults result}) {
    Navigator.push(
      context,
      PopupLayout(
          top: 30,
          left: 30,
          right: 30,
          bottom: 50,
          child: LabResultTemplatePopup(
            labresultId: result.labResultId,
            labserviceName: result.labServiceName,
          )),
    );
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
        body: ListView(
      children: [
        SizedBox(
          height: 20 * SizeConfig.getScaleFactor(),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SizedBox(
                width: 120 * SizeConfig.getScaleFactor(),
                child: Text(
                  "Test",
                  style: TextStyle(
                      fontSize: 18 * SizeConfig.getScaleFactor(),
                      fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(
                width: 5 * SizeConfig.getScaleFactor(),
              ),
              _TextBoxWidget(
                text: "Result",
                width: 150 * SizeConfig.getScaleFactor(),
              ),
              _TextBoxWidget(
                text: "Unit",
                width: 70 * SizeConfig.getScaleFactor(),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 15 * SizeConfig.getScaleFactor(),
        ),
        Consumer<DrugOrderModalDoctor>(
            builder: (ctx, labresults, _) => ListView.builder(
                physics: const NeverScrollableScrollPhysics(),
                padding: EdgeInsets.only(
                  top: 0 * SizeConfig.getScaleFactor(),
                ),
                shrinkWrap: true,
                itemCount: labresults.loadLabResults.length,
                itemBuilder: (context, index) {
                  DoctorLabResults result = labresults.loadLabResults[index];
                  print(labresults.loadLabResults.isEmpty);
                  print(result.description);
                  print(result.labServiceName);
                  return labresults.loadLabResults.isEmpty
                      ? Container()
                      : Card(
                          elevation: 4,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 5),
                            child: result.description == "PARAMETER"
                                ? Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Row(
                                      children: [
                                        SizedBox(
                                          width:
                                              100 * SizeConfig.getScaleFactor(),
                                          child: Text(
                                            result.labServiceName,
                                            style: TextStyle(
                                                fontSize: 16 *
                                                    SizeConfig.getScaleFactor(),
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                        SizedBox(
                                          width:
                                              80 * SizeConfig.getScaleFactor(),
                                        ),
                                        OutlinedButton(
                                            onPressed: () => showPopupParameter(
                                                context,
                                                result: result),
                                            child: Text('View'))
                                      ],
                                    ),
                                  )
                                : result.description == "Culture"
                                    ? Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Row(
                                          children: [
                                            SizedBox(
                                              width: 100 *
                                                  SizeConfig.getScaleFactor(),
                                              child: Text(
                                                result.labServiceName,
                                                style: TextStyle(
                                                    fontSize: 16 *
                                                        SizeConfig
                                                            .getScaleFactor(),
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 80 *
                                                  SizeConfig.getScaleFactor(),
                                            ),
                                            OutlinedButton(
                                                onPressed: () {
                                                  Navigator.push(
                                                    context,
                                                    new MaterialPageRoute(
                                                      builder: (context) =>
                                                          new LabPdfView(
                                                        result: result,
                                                      ),
                                                    ),
                                                  );
                                                },
                                                child: Text('View'))
                                          ],
                                        ),
                                      )
                                    : result.description == "Template"
                                        ? Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Row(
                                              children: [
                                                SizedBox(
                                                  width: 100 *
                                                      SizeConfig
                                                          .getScaleFactor(),
                                                  child: Text(
                                                    result.labServiceName,
                                                    style: TextStyle(
                                                        fontSize: 16 *
                                                            SizeConfig
                                                                .getScaleFactor(),
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 80 *
                                                      SizeConfig
                                                          .getScaleFactor(),
                                                ),
                                                OutlinedButton(
                                                    onPressed: () =>
                                                        showPopupTemplate(
                                                            context,
                                                            result: result),
                                                    child: Text('View'))
                                              ],
                                            ),
                                          )
                                        : Column(
                                            children: [
                                              SizedBox(
                                                height: 15 *
                                                    SizeConfig.getScaleFactor(),
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceEvenly,
                                                children: [
                                                  SizedBox(
                                                    width: 120 *
                                                        SizeConfig
                                                            .getScaleFactor(),
                                                    child: Text(
                                                      result.labServiceName,
                                                      style: TextStyle(
                                                          fontSize: 16 *
                                                              SizeConfig
                                                                  .getScaleFactor(),
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: 5 *
                                                        SizeConfig
                                                            .getScaleFactor(),
                                                  ),
                                                  SizedBox(
                                                    width: 150 *
                                                        SizeConfig
                                                            .getScaleFactor(),
                                                    child: Row(
                                                      mainAxisSize:
                                                          MainAxisSize.min,
                                                      children: [
                                                        Text(
                                                          result.labResultValue,
                                                          textAlign:
                                                              TextAlign.center,
                                                          style: TextStyle(
                                                              fontSize: 16 *
                                                                  SizeConfig
                                                                      .getScaleFactor(),
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500,
                                                              color:
                                                                  Colors.brown),
                                                        ),
                                                        result.resultOutcome ==
                                                                    null ||
                                                                result
                                                                    .resultOutcome
                                                                    .isEmpty
                                                            ? Container()
                                                            : result.resultOutcome ==
                                                                    "L"
                                                                ? Image.asset(
                                                                    'assets/images/local_image/green.png')
                                                                : Image.asset(
                                                                    'assets/images/local_image/red.png')
                                                      ],
                                                    ),
                                                  ),
                                                  result.unit.isNotEmpty
                                                      ? _TextBoxWidgetValue(
                                                          text: result.unit,
                                                          width: 70 *
                                                              SizeConfig
                                                                  .getScaleFactor(),
                                                        )
                                                      : _TextBoxWidgetValue(
                                                          text: "-",
                                                          width: 80 *
                                                              SizeConfig
                                                                  .getScaleFactor(),
                                                        ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 10 *
                                                    SizeConfig.getScaleFactor(),
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: [
                                                  _TextBoxWidgetItemsValue(
                                                    text: "Sample id :",
                                                    width: 80 *
                                                        SizeConfig
                                                            .getScaleFactor(),
                                                  ),
                                                  _TextBoxWidgetItemsValue(
                                                    text: result.sampleid,
                                                    width: 80 *
                                                        SizeConfig
                                                            .getScaleFactor(),
                                                  ),
                                                  _TextBoxWidgetItemsValue(
                                                    text: result.visit,
                                                    width: 80 *
                                                        SizeConfig
                                                            .getScaleFactor(),
                                                  ),
                                                  _TextBoxWidgetItemsValue(
                                                    text: "Status :",
                                                    width: 70 *
                                                        SizeConfig
                                                            .getScaleFactor(),
                                                  ),
                                                  SizedBox(
                                                    width: 50 *
                                                        SizeConfig
                                                            .getScaleFactor(),
                                                    child: Text(
                                                        result.resultStatus,
                                                        style: TextStyle(
                                                            color: Colors.red,
                                                            fontSize: 16,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold)),
                                                  )
                                                ],
                                              ),
                                              SizedBox(
                                                height: 10 *
                                                    SizeConfig.getScaleFactor(),
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: [
                                                  _TextBoxWidgetItemsValue(
                                                    text: "Ordered on :",
                                                    width: 80 *
                                                        SizeConfig
                                                            .getScaleFactor(),
                                                  ),
                                                  _TextBoxWidgetItemsValue(
                                                    text: DateFormat(
                                                            'dd-MM-yyyy')
                                                        .format(DateTime.parse(
                                                            result.orderDate)),
                                                    width: 120 *
                                                        SizeConfig
                                                            .getScaleFactor(),
                                                  ),
                                                  result.referenceRange.isEmpty
                                                      ? _TextBoxWidgetValue(
                                                          text: "-",
                                                          width: 70 *
                                                              SizeConfig
                                                                  .getScaleFactor(),
                                                        )
                                                      : _TextBoxWidgetValue(
                                                          text: result
                                                              .referenceRange,
                                                          width: 70 *
                                                              SizeConfig
                                                                  .getScaleFactor(),
                                                        )
                                                ],
                                              ),
                                              SizedBox(
                                                height: 10 *
                                                    SizeConfig.getScaleFactor(),
                                              ),
                                            ],
                                          ),
                          ),
                        );
                })),
        // )
        SizedBox(
          height: 20 * SizeConfig.getScaleFactor(),
        )
      ],
    ));
  }
}

class _TextBoxWidget extends StatelessWidget {
  final String text;
  final double width;
  final double height;
  _TextBoxWidget({this.height, this.text, this.width});
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: Text(
        text,
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
      ),
    );
  }
}

class _TextBoxWidgetValue extends StatelessWidget {
  final String text;
  final double width;
  final double height;
  _TextBoxWidgetValue({this.height, this.text, this.width});
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: Text(
        text,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 16 * SizeConfig.getScaleFactor(),
        ),
      ),
    );
  }
}

class _TextBoxWidgetItemsValue extends StatelessWidget {
  final String text;
  final double width;
  final double height;
  _TextBoxWidgetItemsValue({this.height, this.text, this.width});
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: Text(
        text,
      ),
    );
  }
}
