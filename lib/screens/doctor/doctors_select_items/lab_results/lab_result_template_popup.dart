import 'package:doctor_portal_cms/app/size_config.dart';
import 'package:doctor_portal_cms/model/doctor/doctor_order_modal.dart';
import 'package:doctor_portal_cms/provider/doctor/doctor_order_provider.dart';
import 'package:doctor_portal_cms/widgets/popup_layout/popup_content.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_html/flutter_html.dart';

class LabResultTemplatePopup extends StatefulWidget {
  final String labresultId;
  final String labserviceName;
  LabResultTemplatePopup({this.labresultId, this.labserviceName});
  @override
  _LabResultTemplatePopupState createState() => _LabResultTemplatePopupState();
}

class _LabResultTemplatePopupState extends State<LabResultTemplatePopup> {
  @override
  void initState() {
    Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .loadLabResultsTemplate(widget.labresultId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return PopupContent(
        content: Container(
      child: Scaffold(
          appBar: AppBar(
            title: Text('${widget.labserviceName}'),
            centerTitle: true,
            leading: new Builder(builder: (context) {
              return IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  try {
                    Navigator.pop(context); //close the popup
                  } catch (e) {
                    print(e);
                  }
                },
              );
            }),
            brightness: Brightness.light,
          ),
          resizeToAvoidBottomInset: false,
          body: ListView(
            children: [
              SizedBox(
                height: 10 * SizeConfig.getScaleFactor(),
              ),
              // Row(
              //   mainAxisAlignment: MainAxisAlignment.center,
              //   children: [
              //     SizedBox(
              //       width: 250 * SizeConfig.getScaleFactor(),
              //       child: Text(
              //         widget.labserviceName,
              //         style: TextStyle(
              //             color: Theme.of(context).primaryColor,
              //             fontSize: 16 * SizeConfig.getScaleFactor(),
              //             fontWeight: FontWeight.bold),
              //       ),
              //     ),
              //   ],
              // ),
              Consumer<DrugOrderModalDoctor>(
                  builder: (ctx, template, _) => ListView.builder(
                      physics: ScrollPhysics(),
                      padding: EdgeInsets.only(top: 0),
                      shrinkWrap: true,
                      itemCount: template.loadLabResultsTemplates.length,
                      itemBuilder: (context, index) {
                        print(template.loadLabResultsTemplates.length);
                        DoctorLabResultTemplate temp =
                            template.loadLabResultsTemplates[index];

                        return template.loadLabResultsTemplates.isEmpty
                            ? CircularProgressIndicator()
                            : Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 10),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Html(data: "${temp.templateResult}"),
                                ));
                      })),
              SizedBox(
                height: 10 * SizeConfig.getScaleFactor(),
              ),
              SizedBox(
                height: 10 * SizeConfig.getScaleFactor(),
              ),
            ],
            // ),
          )),
    ));
  }
}

class _TextBoxWidgetValue extends StatelessWidget {
  final String text;
  final double width;
  final double height;
  _TextBoxWidgetValue({this.height, this.text, this.width});
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: Text(
        text,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 12 * SizeConfig.getScaleFactor(),
        ),
      ),
    );
  }
}
