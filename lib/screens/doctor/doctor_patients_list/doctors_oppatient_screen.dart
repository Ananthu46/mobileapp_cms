import 'package:doctor_portal_cms/app/app_value_config.dart';
import 'package:doctor_portal_cms/model/doctor/doctor_order_modal.dart';
import 'package:doctor_portal_cms/provider/auth/login_auth.dart';
import 'package:doctor_portal_cms/provider/doctor/doctor_order_provider.dart';
import 'package:doctor_portal_cms/provider/drug/drug_order_api_provider.dart';
import 'package:doctor_portal_cms/screens/auth/login_screen.dart';
import 'package:doctor_portal_cms/screens/doctor/main_screen_medical.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DoctorsPatientScreenOP extends StatefulWidget {
  static const routeName = 'doctorsorder';
  @override
  _DoctorsPatientScreenOPState createState() => _DoctorsPatientScreenOPState();
}

class _DoctorsPatientScreenOPState extends State<DoctorsPatientScreenOP> {
  ScrollController _controller;
  String transactionId;
  @override
  void initState() {
    Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .loadOpPatientsDoctorWise(searchDate);
    super.initState();
  }

  Future<void> _refreshProducts(BuildContext context) async {
    await Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .loadOpPatientsDoctorWise(searchDate);
  }

  selectpatient(DoctorsOpPatients opPatientsList) async {
    // transactionId =
    //     Provider.of<DrugOrderModalDoctor>(context, listen: false).tansationId;
    print(transactionId);
    Navigator.of(context)
        .pushNamed(MedicalRecordsMainScreen.routeName, arguments: {
      'patientID': '${opPatientsList.patientId}',
      'mrn': '${opPatientsList.mrn}',
      'patientname': '${opPatientsList.patientName}',
      'age': '${opPatientsList.age}',
      'gender': '${opPatientsList.gender}',
      'visitid': '${opPatientsList.visitId}'
    });
    Provider.of<DrugPatientProvider>(context, listen: false)
        .clearOnNewPatient();
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('selectedPatient', opPatientsList.patientId);
    prefs.setString('selectedVisit', opPatientsList.visitId);
    Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .addSelectedpatientsDetials(opPatientsList, transactionId);
  }

  String searchDate = consultantSelectedDate == null
      ? DateTime.now().toString()
      : consultantSelectedDate;

  TextEditingController _date = new TextEditingController();
  DateTime date = consultantSelectedDate == null
      ? DateTime.now()
      : DateTime.parse(consultantSelectedDate);
  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: date,
        firstDate: DateTime(2010, 1),
        lastDate: DateTime(2026));

    if (picked != null && picked != date)
      setState(() {
        date = picked;
        searchDate = DateFormat('yyyy-MM-dd').format(date);
        print(searchDate);
        _date.value =
            TextEditingValue(text: DateFormat('dd-MM-yyyy').format(date));
      });
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LoginAuth>(
        builder: (ctx, auth, _) => auth.isAuth
            ? Scaffold(
                resizeToAvoidBottomInset: false,
                appBar: AppBar(
                  title: Text('Patient List'),
                  centerTitle: true,
                  actions: [
                    IconButton(
                        icon: Icon(Icons.exit_to_app),
                        onPressed: () {
                          Navigator.of(context).pushReplacementNamed('/');
                          Provider.of<LoginAuth>(context, listen: false)
                              .logout();
                        })
                  ],
                ),
                body: RefreshIndicator(
                  onRefresh: () => _refreshProducts(context),
                  child: Column(children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        height: 60,
                        child: GestureDetector(
                          onTap: () => _selectDate(context),
                          child: AbsorbPointer(
                            child: TextFormField(
                              enabled: false,
                              controller: _date,
                              keyboardType: TextInputType.datetime,
                              textAlignVertical: TextAlignVertical.center,
                              decoration: InputDecoration(
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: const BorderRadius.all(
                                    const Radius.circular(10.0),
                                  ),
                                  borderSide: BorderSide(
                                      color: Theme.of(context).primaryColor,
                                      width: 2),
                                ),
                                border: OutlineInputBorder(
                                  borderRadius: const BorderRadius.all(
                                    const Radius.circular(10.0),
                                  ),
                                  borderSide: BorderSide.none,
                                ),
                                disabledBorder: OutlineInputBorder(
                                  borderRadius: const BorderRadius.all(
                                    const Radius.circular(10.0),
                                  ),
                                  borderSide: BorderSide(
                                      color: Theme.of(context).primaryColor,
                                      width: 2),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: const BorderRadius.all(
                                    const Radius.circular(10.0),
                                  ),
                                  borderSide: BorderSide(
                                      color: Theme.of(context).primaryColor,
                                      width: 2),
                                ),
                                errorBorder: OutlineInputBorder(
                                  borderRadius: const BorderRadius.all(
                                    const Radius.circular(10.0),
                                  ),
                                  borderSide: BorderSide(
                                      color: Theme.of(context).primaryColor,
                                      width: 2),
                                ),
                                focusedErrorBorder: OutlineInputBorder(
                                  borderRadius: const BorderRadius.all(
                                    const Radius.circular(10.0),
                                  ),
                                  borderSide: BorderSide(
                                      color: Theme.of(context).primaryColor,
                                      width: 2),
                                ),
                                errorStyle: TextStyle(color: Colors.red),
                                hintText: DateFormat('dd-MM-yyyy').format(date),
                                hintStyle: TextStyle(
                                    color: Theme.of(context)
                                        .textTheme
                                        .button
                                        .color),
                                contentPadding: EdgeInsets.only(left: 30),
                                suffixIcon: Icon(Icons.date_range,
                                    color: Theme.of(context).primaryColor),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: FutureBuilder(
                        future: _refreshProducts(context),
                        builder: (ctx, snapshot) => snapshot.connectionState ==
                                ConnectionState.waiting
                            ? Center(
                                child: CircularProgressIndicator(),
                              )
                            : Consumer<DrugOrderModalDoctor>(
                                builder: (ctx, opPatients, _) =>
                                    ListView.builder(
                                  controller: _controller,
                                  physics: ScrollPhysics(),
                                  padding: EdgeInsets.only(top: 0),
                                  shrinkWrap: true,
                                  itemCount:
                                      opPatients.loadOpPatientsDoctor.length,
                                  itemBuilder: (context, index) {
                                    DoctorsOpPatients opPatientsList =
                                        opPatients.loadOpPatientsDoctor[index];
                                    return Card(
                                      elevation: 4,
                                      child: Padding(
                                        padding: const EdgeInsets.all(5.0),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'MRNO: ${opPatientsList.mrn}',
                                            ),
                                            ListTile(
                                              onTap: () =>
                                                  selectpatient(opPatientsList),
                                              leading: CircleAvatar(
                                                backgroundColor: Colors.black,
                                                backgroundImage: opPatientsList
                                                            .gender ==
                                                        'MALE'
                                                    ? AssetImage(
                                                        'assets/images/local_image/male.jpg')
                                                    : AssetImage(
                                                        'assets/images/local_image/female.jpg'),
                                              ),
                                              title: Text(
                                                '${opPatientsList.patientName}',
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                              subtitle: Wrap(
                                                direction: Axis.vertical,
                                                children: [
                                                  Text(
                                                    '${opPatientsList.age} '
                                                    'Years',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  SizedBox(
                                                    width: 10,
                                                  ),
                                                  Text(
                                                    '${opPatientsList.gender}',
                                                  ),
                                                ],
                                              ),
                                              trailing: Container(
                                                width: 30,
                                                child: Column(
                                                  children: [
                                                    Text('Q.no'),
                                                    Text(
                                                        '${opPatientsList.queueno}'),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              ),
                      ),
                    )
                  ]),
                ))
            : LoginInScreen());
  }
}
