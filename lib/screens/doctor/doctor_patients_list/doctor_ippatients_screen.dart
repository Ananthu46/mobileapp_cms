import 'package:doctor_portal_cms/model/doctor/doctor_order_modal.dart';
import 'package:doctor_portal_cms/provider/auth/login_auth.dart';
import 'package:doctor_portal_cms/provider/doctor/doctor_order_provider.dart';
import 'package:doctor_portal_cms/provider/drug/drug_order_api_provider.dart';
import 'package:doctor_portal_cms/screens/auth/login_screen.dart';
import 'package:doctor_portal_cms/screens/doctor/doctors_soap_notes.dart';
import 'package:doctor_portal_cms/screens/doctor/main_screen_medical.dart';
import 'package:doctor_portal_cms/widgets/inputfields/round_textformfield.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DoctorsPatientScreenIP extends StatefulWidget {
  static const routeName = 'doctorsippatients';
  @override
  _DoctorsPatientScreenIPState createState() => _DoctorsPatientScreenIPState();
}

class _DoctorsPatientScreenIPState extends State<DoctorsPatientScreenIP> {
  final TextEditingController searchControllerPharmacy =
      new TextEditingController();
  ScrollController _controller;
  String transactionId;
  @override
  void initState() {
    Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .loadIpPatientsDoctorWise(searchControllerPharmacy.text);
    searchControllerPharmacy.addListener(() async {
      Provider.of<DrugOrderModalDoctor>(context, listen: false)
          .loadIpPatientsDoctorWise(searchControllerPharmacy.text);
    });
    super.initState();
  }

  Future<void> _refreshProducts(BuildContext context) async {
    await Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .loadIpPatientsDoctorWise(searchControllerPharmacy.text);
  }

  selectpatient(DoctorsIpPatients ipPatientsList) async {
    print(ipPatientsList.encounterId);
    // transactionId =
    //     Provider.of<DrugOrderModalDoctor>(context, listen: false).tansationId;
    Navigator.of(context)
        .pushNamed(MedicalRecordsMainScreen.routeName, arguments: {
      'patientID': '${ipPatientsList.patientId}',
      'mrn': '${ipPatientsList.mrn}',
      'patientname': '${ipPatientsList.patientName}',
      'age': '${ipPatientsList.age}',
      'gender': '${ipPatientsList.gender}',
      'visitid': '${ipPatientsList.visitId}'
    });
    Provider.of<DrugPatientProvider>(context, listen: false)
        .clearOnNewPatient();
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('selectedPatient', ipPatientsList.patientId);
    prefs.setString('selectedVisit', ipPatientsList.visitId);
    Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .addSelectedIPpatientsDetials(ipPatientsList, transactionId);
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LoginAuth>(
        builder: (ctx, auth, _) => auth.isAuth
            ? Scaffold(
                resizeToAvoidBottomInset: false,
                appBar: AppBar(
                  title: Text('Patient List'),
                  centerTitle: true,
                  actions: [
                    IconButton(
                        icon: Icon(Icons.exit_to_app),
                        onPressed: () {
                          Navigator.of(context).pushReplacementNamed('/');
                          Provider.of<LoginAuth>(context, listen: false)
                              .logout();
                        })
                  ],
                ),
                body: RefreshIndicator(
                  onRefresh: () => _refreshProducts(context),
                  child: Column(children: [
                    Container(
                      height: 60,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Material(
                            elevation: 8,
                            shadowColor: Colors.blue,
                            clipBehavior: Clip.antiAlias,
                            borderRadius: const BorderRadius.all(
                              const Radius.circular(10.0),
                            ),
                            child: RoundTextFormFieldWidget(
                              scrollpadding:
                                  const EdgeInsets.only(bottom: 600.0),
                              isSecureInput: false,
                              controller: searchControllerPharmacy,
                              prefixIcon: Icon(Icons.search),
                              hinttext: 'Search',
                            )),
                      ),
                    ),
                    Expanded(
                      child: FutureBuilder(
                        future: _refreshProducts(context),
                        builder: (ctx, snapshot) => snapshot.connectionState ==
                                ConnectionState.waiting
                            ? Center(
                                child: CircularProgressIndicator(),
                              )
                            : Consumer<DrugOrderModalDoctor>(
                                builder: (ctx, ipPatients, _) =>
                                    ListView.builder(
                                  controller: _controller,
                                  physics: ScrollPhysics(),
                                  padding: EdgeInsets.only(top: 0),
                                  shrinkWrap: true,
                                  itemCount:
                                      ipPatients.loadIpPatientsDoctor.length,
                                  itemBuilder: (context, index) {
                                    DoctorsIpPatients ipPatientsList =
                                        ipPatients.loadIpPatientsDoctor[index];
                                    return Card(
                                      elevation: 4,
                                      child: Padding(
                                        padding: const EdgeInsets.all(5.0),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'MRNO: ${ipPatientsList.mrn}',
                                            ),
                                            ListTile(
                                              onTap: () =>
                                                  selectpatient(ipPatientsList),
                                              leading: CircleAvatar(
                                                backgroundColor: Colors.black,
                                                backgroundImage: ipPatientsList
                                                            .gender ==
                                                        'MALE'
                                                    ? AssetImage(
                                                        'assets/images/local_image/male.jpg')
                                                    : AssetImage(
                                                        'assets/images/local_image/female.jpg'),
                                              ),
                                              title: Text(
                                                '${ipPatientsList.patientName}',
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                              subtitle: Wrap(
                                                direction: Axis.vertical,
                                                children: [
                                                  Text(
                                                    '${ipPatientsList.age} '
                                                    'Years',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  SizedBox(
                                                    width: 10,
                                                  ),
                                                  Text(
                                                    '${ipPatientsList.gender}',
                                                  ),
                                                ],
                                              ),
                                              // trailing: Container(
                                              //   width: 30,
                                              //   child: Column(
                                              //     children: [
                                              //       Text('Q.no'),
                                              //       Text('${ipPatientsList.queueno}'),
                                              //     ],
                                              //   ),
                                              // ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              ),
                      ),
                    )
                  ]),
                ))
            : LoginInScreen());
  }
}
