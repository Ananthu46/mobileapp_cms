import 'package:doctor_portal_cms/screens/doctor/doctors_select_items/admission_request_doctor/admission_request_screen.dart';
import 'package:doctor_portal_cms/screens/doctor/doctors_select_items/allergies_doctor/allergy_selection_screen.dart';
import 'package:doctor_portal_cms/screens/doctor/doctors_select_items/diagnosis_doctor/diagnosis_screen.dart';
import 'package:doctor_portal_cms/screens/doctor/doctors_select_items/diagnosis_doctor/diagnosis_selection_screen.dart';
import 'package:doctor_portal_cms/screens/doctor/doctors_select_items/general_information_doctor/general_Information_Zefr.dart';
import 'package:doctor_portal_cms/screens/doctor/doctors_select_items/general_information_doctor/general_info_screen.dart';
import 'package:doctor_portal_cms/screens/doctor/doctors_select_items/lab_results/labresults.dart';
import 'package:doctor_portal_cms/screens/doctor/doctors_select_items/soapnotes/soapnotes_view_screen.dart';
import 'package:doctor_portal_cms/screens/doctor/doctors_select_items/vital_sign_doctor/vital_sign_doctor_screen.dart';
import 'package:doctor_portal_cms/screens/drug_order/drug_order_main_screen.dart';
import 'package:doctor_portal_cms/screens/ehr/electronic_health_record_screen.dart';
import 'package:doctor_portal_cms/widgets/zefyr/src/widgets/controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

class MedicalRecordsMainScreen extends StatefulWidget {
  static const routeName = "medical-records-mainscreen";

  @override
  _MedicalRecordsMainScreenState createState() =>
      _MedicalRecordsMainScreenState();
}

class _MedicalRecordsMainScreenState extends State<MedicalRecordsMainScreen> {
  Map arguments;

  @override
  void initState() {
    super.initState();
  }

  int _initialIndex = 0;

  Future<void> _refreshProducts(BuildContext context) async {
    arguments = ModalRoute.of(context).settings.arguments as Map;
  }

  ZefyrController provisionalController;
  ZefyrController finalController;
  onPopupSelected(String value) {
    if (value == "EHR") {
      Navigator.of(context).pushNamed(ElectronicHealthRecordScreen.routeName);
    } else if (value == "Orders") {
      Navigator.of(context).pushNamed(DrugOrderTabBar.routeName, arguments: {
        'patientID': '${arguments['patientID']}',
        'patientname': '${arguments['patientname']}',
        'age': '${arguments['age']}',
        'gender': '${arguments['gender']}',
        'service': '${arguments['visitid']}',
        'mrn': '${arguments['mrn']}',
        'visitid': '${arguments['visitid']}',
      });
    }
  }

  static final List<String> menuList = ["EHR", "Orders"];
  @override
  Widget build(BuildContext context) {
    return Container(
        color: Theme.of(context).primaryColor,
        child: SafeArea(
            child: FutureBuilder(
                future: _refreshProducts(context),
                builder: (ctx, snapshot) => snapshot.connectionState ==
                        ConnectionState.waiting
                    ? Center(
                        child: CircularProgressIndicator(),
                      )
                    : Scaffold(
                        body: DefaultTabController(
                          initialIndex: _initialIndex,
                          length: 7,
                          child: NestedScrollView(
                            headerSliverBuilder: (context, value) {
                              return [
                                SliverAppBar(
                                  expandedHeight: 100,
                                  centerTitle: false,
                                  title: ListTile(
                                    onTap: () {},
                                    leading: CircleAvatar(
                                      backgroundColor: Colors.black,
                                      backgroundImage: arguments['gender'] ==
                                              'MALE'
                                          ? AssetImage(
                                              'assets/images/local_image/male.jpg')
                                          : AssetImage(
                                              'assets/images/local_image/female.jpg'),
                                    ),
                                    title: Text(
                                      '${arguments['patientname']}',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Theme.of(context)
                                              .scaffoldBackgroundColor),
                                    ),
                                    subtitle: Wrap(
                                      direction: Axis.horizontal,
                                      children: [
                                        Text(
                                          '${arguments['age']} Years',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Theme.of(context)
                                                  .scaffoldBackgroundColor),
                                        ),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Text(
                                          '${arguments['gender']}',
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .scaffoldBackgroundColor),
                                        ),
                                      ],
                                    ),
                                  ),
                                  bottom: TabBar(
                                    isScrollable: true,
                                    tabs: [
                                      Tab(
                                          icon: Icon(FontAwesome.history),
                                          text: 'Medical Notes'),
                                      Tab(
                                          icon: Icon(FontAwesome.newspaper_o),
                                          text: 'Lab Results'),
                                      Tab(
                                          icon:
                                              Icon(FontAwesome5Solid.allergies),
                                          text: 'Allergies'),
                                      Tab(
                                          icon: Icon(
                                            FontAwesome5Solid.laptop_medical,
                                          ),
                                          text: 'Diagnosis'),
                                      Tab(
                                          icon: Icon(
                                            FontAwesome5Solid.laptop_medical,
                                          ),
                                          text: 'Admission Request'),
                                      Tab(
                                          icon: Icon(Icons.portrait),
                                          text: 'General Information'),
                                      Tab(
                                          icon: Icon(Icons.portrait),
                                          text: 'Vital Signs'),
                                    ],
                                  ),
                                  actions: <Widget>[
                                    PopupMenuButton<String>(
                                      // overflow menu
                                      onSelected: onPopupSelected,
                                      icon: new Icon(Icons.more_vert_rounded,
                                          color: Colors.white),
                                      itemBuilder: (BuildContext context) {
                                        return menuList
                                            .map<PopupMenuItem<String>>(
                                                (String choice) {
                                          return PopupMenuItem<String>(
                                            value: choice,
                                            child: Text(choice),
                                          );
                                        }).toList();
                                      },
                                    ),
                                  ],
                                ),
                              ];
                            },
                            body: TabBarView(
                              children: [
                                SoapNotesViewScreen(),
                                LabResultsDoctor(
                                  patientid: arguments['patientID'],
                                  visitid: arguments['visitid'],
                                ),
                                AllergySelectionscreen(
                                  patientid: arguments['patientID'],
                                ),
                                DiagnosisSelectionScreen(arguments['visitid']),
                                AdmissionRequestScreen(
                                  patientId: arguments['patientID'],
                                  visitId: arguments['visitid'],
                                ),
                                GeneralInformationScreen(
                                    arguments['patientID']),
                                VitalSignDoctor(),
                              ],
                            ),
                          ),
                        ),
                      ))));
  }
}
