import 'package:doctor_portal_cms/app/app_theme.dart';
import 'package:doctor_portal_cms/datastore/database/cms_doctorsportal_database.dart';
import 'package:flutter/material.dart';
import 'package:doctor_portal_cms/provider/auth/login_auth.dart';
import 'package:provider/provider.dart';

class AppDrawer extends StatefulWidget {
  @override
  _AppDrawerState createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {
  List<Profile> list;

  Future<List<Profile>> _refreshProducts(BuildContext context) async {
    list = await CMSDoctorsPortalDatabase().profileDao.getAllProfiles();
    print(list);
    if (list == null || list.isEmpty) {
      Navigator.of(context).pushReplacementNamed('/');
      Provider.of<LoginAuth>(context, listen: false).logout();
    }
    return list;
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: FutureBuilder<List<Profile>>(
      future: _refreshProducts(context),
      // initialData: [],
      builder: (context, snapshot) {
        return snapshot.connectionState == ConnectionState.done && list != null
            ? Column(children: [
                Expanded(
                    child: ListView(
                  padding: EdgeInsets.only(top: 0),
                  shrinkWrap: true,
                  children: <Widget>[
                    UserAccountsDrawerHeader(
                      accountName: ListTile(
                          contentPadding: EdgeInsets.only(top: 25),
                          leading: Icon(
                            Icons.person,
                            color: Colors.white,
                          ),
                          title: new Text(
                            "${list[0].firstname}",
                            style: TextStyle(color: Colors.white),
                          )),
                      accountEmail: ListTile(
                          contentPadding: EdgeInsets.only(bottom: 5),
                          leading: Icon(
                            Icons.email,
                            color: Colors.white,
                          ),
                          title: new Text(
                            "${list[0].email}",
                            style: TextStyle(color: Colors.white),
                          )),
                      decoration:
                          BoxDecoration(color: Theme.of(context).primaryColor),
                      currentAccountPicture: CircleAvatar(
                        // child: Image.memory(
                        //   Base64Codec().decode(list[0].profileImage),
                        // ),

                        //  radius: 47 * SizeConfig.getScaleFactor(),
                        backgroundColor: Colors.white,
                        backgroundImage:
                            AssetImage('assets/images/local_image/dummy.jpg'),
                      ),
                    ),
                    ListTile(
                      leading: Icon(Icons.home),
                      title: Text('Home'),
                      onTap: () {
                        Navigator.of(context).pop();
                        // Navigator.of(context)
                        //     .pushNamed(DoctorsPortalDashbord.routeName);
                      },
                    ),
                    // ListTile(
                    //   leading: Icon(Icons.exit_to_app),
                    //   title: Text('Logout'),
                    //   onTap: () async {
                    //     Navigator.of(context).pushReplacementNamed('/');
                    //     Provider.of<LoginAuth>(context, listen: false).logout();
                    //   },
                    // ),
                  ],
                )),
                Container(
                  child: Align(
                    alignment: FractionalOffset.bottomCenter,
                    child: Container(
                      child: Column(
                        children: <Widget>[
                          ListTile(
                            title: Text(
                              'Sign Out',
                              style: TextStyle(
                                fontFamily: AppTheme.fontName,
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                                color: AppTheme.darkText,
                              ),
                              textAlign: TextAlign.left,
                            ),
                            trailing: Icon(
                              Icons.power_settings_new,
                              color: Colors.red,
                            ),
                            onTap: () async {
                              // Navigator.of(context).pushReplacementNamed('/');
                              await CMSDoctorsPortalDatabase()
                                  .profileDao
                                  .deleteAllProfile();
                              Provider.of<LoginAuth>(context, listen: false)
                                  .logout();
                            },
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).padding.bottom,
                          )
                        ],
                      ),
                    ),
                  ),
                )
              ])
            : LinearProgressIndicator();
      },
    ));
  }
}
