// import 'package:doctor_portal_cms/datastore/database/doctorsportal_database.dart';
// import 'package:doctor_portal_cms/provider/drug/drug_order_api_provider.dart';
// import 'package:doctor_portal_cms/screens/appdrawer/app_drawer.dart';
// import 'package:doctor_portal_cms/screens/doctor/doctor_patients_list/doctors_oppatient_screen.dart';
// import 'package:doctor_portal_cms/screens/drug_order/drug_order_patients_screen.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:provider/provider.dart';

// class DashboardScreen extends StatefulWidget {
//   static const routeName = 'dashboard';
//   @override
//   _DashboardScreenState createState() => _DashboardScreenState();
// }

// class _DashboardScreenState extends State<DashboardScreen>
//     with TickerProviderStateMixin {
//   AnimationController _controller;
//   Animation<double> _animation;
//   GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
//   final _formKey = GlobalKey<FormState>();
//   // DatePickerController _controller = DatePickerController();
//   final dropdownState = GlobalKey<FormFieldState>();
//   Department selectedDepartmentValue;
//   String selectedBranchValue = 'CMS HOSPITAL TRUST';
//   Location selectedLocationValue;
//   String selectedDepartmentID;
//   String selectLocationId;

//   Future<void> _confirmSelection(context) async {
//     if (!_formKey.currentState.validate()) {
//       // Invalid!

//       return;
//     }
//     Provider.of<DrugPatientProvider>(context, listen: false)
//         .addSelecteddepartmentData(
//             selectedBranchValue,
//             selectedDepartmentValue.departmentname,
//             selectedDepartmentValue.depatmentID,
//             selectedLocationValue.locationname,
//             selectedLocationValue.locationID);
//     Navigator.push(
//       context,
//       MaterialPageRoute(
//         builder: (context) =>
//             // selectedDepartmentValue.departmentname == 'GENERAL MEDICINE'
//             //     ? DoctorsPatientScreenOP()
//             //     :
//             DrugOrderPatientScreen(
//           location: selectedLocationValue.locationID,
//         ),
//         //add arguments for searching with branch and location
//       ),
//     );
//   }

//   @override
//   void initState() {
//     super.initState();
//     _controller = AnimationController(
//       duration: const Duration(seconds: 3),
//       vsync: this,
//     )..forward();
//     _animation = CurvedAnimation(
//       parent: _controller,
//       curve: Curves.linear,
//     );
//     loadDepartment();
//     super.initState();
//   }

//   @override
//   void dispose() {
//     super.dispose();
//     _controller.dispose();
//   }

//   List<Department> _department;

//   List<Location> _location;
//   loadDepartment() async {
//     List<Department> department =
//         await DoctorsPortalDatabase().departmentDao.getAllDepartments();
//     List<Location> location =
//         await DoctorsPortalDatabase().locationDao.getAllLocations();
//     setState(() {
//       _department = department;
//       _location = location;
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//         color: Theme.of(context).primaryColor,
//         child: SafeArea(
//             child: Scaffold(
//                 key: _scaffoldKey,
//                 drawer: AppDrawer(),
//                 body: Container(
//                   decoration: BoxDecoration(
//                       image: DecorationImage(
//                           image: AssetImage(
//                               "assets/images/local_image/background03.jpg"),
//                           fit: BoxFit.cover)),
//                   child: CustomScrollView(
//                       physics: ClampingScrollPhysics(),
//                       slivers: <Widget>[
//                         SliverAppBar(
//                           title: Text('ST.JOSEPH\'S HOSPITAL TRUST'),
//                           centerTitle: true,
//                           leading: GestureDetector(
//                             onTap: () {
//                               _scaffoldKey.currentState.openDrawer();
//                             },
//                             child: Padding(
//                               padding: const EdgeInsets.all(8.0),
//                               child: CircleAvatar(
//                                 backgroundColor: Colors.black,
//                                 backgroundImage: AssetImage(
//                                     'assets/images/local_image/dummy.jpg'),
//                               ),
//                             ),
//                           ),
//                           actions: [
//                             Padding(
//                               padding: const EdgeInsets.all(8.0),
//                               child: Icon(Icons.notifications),
//                             )
//                           ],
//                         ),
//                         SliverToBoxAdapter(
//                           child: Padding(
//                             padding: const EdgeInsets.all(8.0),
//                             child: Container(
//                               height: 50,
//                               width: double.infinity,
//                             ),
//                           ),
//                         ),
//                         SliverToBoxAdapter(
//                           child: Padding(
//                             padding: const EdgeInsets.all(8.0),
//                             child: Center(
//                               child:
//                                   //  AnimatedContainer(
//                                   //   duration: Duration(seconds: 1),
//                                   //   curve: Curves.elasticIn,
//                                   //   transform: Matrix4.rotationY(0),
//                                   //   child: ScaleTransition(
//                                   //     scale: _animation,
//                                   //     child:
//                                   Card(
//                                 child: Padding(
//                                   padding: const EdgeInsets.all(20.0),
//                                   child: Form(
//                                     key: _formKey,
//                                     child: Column(
//                                       mainAxisAlignment:
//                                           MainAxisAlignment.center,
//                                       crossAxisAlignment:
//                                           CrossAxisAlignment.center,
//                                       children: [
//                                         Text(
//                                           'Please Select a department to Proceed!',
//                                           textAlign: TextAlign.center,
//                                           style: TextStyle(
//                                               color: Theme.of(context)
//                                                   .primaryColor,
//                                               fontSize: 20,
//                                               fontWeight: FontWeight.bold),
//                                         ),
//                                         SizedBox(
//                                           height: 20,
//                                         ),
//                                         Column(
//                                             crossAxisAlignment:
//                                                 CrossAxisAlignment.start,
//                                             children: [
//                                               Text('Branch'),
//                                               SizedBox(
//                                                 width: 300,
//                                                 child: DropdownButtonFormField(
//                                                   hint: Text('--Select--'),
//                                                   decoration: InputDecoration(
//                                                       enabledBorder:
//                                                           OutlineInputBorder(
//                                                         borderSide: BorderSide(
//                                                             color: Theme.of(
//                                                                     context)
//                                                                 .primaryColor,
//                                                             width: 2),
//                                                       ),
//                                                       errorBorder:
//                                                           OutlineInputBorder(
//                                                         borderSide: BorderSide(
//                                                             color: Theme.of(
//                                                                     context)
//                                                                 .primaryColor,
//                                                             width: 2),
//                                                       ),
//                                                       disabledBorder:
//                                                           OutlineInputBorder(
//                                                         borderSide: BorderSide(
//                                                             color: Theme.of(
//                                                                     context)
//                                                                 .primaryColor,
//                                                             width: 2),
//                                                       ),
//                                                       errorStyle: TextStyle(
//                                                           color: Colors.red),
//                                                       contentPadding:
//                                                           EdgeInsets.only(
//                                                               left: 10)),
//                                                   isExpanded: true,
//                                                   value: selectedBranchValue,
//                                                   items: <String>[
//                                                     'CMS HOSPITAL',
//                                                   ].map<
//                                                           DropdownMenuItem<
//                                                               String>>(
//                                                       (String value) {
//                                                     return DropdownMenuItem<
//                                                         String>(
//                                                       value: value,
//                                                       child: Text(value),
//                                                     );
//                                                   }).toList(),
//                                                   onChanged: (value) {
//                                                     setState(() {
//                                                       selectedBranchValue =
//                                                           value;
//                                                     });
//                                                   },
//                                                   validator: (value) {
//                                                     if (value == null) {
//                                                       return 'Select Branch to Proceed';
//                                                     }
//                                                     return null;
//                                                   },
//                                                 ),
//                                               ),
//                                             ]),
//                                         SizedBox(
//                                           height: 20,
//                                         ),
//                                         _department == null
//                                             ? Container(
//                                                 height: 5,
//                                                 width: 300,
//                                                 child:
//                                                     LinearProgressIndicator())
//                                             : Column(
//                                                 crossAxisAlignment:
//                                                     CrossAxisAlignment.start,
//                                                 children: [
//                                                     Text('Department'),
//                                                     SizedBox(
//                                                       width: 300,
//                                                       child:
//                                                           DropdownButtonFormField<
//                                                               Department>(
//                                                         hint:
//                                                             Text('--Select--'),
//                                                         decoration:
//                                                             InputDecoration(
//                                                           enabledBorder:
//                                                               OutlineInputBorder(
//                                                             borderSide: BorderSide(
//                                                                 color: Theme.of(
//                                                                         context)
//                                                                     .primaryColor,
//                                                                 width: 2),
//                                                           ),
//                                                           errorBorder:
//                                                               OutlineInputBorder(
//                                                             borderSide: BorderSide(
//                                                                 color: Theme.of(
//                                                                         context)
//                                                                     .primaryColor,
//                                                                 width: 2),
//                                                           ),
//                                                           disabledBorder:
//                                                               OutlineInputBorder(
//                                                             borderSide: BorderSide(
//                                                                 color: Theme.of(
//                                                                         context)
//                                                                     .primaryColor,
//                                                                 width: 2),
//                                                           ),
//                                                           contentPadding:
//                                                               EdgeInsets.only(
//                                                                   left: 10),
//                                                           errorStyle: TextStyle(
//                                                               color:
//                                                                   Colors.red),
//                                                         ),
//                                                         isExpanded: true,
//                                                         value:
//                                                             selectedDepartmentValue,
//                                                         onChanged: (Department
//                                                             newValue) {
//                                                           setState(() {
//                                                             selectedDepartmentValue =
//                                                                 newValue;
//                                                           });
//                                                         },
//                                                         items: _department
//                                                             .map((department) {
//                                                           // selectedStoreId =
//                                                           //     selectedstore.storeId;

//                                                           return DropdownMenuItem(
//                                                             child: new Text(
//                                                                 department
//                                                                     .departmentname),
//                                                             value: department,
//                                                           );
//                                                         }).toList(),
//                                                         validator: (value) {
//                                                           if (value == null) {
//                                                             return 'Select Department to Proceed';
//                                                           }
//                                                           return null;
//                                                         },
//                                                       ),
//                                                     ),
//                                                   ]),
//                                         SizedBox(
//                                           height: 20,
//                                         ),
//                                         _department == null
//                                             ? Container()
//                                             : Column(
//                                                 crossAxisAlignment:
//                                                     CrossAxisAlignment.start,
//                                                 children: [
//                                                   Text('Location'),
//                                                   SizedBox(
//                                                     width: 300,
//                                                     child:
//                                                         DropdownButtonFormField<
//                                                             Location>(
//                                                       hint: Text('--Select--'),
//                                                       decoration:
//                                                           InputDecoration(
//                                                         enabledBorder:
//                                                             OutlineInputBorder(
//                                                           borderSide: BorderSide(
//                                                               color: Theme.of(
//                                                                       context)
//                                                                   .primaryColor,
//                                                               width: 2),
//                                                         ),
//                                                         errorBorder:
//                                                             OutlineInputBorder(
//                                                           borderSide: BorderSide(
//                                                               color: Theme.of(
//                                                                       context)
//                                                                   .primaryColor,
//                                                               width: 2),
//                                                         ),
//                                                         disabledBorder:
//                                                             OutlineInputBorder(
//                                                           borderSide: BorderSide(
//                                                               color: Theme.of(
//                                                                       context)
//                                                                   .primaryColor,
//                                                               width: 2),
//                                                         ),
//                                                         contentPadding:
//                                                             EdgeInsets.only(
//                                                                 left: 10),
//                                                         errorStyle: TextStyle(
//                                                             color: Colors.red),
//                                                       ),
//                                                       isExpanded: true,
//                                                       value:
//                                                           selectedLocationValue,
//                                                       onChanged:
//                                                           (Location newValue) {
//                                                         setState(() {
//                                                           selectedLocationValue =
//                                                               newValue;
//                                                         });
//                                                       },
//                                                       items: _location
//                                                           .map((location) {
//                                                         // selectedStoreId =
//                                                         //     selectedstore.storeId;

//                                                         return DropdownMenuItem(
//                                                           child: new Text(
//                                                               location
//                                                                   .locationname),
//                                                           value: location,
//                                                         );
//                                                       }).toList(),
//                                                       validator: (value) {
//                                                         if (value == null) {
//                                                           return 'Select Location to Proceed';
//                                                         }
//                                                         return null;
//                                                       },
//                                                     ),
//                                                   ),
//                                                 ],
//                                               ),
//                                         SizedBox(
//                                           height: 20,
//                                         ),
//                                         Center(
//                                           child: FlatButton(
//                                             autofocus: true,
//                                             clipBehavior: Clip.hardEdge,
//                                             onPressed: () =>
//                                                 _confirmSelection(context),
//                                             child: Text(
//                                               'Confirm',
//                                               style: TextStyle(
//                                                   color: Colors.white),
//                                             ),
//                                             shape: RoundedRectangleBorder(
//                                               borderRadius:
//                                                   BorderRadius.circular(30),
//                                             ),
//                                             minWidth: 250,
//                                             padding: EdgeInsets.symmetric(
//                                                 horizontal: 30.0,
//                                                 vertical: 8.0),
//                                             color:
//                                                 Theme.of(context).primaryColor,
//                                           ),
//                                         ),
//                                       ],
//                                     ),
//                                   ),
//                                 ),
//                               ),
//                             ),
//                           ),
//                         ),
//                         //  ),
//                         //  )
//                       ]),
//                 ))));
//   }
// }
