// import 'package:doctor_portal_cms/app/app_theme.dart';
// import 'package:doctor_portal_cms/app/app_value_config.dart';
// import 'package:doctor_portal_cms/app/size_config.dart';
// import 'package:doctor_portal_cms/datastore/database/doctorsportal_database.dart';
// import 'package:doctor_portal_cms/provider/auth/login_auth.dart';
// import 'package:doctor_portal_cms/provider/doctor/doctor_order_provider.dart';
// import 'package:doctor_portal_cms/widgets/mainscreen_items/doctors_detials_card.dart';
// import 'package:doctor_portal_cms/widgets/mainscreen_items/titleview_widget.dart';
// import 'package:flutter/material.dart';
// import 'package:intl/intl.dart';
// import 'package:provider/provider.dart';

// class DoctorsdashbordScreen extends StatefulWidget {
//   @override
//   _DoctorsdashbordScreenState createState() => _DoctorsdashbordScreenState();
// }

// class _DoctorsdashbordScreenState extends State<DoctorsdashbordScreen>
//     with TickerProviderStateMixin {
//   Animation<double> topBarAnimation;
//   AnimationController animationController;
//   List<Widget> listViews = <Widget>[];
//   final ScrollController scrollController = ScrollController();
//   double topBarOpacity = 0.0;
//   List<Profile> list;
//   Future<void> _refreshProducts(BuildContext context) async {
//     await Future<dynamic>.delayed(const Duration(milliseconds: 50));
//     setState(() {
//       flag = true;
//     });
//     list = await DoctorsPortalDatabase().profileDao.getAllProfiles();
//     print(list);
//     if (list == null || list.isEmpty) {
//       Navigator.of(context).pushReplacementNamed('/');
//       Provider.of<LoginAuth>(context, listen: false).logout();
//     }

//     await Provider.of<DrugOrderModalDoctor>(context, listen: false)
//         .loadIPOPpatientsCounts(searchDate);

//     return list;
//   }

//   bool _isLoaded = false;
//   String searchDate = DateTime.now().toString();
//   String _date;
//   DateTime date = DateTime.now();
//   Future<Null> _selectDate(BuildContext context) async {
//     final DateTime picked = await showDatePicker(
//         context: context,
//         initialDate: date,
//         firstDate: DateTime(2010, 1),
//         lastDate: DateTime(2026));

//     if (picked != null && picked != date)
//       setState(() {
//         flag = false;
//         date = picked;
//         searchDate = DateFormat('yyyy-MM-dd').format(date);
//         print(searchDate);
//         consultantSelectedDate = searchDate;
//         _date = DateFormat('dd-MM-yyyy').format(date);
//       });
//   }

//   bool flag = false;
//   @override
//   void initState() {
//     animationController = AnimationController(
//         duration: const Duration(milliseconds: 600), vsync: this);

//     addAllListData();

//     scrollController.addListener(() {
//       if (scrollController.offset >= 24) {
//         if (topBarOpacity != 1.0) {
//           setState(() {
//             topBarOpacity = 1.0;
//           });
//         }
//       } else if (scrollController.offset <= 24 &&
//           scrollController.offset >= 0) {
//         if (topBarOpacity != scrollController.offset / 24) {
//           setState(() {
//             topBarOpacity = scrollController.offset / 24;
//           });
//         }
//       } else if (scrollController.offset <= 0) {
//         if (topBarOpacity != 0.0) {
//           setState(() {
//             topBarOpacity = 0.0;
//           });
//         }
//       }
//     });
//     super.initState();
//   }

//   @override
//   void didChangeDependencies() {
//     topBarAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
//         CurvedAnimation(
//             parent: animationController,
//             curve: Interval(0, 0.5, curve: Curves.fastOutSlowIn)));
//     super.didChangeDependencies();
//   }

//   void addAllListData() {
//     const int count = 9;

//     listViews.add(
//       TitleView(
//         titleTxt: 'Mediterranean diet',
//         subTxt: 'Details',
//         animation: Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
//             parent: animationController,
//             curve:
//                 Interval((1 / count) * 0, 1.0, curve: Curves.fastOutSlowIn))),
//         animationController: animationController,
//       ),
//     );
//     listViews.add(
//       DoctorsDetialsViewWidget(
//         animation: Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
//             parent: animationController,
//             curve:
//                 Interval((1 / count) * 1, 1.0, curve: Curves.fastOutSlowIn))),
//         animationController: animationController,
//       ),
//     );
//     // listViews.add(
//     //   TitleView(
//     //     titleTxt: 'Meals today',
//     //     subTxt: 'Customize',
//     //     animation: Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
//     //         parent: widget.animationController,
//     //         curve:
//     //             Interval((1 / count) * 2, 1.0, curve: Curves.fastOutSlowIn))),
//     //     animationController: widget.animationController,
//     //   ),
//     // );

//     // listViews.add(
//     //   MealsListView(
//     //     mainScreenAnimation: Tween<double>(begin: 0.0, end: 1.0).animate(
//     //         CurvedAnimation(
//     //             parent: widget.animationController,
//     //             curve: Interval((1 / count) * 3, 1.0,
//     //                 curve: Curves.fastOutSlowIn))),
//     //     mainScreenAnimationController: widget.animationController,
//     //   ),
//     // );

//     // listViews.add(
//     //   TitleView(
//     //     titleTxt: 'Body measurement',
//     //     subTxt: 'Today',
//     //     animation: Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
//     //         parent: widget.animationController,
//     //         curve:
//     //             Interval((1 / count) * 4, 1.0, curve: Curves.fastOutSlowIn))),
//     //     animationController: widget.animationController,
//     //   ),
//     // );

//     // listViews.add(
//     //   BodyMeasurementView(
//     //     animation: Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
//     //         parent: widget.animationController,
//     //         curve:
//     //             Interval((1 / count) * 5, 1.0, curve: Curves.fastOutSlowIn))),
//     //     animationController: widget.animationController,
//     //   ),
//     // );
//     // listViews.add(
//     //   TitleView(
//     //     titleTxt: 'Water',
//     //     subTxt: 'Aqua SmartBottle',
//     //     animation: Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
//     //         parent: widget.animationController,
//     //         curve:
//     //             Interval((1 / count) * 6, 1.0, curve: Curves.fastOutSlowIn))),
//     //     animationController: widget.animationController,
//     //   ),
//     // );

//     // listViews.add(
//     //   WaterView(
//     //     mainScreenAnimation: Tween<double>(begin: 0.0, end: 1.0).animate(
//     //         CurvedAnimation(
//     //             parent: widget.animationController,
//     //             curve: Interval((1 / count) * 7, 1.0,
//     //                 curve: Curves.fastOutSlowIn))),
//     //     mainScreenAnimationController: widget.animationController,
//     //   ),
//     // );
//     // listViews.add(
//     //   GlassView(
//     //       animation: Tween<double>(begin: 0.0, end: 1.0).animate(
//     //           CurvedAnimation(
//     //               parent: widget.animationController,
//     //               curve: Interval((1 / count) * 8, 1.0,
//     //                   curve: Curves.fastOutSlowIn))),
//     //       animationController: widget.animationController),
//     // );
//   }

//   // Future<bool> getData() async {
//   //   await Future<dynamic>.delayed(const Duration(milliseconds: 50));
//   //   return true;
//   // }

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       color: Theme.of(context).primaryColor,
//       child: SafeArea(
//         child: Scaffold(
//           appBar: AppBar(
//             elevation: 0,
//             title: Container(
//               child: // Container(
//                   //   height: 60 * SizeConfig.getScaleFactor(),
//                   //   width: 300 * SizeConfig.getScaleFactor(),
//                   //   child: Image.asset(
//                   //       'assets/images/logo/chazikkat_logo.png'),
//                   // ),
//                   // Center(
//                   //   child: Text(
//                   //       'Chazhikattu MultiSuper-Speciality Hospital',
//                   //       style: TextStyle(
//                   //           fontSize: 18,
//                   //           color: Theme.of(context)
//                   //               .scaffoldBackgroundColor)),
//                   // ),
//                   Row(
//                 mainAxisAlignment: MainAxisAlignment.center,
//                 children: [
//                   CircleAvatar(
//                     radius: 20,
//                     backgroundColor: Colors.black,
//                     backgroundImage:
//                         AssetImage('assets/images/local_image/cms_logo.png'),
//                   ),
//                   SizedBox(
//                     width: 15 * SizeConfig.getScaleFactor(),
//                   ),
//                   Column(
//                     mainAxisAlignment: MainAxisAlignment.start,
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: [
//                       Text('Chazhikattu',
//                           style: TextStyle(
//                               fontSize: 22 * SizeConfig.getScaleFactor(),
//                               fontWeight: FontWeight.bold,
//                               color:
//                                   Theme.of(context).scaffoldBackgroundColor)),
//                       Text('MultiSuper-Speciality Hospital',
//                           style: TextStyle(
//                               fontSize: 18 * SizeConfig.getScaleFactor(),
//                               color:
//                                   Theme.of(context).scaffoldBackgroundColor)),
//                     ],
//                   ),
//                   SizedBox(
//                     width: 10 * SizeConfig.getScaleFactor(),
//                   )
//                 ],
//               ),
//             ),
//             // centerTitle: true,
//             actions: [
//               IconButton(
//                   icon: Icon(Icons.exit_to_app),
//                   onPressed: () {
//                     Navigator.of(context).pushReplacementNamed('/');
//                     Provider.of<LoginAuth>(context, listen: false).logout();
//                   })
//             ],
//           ),
//           backgroundColor: App.background,
//           body: Stack(
//             children: <Widget>[
//               getMainListViewUI(),
//               // getAppBarUI(),
//               SizedBox(
//                 height: MediaQuery.of(context).padding.bottom,
//               )
//             ],
//           ),
//         ),
//       ),
//     );
//   }

//   Widget getMainListViewUI() {
//     return FutureBuilder<void>(
//         future: flag ? null : _refreshProducts(context),
//         builder: (context, snapshot) {
//           return list == null &&
//                   snapshot.connectionState == ConnectionState.done
//               ? Center(child: CircularProgressIndicator())
//               : ListView.builder(
//                   controller: scrollController,
//                   // padding: EdgeInsets.only(
//                   //   top: AppBar().preferredSize.height +
//                   //       MediaQuery.of(context).padding.top +
//                   //       24,
//                   // bottom: 62 + MediaQuery.of(context).padding.bottom,
//                   // ),
//                   itemCount: listViews.length,
//                   scrollDirection: Axis.vertical,
//                   itemBuilder: (BuildContext context, int index) {
//                     animationController.forward();
//                     return listViews[index];
//                   },
//                 );
//         });
//   }

//   Widget getAppBarUI() {
//     SizeConfig().init(context);
//     return Column(
//       children: <Widget>[
//         AnimatedBuilder(
//           animation: animationController,
//           builder: (BuildContext context, Widget child) {
//             return FadeTransition(
//               opacity: topBarAnimation,
//               child: Transform(
//                 transform: Matrix4.translationValues(
//                     0.0, 30 * (1.0 - topBarAnimation.value), 0.0),
//                 child: Container(
//                   decoration: BoxDecoration(
//                     color:
//                         DoctorsDashbordTheme.white.withOpacity(topBarOpacity),
//                     borderRadius: const BorderRadius.only(
//                       bottomLeft: Radius.circular(32.0),
//                     ),
//                     boxShadow: <BoxShadow>[
//                       BoxShadow(
//                           color: DoctorsDashbordTheme.grey
//                               .withOpacity(0.4 * topBarOpacity),
//                           offset: const Offset(1.1, 1.1),
//                           blurRadius: 10.0),
//                     ],
//                   ),
//                   child: Column(
//                     children: <Widget>[
//                       SizedBox(
//                         height: MediaQuery.of(context).padding.top,
//                       ),
//                       Padding(
//                         padding: EdgeInsets.only(
//                             left: 16,
//                             right: 16,
//                             top: 16 - 8.0 * topBarOpacity,
//                             bottom: 12 - 8.0 * topBarOpacity),
//                         child: Row(
//                           mainAxisAlignment: MainAxisAlignment.start,
//                           children: <Widget>[
//                             SizedBox(
//                               width: 25,
//                             ),
//                             CircleAvatar(
//                               radius: 20,
//                               backgroundColor: Colors.black,
//                               backgroundImage: AssetImage(
//                                   'assets/images/local_image/cms_logo.png'),
//                             ),
//                             SizedBox(
//                               width: 15 * SizeConfig.getScaleFactor(),
//                             ),
//                             Column(
//                               mainAxisAlignment: MainAxisAlignment.start,
//                               crossAxisAlignment: CrossAxisAlignment.start,
//                               children: [
//                                 Padding(
//                                   padding: const EdgeInsets.all(2.0),
//                                   child: Text(
//                                     'CMS Hospital',
//                                     textAlign: TextAlign.left,
//                                     style: TextStyle(
//                                       fontFamily: DoctorsDashbordTheme.fontName,
//                                       fontWeight: FontWeight.w700,
//                                       fontSize: 22 + 6 - 6 * topBarOpacity,
//                                       letterSpacing: 1.2,
//                                       color: DoctorsDashbordTheme.darkerText,
//                                     ),
//                                   ),
//                                 ),
//                                 Text(
//                                   'MultiSuper-Speciality Hospital',
//                                   style: TextStyle(
//                                     fontFamily: DoctorsDashbordTheme.fontName,
//                                     fontWeight: FontWeight.w200,
//                                     fontSize: 18,
//                                     letterSpacing: 1.2,
//                                     color: DoctorsDashbordTheme.darkerText,
//                                   ),
//                                 ),
//                               ],
//                             ),
//                             SizedBox(
//                               width: 10 * SizeConfig.getScaleFactor(),
//                             ),
//                           ],
//                         ),
//                       )
//                     ],
//                   ),
//                 ),
//               ),
//             );
//           },
//         )
//       ],
//     );
//   }
// }
