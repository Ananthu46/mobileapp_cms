import 'package:doctor_portal_cms/app/app_theme.dart';
import 'package:doctor_portal_cms/app/app_value_config.dart';
import 'package:doctor_portal_cms/app/size_config.dart';
import 'package:doctor_portal_cms/datastore/database/cms_doctorsportal_database.dart';
import 'package:doctor_portal_cms/provider/auth/login_auth.dart';
import 'package:doctor_portal_cms/provider/doctor/doctor_order_provider.dart';
import 'package:doctor_portal_cms/screens/appdrawer/app_drawer.dart';
import 'package:doctor_portal_cms/screens/auth/login_screen.dart';
import 'package:doctor_portal_cms/screens/doctor/doctor_patients_list/doctor_ippatients_screen.dart';
import 'package:doctor_portal_cms/screens/doctor/doctor_patients_list/doctors_oppatient_screen.dart';
import 'package:doctor_portal_cms/widgets/loading_indicator/rect_loading_indicator.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class DoctorsPortalDashbord extends StatefulWidget {
  static const routeName = 'doctor-dashboard';
  @override
  _DoctorsPortalDashbordState createState() => _DoctorsPortalDashbordState();
}

class _DoctorsPortalDashbordState extends State<DoctorsPortalDashbord>
    with TickerProviderStateMixin {
  AnimationController _controller;
  AnimationController _controller1;
  Animation<Offset> offset;
  Animation<Offset> offset1;
  GlobalKey<ScaffoldState> _key = new GlobalKey<ScaffoldState>();
  List<Profile> list = [];
  Future<void> _refreshProducts(BuildContext context) async {
    setState(() {
      flag = true;
    });
    list = await CMSDoctorsPortalDatabase().profileDao.getAllProfiles();
    print(list);
    if (list == null || list.isEmpty) {}
    await Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .loadIPOPpatientsCounts(searchDate, context);
    String error =
        Provider.of<DrugOrderModalDoctor>(context, listen: false).error;
    if (error == 'Error') {
      Provider.of<LoginAuth>(context, listen: false).logout();
    }
    return list;
  }

  String searchDate = DateTime.now().toString();
  @override
  void initState() {
    Future.delayed(Duration.zero).then((value) async {
      list = await CMSDoctorsPortalDatabase().profileDao.getAllProfiles();
    });
    Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .loadIPOPpatientsCounts(searchDate, context);
    _controller = AnimationController(
      duration: const Duration(milliseconds: 600),
      vsync: this,
    )..forward();
    _controller1 = AnimationController(
      duration: const Duration(milliseconds: 800),
      vsync: this,
    )..forward();
    offset = Tween<Offset>(begin: Offset(0.0, 1.0), end: Offset(0.0, 0.0))
        .animate(_controller);
    offset1 = Tween<Offset>(begin: Offset(0.0, 1.0), end: Offset(0.0, 0.0))
        .animate(_controller1);

    // loadEmployeeDetials();
    super.initState();
  }

  // Future<void> loadEmployeeDetials() async {
  //   list = await DoctorsPortalDatabase().profileDao.getAllProfiles();
  //   setState(() {});
  // }

  // TextEditingController _date = new TextEditingController();
  String _date;
  DateTime date = DateTime.now();
  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: date,
        firstDate: DateTime(2010, 1),
        lastDate: DateTime(2026));

    if (picked != null && picked != date)
      setState(() {
        flag = false;
        date = picked;
        searchDate = DateFormat('yyyy-MM-dd').format(date);
        print(searchDate);
        consultantSelectedDate = searchDate;
        _date = DateFormat('dd-MM-yyyy').format(date);
      });
  }

  bool flag = false;

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  final TextStyle whiteText = TextStyle(color: Colors.white);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
        decoration: BoxDecoration(
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: HexColor('#0da889').withOpacity(0.6),
                  offset: const Offset(1.1, 4.0),
                  blurRadius: 8.0),
            ],
            gradient: LinearGradient(
              colors: <HexColor>[
                HexColor('##02755e'),
                HexColor('#07a384'),
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            ),
            borderRadius: const BorderRadius.only(
              bottomRight: Radius.circular(25.0),
              bottomLeft: Radius.circular(25.0),
              topLeft: Radius.circular(0),
              topRight: Radius.circular(0),
            )),
        child: Consumer<LoginAuth>(
          builder: (ctx, auth, _) => auth.isAuth
              ? Container(
                  color: Theme.of(context).primaryColor,
                  child: SafeArea(
                    child: Scaffold(
                        backgroundColor:
                            Theme.of(context).scaffoldBackgroundColor,
                        drawer: AppDrawer(),
                        key: _key,
                        body: RefreshIndicator(
                            onRefresh: () => _refreshProducts(context),
                            child: ListView(
                              children: [
                                FutureBuilder<void>(
                                    future:
                                        flag ? null : _refreshProducts(context),
                                    // initialData: [],
                                    builder: (context, snapshot) {
                                      return _buildBody(context);
                                    }),
                              ],
                            ))),
                  ),
                )
              : LoginInScreen(),
        ));
  }

  Widget _buildBody(BuildContext context) {
    SizeConfig().init(context);
    return SingleChildScrollView(
        child: Consumer<DrugOrderModalDoctor>(
      builder: (ctx, countIPOP, _) => countIPOP.loadIPOPpatientsCount.isEmpty
          ? Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: 300 * SizeConfig.getScaleFactor(),
                  height: 300 * SizeConfig.getScaleFactor(),
                  child: Image.asset('assets/images/logo/chazikkat_logo.png'),
                ),
                LifeLoadingIndicator(),
              ],
            )
          : Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _buildHeader(),
                const SizedBox(height: 10.0),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      _date == null
                          ? Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 4.0),
                              child: Text(
                                'Today\'s',
                                style: TextStyle(
                                  fontSize: 15 * SizeConfig.getScaleFactor(),
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            )
                          : Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 15.0),
                              child: Text(
                                '$_date',
                                style: TextStyle(
                                  fontSize: 15 * SizeConfig.getScaleFactor(),
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                      IconButton(
                        icon: Icon(
                          Icons.date_range,
                          color: Theme.of(context).primaryColor,
                        ),
                        onPressed: () => _selectDate(context),
                      )
                    ],
                  ),
                ),
                SlideTransition(
                  position: offset,
                  child: Card(
                    elevation: 4.0,
                    margin:
                        const EdgeInsets.only(bottom: 16, left: 0, right: 0),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 8),
                                child: _patientSelectButton(
                                  image: ClipRRect(
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(60.0)),
                                      child: CircleAvatar(
                                        backgroundColor: Colors.white,
                                        child: Image.asset(
                                            'assets/images/local_image/inpatient.png'),
                                        radius: 12,
                                      )),
                                  head: 'IN-PATIENTS',
                                  countIPOP: countIPOP,
                                  count: countIPOP
                                      .loadIPOPpatientsCount[0].ipcount,
                                  onPressed: () {
                                    Navigator.of(context).pushNamed(
                                        DoctorsPatientScreenIP.routeName);
                                  },
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 8),
                                child: _patientSelectButton(
                                  image: ClipRRect(
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(60.0)),
                                      child: CircleAvatar(
                                        backgroundColor: Colors.white,
                                        child: Image.asset(
                                            'assets/images/local_image/outpatient.png'),
                                        radius: 12,
                                      )),
                                  head: 'OUT-PATIENTS',
                                  countIPOP: countIPOP,
                                  count: countIPOP
                                      .loadIPOPpatientsCount[0].opcount,
                                  onPressed: () {
                                    Navigator.of(context).pushNamed(
                                        DoctorsPatientScreenOP.routeName);
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SlideTransition(
                  position: offset1,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          flex: 2,
                          child: _buildTile(
                            colors: <HexColor>[
                              HexColor('#fa0c0c'),
                              HexColor('#fa7d7d'),
                            ],
                            icon: Icons.portrait,
                            title: "Appointments",
                            data:
                                countIPOP.loadIPOPpatientsCount[0].appointments,
                          ),
                        ),
                        const SizedBox(width: 16.0),
                        Expanded(
                          child: _buildTile(
                            colors: <HexColor>[
                              HexColor('#209f8f'),
                              HexColor('#1ac6b0'),
                            ],
                            icon: Icons.person_add_alt,
                            title: "Referral",
                            data: countIPOP.loadIPOPpatientsCount[0].referal,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(height: 16.0),
                SlideTransition(
                  position: offset1,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: _buildTile(
                            colors: <HexColor>[
                              HexColor('#2824f2'),
                              HexColor('#6e6cf5'),
                            ],
                            icon: Icons.note_add,
                            title: "Encounter",
                            data:
                                countIPOP.loadIPOPpatientsCount[0].encountered,
                          ),
                        ),
                        const SizedBox(width: 16.0),
                        Expanded(
                          child: _buildTile(
                            colors: <HexColor>[
                              HexColor('#d234eb'),
                              HexColor('#eba9f5'),
                            ],
                            icon: Icons.people_outline_sharp,
                            title: "Followup",
                            data: countIPOP.loadIPOPpatientsCount[0].followup,
                          ),
                        ),
                        const SizedBox(width: 16.0),
                        Expanded(
                          child: _buildTile(
                            colors: <HexColor>[
                              HexColor('#0076fc'),
                              HexColor('#60a7f7'),
                            ],
                            icon: Icons.calendar_today,
                            title: "Month to Date",
                            data:
                                countIPOP.loadIPOPpatientsCount[0].monthToDate,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(height: 20.0),
              ],
            ),
    ));
  }

  Container _buildHeader() {
    return Container(
      height: 170 * SizeConfig.getScaleFactor(),
      width: double.infinity,
      decoration: BoxDecoration(
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: HexColor('#0da889').withOpacity(0.6),
                offset: const Offset(1.1, 4.0),
                blurRadius: 8.0),
          ],
          gradient: LinearGradient(
            colors: <Color>[
              Color(0xff009688),
              Color(0xff009688),
            ],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          ),
          borderRadius: const BorderRadius.only(
            bottomRight: Radius.circular(25.0),
            bottomLeft: Radius.circular(25.0),
            topLeft: Radius.circular(0),
            topRight: Radius.circular(0),
          )),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 5 * SizeConfig.getScaleFactor(),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 5),
            child: ListTile(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 5 * SizeConfig.getScaleFactor(),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Chazhikattu',
                          style: TextStyle(
                              fontSize: 22 * SizeConfig.getScaleFactor(),
                              fontWeight: FontWeight.bold,
                              color:
                                  Theme.of(context).scaffoldBackgroundColor)),
                      SizedBox(
                        width: 285 * SizeConfig.getScaleFactor(),
                        child: Text(
                            'Multi Super-Speciality Hospital, Thodupuzha',
                            style: TextStyle(
                                fontSize: 14 * SizeConfig.getScaleFactor(),
                                color:
                                    Theme.of(context).scaffoldBackgroundColor)),
                      ),
                    ],
                  ),
                  SizedBox(
                    width: 10 * SizeConfig.getScaleFactor(),
                  )
                ],
              ),
              leading: IconButton(
                color: Theme.of(context).scaffoldBackgroundColor,
                icon: Icon(Icons.menu),
                onPressed: () => _key.currentState.openDrawer(),
              ),
            ),
          ),
          const SizedBox(height: 20.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 20.0),
                    child: Text(
                      "Dr. " + list[0].firstname,
                      style: whiteText.copyWith(
                        fontSize: 18.0,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  const SizedBox(height: 5.0),
                  Padding(
                    padding: const EdgeInsets.only(left: 16.0),
                    child: Text(
                      list[0].qualification + "\n(${list[0].department})",
                      style: whiteText,
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 5, 25, 5),
                child: ClipRRect(
                    borderRadius: const BorderRadius.all(Radius.circular(60.0)),
                    child: CircleAvatar(
                      child: Image.asset('assets/images/local_image/dummy.jpg'),
                      radius: 20,
                    )),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Container _buildTile(
      {IconData icon, String title, String data, List<Color> colors}) {
    return Container(
      padding: const EdgeInsets.all(8.0),
      height: 145.0,
      decoration: BoxDecoration(
        boxShadow: <BoxShadow>[
          BoxShadow(
              color: HexColor('#FFB295').withOpacity(0.6),
              offset: const Offset(1.1, 4.0),
              blurRadius: 8.0),
        ],
        gradient: LinearGradient(
          colors: colors,
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
        ),
        borderRadius: const BorderRadius.only(
          bottomRight: Radius.circular(8.0),
          bottomLeft: Radius.circular(8.0),
          topLeft: Radius.circular(8.0),
          topRight: Radius.circular(54.0),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Icon(
            icon,
            color: Colors.white,
          ),
          Text(
            title,
            style: whiteText.copyWith(fontWeight: FontWeight.bold),
          ),
          Text(
            data,
            style:
                whiteText.copyWith(fontWeight: FontWeight.bold, fontSize: 20.0),
          ),
        ],
      ),
    );
  }

  OutlinedButton _patientSelectButton(
      {String head,
      DrugOrderModalDoctor countIPOP,
      String count,
      Widget image,
      void Function() onPressed}) {
    return OutlinedButton(
      onPressed: onPressed,
      child: Card(
        elevation: 0,
        child: Row(
          children: <Widget>[
            Container(
              height: 48,
              width: 2,
              decoration: BoxDecoration(
                color: HexColor('#87A0E5').withOpacity(0.5),
                borderRadius: BorderRadius.all(Radius.circular(4.0)),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 4, bottom: 3),
                    child: Text(
                      head,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        // fontFamily:
                        //     Colors.fontName,
                        fontWeight: FontWeight.w500,
                        fontSize: 16,
                        letterSpacing: 1,
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      image,
                      SizedBox(
                        width: 15,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20, bottom: 3),
                        child: countIPOP.loadIPOPpatientsCount.isEmpty
                            ? Text(
                                '0',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 16,
                                ),
                              )
                            : Text(
                                count,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 16,
                                ),
                              ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 4, bottom: 3),
                        child: Text(
                          '',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 12,
                            letterSpacing: -0.2,
                            color: Colors.grey.withOpacity(0.5),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
