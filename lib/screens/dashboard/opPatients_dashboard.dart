import 'package:doctor_portal_cms/app/app_value_config.dart';
import 'package:doctor_portal_cms/app/size_config.dart';
import 'package:doctor_portal_cms/datastore/database/cms_doctorsportal_database.dart';
import 'package:doctor_portal_cms/provider/auth/login_auth.dart';
import 'package:doctor_portal_cms/provider/doctor/doctor_order_provider.dart';
import 'package:doctor_portal_cms/screens/doctor/doctor_patients_list/doctor_ippatients_screen.dart';
import 'package:doctor_portal_cms/screens/doctor/doctor_patients_list/doctors_oppatient_screen.dart';
import 'package:doctor_portal_cms/widgets/gradientColors/gradient.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class OpPatientsDashBoard extends StatefulWidget {
  static const routeName = 'oppatient-dashboard';
  @override
  _OpPatientsDashBoardState createState() => _OpPatientsDashBoardState();
}

class _OpPatientsDashBoardState extends State<OpPatientsDashBoard> {
  List<Profile> list;
  // AnimationController animationController;
  // @override
  // void initState() {
  //   animationController = AnimationController(
  //       duration: const Duration(milliseconds: 600), vsync: this);
  //   super.initState();
  // }

  Future<void> _refreshProducts(BuildContext context) async {
    setState(() {
      flag = true;
    });
    list = await CMSDoctorsPortalDatabase().profileDao.getAllProfiles();
    print(list);
    if (list == null || list.isEmpty) {
      Navigator.of(context).pushReplacementNamed('/');
      Provider.of<LoginAuth>(context, listen: false).logout();
    }

    await Provider.of<DrugOrderModalDoctor>(context, listen: false)
        .loadIPOPpatientsCounts(searchDate, context);

    return list;
  }

  String searchDate = DateTime.now().toString();
  // @override
  // void initState() {
  //   Provider.of<DrugOrderModalDoctor>(context, listen: false)
  //       .loadIpPatientsDoctorWise();
  //   super.initState();
  // }
  // TextEditingController _date = new TextEditingController();
  String _date;
  DateTime date = DateTime.now();
  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: date,
        firstDate: DateTime(2010, 1),
        lastDate: DateTime(2026));

    if (picked != null && picked != date)
      setState(() {
        flag = false;
        date = picked;
        searchDate = DateFormat('yyyy-MM-dd').format(date);
        print(searchDate);
        consultantSelectedDate = searchDate;
        _date = DateFormat('dd-MM-yyyy').format(date);
      });
  }

  bool flag = false;
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          elevation: 0,
          title: Container(
            child: // Container(
                //   height: 60 * SizeConfig.getScaleFactor(),
                //   width: 300 * SizeConfig.getScaleFactor(),
                //   child: Image.asset(
                //       'assets/images/logo/chazikkat_logo.png'),
                // ),
                // Center(
                //   child: Text(
                //       'Chazhikattu MultiSuper-Speciality Hospital',
                //       style: TextStyle(
                //           fontSize: 18,
                //           color: Theme.of(context)
                //               .scaffoldBackgroundColor)),
                // ),
                Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircleAvatar(
                  radius: 20,
                  backgroundColor: Colors.black,
                  backgroundImage:
                      AssetImage('assets/images/local_image/cms_logo.png'),
                ),
                SizedBox(
                  width: 15 * SizeConfig.getScaleFactor(),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Chazhikattu',
                        style: TextStyle(
                            fontSize: 22 * SizeConfig.getScaleFactor(),
                            fontWeight: FontWeight.bold,
                            color: Theme.of(context).scaffoldBackgroundColor)),
                    Text('MultiSuper-Speciality Hospital',
                        style: TextStyle(
                            fontSize: 18 * SizeConfig.getScaleFactor(),
                            color: Theme.of(context).scaffoldBackgroundColor)),
                  ],
                ),
                SizedBox(
                  width: 10 * SizeConfig.getScaleFactor(),
                )
              ],
            ),
          ),
          // centerTitle: true,
          actions: [
            IconButton(
                icon: Icon(Icons.exit_to_app),
                onPressed: () {
                  Navigator.of(context).pushReplacementNamed('/');
                  Provider.of<LoginAuth>(context, listen: false).logout();
                })
          ],
        ),
        body: RefreshIndicator(
            onRefresh: () => _refreshProducts(context),
            child: FutureBuilder<void>(
                future: flag ? null : _refreshProducts(context),
                // initialData: [],
                builder: (context, snapshot) {
                  return list != null
                      ? Column(children: [
                          Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: Container(
                              color: Theme.of(context).primaryColor,
                              width: double.infinity,
                              height: 80 * SizeConfig.getScaleFactor(),
                              child: Container(
                                child: Center(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Center(
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 20),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              CircleAvatar(
                                                radius: 25,
                                                backgroundColor: Colors.black,
                                                backgroundImage: AssetImage(
                                                    'assets/images/local_image/dummy.jpg'),
                                              ),
                                              SizedBox(
                                                width: 20 *
                                                    SizeConfig.getScaleFactor(),
                                              ),
                                              Column(
                                                children: [
                                                  Text(
                                                    "${list[0].firstname}",
                                                    style: TextStyle(
                                                        fontSize: 16 *
                                                            SizeConfig
                                                                .getScaleFactor(),
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        color: Theme.of(context)
                                                            .scaffoldBackgroundColor),
                                                  ),
                                                  SizedBox(
                                                    height: 5,
                                                  ),
                                                  Text(
                                                    "${list[0].qualification}",
                                                    style: TextStyle(
                                                        fontSize: 16 *
                                                            SizeConfig
                                                                .getScaleFactor(),
                                                        //fontWeight: FontWeight.bold,
                                                        color: Theme.of(context)
                                                            .scaffoldBackgroundColor),
                                                  ),
                                                ],
                                              )
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          // DoctorsDetialsViewWidget(
                          //   animation: Tween<double>(begin: 0.0, end: 1.0)
                          //       .animate(CurvedAnimation(
                          //           parent: animationController,
                          //           curve: Interval((1 / count) * 1, 1.0,
                          //               curve: Curves.fastOutSlowIn))),
                          //   animationController: animationController,
                          // ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              _date == null
                                  ? Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 15.0),
                                      child: Text(
                                        'Today\'s',
                                        style: TextStyle(
                                          fontSize:
                                              15 * SizeConfig.getScaleFactor(),
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    )
                                  : Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 15.0),
                                      child: Text(
                                        '$_date',
                                        style: TextStyle(
                                          fontSize:
                                              15 * SizeConfig.getScaleFactor(),
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                              IconButton(
                                icon: Icon(
                                  Icons.date_range,
                                  color: Theme.of(context).primaryColor,
                                ),
                                onPressed: () => _selectDate(context),
                              )
                            ],
                          ),
                          Expanded(
                              child: Consumer<DrugOrderModalDoctor>(
                                  builder: (ctx, count, _) => ListView(
                                        // mainAxisAlignment: MainAxisAlignment.start,
                                        // crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          Center(
                                              child: Card(
                                                  color: Theme.of(context)
                                                      .primaryColor,
                                                  elevation: 4,
                                                  child: Container(
                                                    decoration: BoxDecoration(
                                                      color: Colors.orange[400],
                                                      gradient: LinearGradient(
                                                          begin:
                                                              Alignment.topLeft,
                                                          end: Alignment
                                                              .bottomRight,
                                                          colors:
                                                              MoreGradientColors
                                                                  .count,
                                                          stops: [
                                                            0.1,
                                                            0.2,
                                                            0.7,
                                                            0.9
                                                          ]),
                                                    ),
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              2.0),
                                                      child: GestureDetector(
                                                        onTap: () {
                                                          // Navigator.of(context)
                                                          //     .pushNamed(
                                                          //         DoctorsPatientScreenIP
                                                          //             .routeName);
                                                        },
                                                        child: Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(4.0),
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            children: [
                                                              Card(
                                                                elevation: 4,
                                                                color: Colors
                                                                    .teal[400],
                                                                child: SizedBox(
                                                                  width: 150 *
                                                                      SizeConfig
                                                                          .getScaleFactor(),
                                                                  child:
                                                                      Padding(
                                                                    padding: const EdgeInsets
                                                                            .all(
                                                                        15.0),
                                                                    child: Text(
                                                                      'SURGERY',
                                                                      style: TextStyle(
                                                                          fontSize: 16 *
                                                                              SizeConfig
                                                                                  .getScaleFactor(),
                                                                          fontWeight: FontWeight
                                                                              .bold,
                                                                          color:
                                                                              Theme.of(context).scaffoldBackgroundColor),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                              SizedBox(
                                                                width: 160 *
                                                                    SizeConfig
                                                                        .getScaleFactor(),
                                                              ),
                                                              count.loadIPOPpatientsCount
                                                                      .isEmpty
                                                                  ? Card(
                                                                      color: Colors
                                                                              .teal[
                                                                          400],
                                                                      child:
                                                                          Padding(
                                                                              padding: const EdgeInsets.all(
                                                                                  15.0),
                                                                              child:
                                                                                  Text(
                                                                                '0',
                                                                                style: TextStyle(fontSize: 18 * SizeConfig.getScaleFactor(), fontWeight: FontWeight.bold, color: Theme.of(context).scaffoldBackgroundColor),
                                                                              )))
                                                                  : Card(
                                                                      color: Colors
                                                                              .teal[
                                                                          400],
                                                                      child:
                                                                          Padding(
                                                                        padding:
                                                                            const EdgeInsets.all(15.0),
                                                                        child:
                                                                            Text(
                                                                          '0',
                                                                          style: TextStyle(
                                                                              fontSize: 18 * SizeConfig.getScaleFactor(),
                                                                              fontWeight: FontWeight.bold,
                                                                              color: Theme.of(context).scaffoldBackgroundColor),
                                                                        ),
                                                                      ))
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ))),
                                          // SizedBox(
                                          //   height: 5,
                                          // ),
                                          Center(
                                              child: Card(
                                                  color: Theme.of(context)
                                                      .primaryColor,
                                                  elevation: 4,
                                                  child: Container(
                                                    decoration: BoxDecoration(
                                                      color: Colors.orange[400],
                                                      gradient: LinearGradient(
                                                          begin:
                                                              Alignment.topLeft,
                                                          end: Alignment
                                                              .bottomRight,
                                                          colors:
                                                              MoreGradientColors
                                                                  .count,
                                                          stops: [
                                                            0.1,
                                                            0.2,
                                                            0.7,
                                                            0.9
                                                          ]),
                                                    ),
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              2.0),
                                                      child: GestureDetector(
                                                        onTap: () {
                                                          Navigator.of(context)
                                                              .pushNamed(
                                                                  DoctorsPatientScreenIP
                                                                      .routeName);
                                                        },
                                                        child: Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(4.0),
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            children: [
                                                              Card(
                                                                elevation: 4,
                                                                color: Colors
                                                                    .teal[400],
                                                                child: SizedBox(
                                                                  width: 150 *
                                                                      SizeConfig
                                                                          .getScaleFactor(),
                                                                  child:
                                                                      Padding(
                                                                    padding: const EdgeInsets
                                                                            .all(
                                                                        15.0),
                                                                    child: Text(
                                                                      'INPATIENTS',
                                                                      style: TextStyle(
                                                                          fontSize: 16 *
                                                                              SizeConfig
                                                                                  .getScaleFactor(),
                                                                          fontWeight: FontWeight
                                                                              .bold,
                                                                          color:
                                                                              Theme.of(context).scaffoldBackgroundColor),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                              SizedBox(
                                                                width: 160 *
                                                                    SizeConfig
                                                                        .getScaleFactor(),
                                                              ),
                                                              count.loadIPOPpatientsCount
                                                                      .isEmpty
                                                                  ? Card(
                                                                      color: Colors
                                                                              .teal[
                                                                          400],
                                                                      child:
                                                                          Padding(
                                                                              padding: const EdgeInsets.all(
                                                                                  15.0),
                                                                              child:
                                                                                  Text(
                                                                                '0',
                                                                                style: TextStyle(fontSize: 18 * SizeConfig.getScaleFactor(), fontWeight: FontWeight.bold, color: Theme.of(context).scaffoldBackgroundColor),
                                                                              )))
                                                                  : Card(
                                                                      color: Colors
                                                                              .teal[
                                                                          400],
                                                                      child:
                                                                          Padding(
                                                                        padding:
                                                                            const EdgeInsets.all(15.0),
                                                                        child:
                                                                            Text(
                                                                          '${count.loadIPOPpatientsCount[0].ipcount}',
                                                                          style: TextStyle(
                                                                              fontSize: 18 * SizeConfig.getScaleFactor(),
                                                                              fontWeight: FontWeight.bold,
                                                                              color: Theme.of(context).scaffoldBackgroundColor),
                                                                        ),
                                                                      ))
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ))),
                                          // SizedBox(
                                          //   height: 5,
                                          // ),
                                          Center(
                                            child: Card(
                                                color: Theme.of(context)
                                                    .primaryColor,
                                                elevation: 4,
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    color: Colors.orange[400],
                                                    gradient: LinearGradient(
                                                        begin:
                                                            Alignment.topLeft,
                                                        end: Alignment
                                                            .bottomRight,
                                                        colors:
                                                            MoreGradientColors
                                                                .count,
                                                        stops: [
                                                          0.1,
                                                          0.2,
                                                          0.7,
                                                          0.9
                                                        ]),
                                                  ),
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            2.0),
                                                    child: GestureDetector(
                                                      onTap: () {
                                                        Navigator.of(context)
                                                            .pushNamed(
                                                                DoctorsPatientScreenOP
                                                                    .routeName);
                                                      },
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .all(4.0),
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceBetween,
                                                          children: [
                                                            Card(
                                                              elevation: 4,
                                                              color: Colors
                                                                  .teal[400],
                                                              child: Container(
                                                                width: 150 *
                                                                    SizeConfig
                                                                        .getScaleFactor(),
                                                                child: Padding(
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .all(
                                                                          15.0),
                                                                  child: Text(
                                                                    'OUTPATIENTS',
                                                                    style: TextStyle(
                                                                        fontSize: 16 *
                                                                            SizeConfig
                                                                                .getScaleFactor(),
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .bold,
                                                                        color: Theme.of(context)
                                                                            .scaffoldBackgroundColor),
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                            SizedBox(
                                                              width: 160 *
                                                                  SizeConfig
                                                                      .getScaleFactor(),
                                                            ),
                                                            count.loadIPOPpatientsCount
                                                                    .isEmpty
                                                                ? Card(
                                                                    color: Colors
                                                                            .teal[
                                                                        400],
                                                                    child:
                                                                        Padding(
                                                                            padding: const EdgeInsets.all(
                                                                                15.0),
                                                                            child:
                                                                                Text(
                                                                              '0',
                                                                              style: TextStyle(fontSize: 18 * SizeConfig.getScaleFactor(), fontWeight: FontWeight.bold, color: Theme.of(context).scaffoldBackgroundColor),
                                                                            )))
                                                                : Card(
                                                                    color: Colors
                                                                            .teal[
                                                                        400],
                                                                    child:
                                                                        Padding(
                                                                      padding: const EdgeInsets
                                                                              .all(
                                                                          15.0),
                                                                      child:
                                                                          Text(
                                                                        '${count.loadIPOPpatientsCount[0].opcount}',
                                                                        style: TextStyle(
                                                                            fontSize:
                                                                                18 * SizeConfig.getScaleFactor(),
                                                                            fontWeight: FontWeight.bold,
                                                                            color: Theme.of(context).scaffoldBackgroundColor),
                                                                      ),
                                                                    ))
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                )),
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Card(
                                                // color: Theme.of(context)
                                                //     .primaryColor,
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    color: Colors.orange[400],
                                                    gradient: LinearGradient(
                                                        begin:
                                                            Alignment.topLeft,
                                                        end: Alignment
                                                            .bottomRight,
                                                        colors:
                                                            MoreGradientColors
                                                                .countValues,
                                                        stops: [0.1, 0.5, 0.9]),
                                                  ),
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            10.0),
                                                    child: SizedBox(
                                                      width: 105,
                                                      child: Column(
                                                        children: [
                                                          Text(
                                                            'Appointments',
                                                            style: TextStyle(
                                                                fontSize: 16 *
                                                                    SizeConfig
                                                                        .getScaleFactor(),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                color: Theme.of(
                                                                        context)
                                                                    .scaffoldBackgroundColor),
                                                          ),
                                                          SizedBox(
                                                            height: 10,
                                                          ),
                                                          count.loadIPOPpatientsCount
                                                                  .isEmpty
                                                              ? Text(
                                                                  '0',
                                                                  style: TextStyle(
                                                                      fontSize:
                                                                          18,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                      color: Theme.of(
                                                                              context)
                                                                          .scaffoldBackgroundColor),
                                                                )
                                                              : Text(
                                                                  '${count.loadIPOPpatientsCount[0].appointments}',
                                                                  style: TextStyle(
                                                                      fontSize: 18 *
                                                                          SizeConfig
                                                                              .getScaleFactor(),
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                      color: Theme.of(
                                                                              context)
                                                                          .scaffoldBackgroundColor),
                                                                ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Card(
                                                elevation: 4,
                                                // color: Theme.of(context)
                                                //     .primaryColor,
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    color: Colors.orange[400],
                                                    gradient: LinearGradient(
                                                        begin:
                                                            Alignment.topLeft,
                                                        end: Alignment
                                                            .bottomRight,
                                                        colors:
                                                            MoreGradientColors
                                                                .countValues,
                                                        stops: [0.1, 0.5, 0.9]),
                                                  ),
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            10.0),
                                                    child: SizedBox(
                                                      width: 105 *
                                                          SizeConfig
                                                              .getScaleFactor(),
                                                      child: Column(
                                                        children: [
                                                          Text(
                                                            'Encounter',
                                                            style: TextStyle(
                                                                fontSize: 16,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                color: Theme.of(
                                                                        context)
                                                                    .scaffoldBackgroundColor),
                                                          ),
                                                          SizedBox(
                                                            height: 10,
                                                          ),
                                                          count.loadIPOPpatientsCount
                                                                  .isEmpty
                                                              ? Text(
                                                                  '0',
                                                                  style: TextStyle(
                                                                      fontSize: 18 *
                                                                          SizeConfig
                                                                              .getScaleFactor(),
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                      color: Theme.of(
                                                                              context)
                                                                          .scaffoldBackgroundColor),
                                                                )
                                                              : Text(
                                                                  '${count.loadIPOPpatientsCount[0].encountered}',
                                                                  style: TextStyle(
                                                                      fontSize: 18 *
                                                                          SizeConfig
                                                                              .getScaleFactor(),
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                      color: Theme.of(
                                                                              context)
                                                                          .scaffoldBackgroundColor),
                                                                ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Card(
                                                // color: Theme.of(context)
                                                //     .primaryColor,
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    color: Colors.orange[400],
                                                    gradient: LinearGradient(
                                                        begin:
                                                            Alignment.topLeft,
                                                        end: Alignment
                                                            .bottomRight,
                                                        colors:
                                                            MoreGradientColors
                                                                .countValues,
                                                        stops: [0.1, 0.5, 0.9]),
                                                  ),
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            10.0),
                                                    child: SizedBox(
                                                      width: 105 *
                                                          SizeConfig
                                                              .getScaleFactor(),
                                                      child: Column(
                                                        children: [
                                                          Text(
                                                            'Referral',
                                                            style: TextStyle(
                                                                fontSize: 16 *
                                                                    SizeConfig
                                                                        .getScaleFactor(),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                color: Theme.of(
                                                                        context)
                                                                    .scaffoldBackgroundColor),
                                                          ),
                                                          SizedBox(
                                                            height: 10,
                                                          ),
                                                          count.loadIPOPpatientsCount
                                                                  .isEmpty
                                                              ? Text(
                                                                  '0',
                                                                  style: TextStyle(
                                                                      fontSize: 18 *
                                                                          SizeConfig
                                                                              .getScaleFactor(),
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                      color: Theme.of(
                                                                              context)
                                                                          .scaffoldBackgroundColor),
                                                                )
                                                              : Text(
                                                                  '${count.loadIPOPpatientsCount[0].referal}',
                                                                  style: TextStyle(
                                                                      fontSize: 18 *
                                                                          SizeConfig
                                                                              .getScaleFactor(),
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                      color: Theme.of(
                                                                              context)
                                                                          .scaffoldBackgroundColor),
                                                                ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                          SizedBox(
                                            height: 5,
                                          ),
                                          Row(
                                            children: [
                                              Card(
                                                // color: Theme.of(context)
                                                //     .primaryColor,
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    color: Colors.orange[400],
                                                    gradient: LinearGradient(
                                                        begin:
                                                            Alignment.topLeft,
                                                        end: Alignment
                                                            .bottomRight,
                                                        colors:
                                                            MoreGradientColors
                                                                .countValues,
                                                        stops: [0.1, 0.5, 0.9]),
                                                  ),
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            10.0),
                                                    child: SizedBox(
                                                      width: 105 *
                                                          SizeConfig
                                                              .getScaleFactor(),
                                                      child: Column(
                                                        children: [
                                                          Text(
                                                            'Follow UP',
                                                            style: TextStyle(
                                                                fontSize: 16 *
                                                                    SizeConfig
                                                                        .getScaleFactor(),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                color: Theme.of(
                                                                        context)
                                                                    .scaffoldBackgroundColor),
                                                          ),
                                                          SizedBox(
                                                            height: 10 *
                                                                SizeConfig
                                                                    .getScaleFactor(),
                                                          ),
                                                          count.loadIPOPpatientsCount
                                                                  .isEmpty
                                                              ? Text(
                                                                  '0',
                                                                  style: TextStyle(
                                                                      fontSize: 18 *
                                                                          SizeConfig
                                                                              .getScaleFactor(),
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                      color: Theme.of(
                                                                              context)
                                                                          .scaffoldBackgroundColor),
                                                                )
                                                              : Text(
                                                                  '${count.loadIPOPpatientsCount[0].followup}',
                                                                  style: TextStyle(
                                                                      fontSize: 18 *
                                                                          SizeConfig
                                                                              .getScaleFactor(),
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                      color: Theme.of(
                                                                              context)
                                                                          .scaffoldBackgroundColor),
                                                                ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Card(
                                                // color: Theme.of(context)
                                                //     .primaryColor,
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    color: Colors.orange[400],
                                                    gradient: LinearGradient(
                                                        begin:
                                                            Alignment.topLeft,
                                                        end: Alignment
                                                            .bottomRight,
                                                        colors:
                                                            MoreGradientColors
                                                                .countValues,
                                                        stops: [0.1, 0.5, 0.9]),
                                                  ),
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            10.0),
                                                    child: SizedBox(
                                                      width: 105,
                                                      child: Column(
                                                        children: [
                                                          Text(
                                                            'Month to Date',
                                                            style: TextStyle(
                                                                fontSize: 16 *
                                                                    SizeConfig
                                                                        .getScaleFactor(),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                color: Theme.of(
                                                                        context)
                                                                    .scaffoldBackgroundColor),
                                                          ),
                                                          SizedBox(
                                                            height: 10 *
                                                                SizeConfig
                                                                    .getScaleFactor(),
                                                          ),
                                                          count.loadIPOPpatientsCount
                                                                  .isEmpty
                                                              ? Text(
                                                                  '0',
                                                                  style: TextStyle(
                                                                      fontSize: 18 *
                                                                          SizeConfig
                                                                              .getScaleFactor(),
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                      color: Theme.of(
                                                                              context)
                                                                          .scaffoldBackgroundColor),
                                                                )
                                                              : Text(
                                                                  '${count.loadIPOPpatientsCount[0].monthToDate}',
                                                                  style: TextStyle(
                                                                      fontSize: 18 *
                                                                          SizeConfig
                                                                              .getScaleFactor(),
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                      color: Theme.of(
                                                                              context)
                                                                          .scaffoldBackgroundColor),
                                                                ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          )
                                        ],
                                      )))
                        ])
                      : FutureBuilder<void>(
                          future: _refreshProducts(context),
                          // initialData: [],
                          builder: (context, snapshot) {
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          });
                })));
  }
}
