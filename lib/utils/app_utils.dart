import 'package:connectivity/connectivity.dart';
import 'package:trust_fall/trust_fall.dart';

class AppUtils {
  AppUtils._();

  static Future<bool> isNetworkAvailable() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return true;
    }
    return false;
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  static Future<bool> isDeviceRooted() async {
    try {
      return await TrustFall.isTrustFall;
    } catch (error) {
      print(error);
    }
    return false;
  }
}
