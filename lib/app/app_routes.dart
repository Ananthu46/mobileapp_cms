import 'package:doctor_portal_cms/screens/dashboard/dashbord_items.dart/doctors_dashbord.dart';
import 'package:doctor_portal_cms/screens/doctor/doctor_patients_list/doctor_ippatients_screen.dart';
import 'package:doctor_portal_cms/screens/doctor/doctor_patients_list/doctors_oppatient_screen.dart';
import 'package:doctor_portal_cms/screens/doctor/doctors_select_items/allergies_doctor/allergy_selection_screen.dart';
import 'package:doctor_portal_cms/screens/doctor/doctors_select_items/allergies_doctor/existing_allergies.dart';
import 'package:doctor_portal_cms/screens/doctor/doctors_select_items/diagnosis_doctor/diagnosis_screen.dart';
import 'package:doctor_portal_cms/screens/doctor/doctors_select_items/general_information_doctor/general_Information_Zefr.dart';
import 'package:doctor_portal_cms/screens/doctor/doctors_select_items/vital_sign_doctor/vital_sign_doctor_screen.dart';
import 'package:doctor_portal_cms/screens/doctor/doctors_soap_notes.dart';
import 'package:doctor_portal_cms/screens/drug_order/drug_order_main_screen.dart';
import 'package:doctor_portal_cms/screens/ehr/ehr_list_items/ehr_allergies.dart';
import 'package:doctor_portal_cms/screens/ehr/ehr_list_items/ehr_consultation_detials.dart';
import 'package:doctor_portal_cms/screens/ehr/ehr_list_items/ehr_labresult.dart';
import 'package:doctor_portal_cms/screens/ehr/ehr_list_items/ehr_referrals.dart';
import 'package:doctor_portal_cms/screens/ehr/ehr_list_items/ehr_uploadDocument.dart';
import 'package:doctor_portal_cms/screens/ehr/ehr_list_items/ehr_vitalsigns.dart';
import 'package:doctor_portal_cms/screens/ehr/electronic_health_record_screen.dart';
import 'package:doctor_portal_cms/screens/doctor/main_screen_medical.dart';
import 'package:doctor_portal_cms/screens/nursing_orders/nusing_orders_screen.dart';
import 'package:doctor_portal_cms/screens/previous_orders/pharmacy_previous_order_screen.dart';
import 'package:doctor_portal_cms/screens/registration/register_screen.dart';
import 'package:doctor_portal_cms/screens/view_orders/view_pharmacy_orders.dart';
import 'package:doctor_portal_cms/screens/view_orders/view_service_orders.dart';
import 'package:flutter/material.dart';

class AppRouter {
  static Map<String, WidgetBuilder> routes() {
    return {
      DoctorsPortalDashbord.routeName: (ctx) => DoctorsPortalDashbord(),
      RegisterNewScreen.routeName: (ctx) => RegisterNewScreen(),
      DrugOrderTabBar.routeName: (ctx) => DrugOrderTabBar(),
      NursingOrdersScreen.routeName: (ctx) => NursingOrdersScreen(),
      ViewServiceOrdersScreen.routeName: (ctx) => ViewServiceOrdersScreen(),
      DoctorsSoapNotes.routeName: (ctx) => DoctorsSoapNotes(),
      AllergySelectionscreen.routeName: (ctx) => AllergySelectionscreen(),
      PreviousOrderScreen.routeName: (ctx) => PreviousOrderScreen(),
      ViewPharmacyOrdersScreen.routeName: (ctx) => ViewPharmacyOrdersScreen(),
      VitalSignDoctor.routeName: (ctx) => VitalSignDoctor(),
      ExistingAllergiesScreen.routeName: (ctx) => ExistingAllergiesScreen(),
      DoctorsPatientScreenOP.routeName: (ctx) => DoctorsPatientScreenOP(),
      DoctorsPatientScreenIP.routeName: (ctx) => DoctorsPatientScreenIP(),
      ElectronicHealthRecordScreen.routeName: (ctx) =>
          ElectronicHealthRecordScreen(),
      EHRConsultationDetialsScreen.routeName: (ctx) =>
          EHRConsultationDetialsScreen(),
      EHRAllergiesScreen.routeName: (ctx) => EHRAllergiesScreen(),
      EHRLabresultsScreen.routeName: (ctx) => EHRLabresultsScreen(),
      EHRVitalsignsScreen.routeName: (ctx) => EHRVitalsignsScreen(),
      EHRReferralsScreen.routeName: (ctx) => EHRReferralsScreen(),
      EHRUploadDocumentScreen.routeName: (ctx) => EHRUploadDocumentScreen(),
      MedicalRecordsMainScreen.routeName: (ctx) => MedicalRecordsMainScreen(),
      // GeneralInformationZefyr.routeName: (ctx) => GeneralInformationZefyr(),
      // DoctorDiagnosisScreen.routeName: (ctx) => DoctorDiagnosisScreen(),
    };
  }
}
